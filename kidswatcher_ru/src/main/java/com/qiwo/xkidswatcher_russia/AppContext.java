package com.qiwo.xkidswatcher_russia;


import android.app.ActivityManager;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Build;
import android.os.Environment;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.multidex.MultiDex;

/*
import com.appsee.Appsee;
import com.appsee.AppseeListener;
import com.appsee.AppseeScreenDetectedInfo;
import com.appsee.AppseeSessionEndedInfo;
import com.appsee.AppseeSessionEndingInfo;
import com.appsee.AppseeSessionStartedInfo;
import com.appsee.AppseeSessionStartingInfo;
*/
import com.amplitude.api.Amplitude;
import com.crashlytics.android.Crashlytics;
import com.google.common.util.concurrent.ListenableFuture;
import com.google.firebase.FirebaseApp;
import com.google.firebase.remoteconfig.FirebaseRemoteConfig;
import com.google.gson.Gson;
import com.nostra13.universalimageloader.cache.disc.impl.UnlimitedDiscCache;
import com.nostra13.universalimageloader.cache.disc.naming.Md5FileNameGenerator;
import com.nostra13.universalimageloader.cache.memory.impl.UsingFreqLimitedMemoryCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;
import com.nostra13.universalimageloader.utils.StorageUtils;
import com.qiwo.xkidswatcher_russia.AnalyticsUtils.AnalyticsUtils;
import com.qiwo.xkidswatcher_russia.api.ApiHttpClient;
import com.qiwo.xkidswatcher_russia.api.remote.KidsWatApiUrl;
import com.qiwo.xkidswatcher_russia.base.BaseApplication;
import com.qiwo.xkidswatcher_russia.bean.beanForDb___Family;
import com.qiwo.xkidswatcher_russia.bean.beanForDb___LoginMember;
import com.qiwo.xkidswatcher_russia.bean.beanFor___get_family_info;
import com.qiwo.xkidswatcher_russia.bean.beanFor___get_family_list;

import net.intari.AndroidToolboxNanoCore.CoreUtils;
import net.intari.CustomLogger.CustomLog;

import com.qiwo.xkidswatcher_russia.bean.beanFor___get_watch_data;
import com.qiwo.xkidswatcher_russia.util.KidsWatUtils;
import com.qiwo.xkidswatcher_russia.util.StringUtils;
import com.qiwo.xkidswatcher_russia.util.TLog;
import com.yandex.metrica.YandexMetrica;
import com.yandex.metrica.YandexMetricaConfig;

import org.jetbrains.annotations.NotNull;
import org.json.JSONException;
import org.json.JSONObject;
import org.kymjs.kjframe.KJHttp;
import org.kymjs.kjframe.bitmap.BitmapConfig;
import org.kymjs.kjframe.http.HttpCallBack;
import org.kymjs.kjframe.http.HttpConfig;
import org.kymjs.kjframe.utils.KJLoger;

import java.io.File;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import androidx.work.Constraints;
import androidx.work.ExistingWorkPolicy;
import androidx.work.ListenableWorker;
import androidx.work.NetworkType;
import androidx.work.OneTimeWorkRequest;
import androidx.work.Operation;
import androidx.work.PeriodicWorkRequest;
import androidx.work.WorkManager;
import androidx.work.Worker;
import androidx.work.WorkerParameters;
import androidx.work.impl.WorkManagerInitializer;
import androidx.work.impl.utils.futures.SettableFuture;
import butterknife.ButterKnife;
import io.fabric.sdk.android.Fabric;
import ru.yandex.yandexmapkit.utils.GeoPoint;

//import static com.qiwo.xkidswatcher.AppConfig.KEY_FAMILY_ID;
//import static com.qiwo.xkidswatcher.AppConfig.KEY_FRITST_START;
//import static com.qiwo.xkidswatcher.AppConfig.KEY_GCM_APP_ID;

/**
 *
 */

public class AppContext extends BaseApplication {

    public static final String TAG = AppContext.class.getSimpleName();

    private static AppContext instance;


    public  void initLifecycleListeners() {
        CustomLog.i(TAG,"Initing lifecycle managers...");
        CoreUtils.initLifecycleObservers(this);
        CoreUtils.setReportLifecycleEventsForDebug(true);
        AnalyticsUtils.initLifecycleObservers(this);
        AnalyticsUtils.setReportLifecycleEventsForAnalytics(true);
    }

    public void setInternalLocationResponse(JSONObject data) {
        MyWorkManagerInitializer.Companion.setInternalLocationResponse(data);
    }
    public void setInternal_bean___watch_data(beanFor___get_watch_data data) {

        MyWorkManagerInitializer.Companion.setInternal_bean___watch_data(data);

    }
    public beanFor___get_watch_data getInternal_bean___watch_data() {
        return MyWorkManagerInitializer.Companion.getInternal_bean___watch_data();
    }
    /**
     * 获得当前app运行的AppContext
     *
     * @return
     */
    public static AppContext getInstance() {
        return instance;
    }

    /**
     * 判断当前版本是否兼容目标版本的方法
     *
     * @param VersionCode
     * @return
     */
    public static boolean isMethodsCompat(int VersionCode) {
        int currentVersion = android.os.Build.VERSION.SDK_INT;
        return currentVersion >= VersionCode;
    }

    @Override
    protected void attachBaseContext(Context context) {
        super.attachBaseContext(context);
        MultiDex.install(this);
    }

    @Override
    public void onCreate() {
        Log.d(TAG,"onCreate() called. will call .super()");
        super.onCreate();
        Log.d(TAG,"onCreate().Done with .super()");
        instance = this;
        initLifecycleListeners();//SHOULD be 3rd (or 4th) if this functionality is used
        initFirebase();
        initYandexMetrica();//initFirebase() must be called before
        initAmplitude();
        //performInit();
    }

    private void initFirebase() {
        //This is necessary to prevent issues like https://github.com/yandexmobile/metrica-sdk-android/issues/77
        //This call is NOT under release-only config because it's not decided yet if Firebase will be used in release
        FirebaseApp.initializeApp(this);
        //initRemoteConfig()
    }

    private void initAmplitude() {
        if (Constants.AMPLITUDE_GLOBAL_ENABLE) {
            CustomLog.i(TAG,"Will init Amplitude Analytics");
            Amplitude.getInstance().initialize(this, Constants.AMPLITUDE__API_KEY).enableForegroundTracking(this);
        } else {
            CustomLog.i(TAG,"Not initing Amplitude Analytics - global disable");
        }
        AnalyticsUtils.activateAmplitude(Constants.AMPLITUDE_GLOBAL_ENABLE);
    }
    private void initYandexMetrica() {
        /*
        if (BuildConfig.DEBUG) {
            CustomLog.i(TAG,"Not enabling Yandex.Metrica for debug build");
            AnalyticsUtils.activateYandexMetrica(false);
            return;
        }
        */
        if (Constants.YANDEX_METRICA_GLOBAL_ENABLE) {
            CustomLog.i(TAG,"Will init Yandex.Metrica");
            // Создание расширенной конфигурации библиотеки.
            YandexMetricaConfig.Builder builder=YandexMetricaConfig.newConfigBuilder(Constants.YANDEX_API_KEY);

            if (BuildConfig.DEBUG) {
                CustomLog.d(TAG,"Enabling Yandex.Metrica logs...");
                builder.withLogs();
            }
            // Инициализация AppMetrica SDK.
            YandexMetrica.activate(getApplicationContext(), builder.build());
            // Отслеживание активности пользователей.
            YandexMetrica.enableActivityAutoTracking(this);
            CustomLog.i(TAG,"YandexMetrica init done");
        } else {
            CustomLog.i(TAG,"Not initing Yandex.Metrica - global disable");
        }
        AnalyticsUtils.activateYandexMetrica(Constants.YANDEX_METRICA_GLOBAL_ENABLE);

    }
    public beanForDb___Family getCurrentFamily() {
        return MyWorkManagerInitializer.Companion.getCurrentFamily();
    }

    public void clearCurrentFamily() {
        MyWorkManagerInitializer.Companion.setCurrentFamily(null);
    }
    public void setCurrentFamily(beanForDb___Family family) {
        MyWorkManagerInitializer.Companion.setCurrentFamily(family);
    }

    public beanFor___get_family_info getCurrentBeanFamily() {
        return MyWorkManagerInitializer.Companion.getCurrentBeanFamily();
    }
    public void setCurrentBeanFamily(beanFor___get_family_info family) {
        MyWorkManagerInitializer.Companion.setCurrentBeanFamily(family);
    }

    public Map<String, beanForDb___Family>  getMapFamilyInfo() {
        return MyWorkManagerInitializer.Companion.getMap_familyinfo();
    }

    public int getCodess() {
        return MyWorkManagerInitializer.Companion.getCodess();
    }

    public void setCodess(int newCodess) {
        MyWorkManagerInitializer.Companion.setCodess(newCodess);
    }
    public beanForDb___LoginMember getLoginUser_kid() {
        return MyWorkManagerInitializer.Companion.getLoginUser_kid();
    }
    public void setLoginUser_kid(beanForDb___LoginMember kid) {
        MyWorkManagerInitializer.Companion.setLoginUser_kid(kid);
    }

    public static SharedPreferences getSharedPref() {
        return MyWorkManagerInitializer.Companion.getSharedPref();
    }

    public static SharedPreferences.Editor getSharedPrefEditor() {
        return MyWorkManagerInitializer.Companion.getSharedPrefEditor();
    }

    /**
     * 获取App安装包信息
     *
     * @return
     */
    public PackageInfo getPackageInfo() {
        PackageInfo info = null;
        try {
            info = getPackageManager().getPackageInfo(getPackageName(), 0);
        } catch (NameNotFoundException e) {
            e.printStackTrace(System.err);
        }
        if (info == null)
            info = new PackageInfo();
        return info;
    }

    public beanFor___get_family_list getFamilyList() {
        return MyWorkManagerInitializer.Companion.getFamily_list();
    }
    public void setFamilyList(beanFor___get_family_list newList) {
        MyWorkManagerInitializer.Companion.setFamily_list(newList);
    }
    //does not directly related to 2 above functions
    public int getFamilyListCount() {
        return MyWorkManagerInitializer.Companion.getFamilyListCount();
    }
    public Gson getGson() {
        return MyWorkManagerInitializer.Companion.getGson();
    }

    public GeoPoint getLng() {
        return MyWorkManagerInitializer.Companion.getLng();
    }

    public void setLng(GeoPoint point) {
        MyWorkManagerInitializer.Companion.setLng(point);
    }
    public void setIsLogin(boolean isLogin) {
        MyWorkManagerInitializer.Companion.setLogin(isLogin);
    }
    public boolean getIsLogin() {
        return MyWorkManagerInitializer.Companion.isLogin();
    }
    public DisplayImageOptions getCommenOptions() {
        return MyWorkManagerInitializer.Companion.getCommenOptions();
    }
    public DisplayImageOptions getOptions() {
        return MyWorkManagerInitializer.Companion.getOptions();
    }
    public ImageLoader getImageLoader() {
        return MyWorkManagerInitializer.Companion.getImgLoader();
    }
    public String getGCMRegId() {
        return MyWorkManagerInitializer.Companion.getGoogle_gcm_appid();
    }
    public void setGCMRegID(String newGcmAppId) {
        MyWorkManagerInitializer.Companion.setGoogle_gcm_appid(newGcmAppId);
    }
    public static FirebaseRemoteConfig getRemoteConfig() {
        return MyWorkManagerInitializer.Companion.getRemoteConfig();
    }
}
