package com.qiwo.xkidswatcher_russia.event;

public class ChangeFamilySuccess {
	private String family_id;
	private String data;

	public ChangeFamilySuccess(String _family_id, String _data) {
		family_id = _family_id;
		data = _data;
	}

	public String getFamilyId() {
		return family_id;
	}

	public String getData() {
		return data;
	}

}
