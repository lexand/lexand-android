package com.qiwo.xkidswatcher_russia.event;

public class BleWritePwdSuccess {
    private String bluetoothAddress;

    private String blueNickname;

    public BleWritePwdSuccess(String bluetoothAddress, String nickName) {
        this.blueNickname = nickName;
        this.bluetoothAddress = bluetoothAddress;
    }

    public String getBlueNickname() {
        return blueNickname;
    }

    public String getBluetoothAddress() {
        return bluetoothAddress;
    }

}
