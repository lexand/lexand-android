package com.qiwo.xkidswatcher_russia.event;

/**
 * @author Administrator
 */
public class BleDisconnect {
    private String bleName;
    private String bleAddress;
    private int status;

    public BleDisconnect(String _bleName, String _bleAdd, int _status) {

        bleName = _bleName;
        bleAddress = _bleAdd;
        status = _status;
    }

    public String getBleAddress() {
        return bleAddress;
    }

    public String getBleName() {
        return bleName;
    }

    public int getStatus() {
        return status;
    }

}
