package com.qiwo.xkidswatcher_russia.event;

public class BaseEvent {

    public static final int MSGTYPE_1___RELOGIN = 100;

    public static final int MSGTYPE_1___DELETE_DEVICE = 101;
    public static final int MSGTYPE_1___ADMIN_ADD_MEMBER = 102;
    public static final int MSGTYPE_1___ADMIN_DELETE_MEMBER = 103;

    public static final int MSGTYPE_1___ADD_KIDS_OK = 110;
    public static final int MSGTYPE_1___LOGOUT = 150;

    public static final int MSGTYPE_2___FAMILY_LIST_EMPTY = 210;
    public static final int MSGTYPE_2___FAMILY_LIST_HAS_DATA = 215;

    public static final int MSGTYPE_3___CLEAR_NOTICE_DONE = 310;
    public static final int MSGTYPE_3___CHANGE_USER = 320;
    public static final int MSGTYPE_3___LOAD_FAMILY_FIRST_ITEM = 330;
    public static final int MSGTYPE_3___CHANGE_KIDS_PHOTO = 340;
    public static final int MSGTYPE_3___CHANGE_KIDS_NICKNAME = 345;
    public static final int MSGTYPE_3___GCM_LOCATION = 350;
    public static final int MSGTYPE_3___HAS_UNREAD_MSG = 351;
    public static final int MSGTYPE_3___NO_UNREAD_MSG = 352;
    public static final int MSGTYPE_3___BACKGROUND_TO_FRONT = 360;
    public static final int MSGTYPE_3___CHANGE_DEVICE_MODE = 370;

    public static final int MSGTYPE_3___YANDEX_LOCATION = 380;

    public static final int MSGTYPE_5___USERLIST_INDEX_CHANGE = 510;
    public static final int MSGTYPE_3_SHOW_RECORD = 511;

    public static final int MSGTYPE_6___WHITE_TO_FRONT = 1013;

    /*
     * white
     */
    public static final int MSGTYPE_3___WHITE_SETTING_CHANGE_KIDS_PHOTO = 1027;
    public static final int MSGTYPE_6___WHITE_HOME_VOICE_PUSH = 1123;
    // ---------------------

    private String msg;
    private int type;

    /**
     * @param _type 100-199 　需要重启app各种操作
     *              200-299　更新ui类
     *              300-399　在service等后台类类所做操作.
     *              500-599    其他
     *              BaseEvent(210, "get_family_list.count.0"));
     *              BaseEvent(215, "get_family_list.count.>0"));
     *              <p>
     *              BaseEvent(510, "family_id");
     *              <p>
     *              BaseEvent(310, "clear_notice")
     *              BaseEvent(320, "change user")
     *              BaseEvent(330, "load_family_index_0");
     *              BaseEvent(350, "location_msg");
     *              BaseEvent(360, "backGround_to_front");
     *              <p>
     *              BaseEvent(100, "relogin")); BaseEvent(110, "add baby ok")
     *              BaseEvent(150, "logout"));
     * @param _msg
     */
    public BaseEvent(int _type, String _msg) {
        type = _type;
        msg = _msg;
    }

    public String getMsg() {
        return msg;
    }

    public String getTypeAsString() {
        switch (type) {
            case MSGTYPE_1___RELOGIN:
                return "MSGTYPE_1___RELOGIN";
            case MSGTYPE_1___DELETE_DEVICE:
                return "MSGTYPE_1___DELETE_DEVICE";
            case MSGTYPE_1___ADMIN_ADD_MEMBER:
                return "MSGTYPE_1___ADMIN_ADD_MEMBER";
            case MSGTYPE_1___ADMIN_DELETE_MEMBER:
                return "MSGTYPE_1___ADMIN_DELETE_MEMBER";
            case MSGTYPE_1___ADD_KIDS_OK:
                return "MSGTYPE_1___ADD_KIDS_OK";
            case MSGTYPE_1___LOGOUT:
                return "MSGTYPE_1___LOGOUT";
            case MSGTYPE_2___FAMILY_LIST_EMPTY:
                return "MSGTYPE_2___FAMILY_LIST_EMPTY";
            case MSGTYPE_2___FAMILY_LIST_HAS_DATA:
                return "MSGTYPE_2___FAMILY_LIST_HAS_DATA";
            case MSGTYPE_3___CLEAR_NOTICE_DONE:
                return "MSGTYPE_3___CLEAR_NOTICE_DONE";
            case MSGTYPE_3___CHANGE_USER:
                return "MSGTYPE_3___CHANGE_USER";
            case MSGTYPE_3___LOAD_FAMILY_FIRST_ITEM:
                return "MSGTYPE_3___LOAD_FAMILY_FIRST_ITEM";
            case MSGTYPE_3___CHANGE_KIDS_PHOTO:
                return "MSGTYPE_3___CHANGE_KIDS_PHOTO";
            case MSGTYPE_3___CHANGE_KIDS_NICKNAME:
                return "MSGTYPE_3___CHANGE_KIDS_NICKNAME";
            case MSGTYPE_3___GCM_LOCATION:
                return "MSGTYPE_3___GCM_LOCATION";
            case MSGTYPE_3___HAS_UNREAD_MSG:
                return "MSGTYPE_3___HAS_UNREAD_MSG";
            case MSGTYPE_3___NO_UNREAD_MSG:
                return "MSGTYPE_3___NO_UNREAD_MSG";
            case MSGTYPE_3___BACKGROUND_TO_FRONT:
                return "MSGTYPE_3___BACKGROUND_TO_FRONT";
            case MSGTYPE_3___CHANGE_DEVICE_MODE:
                return "MSGTYPE_3___CHANGE_DEVICE_MODE";
            case MSGTYPE_3___YANDEX_LOCATION:
                return "MSGTYPE_3___YANDEX_LOCATION";
            case MSGTYPE_5___USERLIST_INDEX_CHANGE:
                return "MSGTYPE_5___USERLIST_INDEX_CHANGE";
            case MSGTYPE_3_SHOW_RECORD:
                return "MSGTYPE_3_SHOW_RECORD";
            case MSGTYPE_6___WHITE_TO_FRONT:
                return "MSGTYPE_6___WHITE_TO_FRONT";
            case MSGTYPE_3___WHITE_SETTING_CHANGE_KIDS_PHOTO:
                return "MSGTYPE_3___WHITE_SETTING_CHANGE_KIDS_PHOTO";
            case MSGTYPE_6___WHITE_HOME_VOICE_PUSH:
                return "MSGTYPE_6___WHITE_HOME_VOICE_PUSH";


            default:
                return "Not handled type ID:"+type;

        }

    }
    public int getType() {
        return type;
    }
}
