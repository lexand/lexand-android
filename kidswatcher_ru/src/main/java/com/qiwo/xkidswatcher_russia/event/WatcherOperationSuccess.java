package com.qiwo.xkidswatcher_russia.event;

public class WatcherOperationSuccess {
	private int action_type;
	private String msg;

	public WatcherOperationSuccess(int _action_type, String _msg) {
		// TODO Auto-generated constructor stub
		action_type = _action_type;
		_msg = _msg;
	}

	public int getAction_type() {
		return action_type;
	}

	public void setAction_type(int action_type) {
		this.action_type = action_type;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}



}
