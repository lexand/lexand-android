package com.qiwo.xkidswatcher_russia.widget;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;

import com.qiwo.xkidswatcher_russia.R;

//调用方式
/*if(bottom==null){
 bottom=new BottomPopupWindow(getActivity(), 0, getView());
 bottom.addItem("拍照", new onClickItemListener() {

 @Override
 public void clickItem(View v) {
 // TODO Auto-generated method stub
 Toast.makeText(getActivity(), "click 拍照", Toast.LENGTH_SHORT).show();
 }
 }, ItemPosition.TOP);

 bottom.addItem("中间加入", new onClickItemListener() {

 @Override
 public void clickItem(View v) {
 // TODO Auto-generated method stub
 Toast.makeText(getActivity(), "click 中间加入", Toast.LENGTH_SHORT).show();

 }
 }, ItemPosition.MIDDLE);

 bottom.addItem("从相册选择", new onClickItemListener() {

 @Override
 public void clickItem(View v) {
 // TODO Auto-generated method stub
 Toast.makeText(getActivity(), "click 从相册选择", Toast.LENGTH_SHORT).show();

 }
 }, ItemPosition.BOTTOM);

 bottom.addCancelItem("取消", new onClickItemListener() {

 @Override
 public void clickItem(View v) {
 // TODO Auto-generated method stub
 Toast.makeText(getActivity(), "click 取消", Toast.LENGTH_SHORT).show();

 }
 }, 20);
 }
 bottom.show();*/

/**
 * @ClassName: BottomPopupWindow
 */
public class BottomPopupWindow extends APopupWindow {
    FrameLayout frame = null;
    LinearLayout parent = null;
    private Context context;

    public BottomPopupWindow(Context context, int bgColor, View relyview) {
        super(context, relyview, bgColor);
        // TODO Auto-generated constructor stub
        this.context = context;
        setHeight(android.view.ViewGroup.LayoutParams.MATCH_PARENT);
        getContainer().getLayoutParams().height = ViewGroup.LayoutParams.MATCH_PARENT;

        // add FrameLayout
        frame = new FrameLayout(context);
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
        frame.setLayoutParams(lp);
        getContainer().addView(frame);

        // add LienarLayout
        parent = new LinearLayout(mContext);
        // LinearLayout.LayoutParams lp1=new
        // LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT,
        // LayoutParams.WRAP_CONTENT);
        FrameLayout.LayoutParams lp1 = new FrameLayout.LayoutParams(
                LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
        lp1.gravity = Gravity.BOTTOM;
        parent.setOrientation(LinearLayout.VERTICAL);
        parent.setLayoutParams(lp1);
        frame.addView(parent);
        // getContainer().addView(parent);

        getContainer().setOnTouchListener(new OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                // TODO Auto-generated method stub
                dismiss();
                return true;
            }
        });

        // setBackgroundDrawable();
        // setAnimationStyle(R.style.pushup_popup_anim);
    }

    @Override
    protected void showlocation(View relyview) {
        showAtLocation(relyview, Gravity.BOTTOM, 0, 0);
    }

    @Override
    protected View getItemView(Drawable drawable, String title,
                               int bgDrawableId, int textColorResId, int textSize,
                               ItemPosition pos, int marginTop) {
        // TODO Auto-generated method stub
        TextView name = new TextView(context);
        LayoutParams lp = new LinearLayout.LayoutParams(
                android.view.ViewGroup.LayoutParams.MATCH_PARENT,
                android.view.ViewGroup.LayoutParams.WRAP_CONTENT);
        name.setText(title);
        name.setGravity(Gravity.CENTER);
        name.setTextSize(textSize);
        if (textColorResId == 0) {
            // use default textColor
            name.setTextColor(Color.BLACK);
        } else {
            name.setTextColor(context.getResources().getColorStateList(
                    textColorResId));
        }

        if (pos == ItemPosition.TOP) {
            name.setBackgroundResource(R.drawable.selector_popup_item_top);
        } else if (pos == ItemPosition.MIDDLE) {
            name.setBackgroundResource(R.drawable.selector_popup_item_mid);
        } else if (pos == ItemPosition.BOTTOM) {
            name.setBackgroundResource(R.drawable.selector_popup_item_bottom);
        } else {
            lp.setMargins(0, marginTop, 0, 0);
            name.setBackgroundResource(R.drawable.selector_popup_item_cancel);
        }
        name.setLayoutParams(lp);
        int margin = getContext().getResources().getDimensionPixelSize(
                R.dimen.popupwindow_margintop);
        name.setPadding(10, margin, 10, margin);
        return name;
    }

    @Override
    public void addItem(String title, final onClickItemListener listener,
                        int bgDrawableId, int textColorResId, int textSize,
                        Drawable drawable, ItemPosition pos, int marginTop) {
        // TODO Auto-generated method stub
        View item = null;
        item = getItemView(drawable, title, bgDrawableId, textColorResId,
                textSize, pos, marginTop);
        if (item == null) {
            return;
        }

        item.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
                if (listener != null) {
                    listener.clickItem(v);
                }
            }
        });

        // this.container.addView(item);
        this.parent.addView(item);

        // 添加item 分割线
        if (pos == ItemPosition.TOP || pos == ItemPosition.MIDDLE) {
            View divide = getdivideView();
            if (divide != null) {
                this.parent.addView(divide);
            }
        }
    }

    // 增加一个sub view视图,独立的布局
    public void addViewLayout(View view) {
        addViewItemParent(view);
    }

    private void addViewItemParent(View view) {
        this.parent.addView(view);
    }

    @Override
    protected void startInAnimation() {
        // TODO Auto-generated method stub
        // BaseEffects animator = Effectstype.BottomEnter.getAnimator();
        // BaseEffects animator = mStartEffectStype.getAnimator();
        // animator.setDuration(400);
        // animator.start(getContentView());
    }

    // @Override
    // protected BaseEffects startOutAnimation() {
    // // TODO Auto-generated method stub
    // // BaseEffects animator = Effectstype.BottomOut.getAnimator();
    // // animator.setDuration(400);
    // // return animator;
    // }

    @Override
    protected View getdivideView() {
        // TODO Auto-generated method stub
        View divide = new View(mContext);
        FrameLayout.LayoutParams lp = new FrameLayout.LayoutParams(
                LayoutParams.MATCH_PARENT, 1);
        divide.setLayoutParams(lp);
        divide.setBackgroundColor(Color.parseColor("#DEDDDA"));
        return divide;
    }

}
