package com.qiwo.xkidswatcher_russia.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.RelativeLayout;

public class MapScrollRelativeLayout extends RelativeLayout {

    public MapScrollRelativeLayout(Context context) {
        super(context);
    }

    public MapScrollRelativeLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public MapScrollRelativeLayout(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        getParent().requestDisallowInterceptTouchEvent(true);
        return super.onInterceptTouchEvent(ev);
    }

}
