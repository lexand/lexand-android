package com.qiwo.xkidswatcher_russia.widget;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.qiwo.xkidswatcher_russia.widget.wheel.ArrayWheelAdapter;
import com.qiwo.xkidswatcher_russia.widget.wheel.OnWheelChangedListener;
import com.qiwo.xkidswatcher_russia.widget.wheel.OnWheelScrollListener;
import com.qiwo.xkidswatcher_russia.widget.wheel.WheelView;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @ClassName: DatePickerPopupWindow
 */
public class TimePopupWindow extends PopupWindow implements
        OnWheelChangedListener, OnWheelScrollListener, OnClickListener {
    private static final String TAG = "DatePickerPopupWindow";
    private final int visibleItemCount = 5;
    View view = null;
    WheelView wheel_year;
    WheelView wheel_month;
    WheelView wheel_day;
    int currentYearItemIndex = 0;
    int scroll_stop_item = 0;
    boolean need_autoScroll = true;
    // 默认 不允许比当前日期晚
    int autoScroll_direct = 1;
    String wheel_year_data[] = new String[30];
    Map<String, String> map_year = new HashMap<String, String>();
    String wheel_month_data[] = new String[12];
    Map<String, String> map_month = new HashMap<String, String>();
    String wheel_day_data[] = null;
    Map<String, String> map_day = new HashMap<String, String>();
    OnDatePickerInfoListener onDatePickerInfoListener;
    TextView bt_cancel, bt_ok, midTitle;
    String wheel_year_selectedText = "";
    String wheel_month_selectedText = "";
    String wheel_day_selectedText = "";
    int selectedYearIndex;
    int selectedMonthIndex;
    int selectedDayIndex;
    int[] daysOfmonth = {31,// 1
            28,// 2
            31,// 3
            30,// 4
            31,// 5
            30,// 6
            31,// 7
            31,// 8
            30,// 9
            31,// 10
            30,// 11
            31 // 12
    };
    int[] daysOfmonthLeapYear = {31,// 1
            29,// 2
            31,// 3
            30,// 4
            31,// 5
            30,// 6
            31,// 7
            31,// 8
            30,// 9
            31,// 10
            30,// 11
            31 // 12
    };
    Context context;
    List<CommonModel> list_car, list_model;
    String yearStr = "year";
    String monthStr = "month";
    String dayStr = "day";
    boolean canScroll = false;
    boolean srcollOver = true;
    private Handler delayHandler = null;

    public TimePopupWindow(Activity context) {
        super(context);
        this.context = context;
        delayHandler = new Handler();
        initView(context);
        initEvent();
        initData(context);
    }

    @Override
    public void dismiss() {
        // TODO Auto-generated method stub
        super.dismiss();
        if (delayHandler != null) {
            delayHandler.removeCallbacksAndMessages(null);
        }
    }

    /**
     * 设置日期选择控件是否需要自动判断滚动日期到当前日期。 默认是true ，即：会自动判断滚动日期到当前日期。 2014-07-21
     */
    public void setAutoScroll(boolean flag) {
        this.need_autoScroll = flag;
    }

    /**
     * 设置日期控件选择日期时不允许选择比当前日期早或者是不允许选择比当前日期晚。
     *
     * @param int 0-默认值，不允许选择比当前日期早；1-不允许选择比当前日期晚。
     */
    public void setAutoScrollDirect(int direct) {
        if (direct > 1 || direct < 0)
            direct = 0;
        this.autoScroll_direct = direct;
    }

    public void setLastDay() {
        Date date = new Date();
        selectedYearIndex = wheel_year_data.length - 16;
        selectedMonthIndex = date.getMonth() - 1;
        int curr = date.getDay();
        if (curr == 1) {
            // 1月1日
            if (selectedMonthIndex == 0) {
                selectedYearIndex--;
                wheel_year_selectedText = map_year
                        .get(wheel_year_data[selectedYearIndex]);
                wheel_year.setCurrentItem(selectedYearIndex, true);
                selectedMonthIndex = 11;
                wheel_month_selectedText = map_month
                        .get(wheel_month_data[selectedMonthIndex]);
                wheel_month.setCurrentItem(selectedMonthIndex);
                int totalday = getTotalDays(
                        Integer.valueOf(wheel_year_selectedText),
                        wheel_month_selectedText);
                setDay(wheel_month_selectedText,
                        Integer.valueOf(wheel_year_selectedText), totalday);
            } else {
                selectedMonthIndex--;
                wheel_month_selectedText = map_month
                        .get(wheel_month_data[selectedMonthIndex]);
                wheel_month.setCurrentItem(selectedMonthIndex, true);
                int total = getTotalDays(wheel_month_selectedText);
                setDay(wheel_month_selectedText, date.getYear(), total);
            }
        } else {
            wheel_year_selectedText = map_year
                    .get(wheel_year_data[selectedYearIndex]);
            wheel_year.setCurrentItem(selectedYearIndex, true);
            wheel_month_selectedText = map_month
                    .get(wheel_month_data[selectedMonthIndex]);
            wheel_month.setCurrentItem(selectedMonthIndex, true);
            setDay(wheel_month_selectedText, date.getYear(), curr - 1);
        }
    }

    public void setCurrenDay() {
        Date date = new Date();
        selectedYearIndex = wheel_year_data.length - 16;
        selectedMonthIndex = date.getMonth() - 1;
        wheel_year_selectedText = map_year
                .get(wheel_year_data[selectedYearIndex]);
        wheel_month_selectedText = map_month
                .get(wheel_month_data[selectedMonthIndex]);

        wheel_year.setCurrentItem(selectedYearIndex, true);
        wheel_month.setCurrentItem(selectedMonthIndex, true);
        initDay(wheel_month_selectedText);

    }

    public void setNextDay() {
        Date date = new Date();
        selectedYearIndex = wheel_year_data.length - 16;
        selectedMonthIndex = date.getMonth() - 1;

        int total = getTotalDays(wheel_month_selectedText);
        int curr = date.getDay();
        if (curr == total) {
            // 12月30
            if (selectedMonthIndex == 11) {
                selectedYearIndex++;
                wheel_year_selectedText = map_year
                        .get(wheel_year_data[selectedYearIndex]);
                wheel_year.setCurrentItem(selectedYearIndex, true);
                selectedMonthIndex = 0;
                wheel_month_selectedText = map_month
                        .get(wheel_month_data[selectedMonthIndex]);
                wheel_month.setCurrentItem(selectedMonthIndex, true);
                setDay(wheel_month_selectedText,
                        Integer.valueOf(wheel_year_selectedText), 1);
            } else {
                selectedMonthIndex++;
                wheel_month_selectedText = map_month
                        .get(wheel_month_data[selectedMonthIndex]);
                wheel_month.setCurrentItem(selectedMonthIndex, true);
                setDay(wheel_month_selectedText, date.getYear(), 1);
            }
        } else {
            wheel_year_selectedText = map_year
                    .get(wheel_year_data[selectedYearIndex]);
            wheel_year.setCurrentItem(selectedYearIndex, true);
            wheel_month_selectedText = map_month
                    .get(wheel_month_data[selectedMonthIndex]);
            wheel_month.setCurrentItem(selectedMonthIndex, true);
            setDay(wheel_month_selectedText, date.getYear(), curr + 1);
        }
    }

    public String getLastDay() {
        Date time = new Date();
        int year = time.getYear();
        int month = time.getMonth();
        int totalday = getTotalDays(wheel_month_selectedText);
        int day = time.getDay();
        if (totalday == 1) {
            if (month == 1) {
                month = 12;
                year--;
                day = getTotalDays(year, "" + month);
            } else {
                month--;
                day = getTotalDays("" + month);
            }
        } else {
            day--;
        }
        String date = String.format("%s" + yearStr + "-%1$02d" + monthStr
                + "-%1$02d" + dayStr + "", year, month, day);
        return date;
    }

    public String getCurrDay() {
        Date time = new Date();
        int year = time.getYear();
        int month = time.getMonth();
        int day = time.getDay();
        String date = String.format("%s" + yearStr + "-%1$02d" + monthStr
                + "-%1$02d" + dayStr + "", year, month, day);
        return date;
    }

    public String getNextDay() {
        Date time = new Date();
        int year = time.getYear();
        int month = time.getMonth();
        int totalday = getTotalDays(wheel_month_selectedText);
        int day = time.getDay();
        if (totalday == day) {
            if (month == 12) {
                month = 1;
                year++;
            } else {
                month++;
            }
            day = 1;
        } else {
            day++;
        }
        String date = String.format("%s" + yearStr + "-%1$02d" + monthStr
                + "-%1$02d" + dayStr + "", year, month, day);
        return date;
    }

    public void setMidTitle(String midText) {
        if (midText != null) {
            this.midTitle.setText(midText);
        }
    }

    // ------------------------------
    private void initData(Activity context) {

        // Init the year
        for (int i = 0; i < wheel_year_data.length; i++) {
            // 获取当前年份上下十五年的时间
            // wheel_year_data[i] = ""+
            // (TimeHelper.GetMyDate().getYear()-wheel_year_data.length/2+i+1)+"年";
            // 获取截止目前为止含当年三十年的时间
            wheel_year_data[i] = ""
                    + (new Date().getYear() - wheel_year_data.length + i + 1)
                    + yearStr;
            map_year.put(wheel_year_data[i], ""
                    + (new Date().getYear() - wheel_year_data.length + i + 1));
        }

        // Init the month
        for (int i = 0; i < 12; i++) {
            String month = String.format("%1$02d" + monthStr, i + 1);
            wheel_month_data[i] = month;
            // if(i<9){
            // wheel_month_data[i]= "0"+(i+1)+monthStr;
            // }
            // else{
            // wheel_month_data[i]= ""+(i+1)+monthStr;
            // }
            map_month.put(wheel_month_data[i], "" + (i + 1));
        }

        // wheel_year_selectedText=map_year.get(wheel_year_data[0]);
        // wheel_month_selectedText = map_month.get(wheel_month_data[0]);
        // wheel_day_selectedText = "1";

        wheel_year.setVisibleItems(visibleItemCount);
        ArrayWheelAdapter<String> year_adapter = new ArrayWheelAdapter<String>(
                context, wheel_year_data);
        year_adapter.setTextColor(Color.GRAY);
        wheel_year.setViewAdapter(year_adapter);

        wheel_month.setVisibleItems(visibleItemCount);
        ArrayWheelAdapter<String> month_adapter = new ArrayWheelAdapter<String>(
                context, wheel_month_data);
        month_adapter.setTextColor(Color.GRAY);
        wheel_month.setViewAdapter(month_adapter);

        wheel_day.setVisibleItems(visibleItemCount);
        // modify 2015-04-13 09:52
        // ArrayWheelAdapter<String> day_adapter = new
        // ArrayWheelAdapter<String>(context, wheel_day_data);
        // wheel_day.setViewAdapter(day_adapter);
        setToNow();
    }

    private void setToNow() {
        Date date = new Date();
        // selectedYearIndex = wheel_year_data.length-16;

        // 默认选中当前年份
        selectedYearIndex = wheel_year_data.length - 1;

        // 默认选中当前月
        selectedMonthIndex = date.getMonth() - 1;

        wheel_year_selectedText = map_year
                .get(wheel_year_data[selectedYearIndex]);
        wheel_month_selectedText = map_month
                .get(wheel_month_data[selectedMonthIndex]);

        delayHandler.postDelayed(new Runnable() {

            @Override
            public void run() {
                // TODO Auto-generated method stub
                wheel_year.setCurrentItem(selectedYearIndex, true);
            }
        }, 300);

        delayHandler.postDelayed(new Runnable() {

            @Override
            public void run() {
                // TODO Auto-generated method stub
                wheel_month.setCurrentItem(selectedMonthIndex, true);
            }
        }, 400);

        initDay(wheel_month_selectedText);
    }

    private void setToNow(WheelView wheel) {
        Date date = new Date();
        if (wheel == wheel_year) {
            // 不允许比当前时间早
            if (autoScroll_direct == 0) {
                if (date.getYear() > Integer.parseInt(wheel_year_selectedText)) {
                    selectedYearIndex = wheel_year_data.length - 16;
                    wheel_year
                            .scroll(selectedYearIndex - scroll_stop_item, 200);
                    canScroll = false;
                } else if (date.getYear() == Integer
                        .parseInt(wheel_year_selectedText)) {
                    canScroll = false;
                } else {
                    canScroll = true;
                }
            }
            // 不允许比当前时间晚
            else if (autoScroll_direct == 1) {
                if (date.getYear() < Integer.parseInt(wheel_year_selectedText)) {
                    selectedYearIndex = wheel_year_data.length - 16;
                    wheel_year
                            .scroll(selectedYearIndex - scroll_stop_item, 200);
                    canScroll = false;
                } else if (date.getYear() == Integer
                        .parseInt(wheel_year_selectedText)) {
                    canScroll = false;
                } else {
                    canScroll = true;
                }
            }
        } else if (wheel == wheel_month) {
            if (autoScroll_direct == 0) {
                if (date.getMonth() > Integer
                        .parseInt(wheel_month_selectedText) && !canScroll)
                // if(date.getMonth() >
                // Integer.parseInt(wheel_month_selectedText))
                {
                    selectedMonthIndex = date.getMonth() - 1;
                    wheel_month.scroll(selectedMonthIndex - scroll_stop_item,
                            200);
                }
            } else if (autoScroll_direct == 1) {
                if (date.getMonth() < Integer
                        .parseInt(wheel_month_selectedText) && !canScroll)
                // if(date.getMonth() <
                // Integer.parseInt(wheel_month_selectedText))
                {
                    selectedMonthIndex = date.getMonth() - 1;
                    wheel_month.scroll(selectedMonthIndex - scroll_stop_item,
                            200);
                }
            }

        } else if (wheel == wheel_day) {
            int sel_month = Integer.parseInt(wheel_month_selectedText);
            int sel_year = Integer.parseInt(wheel_year_selectedText);
            if (autoScroll_direct == 0) {
                if (date.getDay() > Integer.parseInt(wheel_day_selectedText)
                        && !canScroll && sel_month == new Date().getMonth()
                        && sel_year == new Date().getYear()) {
                    selectedDayIndex = date.getDay() - 1;
                    wheel_day.scroll(selectedDayIndex - scroll_stop_item, 200);
                }
            } else if (autoScroll_direct == 1) {
                if (date.getDay() < Integer.parseInt(wheel_day_selectedText)
                        && !canScroll && sel_month == new Date().getMonth()
                        && sel_year == new Date().getYear()) {
                    selectedDayIndex = date.getDay() - 1;
                    wheel_day.scroll(selectedDayIndex - scroll_stop_item, 200);
                }
            }
        }
    }

    /**
     * 根据月份获取总天数
     *
     * @param _wheel_month_selectedText
     * @return
     */
    private int getTotalDays(String _wheel_month_selectedText) {
        int year = new Date().getYear();
        int total = 0;
        if (isLeapYear(year)) {
            total = daysOfmonthLeapYear[Integer
                    .parseInt(_wheel_month_selectedText) - 1];
        } else {
            total = daysOfmonth[Integer.parseInt(_wheel_month_selectedText) - 1];
        }
        return total;
    }

    /**
     * 更具年月获取总天数
     *
     * @param year
     * @param _wheel_month_selectedText
     * @return
     */
    private int getTotalDays(int year, String _wheel_month_selectedText) {
        int total = 0;
        // Log.d("year", "year:"+year);
        if (isLeapYear(year)) {
            total = daysOfmonthLeapYear[Integer
                    .parseInt(_wheel_month_selectedText) - 1];
            // Log.d("year", "leap year days:"+total);
        } else {
            total = daysOfmonth[Integer.parseInt(_wheel_month_selectedText) - 1];
            // Log.d("year", "year days:"+total);
        }
        return total;
    }

    private void initDay(String _wheel_month_selectedText) {
        String dayStr = "day";

        if (wheel_year_selectedText.equals("")) {
            wheel_year_selectedText = "" + new Date().getYear();
        }
        int total = getTotalDays(Integer.valueOf(wheel_year_selectedText),
                _wheel_month_selectedText);
        wheel_day_data = new String[total];
        for (int i = 0; i < total; i++) {
            String day = String.format("%1$02d" + dayStr, i + 1);
            wheel_day_data[i] = day;
            // if(i<9){
            // wheel_day_data[i] = "0"+(i+1)+dayStr;
            // }else{
            // wheel_day_data[i] = ""+(i+1)+dayStr;
            // }
            map_day.put(wheel_day_data[i], "" + (i + 1));
        }
        int day = new Date().getDay();
        selectedDayIndex = day - 1;
        if (total <= 30) {
            if (selectedDayIndex >= 30) {
                selectedDayIndex = 29;
            }
        }
        if (total <= 28) {
            if (selectedDayIndex >= 28) {
                selectedDayIndex = 27;
            }
        }
        if (total <= 29) {
            if (selectedDayIndex >= 29) {
                selectedDayIndex = 28;
            }
        }
        wheel_day_selectedText = map_day.get(wheel_day_data[selectedDayIndex]);
        ArrayWheelAdapter<String> day_adapter = new ArrayWheelAdapter<String>(
                context, wheel_day_data);
        day_adapter.setTextColor(Color.GRAY);
        wheel_day.setViewAdapter(day_adapter);
        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                // TODO Auto-generated method stub
                wheel_day.setCurrentItem(selectedDayIndex, true);
            }
        }, 500);
    }

    /**
     * @param _wheel_month_selectedText
     * @param day
     */
    private void setDay(String _wheel_month_selectedText, int year, int day) {
        int total = getTotalDays(year, _wheel_month_selectedText);
        String dayStr = "day";
        wheel_day_data = new String[total];
        for (int i = 0; i < total; i++) {
            // wheel_day_data[i] = ""+(i+1)+dayStr;
            String tmpday = String.format("%1$02d" + dayStr, i + 1);
            wheel_day_data[i] = tmpday;
            map_day.put(wheel_day_data[i], "" + (i + 1));
        }
        selectedDayIndex = day - 1;
        if (day > total) {
            selectedDayIndex = total - 1;
        }
        wheel_day_selectedText = map_day.get(wheel_day_data[selectedDayIndex]);
        ArrayWheelAdapter<String> day_adapter = new ArrayWheelAdapter<String>(
                context, wheel_day_data);
        wheel_day.setViewAdapter(day_adapter);
        wheel_day.setCurrentItem(selectedDayIndex, true);
    }

    private void initEvent() {
        wheel_year.addChangingListener(this);
        wheel_year.addScrollingListener(this);
        wheel_month.addChangingListener(this);
        wheel_month.addScrollingListener(this);
        wheel_day.addChangingListener(this);
        wheel_day.addScrollingListener(this);
        bt_cancel.setOnClickListener(this);
        bt_ok.setOnClickListener(this);
    }

    private void initView(Activity context) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        // view = inflater.inflate(R.layout.popup_datepicker, null);
        // bt_cancel = (TextView) view
        // .findViewById(R.id.tv_popup_datapicker_cancel);
        // midTitle = (TextView)
        // view.findViewById(R.id.tv_popup_datapicker_title);
        // bt_ok = (TextView) view.findViewById(R.id.tv_popup_datapicker_ok);
        // wheel_year = (WheelView) view.findViewById(R.id.wheel_year);
        // wheel_year.setCenterDrawable(getContext().getResources().getDrawable(
        // R.drawable.layer_wheel_left));
        // wheel_month = (WheelView) view.findViewById(R.id.wheel_month);
        // wheel_month.setCenterDrawable(getContext().getResources().getDrawable(
        // R.drawable.layer_wheel_mid));
        // wheel_month.setCyclic(true);
        // wheel_day = (WheelView) view.findViewById(R.id.wheel_day);
        // wheel_day.setCenterDrawable(getContext().getResources().getDrawable(
        // R.drawable.layer_wheel_right));
        wheel_day.setCyclic(true);
        this.setContentView(view);
        this.setWidth(android.view.ViewGroup.LayoutParams.MATCH_PARENT);
        this.setHeight(android.view.ViewGroup.LayoutParams.WRAP_CONTENT);
        this.setFocusable(true);
        ColorDrawable dw = new ColorDrawable(0xb0000000);
        this.setBackgroundDrawable(dw);
    }

    @Override
    public void onChanged(WheelView wheel, int oldValue, int newValue) {
        srcollOver = false;
        if (wheel == wheel_year) {
            wheel_year_selectedText = map_year.get(wheel_year_data[newValue]);
            initDay(wheel_month_selectedText);
            // if(newValue == wheel_year_data.length-16)
            // {
            // setToNow();
            // }
        } else if (wheel == wheel_month) {
            wheel_month_selectedText = map_month
                    .get(wheel_month_data[newValue]);
            initDay(wheel_month_selectedText);
        } else if (wheel == wheel_day) {
            wheel_day_selectedText = map_day.get(wheel_day_data[newValue]);
        }
        scroll_stop_item = newValue;
    }

    boolean isLeapYear(int year) {
        if (year % 400 == 0) {
            return true;
        } else if (year % 4 == 0 && year % 100 != 0) {
            return true;
        } else {
            return false;
        }
    }

    // @Override
    // protected void show(View relyview) {
    // // TODO Auto-generated method stub
    // super.show(relyview);
    // }

    @Override
    public void onClick(View v) {
    }

    @Override
    public void onScrollingStarted(WheelView wheel) {
        // TODO Auto-generated method stub
        srcollOver = false;
    }

    @Override
    public void onScrollingFinished(WheelView wheel) {
        // TODO Auto-generated method stub
        if (need_autoScroll) {
            setToNow(wheel);
        }
        srcollOver = true;
    }

    public interface OnDatePickerInfoListener {
        void onDateReceiver(String date);
    }

    public class CommonModel {
        public int id;
        public String name;
    }
}
