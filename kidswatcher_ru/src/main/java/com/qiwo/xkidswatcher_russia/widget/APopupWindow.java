package com.qiwo.xkidswatcher_russia.widget;

import android.animation.Animator;
import android.animation.Animator.AnimatorListener;
import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewTreeObserver;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.PopupWindow;

/**
 * @ClassName: APopupWindow
 */
public abstract class APopupWindow extends PopupWindow {

    Context mContext;

    LinearLayout container;

    int bgColor = 0;

    View rely;

    int textsize = 17;

    /**
     * @param context
     * @param bgColor
     * @param relyview
     */
    public APopupWindow(Context context, int bgColor, View relyview) {
        this(context, relyview, bgColor);
    }

    /**
     * @param context
     * @param relyview
     */
    public APopupWindow(Context context, View relyview, int bgColor) {
        super(context);
        this.mContext = context;
        this.rely = relyview;
        container = new LinearLayout(context);
        LayoutParams params = new LayoutParams(
                android.view.ViewGroup.LayoutParams.MATCH_PARENT,
                android.view.ViewGroup.LayoutParams.WRAP_CONTENT);
        params.gravity = Gravity.CENTER_HORIZONTAL;
        container.setLayoutParams(params);
        container.setPadding(20, 20, 20, 20);
        container.setOrientation(LinearLayout.VERTICAL);
        this.setContentView(container);
        this.setWidth(android.view.ViewGroup.LayoutParams.MATCH_PARENT);
        this.setHeight(android.view.ViewGroup.LayoutParams.WRAP_CONTENT);
        this.setFocusable(true);
        this.setTouchable(true);
        this.setOutsideTouchable(true);
        setBackgroundDrawable();
    }

    public void addItem(String title, onClickItemListener listener,
                        ItemPosition pos) {
        addItem(title, listener, null, pos);
    }

    public void addItem(String title, onClickItemListener listener,
                        Drawable drawable, ItemPosition pos) {
        addItem(title, listener, 0, 0, drawable, pos);
    }

    public void addItem(String title, onClickItemListener listener,
                        int bgDrawableId, int textColorResId, Drawable drawable,
                        ItemPosition pos) {
        int default_textSize = textsize;
        addItem(title, listener, bgDrawableId, textColorResId,
                default_textSize, drawable, pos, 0);
    }

    public void addCancelItem(String title, onClickItemListener listener,
                              int marginTop) {
        int default_textSize = textsize;
        addItem(title, listener, 0, 0, default_textSize, null,
                ItemPosition.OTHER, marginTop);
    }

    public void addItem(String title, final onClickItemListener listener,
                        int bgDrawableId, int textColorResId, int textSize,
                        Drawable drawable, ItemPosition pos, int marginTop) {
        View item = null;
        item = getItemView(drawable, title, bgDrawableId, textColorResId,
                textSize, pos, 0);
        if (item == null) {
            return;
        }
        item.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                APopupWindow.this.dismiss();
                if (listener != null) {
                    listener.clickItem(v);
                }
            }
        });

        this.container.addView(item);
    }

    protected abstract View getItemView(Drawable drawable, String title,
                                        int bgDrawableId, int textColorResId, int textSize,
                                        ItemPosition pos, int marginTop);

    protected abstract void showlocation(View relyview);

    public void show() {
        ViewTreeObserver vto = getContentView().getViewTreeObserver();
        vto.addOnGlobalLayoutListener(new OnGlobalLayoutListener() {

            @SuppressLint("NewApi")
            @Override
            public void onGlobalLayout() {
                // TODO Auto-generated method stub
                // int height=getContainer().getHeight();
                // Debug.d("pop", "get contentView height:"+height);
                if (Build.VERSION.SDK_INT >= 16) {
                    getContentView().getViewTreeObserver()
                            .removeOnGlobalLayoutListener(this);
                } else {
                    getContentView().getViewTreeObserver()
                            .removeGlobalOnLayoutListener(this);
                }
                startInAnimation();
            }
        });
        showlocation(this.rely);
    }

    @Override
    public void dismiss() {
        // TODO Auto-generated method stub
        BaseEffects animator = startOutAnimation();
        // super.dismiss()在动画结束时调用
        if (animator == null) {
            super.dismiss();
        } else {
            animator.getAnimatorSet().addListener(new AnimatorListener() {

                public void onAnimationStart(Animator arg0) {
                    // TODO Auto-generated method stub
                }

                public void onAnimationRepeat(Animator arg0) {
                    // TODO Auto-generated method stub

                }

                public void onAnimationEnd(Animator arg0) {
                    // TODO Auto-generated method stub
                    APopupWindow.this.dismissSelf();
                }

                public void onAnimationCancel(Animator arg0) {
                    // TODO Auto-generated method stub
                    APopupWindow.this.dismissSelf();
                }
            });

            animator.start(getContentView());
        }
    }

    private void dismissSelf() {
        super.dismiss();
    }

    protected void startInAnimation() {
        // TODO
        // 交给子类实现，可以有动画，也可以无动画
    }

    protected BaseEffects startOutAnimation() {
        // TODO
        // 交给子类实现，可以有动画，也可以无动画
        // 子类动画不可调用start,交给父类调用
        return null;
    }

    protected void setBackgroundDrawable() {
        ColorDrawable dw = new ColorDrawable(0xb0000000);
        this.setBackgroundDrawable(dw);
    }

    /**
     * @param
     * @return void
     * @throws
     * @Description: TODO 设置为透明背景
     */
    protected void setBackgroundAlpha() {
        ColorDrawable dw = new ColorDrawable(0x00000000);
        this.setBackgroundDrawable(dw);
    }

    protected LinearLayout getContainer() {
        return this.container;
    }

    /**
     * @param @return
     * @return View
     * @throws
     * @Description: TODO 获取每个item view 之间的分割线View
     */
    protected View getdivideView() {
        return null;
    }

    protected View getRelyView() {
        return this.rely;
    }

    protected int getBgColor() {
        return this.bgColor;
    }

    protected Context getContext() {
        return this.mContext;
    }

    public static enum ItemPosition {
        // 顶部，中间，底部
        // 其他为底部返回专用，或者是特殊情况
        // 缺省什么都不是
        TOP, MIDDLE, BOTTOM, OTHER, DEFAULT;
    }

    public interface onClickItemListener {
        void clickItem(View v);
    }
}
