package com.qiwo.xkidswatcher_russia.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.google.gson.Gson;
import com.qiwo.xkidswatcher_russia.KidsWatConfig;
import com.qiwo.xkidswatcher_russia.bean.FamilyMember;
import com.qiwo.xkidswatcher_russia.bean.beanForDb___Family;
import com.qiwo.xkidswatcher_russia.bean.beanForDb___GBilling;
import com.qiwo.xkidswatcher_russia.bean.beanForDb___LoginMember;
import com.qiwo.xkidswatcher_russia.bean.beanForDb___Message;
import com.qiwo.xkidswatcher_russia.bean.beanFor___get_family_info;
import com.qiwo.xkidswatcher_russia.bean.beanFor___get_family_info_v2;
import com.qiwo.xkidswatcher_russia.bean.beanFor___get_family_list;
import com.qiwo.xkidswatcher_russia.bean.beanFor___get_safe_area_config;
import com.qiwo.xkidswatcher_russia.bean.beanFor___get_watch_info;
import com.qiwo.xkidswatcher_russia.bean.beanFor___login;
import com.qiwo.xkidswatcher_russia.util.Base64Encode;
import com.qiwo.xkidswatcher_russia.util.SimpleDateUtil;
import net.intari.CustomLogger.CustomLog;

import net.intari.CustomLogger.CustomLog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class SqlDb {

    final String TAG = SqlDb.class.getSimpleName();

    // SqlDb db = SqlDb.get(appContext);
    // db.saveMember(response);
    private DbHelper dbHelper;
    private SQLiteDatabase db;
    private Context c;
    private Gson gson;

    private SqlDb(Context context) {
        c = context;
        dbHelper = new DbHelper(c);
        db = dbHelper.getWritableDatabase();
        gson = new Gson();
    }

    public static SqlDb get(Context context) {
        return new SqlDb(context);
    }

    public SQLiteDatabase getDb() {
        return db;
    }

    public synchronized void closeDb() {
        if (db.isOpen()) {
            db.close();
        }
    }

    public boolean saveFamily(String family_id, String base64_img,
                              String img_path) {
        SQLiteDatabase db = getDb();
        final String TABLE = "kw_family";
        String strdate = new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
                .format(new Date());

        ContentValues cv = new ContentValues();
        cv.put("base64_img", base64_img);
        cv.put("img_path", img_path);
        cv.put("updateTime", strdate);
        db.update(TABLE, cv, "family_id=?", new String[]{family_id});
        // db.close();
        return true;
    }

    public boolean saveFamilyValid_date(String device_id, String valid_date) {
        SQLiteDatabase db = getDb();
        final String TABLE = "kw_family";
        String strdate = new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
                .format(new Date());

        ContentValues cv = new ContentValues();
        cv.put("valid_date", valid_date);
        cv.put("updateTime", strdate);
        db.update(TABLE, cv, "device_id=?", new String[]{device_id});
        // db.close();
        return true;
    }

    public boolean saveFamily___last_record_no(String device_id,
                                               String last_record_no) {
        SQLiteDatabase db = getDb();
        final String TABLE = "kw_family";
        String strdate = new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
                .format(new Date());

        ContentValues cv = new ContentValues();
        cv.put("last_record_no", last_record_no);
        cv.put("updateTime", strdate);
        db.update(TABLE, cv, "device_id=?", new String[]{device_id});
        // db.close();
        return true;
    }

    public boolean saveFamily(String family_id, beanFor___get_family_info b) {
        if (b.getError() != 0 || b.getInfo().getBaby() == null)
            return false;

        SQLiteDatabase db = getDb();
        final String TABLE = "kw_family";
        String strdate = new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
                .format(new Date());

        // String family_id = b.getInfo().getBaby().getFamily_id();

        ContentValues cv = new ContentValues();
        Cursor cursor = db.query(TABLE, new String[]{"family_id"},
                "family_id=?", new String[]{family_id}, null, null, null);

        cv.put("device_id", b.getInfo().getDdata().getDevice_id());
        cv.put("device_iccid", b.getInfo().getDdata().getDevice_iccid());
        cv.put("create_uid", b.getInfo().getBaby().getUid());
        cv.put("nickname", b.getInfo().getBaby().getNickname());
        cv.put("birthday", b.getInfo().getBaby().getBirthday());
        cv.put("birthdayText", b.getInfo().getBaby().getBirthdayText());
        cv.put("sex", b.getInfo().getBaby().getSex());
        cv.put("sexText", b.getInfo().getBaby().getSexText());
        cv.put("height", b.getInfo().getBaby().getHeight());
        cv.put("weight", b.getInfo().getBaby().getWeight());
        cv.put("grade", b.getInfo().getBaby().getGrade());
        cv.put("version", b.getInfo().getVersion());
        String imgBase64 = b.getInfo().getBaby().getBase64Img();


        String img_path = "";
        if (imgBase64 != null && imgBase64.length() > 5) {
            img_path = saveBbImg(family_id, imgBase64);
        } else {
            imgBase64 = "";
        }
        img_path = b.getInfo().getBaby().getImg_path();
        if (cursor.getCount() > 0) {

            cv.put("base64_img", imgBase64);
            cv.put("img_path", img_path);
            cv.put("updateTime", strdate);
            int r = db.update(TABLE, cv, "family_id=?",
                    new String[]{family_id});
            CustomLog.d(TAG,"r=" + r);
        } else {
            cv.put("family_id", family_id);
            cv.put("img_path", img_path);
            cv.put("base64_img", imgBase64);
            cv.put("addTime", strdate);
            db.insert(TABLE, null, cv);
        }

        // db.close();
        return true;
    }

    public boolean saveFamily(String family_id, beanFor___get_family_info_v2 b) {

        SQLiteDatabase db = getDb();
        final String TABLE = "kw_family";
        String strdate = new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
                .format(new Date());

        // String family_id = b.getInfo().getBaby().getFamily_id();

        ContentValues cv = new ContentValues();
        Cursor cursor = db.query(TABLE, new String[]{"family_id"},
                "family_id=?", new String[]{family_id}, null, null, null);

        cv.put("device_id", b.getDevice_id());
        cv.put("create_uid", b.getBaby_info().getUid());
        cv.put("nickname", b.getBaby_info().getNickname());
        cv.put("birthday", b.getBaby_info().getBirthday());
        cv.put("birthdayText", b.getBaby_info().getBirthdayText());
        cv.put("sex", b.getBaby_info().getSex());
        cv.put("sexText", b.getBaby_info().getSexText());
        cv.put("height", b.getBaby_info().getHeight());
        cv.put("weight", b.getBaby_info().getWeight());
        cv.put("grade", b.getBaby_info().getGrade());
        cv.put("version", b.getVersion() + "");
		
		/*//
		String imgBase64 = b.getBaby_info().getImg();
		
		String img_path = "";
		if (imgBase64 != null && imgBase64.length() > 5) {
			img_path = saveBbImg(family_id, imgBase64);
		} else {
			imgBase64 = "";
		}*/
        String bb_imgpath = b.getBaby_info().getImg_path();

        if (cursor.getCount() > 0) {

            cv.put("img_path", bb_imgpath);
            cv.put("updateTime", strdate);
            int r = db.update(TABLE, cv, "family_id=?",
                    new String[]{family_id});
            CustomLog.d(TAG,"r=" + r);
        } else {
            cv.put("family_id", family_id);
            cv.put("img_path", bb_imgpath);
            cv.put("addTime", strdate);
            db.insert(TABLE, null, cv);
        }

        // db.close();
        return true;
    }

    public boolean saveFamily(String device_id, beanFor___get_watch_info b) {
        if (b.error != 0 || b.info.ddata == null)
            return false;

        SQLiteDatabase db = getDb();
        final String TABLE = "kw_family";
        String strdate = new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
                .format(new Date());

        // String family_id = b.info.ddata.family_id;

        ContentValues cv = new ContentValues();
        // ,"device_id", "create_uid", "img_path", "nickname", "birthday","sex",
        // "height", "weight", "sim", "qrcode", "hard_code","bt_addr",
        // "bt_addr_pwd"
        Cursor cursor = db.query(TABLE, new String[]{"family_id"},
                "device_id=?", new String[]{device_id}, null, null, null);

        cv.put("sim", b.info.ddata.sim);
        cv.put("qrcode", b.info.ddata.qrcode);
        cv.put("verify", b.info.ddata.verify);
        cv.put("hard_code", b.info.ddata.hard_code);
        cv.put("device_key", b.info.ddata.device_key);
        cv.put("bt_addr", b.info.ddata.bt_addr);
        cv.put("bt_addr_ex", getBt_addr_ex(b.info.ddata.bt_addr));
        cv.put("active_time", b.info.ddata.active_time);
        cv.put("bt_addr_pwd", b.info.ddata.bt_addr_pwd);

        // 鏌ヨ鏄惁瀛樺湪鎸囧畾鎵嬫満鍜寀id鐨勬暟鎹�鏈夊垯淇敼,鏃犲垯娣诲姞
        if (cursor.getCount() > 0) {
            cv.put("updateTime", strdate);
            int r = db.update(TABLE, cv, "device_id=?",
                    new String[]{device_id});
            CustomLog.d(TAG,"r=" + r);
        } else {
            cv.put("addTime", strdate);
            db.insert(TABLE, null, cv);
        }

        // db.close();
        return true;
    }

    public int updateFamily(String family_id, String nickname, long birthday,
                            int sex, float height, float weight) {
        // family_id, nickname, birthday, sex, height,weight

        SQLiteDatabase db = getDb();
        final String TABLE = "kw_family";
        String strdate = new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
                .format(new Date());

        ContentValues cv = new ContentValues();

        String birthdayText = SimpleDateUtil.convert2String(birthday * 1000,
                SimpleDateUtil.DATE_FORMAT);
        cv.put("nickname", nickname);
        cv.put("birthday", birthday);
        cv.put("birthdayText", birthdayText);
        cv.put("sex", sex);
        cv.put("sexText", sex == 1 ? "boy" : "girl");
        cv.put("height", height);
        cv.put("weight", weight);

        cv.put("updateTime", strdate);
        int r = db.update(TABLE, cv, "family_id=?", new String[]{family_id});
        CustomLog.d(TAG,"r=" + r);

        // db.close();
        return r;
    }

    public boolean saveMember(beanFor___login b) {
        if (b.error != 0)
            return false;
        String phone = b.info.user.phone;
        String uid = b.info.user.uid;

        SQLiteDatabase db = getDb();
        final String TABLE = "kw_member";
        ContentValues cv = new ContentValues();
        cv.put("name", b.info.user.name);
        cv.put("country_code", b.info.user.country_code);
        cv.put("reg_time", b.info.user.reg_time);
        cv.put("push_token", b.info.user.push_token);
        cv.put("start_time", b.info.user.start_time);
        cv.put("end_time", b.info.user.end_time);

        String strdate = new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
                .format(new Date());
        Cursor cursor = db.query(TABLE, new String[]{"phone", "uid",
                        "family_id"}, "phone=? and uid=?",
                new String[]{phone, uid}, null, null, null);
        // 鏌ヨ鏄惁瀛樺湪鎸囧畾鎵嬫満鍜寀id鐨勬暟鎹�鏈夊垯淇敼,鏃犲垯娣诲姞
        if (cursor.getCount() > 0) {
            cv.put("updateTime", strdate);
            db.update(TABLE, cv, "phone = ? and uid=?", new String[]{phone,
                    uid});
        } else {
            cv.put("phone", phone);
            cv.put("uid", uid);
            cv.put("addTime", strdate);
            db.insert(TABLE, null, cv);
        }

        // db.close();
        return true;
    }

    public boolean saveMember(beanFor___get_family_list b) {
        if (b.getError() != 0 || b.getInfo().getTotal() == 0)
            return false;

        SQLiteDatabase db = getDb();
        final String TABLE = "kw_member";
        String strdate = new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
                .format(new Date());

        for (FamilyMember f : b.getInfo().getDdata()) {
            String uid = f.getUid();

            Cursor cursor = db.query(TABLE, new String[]{"phone", "uid",
                            "family_id"}, "uid=?", new String[]{uid}, null, null,
                    null);
            ContentValues cv = new ContentValues();
            cv.put("family_id", f.getFamily_id());
            cv.put("admin", f.getAdmin());
            cv.put("relation", f.getRelation());
            if (cursor.getCount() > 0) {
                cv.put("updateTime", strdate);
                db.update(TABLE, cv, "uid=?", new String[]{uid});
            } else {
                cv.put("uid", uid);
                cv.put("addTime", strdate);
                db.insert(TABLE, null, cv);
            }
        }
        // db.close();
        return true;
    }


    public boolean save___member_family(beanFor___get_family_info_v2 b) {

        SQLiteDatabase db = getDb();
        final String TABLE = "kw_member_family";
        String strdate = new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
                .format(new Date());

        String uid = KidsWatConfig.getUserUid();
        String family_id = b.getBaby_info().getFamily_id();

        Cursor cursor = db.query(TABLE, new String[]{"phone", "uid",
                "family_id"}, "uid=? and family_id=?", new String[]{uid,
                family_id}, null, null, null);
        ContentValues cv = new ContentValues();
        cv.put("country_code", b.getCountry_code());
        cv.put("phone", b.getMobile());
        cv.put("isAdmin", b.getAdmin());
        cv.put("relation", b.getRelation());
        if (cursor.getCount() > 0) {
            cv.put("updateTime", strdate);
            db.update(TABLE, cv, "uid=? and family_id=?", new String[]{
                    uid, family_id});
        } else {
            cv.put("uid", uid);
            cv.put("family_id", family_id);
            cv.put("addTime", strdate);
            db.insert(TABLE, null, cv);
        }

        return true;
    }

    public boolean save___member_family(beanFor___get_family_list b) {
        if (b.getError() != 0 || b.getInfo().getTotal() == 0)
            return false;

        SQLiteDatabase db = getDb();
        final String TABLE = "kw_member_family";
        String strdate = new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
                .format(new Date());

        for (FamilyMember f : b.getInfo().getDdata()) {
            String uid = f.getUid();
            String family_id = f.getFamily_id();

            Cursor cursor = db.query(TABLE, new String[]{"phone", "uid",
                    "family_id"}, "uid=? and family_id=?", new String[]{uid,
                    family_id}, null, null, null);
            ContentValues cv = new ContentValues();
            cv.put("country_code", f.getCountry_code());
            cv.put("phone", f.getMobile());
            cv.put("isAdmin", f.getAdmin());
            cv.put("relation", f.getRelation());
            if (cursor.getCount() > 0) {
                cv.put("updateTime", strdate);
                db.update(TABLE, cv, "uid=? and family_id=?", new String[]{
                        uid, family_id});
            } else {
                cv.put("uid", uid);
                cv.put("family_id", family_id);
                cv.put("addTime", strdate);
                db.insert(TABLE, null, cv);
            }
        }
        // db.close();
        return true;
    }

    public int set___relation_by_uid_fid(String uid, String family_id,
                                         String relation) {
        SQLiteDatabase db = getDb();
        ContentValues cv = new ContentValues();
        cv.put("relation", relation);
        int r = db.update("kw_member_family", cv, "uid=? and family_id=?",
                new String[]{uid, family_id});
        return r;
    }

    public boolean saveMessage(String uid, String family_id, JSONObject js)
            throws JSONException {
        SQLiteDatabase db = getDb();
        final String TABLE = "kw_message";

        int code = js.getInt("error");
        if (code == 0) {
            JSONArray msg_arr = js.getJSONArray("data");
            for (int i = 0; i < msg_arr.length(); i++) {

                JSONObject msg_item = msg_arr.getJSONObject(i);

                int content_type = msg_item.getInt("content_type");
                String message_id = msg_item.getString("message_id");
                String content_type_text = msg_item
                        .getString("content_type_text");
                long time = msg_item.getLong("time");
                String content = msg_item.getString("content");

                Cursor cursor = db.query(TABLE, new String[]{"message_id",
                                "content_type"}, "message_id=?",
                        new String[]{message_id}, null, null, null);

                if (cursor.getCount() == 0) {
                    String strdate = new java.text.SimpleDateFormat(
                            "yyyy-MM-dd HH:mm:ss").format(new Date());

                    ContentValues cv = new ContentValues();
                    cv.put("message_id", message_id);
                    cv.put("uid", uid);
                    cv.put("family_id", family_id);
                    cv.put("content_type", content_type);
                    cv.put("content_type_text", content_type_text);
                    cv.put("time", time);
                    cv.put("content", content);
                    cv.put("isRead", 0);
                    cv.put("jsonText", msg_item.toString());
                    cv.put("addTime", strdate);

                    db.insert(TABLE, null, cv);
                }

            }

        }

        // db.close();
        return true;
    }

    public boolean addSafezone(String uid, String family_id,
                               beanFor___get_safe_area_config b) throws JSONException {
        SQLiteDatabase db = getDb();
        final String TABLE = "kw_safezone";

        String strdate = new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
                .format(new Date());

        for (beanFor___get_safe_area_config.CConfig f : b.info.ddata.config) {

            ContentValues cv = new ContentValues();
            cv.put("uid", uid);
            cv.put("family_id", family_id);
            cv.put("latitude", f.latitude);
            cv.put("longitude", f.longitude);

            cv.put("radius", f.radius);
            cv.put("name", f.name);
            cv.put("address", f.address);
            cv.put("create_user", f.create_user);
            // userbean b=gson.fromJson("{"user":"admin","pwd":"123456"}",
            // userbean.class);
            String jsonText = gson.toJson(f);
            cv.put("jsonText", jsonText);

            cv.put("addTime", strdate);
            db.insert(TABLE, null, cv);
        }
        // db.close();
        return true;
    }

    public boolean saveGcmMessage(String title, String content,
                                  String bundleText) {
        SQLiteDatabase db = getDb();
        final String TABLE = "gcm_message";
        String strdate = new java.text.SimpleDateFormat(
                "yyyy-MM-dd HH:mm:ss:SSS").format(new Date());
        ContentValues cv = new ContentValues();
        cv.put("title", title);
        cv.put("content", content);
        cv.put("content", content);
        cv.put("bundleText", bundleText);

        cv.put("ltime", System.currentTimeMillis() / 1000);
        cv.put("addTime", strdate);
        db.insert(TABLE, null, cv);

        return true;
    }

    public boolean saveGoogleBilling(String device_id, String family_id,
                                     String price, String orderId, String jsonText,
                                     String policy_sequence) {
        SQLiteDatabase db = getDb();
        final String TABLE = "g_googleBilling";
        String strdate = new java.text.SimpleDateFormat(
                "yyyy-MM-dd HH:mm:ss:SSS").format(new Date());
        ContentValues cv = new ContentValues();
        cv.put("device_id", device_id);
        cv.put("family_id", family_id);

        cv.put("price", price);
        cv.put("orderId", orderId);

        cv.put("jsonText", jsonText);
        cv.put("policy_sequence", policy_sequence);
        cv.put("isUpload", 0);

        cv.put("addTime", strdate);
        db.insert(TABLE, null, cv);

        return true;
    }

    public beanForDb___Family getFamilyBy_did(String device_id) {
        CustomLog.d(TAG,"Asking for family based on device_id:"+device_id+"|");

        SQLiteDatabase db = getDb();
        final String TABLE = "kw_family";
        Cursor cursor = db.query(TABLE, new String[]{"family_id",
                        "device_id", "create_uid", "base64_img", "img_path",
                        "nickname", "birthday", "birthdayText", "sex", "sexText",
                        "height", "weight", "grade", "gradeText", "sim", "qrcode",
                        "hard_code", "bt_addr", "bt_addr_ex", "bt_addr_pwd",
                        "valid_date", "last_record_no", "d_mode", "version", "device_iccid"},
                "device_id=?", new String[]{device_id}, null, null, null);
        return getFamily_where(cursor);
    }

    public beanForDb___Family getFamilyBy_fid(String family_id) {
        CustomLog.d(TAG,"Asking for family based on id:"+family_id);

        SQLiteDatabase db = getDb();
        final String TABLE = "kw_family";
        Cursor cursor = db.query(TABLE, new String[]{"family_id",
                        "device_id", "create_uid", "base64_img", "img_path",
                        "nickname", "birthday", "birthdayText", "sex", "sexText",
                        "height", "weight", "grade", "gradeText", "sim", "qrcode",
                        "hard_code", "bt_addr", "bt_addr_ex", "bt_addr_pwd",
                        "valid_date", "last_record_no", "d_mode", "version", "device_iccid"},
                "family_id=?", new String[]{family_id}, null, null, null);
        return getFamily_where(cursor);
    }

    public beanForDb___Family getFamilyBy_bluetoothAddress(
            String bluetoothAddress) {
        CustomLog.d(TAG,"Asking for family based on bt addr:"+bluetoothAddress+"|");
        SQLiteDatabase db = getDb();
        final String TABLE = "kw_family";
        Cursor cursor = db.query(TABLE, new String[]{"family_id",
                        "device_id", "create_uid", "base64_img", "img_path",
                        "nickname", "birthday", "birthdayText", "sex", "sexText",
                        "height", "weight", "grade", "gradeText", "sim", "qrcode",
                        "hard_code", "bt_addr", "bt_addr_ex", "bt_addr_pwd",
                        "valid_date", "last_record_no", "d_mode", "version", "device_iccid"},
                "bt_addr_ex=?", new String[]{bluetoothAddress}, null, null,
                null);
        return getFamily_where(cursor);
    }

    private beanForDb___Family getFamily_where(Cursor cursor) {

        beanForDb___Family b = new beanForDb___Family();
        if (cursor.getCount() > 0) {
            cursor.moveToFirst();

            b.family_id = cursor.getString(cursor.getColumnIndex("family_id"));
            b.device_id = cursor.getString(cursor.getColumnIndex("device_id"));
            b.device_iccid = cursor.getString(cursor.getColumnIndex("device_iccid"));
            b.create_uid = cursor
                    .getString(cursor.getColumnIndex("create_uid"));
            b.base64_img = cursor
                    .getString(cursor.getColumnIndex("base64_img"));
            b.img_path = cursor.getString(cursor.getColumnIndex("img_path"));
            b.nickname = cursor.getString(cursor.getColumnIndex("nickname"));
            b.birthday = cursor.getLong(cursor.getColumnIndex("birthday"));
            b.birthdayText = cursor.getString(cursor
                    .getColumnIndex("birthdayText"));
            b.sex = cursor.getInt(cursor.getColumnIndex("sex"));
            b.sexText = cursor.getString(cursor.getColumnIndex("sexText"));
            b.height = cursor.getFloat(cursor.getColumnIndex("height"));
            b.weight = cursor.getFloat(cursor.getColumnIndex("weight"));
            b.grade = cursor.getInt(cursor.getColumnIndex("grade"));
            b.gradeText = cursor.getString(cursor.getColumnIndex("gradeText"));
            b.sim = cursor.getString(cursor.getColumnIndex("sim"));

            b.qrcode = cursor.getString(cursor.getColumnIndex("qrcode"));
            b.hard_code = cursor.getString(cursor.getColumnIndex("hard_code"));
            b.bt_addr = cursor.getString(cursor.getColumnIndex("bt_addr"));
            b.bt_addr_ex = cursor
                    .getString(cursor.getColumnIndex("bt_addr_ex"));
            b.bt_addr_pwd = cursor.getString(cursor
                    .getColumnIndex("bt_addr_pwd"));
            b.valid_date = cursor
                    .getString(cursor.getColumnIndex("valid_date"));
            b.last_record_no = cursor.getString(cursor
                    .getColumnIndex("last_record_no"));
            b.d_mode = cursor.getInt(cursor.getColumnIndex("d_mode"));
            b.version = cursor.getString(cursor.getColumnIndex("version"));
        } else {
            CustomLog.w(TAG,"getFamily_where got empty cursor");
        }
        // db.close();
        if (b==null) {
            CustomLog.w(TAG,"getFamily_where wantes to return ZERO family");
        }
        return b;
    }

    public List<beanForDb___LoginMember> getLoginMemberBy_uid(String uid) {
        SQLiteDatabase db = getDb();
        Cursor cursor = db
                .rawQuery(
                        "select _id,uid,country_code,phone,family_id,isAdmin,relation from kw_member_family where uid=?",
                        new String[]{uid});

        List<beanForDb___LoginMember> l = new ArrayList<beanForDb___LoginMember>();
        if (cursor.getCount() > 0) {
            while (cursor.moveToNext()) {
                beanForDb___LoginMember b = new beanForDb___LoginMember();
                b.id = cursor.getInt(cursor.getColumnIndex("_id"));
                b.uid = cursor.getString(cursor.getColumnIndex("uid"));
                b.country_code = cursor.getString(cursor
                        .getColumnIndex("country_code"));
                b.phone = cursor.getString(cursor.getColumnIndex("phone"));
                b.family_id = cursor.getString(cursor
                        .getColumnIndex("family_id"));
                b.isAdmin = cursor.getInt(cursor.getColumnIndex("isAdmin"));
                b.relation = cursor
                        .getString(cursor.getColumnIndex("relation"));

                l.add(b);
            }
        }
        // db.close();

        return l;
    }

    public beanForDb___LoginMember getLoginMemberBy_uid_fid(String uid,
                                                            String family_id) {
        SQLiteDatabase db = getDb();
        Cursor cursor = db
                .rawQuery(
                        "select _id,uid,country_code,phone,family_id,isAdmin,relation from kw_member_family where uid=? and family_id=?",
                        new String[]{uid, family_id});

        beanForDb___LoginMember b = new beanForDb___LoginMember();
        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            b.id = cursor.getInt(cursor.getColumnIndex("_id"));
            b.uid = cursor.getString(cursor.getColumnIndex("uid"));
            b.country_code = cursor.getString(cursor
                    .getColumnIndex("country_code"));
            b.phone = cursor.getString(cursor.getColumnIndex("phone"));
            b.family_id = cursor.getString(cursor.getColumnIndex("family_id"));
            b.isAdmin = cursor.getInt(cursor.getColumnIndex("isAdmin"));
            b.relation = cursor.getString(cursor.getColumnIndex("relation"));
        }
        // db.close();

        return b;
    }

    public boolean getLoginMember_isAdmin(String uid, String family_id) {
        beanForDb___LoginMember b = getLoginMemberBy_uid_fid(uid, family_id);
        return b.isAdmin == 0 ? false : true;
    }

    public int getMessageCount(String uid) {
        SQLiteDatabase db = getDb();
        Cursor cursor = db
                .rawQuery(
                        "select count(_id) as c from kw_message where uid=? order by time desc",
                        new String[]{uid});

        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            int rows = cursor.getInt(cursor.getColumnIndex("c"));
            return rows;
        }
        return 0;
    }

    public int getUnReadMessageCount(String uid) {
        SQLiteDatabase db = getDb();
        Cursor cursor = db
                .rawQuery(
                        "select count(_id) as c from kw_message where uid=? and isRead=0 order by time desc",
                        new String[]{uid});

        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            int rows = cursor.getInt(cursor.getColumnIndex("c"));
            return rows;
        }
        return 0;
    }

    public List<beanForDb___Message> getMessage(String uid, String family_id,
                                                int pageSize, int pageIndex) {
        // final String TABLE = "kw_message";
        // Cursor cursor = db.query(TABLE, new String[] { "_id", "message_id",
        // "content_type", "content_type_text", "time", "content",
        // "isRead", "jsonText" }, "uid=? and family_id=?", new String[] {
        // uid, family_id }, null, null, "time desc");

        // (content_type<>2 and content_type<>3 and and content_type<>4 and and
        // content_type<>5 and content_type<>10 and content_type<>11) and uid=?

        // Cursor cursor = db
        // .query(TABLE,
        // new String[] { "_id", "message_id", "content_type",
        // "content_type_text", "time", "content",
        // "isRead", "jsonText" },
        // "(content_type<>2 and content_type<>3  and content_type<>4  and content_type<>5 and  content_type<>9 and  content_type<>10  and content_type<>11) and uid=?",
        // new String[] { uid }, null, null, "time desc");

        SQLiteDatabase db = getDb();
        Cursor cursor = db
                .rawQuery(
                        "select _id, message_id, content_type, content_type_text, time, content, isRead, jsonText from kw_message where uid=? order by time desc Limit ? Offset ?",
                        new String[]{uid, String.valueOf(pageSize),
                                String.valueOf((pageIndex - 1) * pageSize)});

        List<beanForDb___Message> l = new ArrayList<beanForDb___Message>();
        if (cursor.getCount() > 0) {
            while (cursor.moveToNext()) {
                beanForDb___Message b = new beanForDb___Message();
                b.id = cursor.getInt(cursor.getColumnIndex("_id"));
                b.message_id = cursor.getString(cursor
                        .getColumnIndex("message_id"));
                b.content_type = cursor.getInt(cursor
                        .getColumnIndex("content_type"));
                b.content_type_text = cursor.getString(cursor
                        .getColumnIndex("content_type_text"));
                b.time = cursor.getLong(cursor.getColumnIndex("time"));
                b.content = cursor.getString(cursor.getColumnIndex("content"));
                b.isRead = cursor.getInt(cursor.getColumnIndex("isRead"));
                b.jsonText = cursor
                        .getString(cursor.getColumnIndex("jsonText"));

                l.add(b);
            }
        }

        return l;
    }

    public List<beanForDb___GBilling> getFailedGoogleBilling(String device_id) {
        SQLiteDatabase db = getDb();
        final String TABLE = "g_googleBilling";
        Cursor cursor = db.query(TABLE,
                new String[]{"_id", "device_id", "family_id", "price",
                        "orderId", "jsonText", "policy_sequence"},
                "isUpload=0 and device_id=?", new String[]{device_id}, null,
                null, null);
        List<beanForDb___GBilling> l = new ArrayList<beanForDb___GBilling>();
        if (cursor.getCount() > 0) {
            while (cursor.moveToNext()) {
                beanForDb___GBilling b = new beanForDb___GBilling();
                b.id = cursor.getInt(cursor.getColumnIndex("_id"));
                b.device_id = cursor.getString(cursor
                        .getColumnIndex("device_id"));
                b.family_id = cursor.getString(cursor
                        .getColumnIndex("family_id"));

                b.price = cursor.getString(cursor.getColumnIndex("price"));
                b.orderId = cursor.getString(cursor.getColumnIndex("orderId"));
                b.jsonText = cursor
                        .getString(cursor.getColumnIndex("jsonText"));
                b.policy_sequence = cursor.getString(cursor
                        .getColumnIndex("policy_sequence"));

                l.add(b);
            }
        }

        return l;
    }

    public int setDeviceModeBy_fid(String family_id, int d_mode) {
        SQLiteDatabase db = getDb();
        ContentValues cv = new ContentValues();
        cv.put("d_mode", d_mode);
        int r = db.update("kw_family", cv, "d_mode=?",
                new String[]{family_id});
        return r;
    }

    public int setDeviceModeBy_did(String device_id, int d_mode) {
        SQLiteDatabase db = getDb();
        ContentValues cv = new ContentValues();
        cv.put("d_mode", d_mode);
        int r = db.update("kw_family", cv, "device_id=?",
                new String[]{device_id});
        return r;
    }

    public int setMessageReadedBy_mid(String message_id) {
        SQLiteDatabase db = getDb();
        ContentValues cv = new ContentValues();
        cv.put("isRead", 1);
        int r = db.update("kw_message", cv, "message_id=?",
                new String[]{message_id});
        return r;
    }

    public int setGoogleBillingUploadedBy_did(String device_id, String order_id) {
        SQLiteDatabase db = getDb();
        ContentValues cv = new ContentValues();
        cv.put("isUpload", 1);
        int r = db.update("g_googleBilling", cv, "device_id=? and orderId=?",
                new String[]{device_id, order_id});
        return r;
    }

    public int deleteMessageBy_mid(String message_id) {
        SQLiteDatabase db = getDb();
        int r = db.delete("kw_message", "message_id = ?",
                new String[]{message_id});
        return r;
    }

    public int deleteSafezoneBy_uid_fid(String uid, String family_id) {
        SQLiteDatabase db = getDb();
        int r = db.delete("kw_safezone", "uid = ? and family_id=?",
                new String[]{uid, family_id});
        return r;
    }

    public int deleteMember_family_By_uid(String uid) {
        SQLiteDatabase db = getDb();
        int r = db.delete("kw_member_family", "uid = ?", new String[]{uid});
        return r;
    }

    // 鍒犻櫎涓�釜鏈堝墠鐨勬秷鎭�
    public int deleteGcm_message() {
        SQLiteDatabase db = getDb();
        long time_30day_before = System.currentTimeMillis() / 1000 - 30 * 24
                * 60 * 60;
        int r = db.delete("gcm_message", "ltime < ?",
                new String[]{String.valueOf(time_30day_before)});
        CustomLog.d(TAG,"delete rows=" + r);
        return r;
    }

    public int clearMessage(String uid) {
        SQLiteDatabase db = getDb();
        int r = db.delete("kw_message", "uid = ?", new String[]{uid});
        CustomLog.d(TAG,"delete rows=" + r);
        return r;
    }

    private String getBt_addr_ex(String bt_addr) {
        if (bt_addr != null && bt_addr.length() == 12) {
            String bleAdd = bt_addr.toUpperCase();
            String b1 = bleAdd.substring(0, 2);
            String b2 = bleAdd.substring(2, 4);
            String b3 = bleAdd.substring(4, 6);
            String b4 = bleAdd.substring(6, 8);
            String b5 = bleAdd.substring(8, 10);
            String b6 = bleAdd.substring(10, 12);

            String bleAddx = String.format("%s:%s:%s:%s:%s:%s", b1, b2, b3, b4,
                    b5, b6);
            return bleAddx;
            // F8:35:DD:85:6B:78
        } else {
            return "";
        }
    }

    private String saveBbImg(String family_id, String base64_img) {
        // Common.logDebugTxt(base64_img);
        String filepath = String.format("%sbb_%s.jpg",
                KidsWatConfig.getTempFilePath(), family_id);
        try {
            Base64Encode.decoderBase64File(base64_img, filepath);
            return filepath;
        } catch (FileNotFoundException e) {
            CustomLog.logException(TAG,e);

        } catch (IOException e) {
            CustomLog.logException(TAG,e);

        } catch (Exception e) {
            // TODO Auto-generated catch block
            CustomLog.logException(TAG,e);
            }
        return "";
    }
    // --------
}
