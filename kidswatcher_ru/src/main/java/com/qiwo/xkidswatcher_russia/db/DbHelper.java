package com.qiwo.xkidswatcher_russia.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;
import net.intari.CustomLogger.CustomLog;

public class DbHelper extends SQLiteOpenHelper {

    final String TAG = DbHelper.class.getSimpleName();

    private static final int VERSION = 8;
    private static final String DBNAME = "xkidswatcher.db";
    private Context cx;

    private DbHelper(Context context, String name, CursorFactory factory,
                     int version) {
        // 必须通过super调用父类当中的构造函数
        super(context, name, factory, version);
        cx = context;
    }

    private DbHelper(Context context, String name, int version) {
        this(context, name, null, version);
    }

    public DbHelper(Context context) {
        this(context, DBNAME, VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        initDb(db);
    }

    private void initDb(SQLiteDatabase db) {
        CustomLog.d(TAG,"create a Database");
        db.beginTransaction();
        try {
            // StringBuffer m_sql = new StringBuffer();
            db.execSQL(makeSqlFor___createTable_kw_family());
            db.execSQL(makeSqlFor___createTable_kw_member());
            db.execSQL(makeSqlFor___createTable_kw_member_family());
            db.execSQL(makeSqlFor___createTable_kw_message());
            db.execSQL(makeSqlFor___createTable_kw_safezone());
            db.execSQL(makeSqlFor___createTable_gcm_message());
            db.execSQL(makeSqlFor___createTable_g_googleBilling());
            db.setTransactionSuccessful();

            CustomLog.d(TAG,"create db ok.");
        } catch (Exception ex) {
            CustomLog.d(TAG,ex.toString());
            CustomLog.logException(TAG,ex);
        } finally {
            db.endTransaction();
        }
    }

    // SQLite的规范，主键以“_id”为标准
    // -----家庭圈,宝宝,手表------
    private String makeSqlFor___createTable_kw_family() {
        StringBuilder sql = new StringBuilder();
        // ---table----
        sql.append("CREATE TABLE [kw_family] (");
        // -----filed------
        sql.append("[_id] INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,");
        sql.append("[family_id] NVARCHAR(20) NULL,");
        sql.append("[device_id] NVARCHAR(20) NULL,");
        sql.append("[device_iccid] NVARCHAR(20) NULL,");
        sql.append("[create_uid] NVARCHAR(20) NULL,");
        sql.append("[base64_img] TEXT NULL,");
        sql.append("[img_path] NVARCHAR(20) NULL,");
        sql.append("[nickname] NVARCHAR(20) NULL,");
        sql.append("[birthday] INTEGER NULL,");
        sql.append("[birthdayText] NVARCHAR(20) NULL,");
        sql.append("[sex] INTEGER NULL,");
        sql.append("[sexText] NVARCHAR(20) NULL,");
        sql.append("[height] REAL NULL,");
        sql.append("[weight] REAL NULL,");
        sql.append("[grade] INTEGER NULL,");
        sql.append("[gradeText] NVARCHAR(20) NULL,");

        sql.append("[sim] NVARCHAR(20) NULL,");
        sql.append("[qrcode] NVARCHAR(20) NULL,");
        sql.append("[verify] NVARCHAR(20) NULL,");
        sql.append("[hard_code] NVARCHAR(20) NULL,");
        sql.append("[device_key] NVARCHAR(20) NULL,");
        sql.append("[bt_addr] NVARCHAR(20) NULL,");
        sql.append("[bt_addr_ex] NVARCHAR(20) NULL,");
        sql.append("[active_time] TIMESTAMP NULL,");
        sql.append("[bt_addr_pwd] NVARCHAR(20) NULL,");
        sql.append("[valid_date] NVARCHAR(20) NULL,");
        sql.append("[version] NVARCHAR(20) NULL,");
        sql.append("[bb_imgpath] NVARCHAR(20) NULL,");

        sql.append("[d_mode] INTEGER NULL,");// 1正常模式,2飞行模式
        // ----------最后一次录音no---------
        sql.append("[last_record_no] NVARCHAR(20) NULL,");
        // ----last filed-----
        // sql.append("[updateTime] TIMESTAMP NULL,");
        // sql.append("[addTime] TIMESTAMP NULL);");
        sql.append("[updateTime] NVARCHAR(20) NULL,");
        sql.append("[addTime] NVARCHAR(20) NULL);");

        return sql.toString();
    }

    // ---家庭成员(登陆用户)---
    private String makeSqlFor___createTable_kw_member() {
        StringBuilder sql = new StringBuilder();
        // ---table----
        sql.append("CREATE TABLE [kw_member] (");
        // -----filed------
        sql.append("[_id] INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,");
        sql.append("[name] NVARCHAR(20) NULL,");
        sql.append("[country_code] NVARCHAR(12) NULL,");
        sql.append("[phone] NVARCHAR(20) NULL,");
        sql.append("[reg_time] TIMESTAMP NULL,");
        sql.append("[uid] NVARCHAR(20) NULL,");
        sql.append("[push_token] NVARCHAR(100) NULL,");
        sql.append("[start_time] INTEGER NULL,");
        sql.append("[end_time] INTEGER NULL,");
        // ----和家庭圈有关,基本没用到,可考虑移除这些字段----
        sql.append("[family_id] NVARCHAR(20) NULL,");
        sql.append("[admin] INTEGER NULL,");
        sql.append("[relation] NVARCHAR(20) NULL,");
        // ----last filed-----
        sql.append("[updateTime] NVARCHAR(20) NULL,");
        sql.append("[addTime] NVARCHAR(20) NULL);");

        // // ------新加入字段---------------
        // sql.append("[datatype] NVARCHAR(20) NULL,");
        // sql.append("[group] NVARCHAR(20) NULL);");
        // sql.append("[nickname] NVARCHAR(20) NULL,");
        // sql.append("[birthday] NVARCHAR(20) NULL);");
        // sql.append("[sex] NVARCHAR(20) NULL,");
        // sql.append("[height] NVARCHAR(20) NULL);");
        // sql.append("[weight] NVARCHAR(20) NULL,");
        // sql.append("[grade] NVARCHAR(20) NULL);");
        // sql.append("[doc_type] NVARCHAR(20) NULL);");
        // sql.append("[img] NVARCHAR(20) NULL);");

        return sql.toString();
    }

    // ---登陆用户和家庭圈对应关系---
    private String makeSqlFor___createTable_kw_member_family() {
        StringBuilder sql = new StringBuilder();
        // ---table----
        sql.append("CREATE TABLE [kw_member_family] (");
        // -----filed------
        sql.append("[_id] INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,");

        sql.append("[uid] NVARCHAR(20) NULL,");
        sql.append("[country_code] NVARCHAR(10) NULL,");
        sql.append("[phone] NVARCHAR(20) NULL,");

        sql.append("[family_id] NVARCHAR(20) NULL,");
        sql.append("[isAdmin] INTEGER NULL,");
        sql.append("[relation] NVARCHAR(40) NULL,");

        // ----last filed-----
        sql.append("[updateTime] NVARCHAR(20) NULL,");
        sql.append("[addTime] NVARCHAR(20) NULL);");

        return sql.toString();
    }

    // ---消息---
    private String makeSqlFor___createTable_kw_message() {
        StringBuilder sql = new StringBuilder();
        // ---table----
        sql.append("CREATE TABLE [kw_message] (");
        // -----filed------
        sql.append("[_id] INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,");
        sql.append("[uid] NVARCHAR(20) NULL,");
        sql.append("[family_id] NVARCHAR(20) NULL,");
        sql.append("[message_id] NVARCHAR(200) NULL,");
        sql.append("[content_type] INTEGER NULL,");
        sql.append("[content_type_text] NVARCHAR(40) NULL,");
        sql.append("[time] TIMESTAMP NULL,");
        sql.append("[content] TEXT NULL,");
        sql.append("[isRead] INTEGER NULL,");// 0未读,1已读

        sql.append("[jsonText] TEXT NULL,");

        // ----last filed-----
        sql.append("[updateTime] NVARCHAR(20) NULL,");
        sql.append("[addTime] NVARCHAR(20) NULL);");

        return sql.toString();
    }

    // ---安全区域---
    private String makeSqlFor___createTable_kw_safezone() {
        StringBuilder sql = new StringBuilder();
        // ---table----
        sql.append("CREATE TABLE [kw_safezone] (");
        // -----filed------
        sql.append("[_id] INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,");
        sql.append("[uid] NVARCHAR(20) NULL,");
        sql.append("[family_id] NVARCHAR(20) NULL,");
        sql.append("[latitude] REAL NULL,");
        sql.append("[longitude] REAL NULL,");
        sql.append("[radius] INTEGER NULL,");
        sql.append("[name] NVARCHAR(40) NULL,");
        sql.append("[address] NVARCHAR(300) NULL,");
        sql.append("[create_user] NVARCHAR(40) NULL,");

        sql.append("[jsonText] TEXT NULL,");

        // ----last filed-----
        sql.append("[updateTime] NVARCHAR(20) NULL,");
        sql.append("[addTime] NVARCHAR(20) NULL);");

        return sql.toString();
    }

    // ---本地保存的从google推送过来的消息记录---
    private String makeSqlFor___createTable_gcm_message() {
        StringBuilder sql = new StringBuilder();
        // ---table----
        sql.append("CREATE TABLE [gcm_message] (");
        // -----filed------
        sql.append("[_id] INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,");
        sql.append("[title] NVARCHAR(40) NULL,");
        sql.append("[content] TEXT NULL,");
        sql.append("[bundleText]  TEXT NULL,");
        // ----last filed-----
        sql.append("[ltime] INTEGER,");// 记录时间,用于删除用
        sql.append("[addTime] NVARCHAR(20) NULL);");

        return sql.toString();
    }

    // ---本地保存的google支付记录---
    private String makeSqlFor___createTable_g_googleBilling() {
        StringBuilder sql = new StringBuilder();
        // ---table----
        sql.append("CREATE TABLE [g_googleBilling] (");
        // -----filed------
        sql.append("[_id] INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,");
        sql.append("[device_id] NVARCHAR(40) NULL,");
        sql.append("[family_id] NVARCHAR(40) NULL,");
        sql.append("[price] NVARCHAR(40) NULL,");
        sql.append("[orderId]  TEXT NULL,");
        sql.append("[jsonText] TEXT NULL,");
        // ----------
        sql.append("[policy_sequence] NVARCHAR(40) NULL,");

        // ---订单是否已上传,1为已上传,0为未上传--
        sql.append("[isUpload]  INTEGER NOT NULL,");
        // ----last filed-----
        sql.append("[updateTime] NVARCHAR(20) NULL,");
        sql.append("[addTime] NVARCHAR(20) NULL);");

        return sql.toString();
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        CustomLog.d(TAG,"update Database");

        boolean b = cx.deleteDatabase(DBNAME);
        CustomLog.d(TAG,"delete db:" + b);
        initDb(db);
        CustomLog.d(TAG,String.format(
                "db %s->%s====delete db and create table====", oldVersion,
                newVersion));
    }

}
