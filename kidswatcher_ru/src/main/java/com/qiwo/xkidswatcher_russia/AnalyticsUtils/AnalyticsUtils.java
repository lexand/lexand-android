package com.qiwo.xkidswatcher_russia.AnalyticsUtils;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.os.Looper;

//import com.appsee.Appsee;
import com.amplitude.api.Amplitude;
import com.amplitude.api.Identify;
import com.qiwo.xkidswatcher_russia.AnalyticsUtils.observers.AnalyticsActivityLifecycleObserver;
import com.qiwo.xkidswatcher_russia.Constants;
import com.qiwo.xkidswatcher_russia.bean.beanForDb___LoginMember;
import com.qiwo.xkidswatcher_russia.bean.beanFor___login;
import com.yandex.metrica.YandexMetrica;

import net.intari.CustomLogger.CustomLog;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * (c) Dmitriy Kazimirov, 2018-2019
 * e-mail:dmitriy.kazimirov@viorsan.com
 */

public class AnalyticsUtils {
    public static final String TAG = AnalyticsUtils.class.getSimpleName();


    public  enum ReportLifecycleAs {
        JUST_EVENT,
        PREFIXED_SHORT_CLASS_NAME,
        PREFIXED_FULL_CLASS_NAME,
    }


    private static  Map<String, Object> superAttributes = new HashMap<>();
    private static boolean analytics_YandexMetricaActive = false;
    private static boolean analytics_AppseeActive = false;
    private static boolean analytics_UXCamActive = false;
    private static boolean analytics_AmplitudeActive = false;


    /**
     * Adds 'super attribute' to be sent with each report (or replace one if it's exist)
     * @param key attribute name
     * @param obj attribute value
     */
    public static void addAnalyticsSuperAttribute(String key,Object obj) {
        superAttributes.put(key,obj);

    }

    /**
     * Enable Yandex App Metrica for reportAnalyticsEvent.
     * It's up to client to perform actual initialization and provide keys
     * See
     * https://tech.yandex.ru/appmetrica/doc/mobile-sdk-dg/concepts/android-initialize-docpage/ or
     * @param activate
     */
    public static void activateYandexMetrica(boolean activate) {
        analytics_YandexMetricaActive=activate;
    }

    /**
     * Init Amplitude and enable foreground tracking
     * https://developers.amplitude.com/?java#installation
     * Call this one and call activateAmplitude(true) after it
     * @param context
     * @param apiKey
     * @param application
     */
    public static void initAmplitude(Context context, String apiKey, Application application) {
        Amplitude.getInstance().trackSessionEvents(true);
        Amplitude.getInstance().initialize(context,apiKey)
                .enableForegroundTracking(application);
    }
    /**
     * Enable Amplitude for reportAnalyticsEvent.
     * It's up to client to perform actual initialization and provide keys
     * See https://amplitude.zendesk.com/hc/en-us/articles/115002935588#installation
     * @param activate
     */
    public static void activateAmplitude(boolean activate) {
        analytics_AmplitudeActive=activate;
    }


    /**
     * Enable Appsee  for reportAnalyticsEvent.
     * It's up to client to perform actual initialization and provide keys
     * @param activate
     */
    public static void activateAppsee(boolean activate) {
        analytics_AppseeActive=activate;
    }

    /**
     * Enable UXCam  for reportAnalyticsEvent.
     * It's up to client to perform actual initialization and provide keys
     * @param activate
     */
    public static void activateUXCam(boolean activate) {
        analytics_UXCamActive=activate;
    }

    public static void reportThrowable(String message,Throwable throwable) {
        if (analytics_YandexMetricaActive) {
            YandexMetrica.reportError(message,throwable);
        }
        Map<String,Object> eventProperties=new HashMap<>();
        eventProperties.put(Constants.MESSAGE,throwable.getMessage());
        /* if (analytics_AppseeActive) {
            Appsee.addEvent(Constants.EVENT_EXCEPTION,eventProperties);
        } */
    }
    /**
     * Report event to analytics
     * It's assumed that analytics libs are initialized
     * @param event event name
     * @param eventAttributes - Map<String, Object> attributes to send with event
     *
     */
    public static void reportAnalyticsEvent(String event, Map<String, Object> eventAttributes) {
        //add super attributes
        Map<String, Object> attributes=new HashMap<>();
        if (eventAttributes!=null) {
            attributes.putAll(eventAttributes);
        }
        attributes.putAll(superAttributes);

        //pre-process attributes to avoid metrica crash

        //send event to  Yandex.Metrica
        if (analytics_YandexMetricaActive) {
            YandexMetrica.reportEvent(event,attributes);
        }
        /*
        if (analytics_AppseeActive) {
            Appsee.addEvent(event,attributes);
        } */
        if (analytics_UXCamActive) {
            //UXCam.logEvent(event,attributes);
        }
        //Convert attributes for other analytics systems

        try {
            JSONObject props = new JSONObject();
            for (String key:attributes.keySet()) {
                props.put(key,attributes.get(key));
            }

            if (analytics_AmplitudeActive) {
                Amplitude.getInstance().logEvent(event,props);
            }

        }  catch (JSONException e) {
            CustomLog.logException(e);
        }

        //TODO: also write to (encrypted) log file
    }

    public static  void analytics_setUserInfo(String tag, String phone) {
        CustomLog.i(tag,"Setting user info: phone:"+phone);
        /*
        if (analytics_AppseeActive) {
            Appsee.setUserId(loginMember.phone);
        }
        */
        if (analytics_YandexMetricaActive) {
            YandexMetrica.setUserProfileID(phone);
        }
        if (analytics_UXCamActive) {
            //UXCam.setUserIdentity(phone);
        }
        if (analytics_AmplitudeActive) {
            setUserIdAmplitude(phone);
        }

    }

    public static  void analytics_setUserInfo(String tag, beanForDb___LoginMember loginMember) {
        CustomLog.i(tag,"Setting user info:uid="+loginMember.uid+", phone:"+loginMember.phone);
        /*
        if (analytics_AppseeActive) {
            Appsee.setUserId(loginMember.phone);
        }
        */
        if (analytics_YandexMetricaActive) {
            YandexMetrica.setUserProfileID(loginMember.phone);
        }
        if (analytics_AmplitudeActive) {
            setUserIdAmplitude(loginMember.phone);
        }
        if (analytics_UXCamActive) {
            //UXCam.setUserIdentity(loginMember.phone);
        }

    }

    /**
     *
     * @param identityAmplitude
     */
    public static void setUserIdentityAmplitude(Identify identityAmplitude) {
        if (analytics_AmplitudeActive) {
            try {
                Amplitude.getInstance().identify(identityAmplitude);
            } catch (Exception e) {
                CustomLog.logException(e);
            }
        }

    }
    /**
     * Sets Amplitude user Id
     * @param userIdAmplitude
     */
    public static void setUserIdAmplitude(String userIdAmplitude) {
        if (analytics_AmplitudeActive) {
            try {
                Amplitude.getInstance().setUserId(userIdAmplitude);
            } catch (Exception e) {
                CustomLog.logException(e);
            }
        }

    }
    /**
     * Report custom user properties to Amplitude
     * <code>
     *     JSONObject songProperties = new JSONObject();
     *      try {
     *            eventProperties.put("title", "Here comes the Sun");
     *            eventProperties.put("artist", "The Beatles");
     *            eventProperties.put("genre", "Rock");
     *          } catch (JSONException exception) {
     *       }
     * </code>
     * @param properties
     */
    public static void reportUserPropertiesAmplitude(JSONObject properties) {
        if (analytics_AmplitudeActive) {
            try {
                Amplitude.getInstance().setUserProperties(properties);
            } catch (Exception e) {
                CustomLog.logException(e);
            }
        }
    }

    /**
     * Reports revenue to Amplitude
     * @param revenue
     */
    public static  void reportRevenueAmplitude(com.amplitude.api.Revenue revenue) {
        if (analytics_AmplitudeActive) {
            try {
                Amplitude.getInstance().logRevenueV2(revenue);
            } catch (Exception e) {
                CustomLog.logException(e);
            }
        }
    }

    /**
     * Reports revenu to Yandex
     * @param yandexRev
     */
    public static void reportRevenuYandex(com.yandex.metrica.Revenue yandexRev) {
        if (analytics_YandexMetricaActive) {
            try {
                YandexMetrica.reportRevenue(yandexRev);

            } catch (Exception e) {
                CustomLog.logException(e);
            }
        }
    }
    public static  void analytics_setUserInfo(String tag, beanFor___login.CUser user) {
        CustomLog.i(TAG+" for:"+tag,"Setting user info:uid="+user.uid+", phone:"+user.phone);
        /*
        if (analytics_AppseeActive) {
            Appsee.setUserId(user.phone);
        }
        */
        if (analytics_YandexMetricaActive) {
            YandexMetrica.setUserProfileID(user.phone);
        }
        if (analytics_AmplitudeActive) {
            setUserIdAmplitude(user.phone);
        }
        if (analytics_UXCamActive) {
            //UXCam.setUserIdentity(user.phone);
        }
    }

    //TODO:Check if it's ok to use regular lifecycle listeners for this (instead of manual)
    public static void processActivityOnCreate(Activity activity) {
        String t=TAG+" for:"+activity.getClass().getSimpleName();
        CustomLog.i(t,"Processing create lifecycle event for "+activity.getClass().getSimpleName());
        /*
        if (Constants.APPSEE_GLOBAL_ENABLE) {
            CustomLog.i(t,"Will use Appsee");
            //Appsee.start();
            Appsee.start(Constants.APPSEE_API_KEY);
        }
        */
        /*
        if (Constants.UXCAM_GLOBAL_ENABLE) {
            CustomLog.i(t,"Will use UXCam");
            UXCam.startWithKey(Constants.UXCAM_API_KEY);
        }
        */
    }


    /**
     * Is this UI Thread?
     * @return
     */
    public static boolean isUiThread()
    {
        return Looper.getMainLooper().equals(Looper.myLooper());
    }


    private static int resumed;
    private static int paused;
    private static int started;
    private static int stopped;

    public static boolean isApplicationVisible() {
        return started > stopped;
    }

    public static boolean isApplicationInForeground() {
        return resumed > paused;
    }


    private static AnalyticsActivityLifecycleObserver analyticsActivityLifecycleObserver;

    private static Application internalApp;
    private static boolean reportLifecycleEventsForAnalytics=false;



    /**
     * Show we report only start events and not stop events?
     * @param reportLifecycleEventsForAnalytics_OnlyStart
     */
    public static void setReportLifecycleEventsForAnalytics_OnlyStart(boolean reportLifecycleEventsForAnalytics_OnlyStart) {
        reportLifecycleEventsForAnalytics_OnlyStart = reportLifecycleEventsForAnalytics_OnlyStart;
    }

    public static boolean isReportLifecycleEventsForAnalytics_OnlyStart() {
        return reportLifecycleEventsForAnalytics_OnlyStart;
    }

    private static boolean reportLifecycleEventsForAnalytics_OnlyStart=false;

    private static ReportLifecycleAs reportLifecycleForAnalyticsAs= ReportLifecycleAs.JUST_EVENT;

    /**
     * How to name analytics events ?
     * it's not very good idea to change this from JUST_EVENT
     * @param reportLifecycleAs
     */
    public static void setReportLifecycleForAnalyticsAs(ReportLifecycleAs reportLifecycleAs) {
        reportLifecycleForAnalyticsAs=reportLifecycleAs;
    }

    /**
     * How analytics events are named
     * @return
     */
    public static ReportLifecycleAs getReportLifecycleForAnalyticsAs() {
        return reportLifecycleForAnalyticsAs;
    }

    /**
     * Should observers report lifecycle events to analytics?
     * @param report
     */
    public static void setReportLifecycleEventsForAnalytics(boolean report) {
        reportLifecycleEventsForAnalytics=report;
    }

    public static boolean isReportLifecycleEventsForAnalytics() {
        return reportLifecycleEventsForAnalytics;
    }

    /**
     * Inits lifecycle handlers
     * @param app
     */
    public static void initLifecycleObservers(Application app) {
        if (analyticsActivityLifecycleObserver == null) {
            analyticsActivityLifecycleObserver = new AnalyticsActivityLifecycleObserver();
            app.registerActivityLifecycleCallbacks(analyticsActivityLifecycleObserver);
            internalApp=app;
        }
    }

    /**
     * Reverses effects of initLifecycleReporters
     */
    public static void unInitLifecyleListeners() {
        if ((internalApp!=null) && (analyticsActivityLifecycleObserver !=null)) {
            internalApp.unregisterActivityLifecycleCallbacks(analyticsActivityLifecycleObserver);
        }
    }

    public static final String getAppSeeSessionURL(String sessionId,String appseeName) {
        //5c5a810b15810d0900a7edfa
        //https://dashboard.appsee.com/home/kidswatch-test#/Videos/Index/5c5a810b15810d0900a7edfa#android/all/month/phone

        StringBuilder stringBuilder=new StringBuilder();
        stringBuilder.append("https://dashboard.appsee.com/home/");
        stringBuilder.append(appseeName);
        stringBuilder.append("#/Videos/Index/");
        stringBuilder.append(sessionId);
        stringBuilder.append("#android/all/month/phone");

        return stringBuilder.toString();
    }
}
