package com.qiwo.xkidswatcher_russia.AnalyticsUtils.observers;

/**
 * (c) Dmitriy Kazimirov, 2018-2019
 * e-mail:dmitriy.kazimirov@viorsan.com
 */


import android.app.Activity;
import android.app.Application;
import android.os.Bundle;

import com.qiwo.xkidswatcher_russia.AnalyticsUtils.AnalyticsUtils;
import com.yandex.metrica.YandexMetrica;

import net.intari.AndroidToolboxNanoCore.Constants;

import java.util.HashMap;
import java.util.Map;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;


public class AnalyticsActivityLifecycleObserver implements Application.ActivityLifecycleCallbacks {
    public static final String TAG = AnalyticsActivityLifecycleObserver.class.getSimpleName();

    private static int resumed;
    private static int paused;
    private static int started;
    private static int stopped;

    private long actStarted=0L;
    private long actResumed=0L;


    public static final String EVENT_ACTIVITY_PAUSED="activityPaused";
    public static final String EVENT_ACTIVITY_RESUMED="activityResumed";
    public static final String EVENT_ATTRIBUTE_ACTIVITY_FULL_NAME="activityFullName";
    public static final String EVENT_ATTRIBUTE_ACTIVITY_NAME="activity";
    public static final String EVENT_ATTRIBUTE_TIME_PASSED="timePassedInSeconds";


    @Override
    public void onActivityCreated(Activity activity, Bundle savedInstanceState) {

        if (activity instanceof AppCompatActivity) {
            FragmentManager fragmentManager=((AppCompatActivity) activity).getSupportFragmentManager();
            fragmentManager.registerFragmentLifecycleCallbacks(new AnalyticsFragmentLifecycleObserver(),true);
        } else {
            //not appcompat - no supportFragmentManager
        }
        //send to analytics for special processing
        AnalyticsUtils.processActivityOnCreate(activity);
    }

    @Override
    public void onActivityDestroyed(Activity activity) {
    }

    @Override
    public void onActivityResumed(Activity activity) {

        ++resumed;
        actResumed = System.currentTimeMillis();
        if (AnalyticsUtils.isReportLifecycleEventsForAnalytics()) {
            Map<String, Object> eventAttributes=new HashMap<String, Object>();
            eventAttributes.put(EVENT_ATTRIBUTE_ACTIVITY_NAME,activity.getClass().getSimpleName());
            eventAttributes.put(EVENT_ATTRIBUTE_ACTIVITY_FULL_NAME,activity.getClass().getCanonicalName());
            switch (AnalyticsUtils.getReportLifecycleForAnalyticsAs()) {
                case JUST_EVENT:
                    AnalyticsUtils.reportAnalyticsEvent(EVENT_ACTIVITY_RESUMED, eventAttributes);
                    break;
                case PREFIXED_FULL_CLASS_NAME:
                    AnalyticsUtils.reportAnalyticsEvent(activity.getClass().getCanonicalName() + " resumed", eventAttributes);
                    break;
                case PREFIXED_SHORT_CLASS_NAME:
                    AnalyticsUtils.reportAnalyticsEvent(activity.getClass().getSimpleName() + " resumed", eventAttributes);
                    break;
            }

        }
    }

    @Override
    public void onActivityPaused(Activity activity) {

        ++paused;
        long stopTime= System.currentTimeMillis();
        long timePassed=(stopTime-actResumed)/ Constants.MS_PER_SECOND;
        if (AnalyticsUtils.isReportLifecycleEventsForAnalytics_OnlyStart()) {
            return;
        }
        if (AnalyticsUtils.isReportLifecycleEventsForAnalytics()) {
            Map<String, Object> eventAttributes=new HashMap<String, Object>();
            eventAttributes.put(EVENT_ATTRIBUTE_ACTIVITY_NAME,activity.getClass().getSimpleName());
            eventAttributes.put(EVENT_ATTRIBUTE_ACTIVITY_FULL_NAME,activity.getClass().getCanonicalName());
            eventAttributes.put(EVENT_ATTRIBUTE_TIME_PASSED,timePassed);

            switch (AnalyticsUtils.getReportLifecycleForAnalyticsAs()) {
                case JUST_EVENT:
                    AnalyticsUtils.reportAnalyticsEvent(EVENT_ACTIVITY_PAUSED, eventAttributes);
                    break;
                case PREFIXED_FULL_CLASS_NAME:
                    AnalyticsUtils.reportAnalyticsEvent(activity.getClass().getCanonicalName() + " paused", eventAttributes);
                    break;
                case PREFIXED_SHORT_CLASS_NAME:
                    AnalyticsUtils.reportAnalyticsEvent(activity.getClass().getSimpleName() + " paused", eventAttributes);
                    break;
            }

        }

        //YandexMetrica.sendEventsBuffer();

    }

    @Override
    public void onActivitySaveInstanceState(Activity activity, Bundle outState) {

    }

    @Override
    public void onActivityStarted(Activity activity) {
        ++started;
        actStarted =  System.currentTimeMillis();

    }

    @Override
    public void onActivityStopped(Activity activity) {
        ++stopped;
        long stopTime= System.currentTimeMillis();
        long timePassed=(stopTime-actStarted)/ Constants.MS_PER_SECOND;

    }

    public static boolean isApplicationVisible() {
        return started > stopped;
    }

    public static boolean isApplicationInForeground() {
        return resumed > paused;
    }


}