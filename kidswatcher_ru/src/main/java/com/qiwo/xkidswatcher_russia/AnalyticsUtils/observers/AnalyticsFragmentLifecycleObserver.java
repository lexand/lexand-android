package com.qiwo.xkidswatcher_russia.AnalyticsUtils.observers;
/**
 * (c) Dmitriy Kazimirov, 2018-2019
 * e-mail:dmitriy.kazimirov@viorsan.com
 */


import android.content.Context;
import android.os.Bundle;
import android.view.View;

import com.qiwo.xkidswatcher_russia.AnalyticsUtils.AnalyticsUtils;

import net.intari.AndroidToolboxNanoCore.Constants;

import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

/**
 * TODO:it IS possible to have mor than one fragment on screen. resume/paused couters don't make sense (until they are really per-fragment)
 */
public class AnalyticsFragmentLifecycleObserver extends FragmentManager.FragmentLifecycleCallbacks {
    public static final String TAG = AnalyticsFragmentLifecycleObserver.class.getSimpleName();



    public static final String EVENT_FRAGMENT_PAUSED="fragmentPaused";
    public static final String EVENT_FRAGMENT_RESUMED="fragmentResumed";
    public static final String EVENT_ATTRIBUTE_FRAGMENT_FULL_NAME="fragmentFullName";
    public static final String EVENT_ATTRIBUTE_FRAGMENT_NAME="fragment";
    public static final String EVENT_ATTRIBUTE_TIME_PASSED="timePassedInSeconds";

    private static int resumed;
    private static int paused;
    private static int started;
    private static int stopped;

    private Map<Fragment,Long> fragmentStartedMap=new HashMap<>();
    private Map<Fragment,Long> fragmentResumedMap=new HashMap<>();

    /**
     * Called right before the fragment's {@link Fragment#onAttach(Context)} method is called.
     * This is a good time to inject any required dependencies or perform other configuration
     * for the fragment before any of the fragment's lifecycle methods are invoked.
     *
     * @param fm      Host FragmentManager
     * @param f       Fragment changing state
     * @param context Context that the Fragment is being attached to
     */
    @Override
    public void onFragmentPreAttached(FragmentManager fm, Fragment f, Context context) {
        super.onFragmentPreAttached(fm, f, context);

    }

    /**
     * Called after the fragment has been attached to its host. Its host will have had
     * <code>onAttachFragment</code> called before this call happens.
     *
     * @param fm      Host FragmentManager
     * @param f       Fragment changing state
     * @param context Context that the Fragment was attached to
     */
    @Override
    public void onFragmentAttached(FragmentManager fm, Fragment f, Context context) {
        super.onFragmentAttached(fm, f, context);
    }

    /**
     * Called right before the fragment's {@link Fragment#onCreate(Bundle)} method is called.
     * This is a good time to inject any required dependencies or perform other configuration
     * for the fragment.
     *
     * @param fm                 Host FragmentManager
     * @param f                  Fragment changing state
     * @param savedInstanceState Saved instance bundle from a previous instance
     */
    @Override
    public void onFragmentPreCreated(FragmentManager fm, Fragment f, Bundle savedInstanceState) {
        super.onFragmentPreCreated(fm, f, savedInstanceState);

    }

    /**
     * Called after the fragment has returned from the FragmentManager's call to
     * {@link Fragment#onCreate(Bundle)}. This will only happen once for any given
     * fragment instance, though the fragment may be attached and detached multiple times.
     *
     * @param fm                 Host FragmentManager
     * @param f                  Fragment changing state
     * @param savedInstanceState Saved instance bundle from a previous instance
     */
    @Override
    public void onFragmentCreated(FragmentManager fm, Fragment f, Bundle savedInstanceState) {
        super.onFragmentCreated(fm, f, savedInstanceState);

    }

    /**
     * Called after the fragment has returned from the FragmentManager's call to
     * {@link Fragment#onActivityCreated(Bundle)}. This will only happen once for any given
     * fragment instance, though the fragment may be attached and detached multiple times.
     *
     * @param fm                 Host FragmentManager
     * @param f                  Fragment changing state
     * @param savedInstanceState Saved instance bundle from a previous instance
     */
    @Override
    public void onFragmentActivityCreated(FragmentManager fm, Fragment f, Bundle savedInstanceState) {
        super.onFragmentActivityCreated(fm, f, savedInstanceState);


    }

    /**
     * Called after the fragment has returned a non-null view from the FragmentManager's
     * request to {@link Fragment#onCreateView(LayoutInflater, ViewGroup, Bundle)}.
     *
     * @param fm                 Host FragmentManager
     * @param f                  Fragment that created and owns the view
     * @param v                  View returned by the fragment
     * @param savedInstanceState Saved instance bundle from a previous instance
     */
    @Override
    public void onFragmentViewCreated(FragmentManager fm, Fragment f, View v, Bundle savedInstanceState) {
        super.onFragmentViewCreated(fm, f, v, savedInstanceState);

    }

    /**
     * Called after the fragment has returned from the FragmentManager's call to
     * {@link Fragment#onStart()}.
     *
     * @param fm Host FragmentManager
     * @param f  Fragment changing state
     */
    @Override
    public void onFragmentStarted(FragmentManager fm, Fragment f) {
        super.onFragmentStarted(fm, f);
        //++started;
        fragmentStartedMap.put(f,System.currentTimeMillis());


    }

    /**
     * Called after the fragment has returned from the FragmentManager's call to
     * {@link Fragment#onResume()}.
     *
     * @param fm Host FragmentManager
     * @param f  Fragment changing state
     */
    @Override
    public void onFragmentResumed(FragmentManager fm, Fragment f) {

        super.onFragmentResumed(fm, f);
        //++resumed;

        fragmentResumedMap.put(f,System.currentTimeMillis());


        if (AnalyticsUtils.isReportLifecycleEventsForAnalytics()) {
            TreeMap<String, Object> eventAttributes=new TreeMap<String, Object>();
            eventAttributes.put(EVENT_ATTRIBUTE_FRAGMENT_NAME,f.getClass().getSimpleName());
            eventAttributes.put(EVENT_ATTRIBUTE_FRAGMENT_FULL_NAME,f.getClass().getCanonicalName());
            switch (AnalyticsUtils.getReportLifecycleForAnalyticsAs()) {
                case JUST_EVENT:
                    AnalyticsUtils.reportAnalyticsEvent(EVENT_FRAGMENT_RESUMED,eventAttributes);
                    break;
                case PREFIXED_FULL_CLASS_NAME:
                    AnalyticsUtils.reportAnalyticsEvent(f.getClass().getCanonicalName()+" resumed",eventAttributes);
                    break;
                case PREFIXED_SHORT_CLASS_NAME:
                    AnalyticsUtils.reportAnalyticsEvent(f.getClass().getSimpleName()+" resumed",eventAttributes);
                    break;
            }
        }


    }

    /**
     * Called after the fragment has returned from the FragmentManager's call to
     * {@link Fragment#onPause()}.
     *
     * @param fm Host FragmentManager
     * @param f  Fragment changing state
     */
    @Override
    public void onFragmentPaused(FragmentManager fm, Fragment f) {


        super.onFragmentPaused(fm, f);
        ++paused;
        long stopTime= System.currentTimeMillis();
        Long startTime=fragmentResumedMap.get(f);
        if (startTime==null) {
            startTime=0L;
        }
        long timePassed=(stopTime-startTime)/ Constants.MS_PER_SECOND;

        if (AnalyticsUtils.isReportLifecycleEventsForAnalytics_OnlyStart()) {
            return;
        }
        if (AnalyticsUtils.isReportLifecycleEventsForAnalytics()) {
            Map<String, Object> eventAttributes=new TreeMap<>();
            eventAttributes.put(EVENT_ATTRIBUTE_FRAGMENT_NAME,f.getClass().getSimpleName());
            eventAttributes.put(EVENT_ATTRIBUTE_FRAGMENT_FULL_NAME,f.getClass().getCanonicalName());
            eventAttributes.put(EVENT_ATTRIBUTE_TIME_PASSED,timePassed);
            switch (AnalyticsUtils.getReportLifecycleForAnalyticsAs()) {
                case JUST_EVENT:
                    AnalyticsUtils.reportAnalyticsEvent(EVENT_FRAGMENT_PAUSED, eventAttributes);
                    break;
                case PREFIXED_FULL_CLASS_NAME:
                    AnalyticsUtils.reportAnalyticsEvent(f.getClass().getCanonicalName() + " paused", eventAttributes);
                    break;
                case PREFIXED_SHORT_CLASS_NAME:
                    AnalyticsUtils.reportAnalyticsEvent(f.getClass().getSimpleName() + " paused", eventAttributes);
                    break;
            }
        }

    }

    /**
     * Called after the fragment has returned from the FragmentManager's call to
     * {@link Fragment#onStop()}.
     *
     * @param fm Host FragmentManager
     * @param f  Fragment changing state
     */
    @Override
    public void onFragmentStopped(FragmentManager fm, Fragment f) {
        super.onFragmentStopped(fm, f);
        ++stopped;
        long stopTime= System.currentTimeMillis();
        Long startTime=fragmentStartedMap.get(f);
        if (startTime==null) {
            startTime=0L;
        }

        long timePassed=(stopTime-startTime)/ Constants.MS_PER_SECOND;



    }

    /**
     * Called after the fragment has returned from the FragmentManager's call to
     * {@link Fragment#onSaveInstanceState(Bundle)}.
     *
     * @param fm       Host FragmentManager
     * @param f        Fragment changing state
     * @param outState Saved state bundle for the fragment
     */
    @Override
    public void onFragmentSaveInstanceState(FragmentManager fm, Fragment f, Bundle outState) {
        super.onFragmentSaveInstanceState(fm, f, outState);

    }

    /**
     * Called after the fragment has returned from the FragmentManager's call to
     * {@link Fragment#onDestroyView()}.
     *
     * @param fm Host FragmentManager
     * @param f  Fragment changing state
     */
    @Override
    public void onFragmentViewDestroyed(FragmentManager fm, Fragment f) {
        super.onFragmentViewDestroyed(fm, f);


    }

    /**
     * Called after the fragment has returned from the FragmentManager's call to
     * {@link Fragment#onDestroy()}.
     *
     * @param fm Host FragmentManager
     * @param f  Fragment changing state
     */
    @Override
    public void onFragmentDestroyed(FragmentManager fm, Fragment f) {
        super.onFragmentDestroyed(fm, f);


    }

    /**
     * Called after the fragment has returned from the FragmentManager's call to
     * {@link Fragment#onDetach()}.
     *
     * @param fm Host FragmentManager
     * @param f  Fragment changing state
     */
    @Override
    public void onFragmentDetached(FragmentManager fm, Fragment f) {
        super.onFragmentDetached(fm, f);

    }
}
