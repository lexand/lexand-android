package com.qiwo.xkidswatcher_russia;

import android.content.Context;

import com.google.common.util.concurrent.ListenableFuture;
import com.qiwo.xkidswatcher_russia.AnalyticsUtils.AnalyticsUtils;
import com.qiwo.xkidswatcher_russia.api.ApiHttpClient;
import com.qiwo.xkidswatcher_russia.api.remote.KidsWatApiUrl;
import com.qiwo.xkidswatcher_russia.bean.beanFor___get_watch_data;
import com.qiwo.xkidswatcher_russia.util.StringUtils;
import com.qiwo.xkidswatcher_russia.utils.NetworkUtils;

import net.intari.CustomLogger.CustomLog;

import org.json.JSONException;
import org.json.JSONObject;
import org.kymjs.kjframe.http.HttpCallBack;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executor;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import androidx.annotation.NonNull;
import androidx.concurrent.futures.ResolvableFuture;
import androidx.work.ListenableWorker;
import androidx.work.WorkerParameters;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

import static androidx.concurrent.futures.ResolvableFuture.create;
import static com.qiwo.xkidswatcher_russia.Constants.ADDRESS;
import static com.qiwo.xkidswatcher_russia.Constants.CHARGE;
import static com.qiwo.xkidswatcher_russia.Constants.DEVICE_ID;
import static com.qiwo.xkidswatcher_russia.Constants.ERREXCEPTION;
import static com.qiwo.xkidswatcher_russia.Constants.ERRORCODE;
import static com.qiwo.xkidswatcher_russia.Constants.EVENT_EXCEPTION;
import static com.qiwo.xkidswatcher_russia.Constants.EVENT_INTERNAL_GET_LOCATION_SUCCESS;
import static com.qiwo.xkidswatcher_russia.Constants.FAMILIYID;
import static com.qiwo.xkidswatcher_russia.Constants.GEOPOINT;
import static com.qiwo.xkidswatcher_russia.Constants.LOCATION;
import static com.qiwo.xkidswatcher_russia.Constants.LOCATION_TYPE;
import static com.qiwo.xkidswatcher_russia.Constants.LOG_INTERNAL_GET_LOCATION;
import static com.qiwo.xkidswatcher_russia.Constants.LTIME;
import static com.qiwo.xkidswatcher_russia.Constants.LTIME_FORMATTED;
import static com.qiwo.xkidswatcher_russia.Constants.TIME;
import static com.qiwo.xkidswatcher_russia.Constants.UID;
import static com.qiwo.xkidswatcher_russia.Constants.URL;

/**
 * Created by Dmitriy Kazimirov on 21/02/2019.
 */

public class LocationUpdateWorker extends ListenableWorker {
    public static final String TAG = AppContext.TAG+":"+LocationUpdateWorker.class.getSimpleName();

    /**
     * @param appContext   The application {@link Context}
     * @param workerParams Parameters to setup the internal state of this worker
     */
    public LocationUpdateWorker(@NonNull Context appContext, @NonNull WorkerParameters workerParams) {
        super(appContext, workerParams);
        CustomLog.i(TAG,"internal_get_location:Worker IS BEING CONSTRUCTED with params:"+workerParams.toString());
    }

    @NonNull
    @Override
    public ListenableFuture<Result> startWork() {
        ResolvableFuture<Result> future=ResolvableFuture.create();


        String uid = KidsWatConfig.getUserUid();
        String access_token = KidsWatConfig.getUserToken();
        String device_id = KidsWatConfig.getUserDeviceId();
        String family_id = KidsWatConfig.getDefaultFamilyId();

        // KidsWatApi.get_location_ex(uid, access_token, device_id, family_id,
        //use get_location_ex to get RECENT data
        final String request_url = KidsWatApiUrl.getUrlFor___get_location_ex(
                uid, access_token, device_id, family_id);

        CustomLog.d(TAG,"internal_get_location:Starting work...");
        Map<String,Object> eventProperties=new HashMap<>();
        eventProperties.put(URL,request_url);
        eventProperties.put(FAMILIYID,family_id);
        eventProperties.put(DEVICE_ID,device_id);
        eventProperties.put(UID,uid);

        if (LOG_INTERNAL_GET_LOCATION) {
            AnalyticsUtils.reportAnalyticsEvent(Constants.EVENT_INTERNAL_GET_LOCATION_START,eventProperties);
        }

        Request request=new Request.Builder()
                .url(request_url)
                .build();

        Call call = NetworkUtils.getOkHttpClient().newCall(request);
        call.enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                CustomLog.d(TAG,"internal_get_location:onFailure:" +e+", call:"+call.toString());
                CustomLog.logException(e);
                Map<String,Object> eventProperties=new HashMap<>();
                eventProperties.put(URL,call.request().url());
                eventProperties.put(ERREXCEPTION,e);
                if (LOG_INTERNAL_GET_LOCATION) {
                    AnalyticsUtils.reportAnalyticsEvent(Constants.EVENT_INTERNAL_GET_LOCATION_SERVER_ERROR,eventProperties);
                }

                future.set(Result.failure());

            }

            @Override
            public void onResponse(Call call, Response networkResponse) throws IOException {
                String t=networkResponse.body().string();
                CustomLog.d(TAG,String.format("internal_get_location:onResponse:url:%s\nt:%s", request_url, t));
                try {
                    JSONObject response = new JSONObject(t);

                    int code = response.getInt("error");

                    Map<String,Object> eventProperties=new HashMap<>();
                    eventProperties.put(URL,request_url);
                    eventProperties.put(FAMILIYID,family_id);
                    eventProperties.put(DEVICE_ID,device_id);
                    eventProperties.put(UID,uid);

                    eventProperties.put(URL,call.request().url());
                    eventProperties.put(ERRORCODE,code);

                    if (code == 0) {
                        //lnearLayout_audio_play.setVisibility(View.GONE);

                        AppContext.getInstance().setInternalLocationResponse(response);
                        long ltime = response.getJSONObject("data")
                                .getJSONObject("parameter")
                                .getInt("location_time");
                        String address = response.getJSONObject("data")
                                .getJSONObject("parameter")
                                .getString("address");
                        double longitude = response.getJSONObject("data")
                                .getJSONObject("parameter")
                                .getJSONObject("location")
                                .getDouble("longitude");
                        double latitude = response.getJSONObject("data")
                                .getJSONObject("parameter")
                                .getJSONObject("location")
                                .getDouble("latitude");
                        int electricity = response.getJSONObject("data")
                                .getJSONObject("parameter")
                                .getInt("electricity");
                        int location_type = response.getJSONObject("data")
                                .getJSONObject("parameter")
                                .getInt("location_type");// 1基站,0,gps

                        String ts=Utils.getTimeFromTimestamp(ltime);
                        eventProperties.put(LTIME_FORMATTED, ts);


                        // ----------------
                        String strdate = new java.text.SimpleDateFormat("HH:mm")
                                .format(new java.util.Date(ltime * 1000));
                        String otherText = String.format("%s",
                                location_type == 1 ? "GPS"
                                        : "GSM/WIFI");

                        String locationDetails=String.format("lat:%f, lon:%f, %s, charge:%d,ltime:%d, ltime_conv:%s",latitude,longitude,address,electricity,ltime,ts);
                        String geoPoint=String.format("lat:%f, lon:%f",latitude,longitude);
                        eventProperties.put(LOCATION_TYPE,otherText);
                        eventProperties.put(TIME,strdate);
                        eventProperties.put(CHARGE,electricity);
                        eventProperties.put(ADDRESS,address);
                        eventProperties.put(GEOPOINT,geoPoint);
                        eventProperties.put(LOCATION,locationDetails);
                        eventProperties.put(LTIME,ltime);



                        if (LOG_INTERNAL_GET_LOCATION) {
                            AnalyticsUtils.reportAnalyticsEvent(EVENT_INTERNAL_GET_LOCATION_SUCCESS,eventProperties);
                        }

                        //update address,etc

                        beanFor___get_watch_data.CRows row = new beanFor___get_watch_data.CRows();
                        row.address = address;
                        row.time = ltime;
                        row.latitude = latitude;
                        row.longitude = longitude;
                        row.electricity = electricity;
                        row.type = location_type;

                        beanFor___get_watch_data watch_data=AppContext.getInstance().getInternal_bean___watch_data();
                        if (watch_data!=null) {
                            try {
                                int rows = watch_data.info.ddata.rows.size();//// internal_bean___watch_data.info.ddata.rows.size();

                                if (rows > 0) {
                                    watch_data.info.ddata.rows.set(0, row);
                                    AppContext.getInstance().setInternal_bean___watch_data(watch_data);
                                }
                                //internal_setLastLocation(row);
                                //internal_showLastLocation();
                                future.set(Result.success());


                            } catch (Exception ex) {{
                                CustomLog.logException(TAG,ex);
                                future.set(Result.failure());
                            }}


                        }

                    } else if (code == 2015001) {

                        CustomLog.w(TAG,"internal_get_location:Sleep mode was active");

                        if (LOG_INTERNAL_GET_LOCATION) {
                            AnalyticsUtils.reportAnalyticsEvent(Constants.EVENT_INTERNAL_GET_LOCATION_ERROR_DEVICE_SLEEPS,eventProperties);
                        }

                        //showLongToast(getActivity().getApplicationContext().getResources().getString(R.string.tip_operate_sleep_mode));
                        future.set(Result.success());

                    } else if (code == -2) {
                        int electricity = response.getJSONObject("data")
                                .getJSONObject("parameter")
                                .getInt("electricity");

                        eventProperties.put(CHARGE,electricity);

                        if (LOG_INTERNAL_GET_LOCATION) {
                            AnalyticsUtils.reportAnalyticsEvent(Constants.EVENT_INTERNAL_GET_LOCATION_ERROR,eventProperties);
                        }

                        CustomLog.d(TAG,"internal_get_location:Electricity:"+electricity+"%");
                        future.set(Result.success());

                    } else {
                        if (LOG_INTERNAL_GET_LOCATION) {
                            AnalyticsUtils.reportAnalyticsEvent(Constants.EVENT_INTERNAL_GET_LOCATION_ERROR,eventProperties);
                        }
                        CustomLog.d(TAG,"internal_get_location:Can't get location, error code: "+code);
                        future.set(Result.failure());

                    }

                } catch (JSONException e) {
                    CustomLog.logException(TAG,e);
                    Map<String,Object> eventProperties=new HashMap<>();
                    eventProperties.put(URL,call.request().url());
                    eventProperties.put(ERREXCEPTION,e);
                    if (LOG_INTERNAL_GET_LOCATION) {
                        AnalyticsUtils.reportAnalyticsEvent(Constants.EVENT_INTERNAL_GET_LOCATION_EXCEPTION,eventProperties);
                    }

                    future.set(Result.failure());
                }
            }
        });

        /*
        ApiHttpClient.get(request_url, new HttpCallBack() {

            @Override
            public void onPreStart() {
                CustomLog.d(TAG,"internal_get_location:Pre-starting...");

            }

            @Override
            public void onSuccess(String t) {
                super.onSuccess(t);
                CustomLog.d(TAG,String.format("internal_get_location:onSuccess:url:%s\nt:%s", request_url, t));
                try {
                    JSONObject response = new JSONObject(t);

                    int code = response.getInt("error");

                    Map<String,Object> eventProperties=new HashMap<>();
                    eventProperties.put(URL,request_url);
                    eventProperties.put(FAMILIYID,family_id);
                    eventProperties.put(DEVICE_ID,device_id);
                    eventProperties.put(UID,uid);

                    eventProperties.put(Constants.URL,request_url);
                    eventProperties.put(ERRORCODE,code);

                    if (code == 0) {
                        //lnearLayout_audio_play.setVisibility(View.GONE);

                        AppContext.getInstance().setInternalLocationResponse(response);
                        long ltime = response.getJSONObject("data")
                                .getJSONObject("parameter")
                                .getInt("location_time");
                        String address = response.getJSONObject("data")
                                .getJSONObject("parameter")
                                .getString("address");
                        double longitude = response.getJSONObject("data")
                                .getJSONObject("parameter")
                                .getJSONObject("location")
                                .getDouble("longitude");
                        double latitude = response.getJSONObject("data")
                                .getJSONObject("parameter")
                                .getJSONObject("location")
                                .getDouble("latitude");
                        int electricity = response.getJSONObject("data")
                                .getJSONObject("parameter")
                                .getInt("electricity");
                        int location_type = response.getJSONObject("data")
                                .getJSONObject("parameter")
                                .getInt("location_type");// 1基站,0,gps

                        String ts=Utils.getTimeFromTimestamp(ltime);
                        eventProperties.put(LTIME_FORMATTED, ts);


                        // ----------------
                        String strdate = new java.text.SimpleDateFormat("HH:mm")
                                .format(new java.util.Date(ltime * 1000));
                        String otherText = String.format("%s",
                                location_type == 1 ? "GPS"
                                        : "GSM/WIFI");

                        String locationDetails=String.format("lat:%f, lon:%f, %s, charge:%d,ltime:%d, ltime_conv:%s",latitude,longitude,address,electricity,ltime,ts);
                        String geoPoint=String.format("lat:%f, lon:%f",latitude,longitude);
                        eventProperties.put(LOCATION_TYPE,otherText);
                        eventProperties.put(TIME,strdate);
                        eventProperties.put(CHARGE,electricity);
                        eventProperties.put(ADDRESS,address);
                        eventProperties.put(GEOPOINT,geoPoint);
                        eventProperties.put(LOCATION,locationDetails);
                        eventProperties.put(LTIME,ltime);



                        if (LOG_INTERNAL_GET_LOCATION) {
                            AnalyticsUtils.reportAnalyticsEvent(EVENT_INTERNAL_GET_LOCATION_SUCCESS,eventProperties);
                        }

                        //update address,etc

                        beanFor___get_watch_data.CRows row = new beanFor___get_watch_data.CRows();
                        row.address = address;
                        row.time = ltime;
                        row.latitude = latitude;
                        row.longitude = longitude;
                        row.electricity = electricity;
                        row.type = location_type;

                        beanFor___get_watch_data watch_data=AppContext.getInstance().getInternal_bean___watch_data();
                        if (watch_data!=null) {
                            try {
                                int rows = watch_data.info.ddata.rows.size();//// internal_bean___watch_data.info.ddata.rows.size();

                                if (rows > 0) {
                                    watch_data.info.ddata.rows.set(0, row);
                                    AppContext.getInstance().setInternal_bean___watch_data(watch_data);
                                }
                                //internal_setLastLocation(row);
                                //internal_showLastLocation();
                                future.set(Result.success());


                            } catch (Exception ex) {{
                                CustomLog.logException(TAG,ex);
                                future.set(Result.failure());
                            }}


                        }

                    } else if (code == 2015001) {

                        CustomLog.w(TAG,"internal_get_location:Sleep mode was active");

                        if (LOG_INTERNAL_GET_LOCATION) {
                            AnalyticsUtils.reportAnalyticsEvent(Constants.EVENT_INTERNAL_GET_LOCATION_ERROR_DEVICE_SLEEPS,eventProperties);
                        }

                        //showLongToast(getActivity().getApplicationContext().getResources().getString(R.string.tip_operate_sleep_mode));
                        future.set(Result.success());

                    } else if (code == -2) {
                        int electricity = response.getJSONObject("data")
                                .getJSONObject("parameter")
                                .getInt("electricity");

                        eventProperties.put(CHARGE,electricity);

                        if (LOG_INTERNAL_GET_LOCATION) {
                            AnalyticsUtils.reportAnalyticsEvent(Constants.EVENT_INTERNAL_GET_LOCATION_ERROR,eventProperties);
                        }

                        CustomLog.d(TAG,"internal_get_location:Electricity:"+electricity+"%");
                        future.set(Result.success());

                    } else {
                        if (LOG_INTERNAL_GET_LOCATION) {
                            AnalyticsUtils.reportAnalyticsEvent(Constants.EVENT_INTERNAL_GET_LOCATION_ERROR,eventProperties);
                        }
                        CustomLog.d(TAG,"internal_get_location:Can't get location, error code: "+code);
                        future.set(Result.failure());

                    }

                } catch (JSONException e) {
                    CustomLog.logException(TAG,e);
                    Map<String,Object> eventProperties=new HashMap<>();
                    eventProperties.put(Constants.URL,request_url);
                    eventProperties.put(Constants.ERREXCEPTION,e);
                    if (LOG_INTERNAL_GET_LOCATION) {
                        AnalyticsUtils.reportAnalyticsEvent(Constants.EVENT_INTERNAL_GET_LOCATION_EXCEPTION,eventProperties);
                    }

                    future.set(Result.failure());
                }
            }

            @Override
            public void onFailure(int errorNo, String strMsg) {
                super.onFailure(errorNo, strMsg);
                String msg = String.format("errorNo:%s\n%s", errorNo, strMsg);
                CustomLog.d(TAG,"internal_get_location:onFailure:error=" + msg);
                Map<String,Object> eventProperties=new HashMap<>();
                //eventProperties.put(Constants.URL,request_url);
                eventProperties.put(ERRORCODE,errorNo);
                eventProperties.put(Constants.MESSAGE, StringUtils.unescapeString(strMsg));
                if (LOG_INTERNAL_GET_LOCATION) {
                    AnalyticsUtils.reportAnalyticsEvent(Constants.EVENT_INTERNAL_GET_LOCATION_SERVER_ERROR,eventProperties);
                }

                future.set(Result.failure());


            }

            @Override
            public void onFinish() {
                CustomLog.d(TAG,"internal_get_location:onFinish");

            }
        });
        */

        //TODO:is this REALLY ok?
        return (ListenableFuture)future;
    }
}