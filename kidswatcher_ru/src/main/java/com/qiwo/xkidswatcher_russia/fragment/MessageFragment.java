package com.qiwo.xkidswatcher_russia.fragment;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Toast;

import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshBase.Mode;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnRefreshListener2;
import com.handmark.pulltorefresh.library.PullToRefreshSwipeListView;
import com.handmark.pulltorefresh.library.SwipeListView;
import com.qiwo.xkidswatcher_russia.KidsWatConfig;
import com.qiwo.xkidswatcher_russia.R;
import com.qiwo.xkidswatcher_russia.adapter.SwipeAdapter;
import com.qiwo.xkidswatcher_russia.api.ApiHttpClient;
import com.qiwo.xkidswatcher_russia.api.remote.KidsWatApiUrl;
import com.qiwo.xkidswatcher_russia.base.BaseFragment;
import com.qiwo.xkidswatcher_russia.db.SqlDb;
import com.qiwo.xkidswatcher_russia.util.KidsWatUtils;
import net.intari.CustomLogger.CustomLog;

import org.json.JSONException;
import org.json.JSONObject;
import org.kymjs.kjframe.http.HttpCallBack;

import butterknife.ButterKnife;
import butterknife.BindView;;

/**
 *
 */
@Deprecated
public class MessageFragment extends BaseFragment {
    public static final String TAG = MessageFragment.class.getSimpleName();

    // @BindView(R.id.note_detail_edit)
    // EditText mEtContent;

    @BindView(R.id.pull_refresh_list)
    PullToRefreshSwipeListView mPullRefreshListView;

    // private ScrollView mScrollView;
    private SwipeListView mSwipeListView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View rootView = inflater.inflate(R.layout.fragment_message, container,
                false);
        ButterKnife.bind(this, rootView);
        initView(rootView);
        initData();
        return rootView;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case 1:
                break;
        }
    }

    @Override
    public void initView(View view) {
        // mImgGreen.setOnClickListener(this);

        mPullRefreshListView.setMode(Mode.BOTH);
        mPullRefreshListView
                .setOnRefreshListener(new OnRefreshListener2<SwipeListView>() {
                    // .setOnRefreshListener(new
                    // OnRefreshListener<SwipeListView>() {
                    @Override
                    public void onPullDownToRefresh(
                            PullToRefreshBase<SwipeListView> refreshView) {
                        Log.i("aa-im", "onPullDownToRefresh");
                        mPullRefreshListView.setRefreshing();
                        // mPullRefreshListView.
                        get_notice_list_ex(1);
                    }

                    @Override
                    public void onPullUpToRefresh(
                            PullToRefreshBase<SwipeListView> refreshView) {
                        Log.i("aa-im", "onPullUpToRefresh");
                        mPullRefreshListView.setRefreshing();
                    }
                });

        mSwipeListView = mPullRefreshListView.getRefreshableView();
        mSwipeListView.setFooterAndHeaderCanSwipe(true, true);
        // mListView = (SwipeListView)findViewById(R.id.listview);
        SwipeAdapter adapter = new SwipeAdapter(getActivity(),
                mSwipeListView.getRightViewWidth(),
                new SwipeAdapter.IOnItemRightClickListener() {
                    @Override
                    public void onRightClick(View v, int position) {
                        // TODO Auto-generated method stub
                        mSwipeListView.resetItems();
                        showLongToast("right onclick " + position);
                    }
                });
        mSwipeListView.setAdapter(adapter);
        mSwipeListView
                .setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view,
                                            int position, long id) {
                        showLongToast("item onclick " + position);
                    }
                });
    }

    @Override
    public void initData() {
    }

    // -------------------------
    private void get_notice_list_ex(final int pageindex) {
        // -----------------
        boolean isNetworkAvailable = KidsWatUtils
                .isNetworkAvailable(getActivity());
        if (!isNetworkAvailable) {
            Toast.makeText(getActivity(),
                    "Network unavailable, Please check network condition.",
                    Toast.LENGTH_LONG).show();
            return;
        }
        final String uid = KidsWatConfig.getUserUid();
        final String access_token = KidsWatConfig.getUserToken();
        final String family_id = KidsWatConfig.getDefaultFamilyId();

        final String request_url = KidsWatApiUrl
                .getUrlFor___get_notice_list_ex(uid, access_token, pageindex,
                        20);
        // -------------------------

        ApiHttpClient.get(request_url, new HttpCallBack() {

            @Override
            public void onPreStart() {
            }

            @Override
            public void onSuccess(String t) {
                super.onSuccess(t);
                CustomLog.d(TAG,String.format("url:%s\nt:%s", request_url, t));

                try {
                    JSONObject response = new JSONObject(t);

                    try {
                        SqlDb db = SqlDb.get(getActivity());
                        if (response.getJSONArray("data").length() > 0) {
                            db.saveMessage(uid, family_id, response);
                        } else {
                            db.clearMessage(uid);
                        }
                        db.closeDb();
                    } catch (NullPointerException ex) {
                        CustomLog.logException(TAG,ex);
                    }

                    // adapter.notifyDataSetChanged();
                    // 停止刷新动画
                    // swipeRefreshLayout.setRefreshing(false);

                } catch (JSONException e) {
                    CustomLog.logException(TAG,e);
                }

            }

            @Override
            public void onFailure(int errorNo, String strMsg) {
                super.onFailure(errorNo, strMsg);
                String msg = String
                        .format("%s(error=%s)",
                                errorNo == -1 ? "Connect to the server failed"
                                        : strMsg, errorNo);
                CustomLog.d(TAG,msg);
                showLongToast(msg);
            }

            @Override
            public void onFinish() {
                mPullRefreshListView.setRefreshEnd();
                CustomLog.d(TAG,"-----finish-------");
            }
        });
        // -------------------------

    }

}
