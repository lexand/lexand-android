package com.qiwo.xkidswatcher_russia.fragment;

//import net.oschina.app.AppContext;
//import net.oschina.app.R;
//import net.oschina.app.base.BaseFragment;
//import net.oschina.app.bean.NotebookData;
//import net.oschina.app.bean.SimpleBackPage;
//import net.oschina.app.db.NoteDatabase;
//import net.oschina.app.ui.SimpleBackActivity;
//import net.oschina.app.ui.dialog.CommonDialog;
//import net.oschina.app.ui.dialog.DialogHelper;
//import net.oschina.app.util.KJAnimations;
//import net.oschina.app.util.StringUtils;
//import net.oschina.app.util.UIHelper;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.qiwo.xkidswatcher_russia.AppContext;
import com.qiwo.xkidswatcher_russia.KidsWatConfig;
import com.qiwo.xkidswatcher_russia.R;
import com.qiwo.xkidswatcher_russia.adapter.ChatMsgViewAdapter;
import com.qiwo.xkidswatcher_russia.adapter.Record461Adapter;
import com.qiwo.xkidswatcher_russia.api.remote.KidsWatApiUrl;
import com.qiwo.xkidswatcher_russia.base.BaseFragment;
import com.qiwo.xkidswatcher_russia.bean.ChatMsgEntity;
import com.qiwo.xkidswatcher_russia.bean.Record461;
import com.qiwo.xkidswatcher_russia.bean.beanForDb___Family;
import com.qiwo.xkidswatcher_russia.db.SqlDb;
import com.qiwo.xkidswatcher_russia.event.BaseEvent;
import com.qiwo.xkidswatcher_russia.thread.CommonThreadMethod;
import com.qiwo.xkidswatcher_russia.thread.ThreadParent;
import com.qiwo.xkidswatcher_russia.ui.QrcodeActivity;
import com.qiwo.xkidswatcher_russia.util.SoundMeter;
import net.intari.CustomLogger.CustomLog;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.ButterKnife;
import butterknife.BindView;;

/**
 *
 */
public class RecordFragment extends BaseFragment {
    // implements OnTouchListener {
    public static final String TAG = RecordFragment.class.getSimpleName();

    private static final int POLL_INTERVAL = 300;

    // @BindView(R.id.btn_bottom)
    // private RelativeLayout mBottom;
    private static final String GETDEVICECALLRECORDACTION = "w461_get_device_call_record_v1";
    private static final int GETDEVICECALLRECORDCODE = 2201;
    @BindView(R.id.btn_rcd)
    TextView mBtnRcd;
    @BindView(R.id.listview)
    ListView mListView;
    @BindView(R.id.voice_rcd_hint_loading)
    LinearLayout voice_rcd_hint_loading;
    @BindView(R.id.voice_rcd_hint_rcding)
    LinearLayout voice_rcd_hint_rcding;
    @BindView(R.id.voice_rcd_hint_tooshort)
    LinearLayout voice_rcd_hint_tooshort;
    @BindView(R.id.img1)
    ImageView img1;
    @BindView(R.id.sc_img1)
    ImageView sc_img1;
    @BindView(R.id.rcChat_popup)
    View rcChat_popup;
    @BindView(R.id.del_re)
    LinearLayout del_re;
    @BindView(R.id.lnearLayout_361)
    LinearLayout lnearLayout_361;

    // @BindView(R.id.ivPopUp)
    // private ImageView chatting_mode_btn;

    // @BindView(R.id.volume)
    // private ImageView volume;
    @BindView(R.id.relativeLayout_bottom)
    RelativeLayout relativeLayout_bottom;
    @BindView(R.id.textView_link)
    TextView textView_link;
    @BindView(R.id.callwatch)
    LinearLayout callwatch;
    View.OnClickListener view_ll_listener = new View.OnClickListener() {
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.textView_link:
                    Uri uri = Uri.parse("http://www.misafes.com");
                    Intent it = new Intent(Intent.ACTION_VIEW, uri);
                    startActivity(it);
                    break;
                case R.id.button1:
                    SqlDb db = SqlDb.get(getActivity());
                    beanForDb___Family bb = db.getFamilyBy_fid(KidsWatConfig
                            .getDefaultFamilyId());
                    db.closeDb();

                    Intent intent = new Intent(getActivity(), QrcodeActivity.class);
                    intent.putExtra("qrcode", bb.qrcode);
                    intent.putExtra("sim", bb.sim);
                    startActivity(intent);
                    break;
            }
            // menu.showContent();
        }
    };
    OnClickListener callwatchOnClick = new OnClickListener() {

        @Override
        public void onClick(View v) {

            // int absent = TelephonyManager.SIM_STATE_ABSENT;
            // if (1 == absent) {
            // Toast.makeText(getActivity(), "请确认sim卡是否插入或者sim卡暂时不可用！",
            // Toast.LENGTH_SHORT).show();
            // } else {

            SqlDb db = SqlDb.get(getActivity());
            beanForDb___Family bb = db.getFamilyBy_fid(KidsWatConfig
                    .getDefaultFamilyId());
            db.closeDb();

            // Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:"
            // + "+"
            // + AppContext.getInstance().currentBeanFamily.getInfo()
            // .getW461_device_country_code()
            // + AppContext.getInstance().currentBeanFamily.getInfo()
            // .getW461_device_sim_no()));

            Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:"
                    + "+" + bb.sim));

            startActivity(intent);
            // }

        }
    };
    List<Record461> record = null;
    Record461Adapter adapter = null;
    private SoundMeter mSensor;
    private int flag = 1;
    private Handler mxHandler = new Handler();
    private String voiceName;
    private long startVoiceT, endVoiceT;
    private ChatMsgViewAdapter mAdapter;
    private List<ChatMsgEntity> mDataArrays = new ArrayList<ChatMsgEntity>();

    // private String[] msgArray = new String[] { "有人就有恩怨", "有恩怨就有江湖", "人就是江湖",
    // "你怎么退出？ ", "生命中充满了巧合", "两条平行线也会有相交的一天。" };
    //
    // private String[] dataArray = new String[] { "2014-10-31 18:00",
    // "2014-10-31 18:10", "2014-10-31 18:11", "2014-10-31 18:20",
    // "2014-10-31 18:30", "2014-10-31 18:35" };
    // private final static int COUNT = 0;
    private boolean isShosrt = false;
    private View rootView;
    private Runnable mPollTask = new Runnable() {
        public void run() {
            double amp = mSensor.getAmplitude();
            mxHandler.postDelayed(mPollTask, POLL_INTERVAL);

        }
    };
    private Runnable mSleepTask = new Runnable() {
        public void run() {
            stop();
        }
    };
    @SuppressLint("HandlerLeak")
    private Handler handler = new Handler() {

        @Override
        public void handleMessage(Message msg) {

            String mess = msg.obj.toString();
            int code = msg.arg1;
            CustomLog.e(TAG,"mess:" + mess);

            if (mess == null || mess.equals(""))
                return;
            JSONObject json = null;
            try {
                json = new JSONObject(mess);

                int errorCode = json.getInt("error");

                switch (code) {
                    case GETDEVICECALLRECORDCODE:
                        JSONObject infoJson = json.getJSONObject("info");
                        if (errorCode == 0) {
                            record = getRecordlist(infoJson
                                    .getJSONArray("record_list"));

                            adapter = new Record461Adapter(getActivity(), record,
                                    infoJson.getString("kid_name"));
                            mListView.setAdapter(adapter);

                            // mListView
                            // .setOnItemLongClickListener(new
                            // OnItemLongClickListener() {
                            //
                            // @Override
                            // public boolean onItemLongClick(
                            // AdapterView<?> parent, View view,
                            // int position, long id) {
                            //
                            // for (int i = 0; i < record.size(); i++) {
                            // record.get(i).setType(1);
                            // }
                            // adapter.notifyDataSetChanged();
                            // return true;
                            // }
                            // });
                        }
                        break;
                }
            } catch (JSONException e) {
                CustomLog.logException(TAG,e);
            }
            hideWaitDialog();
        }
    };

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        if (rootView == null) {
            rootView = inflater.inflate(R.layout.fragment_record, container,
                    false);
            ButterKnife.bind(this, rootView);
            initData();
            initView(rootView);
        }

        ViewGroup parent = (ViewGroup) rootView.getParent();
        if (parent != null) {
            parent.removeView(rootView);
        }

        return rootView;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        CustomLog.d(TAG,"--------onStart--------");
        setHasOptionsMenu(true);
    }

    @Override
    public void onResume() {
        super.onResume();
        CustomLog.d(TAG,"--------onResume--------");

        /**
         * 白浩 如果是461
         */
        showView();
    }

    @Override
    public void onPause() {
        super.onPause();
        CustomLog.d(TAG,"--------onPause--------");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        CustomLog.d(TAG,"--------onDestroy--------");
    }

    @Override
    public void initData() {
        //
        // for (int i = 0; i < COUNT; i++) {
        // ChatMsgEntity entity = new ChatMsgEntity();
        // entity.setDate(dataArray[i]);
        // if (i % 2 == 0) {
        // entity.setName("白富美");
        // entity.setMsgType(true);
        // } else {
        // entity.setName("高富帅");
        // entity.setMsgType(false);
        // }
        //
        // entity.setText(msgArray[i]);
        // mDataArrays.add(entity);
        // }
        //
        // mAdapter = new ChatMsgViewAdapter(getActivity(), mDataArrays);
        // mListView.setAdapter(mAdapter);

    }

    // ------------------------------------------------------------------------------461---------------------------------------------------------------
    // ------------------------------------------------------------------------------461---------------------------------------------------------------
    // ------------------------------------------------------------------------------461---------------------------------------------------------------
    // ------------------------------------------------------------------------------461---------------------------------------------------------------
    // ------------------------------------------------------------------------------461---------------------------------------------------------------
    // ------------------------------------------------------------------------------461---------------------------------------------------------------

    @Override
    public void initView(View view) {
        textView_link.setText(Html.fromHtml("<u>www.misafes.com</u>"));
        textView_link.setOnClickListener(view_ll_listener);
        showView();
        mSensor = new SoundMeter();
        mBtnRcd.setOnTouchListener(new OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                // 按下语音录制按钮时返回false执行父类OnTouch
                return false;
            }
        });

        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
    }

    private void showView() {
        if (AppContext.getInstance().getCurrentFamily() != null) {
            String version = AppContext.getInstance().getCurrentFamily().version;
            if (version.equalsIgnoreCase("361")) {
                mListView.setVisibility(View.GONE);
                relativeLayout_bottom.setVisibility(View.GONE);
                lnearLayout_361.setVisibility(View.VISIBLE);
                callwatch.setVisibility(8);
            } else if (version.equalsIgnoreCase("461")) {
                if (AppContext.getInstance().getLoginUser_kid() != null)
                    set461Data();
            } else if (version.equalsIgnoreCase("601")) {

            }
        } else {
            mListView.setVisibility(View.GONE);
            relativeLayout_bottom.setVisibility(View.GONE);
            lnearLayout_361.setVisibility(View.VISIBLE);
        }
    }

    private String getDate() {
        Calendar c = Calendar.getInstance();

        String year = String.valueOf(c.get(Calendar.YEAR));
        String month = String.valueOf(c.get(Calendar.MONTH));
        String day = String.valueOf(c.get(Calendar.DAY_OF_MONTH) + 1);
        String hour = String.valueOf(c.get(Calendar.HOUR_OF_DAY));
        String mins = String.valueOf(c.get(Calendar.MINUTE));

        StringBuffer sbBuffer = new StringBuffer();
        sbBuffer.append(year + "-" + month + "-" + day + " " + hour + ":"
                + mins);

        return sbBuffer.toString();
    }

    // 按下语音录制按钮时
    public boolean onTouchEvent(MotionEvent event) {

        if (!Environment.getExternalStorageDirectory().exists()) {
            showLongToast("No SDCard");
            return false;
        }

        CustomLog.d(TAG,"1");
        int[] location = new int[2];
        mBtnRcd.getLocationInWindow(location); // 获取在当前窗口内的绝对坐标
        int btn_rc_Y = location[1];
        int btn_rc_X = location[0];
        int[] del_location = new int[2];
        del_re.getLocationInWindow(del_location);
        int del_Y = del_location[1];
        int del_x = del_location[0];
        if (event.getAction() == MotionEvent.ACTION_DOWN && flag == 1) {
            if (!Environment.getExternalStorageDirectory().exists()) {
                showLongToast("No SDCard");
                return false;
            }
            CustomLog.d(TAG,"2");
            if (event.getY() > btn_rc_Y && event.getX() > btn_rc_X) {// 判断手势按下的位置是否是语音录制按钮的范围内
                CustomLog.d(TAG,"3");
                mBtnRcd.setBackgroundResource(R.drawable.voice_rcd_btn_pressed);
                rcChat_popup.setVisibility(View.VISIBLE);
                voice_rcd_hint_loading.setVisibility(View.VISIBLE);
                voice_rcd_hint_rcding.setVisibility(View.GONE);
                voice_rcd_hint_tooshort.setVisibility(View.GONE);
                mxHandler.postDelayed(new Runnable() {
                    public void run() {
                        if (!isShosrt) {
                            voice_rcd_hint_loading.setVisibility(View.GONE);
                            voice_rcd_hint_rcding.setVisibility(View.VISIBLE);
                        }
                    }
                }, 300);
                img1.setVisibility(View.VISIBLE);
                del_re.setVisibility(View.GONE);
                startVoiceT = System.currentTimeMillis();// SystemClock.currentThreadTimeMillis();
                voiceName = startVoiceT + ".amr";
                start(voiceName);
                flag = 2;
            }
        } else if (event.getAction() == MotionEvent.ACTION_UP && flag == 2) {// 松开手势时执行录制完成
            CustomLog.d(TAG,"4");
            mBtnRcd.setBackgroundResource(R.drawable.voice_rcd_btn_nor);
            if (event.getY() >= del_Y
                    && event.getY() <= del_Y + del_re.getHeight()
                    && event.getX() >= del_x
                    && event.getX() <= del_x + del_re.getWidth()) {
                rcChat_popup.setVisibility(View.GONE);
                img1.setVisibility(View.VISIBLE);
                del_re.setVisibility(View.GONE);
                stop();
                flag = 1;
                File file = new File(
                        android.os.Environment.getExternalStorageDirectory()
                                + "/" + voiceName);
                if (file.exists()) {
                    file.delete();
                }
            } else {

                voice_rcd_hint_rcding.setVisibility(View.GONE);
                stop();
                endVoiceT = System.currentTimeMillis();// SystemClock.currentThreadTimeMillis();
                flag = 1;
                int time = (int) ((endVoiceT - startVoiceT) / 1000);
                if (time < 5) {
                    isShosrt = true;
                    voice_rcd_hint_loading.setVisibility(View.GONE);
                    voice_rcd_hint_rcding.setVisibility(View.GONE);
                    voice_rcd_hint_tooshort.setVisibility(View.VISIBLE);
                    mxHandler.postDelayed(new Runnable() {
                        public void run() {
                            voice_rcd_hint_tooshort.setVisibility(View.GONE);
                            rcChat_popup.setVisibility(View.GONE);
                            isShosrt = false;
                        }
                    }, 500);
                    return false;
                }
                ChatMsgEntity entity = new ChatMsgEntity();
                entity.setDate(getDate());
                entity.setName("高富帅");
                entity.setMsgType(false);
                entity.setTime(time + "\"");
                entity.setText(voiceName);
                mDataArrays.add(entity);
                mAdapter.notifyDataSetChanged();
                mListView.setSelection(mListView.getCount() - 1);
                rcChat_popup.setVisibility(View.GONE);

            }
        }
        if (event.getY() < btn_rc_Y) {// 手势按下的位置不在语音录制按钮的范围内
            CustomLog.d(TAG,"5");
            Animation mLitteAnimation = AnimationUtils.loadAnimation(
                    getActivity(), R.anim.cancel_rc);
            Animation mBigAnimation = AnimationUtils.loadAnimation(
                    getActivity(), R.anim.cancel_rc2);
            img1.setVisibility(View.GONE);
            del_re.setVisibility(View.VISIBLE);
            del_re.setBackgroundResource(R.drawable.voice_rcd_cancel_bg);
            if (event.getY() >= del_Y
                    && event.getY() <= del_Y + del_re.getHeight()
                    && event.getX() >= del_x
                    && event.getX() <= del_x + del_re.getWidth()) {
                del_re.setBackgroundResource(R.drawable.voice_rcd_cancel_bg_focused);
                sc_img1.startAnimation(mLitteAnimation);
                sc_img1.startAnimation(mBigAnimation);
            }
        } else {

            img1.setVisibility(View.VISIBLE);
            del_re.setVisibility(View.GONE);
            del_re.setBackgroundResource(0);
        }
        return false;// super.onTouchEvent(event);
    }

    private void start(String name) {
        mSensor.start(name);
        mxHandler.postDelayed(mPollTask, POLL_INTERVAL);
    }

    private void stop() {
        mxHandler.removeCallbacks(mSleepTask);
        mxHandler.removeCallbacks(mPollTask);
        mSensor.stop();
    }

    public void set461Data() {

        mListView.setVisibility(0);
        lnearLayout_361.setVisibility(8);
        callwatch.setVisibility(0);

        callwatch.removeAllViews();
        callwatch.setBackgroundResource(R.drawable.call_btn);
        callwatch.setOnClickListener(callwatchOnClick);

        GetRecord();
    }

    /**
     * 获取记录
     */
    public void GetRecord() {

        showWaitDialog();

        new Thread(new ThreadParent(TAG,KidsWatApiUrl.getApiUrl(),
                CommonThreadMethod.JsonFormat(GetRecordParameter()), handler,
                GETDEVICECALLRECORDCODE)).start();
    }

    /*
     * 参数
     */
    public Map<String, String> GetRecordParameter() {
        Map<String, String> hash = new HashMap<String, String>();
        hash.put("uid", KidsWatConfig.getUserUid());
        hash.put("device_id", KidsWatConfig.getUserDeviceId());
        hash.put("access_token", KidsWatConfig.getUserToken());

        //
        hash.put("page", 0 + "");
        hash.put("page_size", 0 + "");

        KidsWatApiUrl.addExtraPara(GETDEVICECALLRECORDACTION, hash);

        return hash;
    }

    /*
     * 格式化记录数据
     */

    public List<Record461> getRecordlist(JSONArray StrArray) {
        List<Record461> recordList = new ArrayList<Record461>();
        if (StrArray.length() == 0)
            return recordList;
        Record461 record = null;
        for (int i = 0; i < StrArray.length(); i++) {
            record = new Record461();
            JSONObject json;
            try {
                json = StrArray.getJSONObject(i);

                record.setBegin_time(json.getString("begin_time"));
                record.setCall_type(json.getInt("call_type"));
                record.setLast_time(json.getString("last_time"));
                record.setMessage_id(json.getString("message_id"));
                record.setTime(json.getString("time"));
                record.setRelation(json.getString("relation"));
                record.setType(0);
                recordList.add(record);
            } catch (JSONException e) {
                CustomLog.logException(TAG,e);
            }
        }

        record = new Record461();
        record.setCall_type(119);
        recordList.add(record);

        return recordList;
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEventMainThread(BaseEvent event) {
        CustomLog.d(TAG,"------onEventMainThread----" + event.getMsg()+" of type "+event.getType()+" aka "+event.getTypeAsString());

        int type = event.getType();
        String msg = event.getMsg();

        if (type == BaseEvent.MSGTYPE_6___WHITE_TO_FRONT) {
            CustomLog.e(TAG,"我收到了 - I received");
            showView();
        }
    }
}
