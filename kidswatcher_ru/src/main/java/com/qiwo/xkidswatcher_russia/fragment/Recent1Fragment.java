package com.qiwo.xkidswatcher_russia.fragment;


import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.qiwo.xkidswatcher_russia.AppContext;
import com.qiwo.xkidswatcher_russia.KidsWatConfig;
import com.qiwo.xkidswatcher_russia.R;
import com.qiwo.xkidswatcher_russia.adapter.Record461Adapter;
import com.qiwo.xkidswatcher_russia.api.remote.KidsWatApiUrl;
import com.qiwo.xkidswatcher_russia.base.BaseFragment;
import com.qiwo.xkidswatcher_russia.bean.Record461;
import com.qiwo.xkidswatcher_russia.bean.beanForDb___Family;
import com.qiwo.xkidswatcher_russia.db.SqlDb;
import com.qiwo.xkidswatcher_russia.event.BaseEvent;
import com.qiwo.xkidswatcher_russia.thread.CommonThreadMethod;
import com.qiwo.xkidswatcher_russia.thread.ThreadParent;
import com.qiwo.xkidswatcher_russia.util.SoundMeter;
import net.intari.CustomLogger.CustomLog;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.ButterKnife;
import butterknife.BindView;;

/**
 *
 */
@Deprecated
public class Recent1Fragment extends BaseFragment {

    public static final String TAG = Recent1Fragment.class.getSimpleName();

    //TODO
    //----------461 新版 ---------- 2015-12-31 18:04:55
    public static final int setMonitorCode = 15999; //开启监听电话模式
    public static final List<Record461> selections = new ArrayList<Record461>();
    private static final int deleteRecord = 15998;    //删除通话记录
    private static final int POLL_INTERVAL = 300;
    private static final String GETDEVICECALLRECORDACTION = "w461_get_device_call_record_v1";
    private static final int GETDEVICECALLRECORDCODE = 2201;
    public static List<Integer> subTime = new ArrayList<Integer>();
    static boolean isEditMode = false;
    @BindView(R.id.recent_not_tv)
    TextView recent_not_tv;
    @BindView(R.id.listview)
    ListView mListView;
    List<Record461> record = null;
    Record461Adapter adapter = null;
    private SoundMeter mSensor;
    private Handler mxHandler = new Handler();
    private View rootView;
    private Runnable mPollTask = new Runnable() {
        public void run() {
            double amp = mSensor.getAmplitude();
            mxHandler.postDelayed(mPollTask, POLL_INTERVAL);

        }
    };
    private Runnable mSleepTask = new Runnable() {
        public void run() {
            stop();
        }
    };
    @SuppressLint("HandlerLeak")
    private Handler handler = new Handler() {

        @Override
        public void handleMessage(Message msg) {

            String mess = msg.obj.toString();
            int code = msg.arg1;
            CustomLog.e(TAG,"mess:" + mess);

            if (mess == null || mess.equals(""))
                return;
            JSONObject json = null;
            try {
                json = new JSONObject(mess);

                int errorCode = json.getInt("error");

                switch (code) {
                    case GETDEVICECALLRECORDCODE:
                        JSONObject infoJson = json.getJSONObject("info");
                        if (errorCode == 0) {
                            record = getRecordlist(infoJson
                                    .getJSONArray("record_list"));
                            if (record != null && record.size() > 0) {
                                mListView.setVisibility(View.VISIBLE);
                                recent_not_tv.setVisibility(View.GONE);

                                adapter = new Record461Adapter(getActivity(), record,
                                        infoJson.getString("kid_name"));
                                isEditMode = true;
                                mListView.setAdapter(adapter);
                                mListView.setSelection(adapter.getCount() - 1);
                                long temp = 0;
                                for (int i = 0; i < record.size() - 1; i++) {
							/*if(record.get(i).getBegin_time() == null){
								CustomLog.d(TAG,"---------- begin time == null , position == "+i+" ---------------");
							}else */
                                    if (Long.parseLong(record.get(i).getBegin_time()) - temp > 10 * 60) {
                                        temp = Long.parseLong(record.get(i).getBegin_time());
                                        CustomLog.d(TAG,"time : " + temp);
                                    } else {
                                        subTime.add(i);
                                        //		CustomLog.d(TAG,"subTime : "+i +"temp : "+temp);
                                    }
                                }
                            } else {
                                mListView.setVisibility(View.GONE);
                                recent_not_tv.setVisibility(View.VISIBLE);
                                recent_not_tv.setText(getString(R.string.not_recent1));
                            }
                        }
                        hideWaitDialog();
                        break;
                    case setMonitorCode:
                        if (errorCode == 0) {
                            //获取monitor的状态

                            //打电话
                            SqlDb db = SqlDb.get(getActivity());
                            beanForDb___Family bb = db.getFamilyBy_fid(KidsWatConfig
                                    .getDefaultFamilyId());
                            db.closeDb();
                            Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:"
                                    + "+" + bb.sim));
                            startActivity(intent);
                        }
                        hideWaitDialog();
                        break;
                    case deleteRecord:
                        //TODO
                        if (errorCode == 0) {
                            CustomLog.d(TAG,"delete finish!");
                            selections.clear();
                            subTime.clear();
                            GetRecord();
                            isEditMode = true;
                            adapter.notifyDataSetChanged();
                        } else if (errorCode == 1 || errorCode == 101) {
                            //no record selected
                            subTime.clear();
                            GetRecord();
                            isEditMode = true;
                            adapter.notifyDataSetChanged();
                        }
                        break;
                }
            } catch (JSONException e) {
                CustomLog.logException(TAG,e);
            }
        }
    };

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        if (rootView == null) {
            rootView = inflater.inflate(R.layout.fragment_record3, container,
                    false);
            ButterKnife.bind(this, rootView);
            initData();
            initView(rootView);
        }

        ViewGroup parent = (ViewGroup) rootView.getParent();
        if (parent != null) {
            parent.removeView(rootView);
        }

        return rootView;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        CustomLog.d(TAG,"--------onStart--------");
        setHasOptionsMenu(true);

    }

    @Override
    public void onResume() {
        super.onResume();
        subTime.clear();
        selections.clear();
        CustomLog.d(TAG,"--------onResume--------");

        showNewView();
    }

    private void showNewView() {
        CustomLog.d(TAG,"version:" + AppContext.getInstance().getCurrentFamily());
        if (AppContext.getInstance().getCurrentFamily() != null) {
            String version = AppContext.getInstance().getCurrentFamily().version;
            if ("601".equalsIgnoreCase(version)) {
                mListView.setVisibility(View.VISIBLE);
                recent_not_tv.setVisibility(View.GONE);
                recent_not_tv.setText(getString(R.string.not_recent));
                GetRecord();
            } else {
                mListView.setVisibility(View.GONE);
                recent_not_tv.setVisibility(View.VISIBLE);
                recent_not_tv.setText(getString(R.string.not_recent));
            }
        } else {
            mListView.setVisibility(View.GONE);
            recent_not_tv.setVisibility(View.VISIBLE);
            recent_not_tv.setText(getString(R.string.not_recent));
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        isEditMode = true;
        selections.clear();
        subTime.clear();
        CustomLog.d(TAG,"--------onPause--------");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        CustomLog.d(TAG,"--------onDestroy--------");
    }

    @Override
    public void initData() {

    }

    @Override
    public void initView(View view) {
        showNewView();
        mSensor = new SoundMeter();

        if (!isEditMode) {
            mListView.setOnItemClickListener(new OnItemClickListener() {

                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    //在编辑模式下
                    if (!isEditMode) {
                        if (selections.contains(record.get(position))) {
                            selections.remove(record.get(position));
                            ImageView iv_left = (ImageView) view.findViewById(R.id.recordchoice_left);
                            ImageView iv_right = (ImageView) view.findViewById(R.id.recordchoice_right);
                            iv_left.setImageResource(R.drawable.recordchoice);
                            iv_right.setImageResource(R.drawable.recordchoice);
                        } else {
                            ImageView iv_left = (ImageView) view.findViewById(R.id.recordchoice_left);
                            ImageView iv_right = (ImageView) view.findViewById(R.id.recordchoice_right);
                            iv_left.setImageResource(R.drawable.recordchoice_s);
                            iv_right.setImageResource(R.drawable.recordchoice_s);
                            selections.add(record.get(position));
                        }
                    }
                }

            });
        }
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
    }

    public void showConfirmInformation(String title, String message) {
        if (title == null)
            message = "\n" + message;
        new AlertDialog.Builder(getActivity()).setMessage(message).setTitle(title)
                .setPositiveButton("OK", null).show();
    }

    private String getDate() {
        Calendar c = Calendar.getInstance();

        String year = String.valueOf(c.get(Calendar.YEAR));
        String month = String.valueOf(c.get(Calendar.MONTH));
        String day = String.valueOf(c.get(Calendar.DAY_OF_MONTH) + 1);
        String hour = String.valueOf(c.get(Calendar.HOUR_OF_DAY));
        String mins = String.valueOf(c.get(Calendar.MINUTE));

        StringBuffer sbBuffer = new StringBuffer();
        sbBuffer.append(year + "-" + month + "-" + day + " " + hour + ":"
                + mins);

        return sbBuffer.toString();
    }

    private void stop() {
        mxHandler.removeCallbacks(mSleepTask);
        mxHandler.removeCallbacks(mPollTask);
        mSensor.stop();
    }

    /**
     * 获取记录
     */
    public void GetRecord() {

        showWaitDialog();

        new Thread(new ThreadParent(TAG,KidsWatApiUrl.getApiUrl(),
                CommonThreadMethod.JsonFormat(GetRecordParameter()), handler,
                GETDEVICECALLRECORDCODE)).start();
    }

    /*
     * 参数
     */
    public Map<String, String> GetRecordParameter() {
        Map<String, String> hash = new HashMap<String, String>();
        hash.put("uid", KidsWatConfig.getUserUid());
        hash.put("device_id", KidsWatConfig.getUserDeviceId());
        hash.put("access_token", KidsWatConfig.getUserToken());

        //
        hash.put("page", 0 + "");
        hash.put("page_size", 0 + "");

        KidsWatApiUrl.addExtraPara(GETDEVICECALLRECORDACTION, hash);

        return hash;
    }

    /*
     * 格式化记录数据
     */

    public List<Record461> getRecordlist(JSONArray StrArray) {
        List<Record461> recordList = new ArrayList<Record461>();
        if (StrArray.length() == 0)
            return recordList;
        Record461 record = null;
        for (int i = 0; i < StrArray.length(); i++) {
            record = new Record461();
            JSONObject json;
            try {
                json = StrArray.getJSONObject(i);

                record.setBegin_time(json.getString("begin_time"));
                record.setCall_type(json.getInt("call_type"));
                record.setLast_time(json.getString("last_time"));
                record.setMessage_id(json.getString("message_id"));
                record.setTime(json.getString("time"));
                record.setRelation(json.getString("relation"));
                record.setType(0);
                recordList.add(record);
            } catch (JSONException e) {
                CustomLog.logException(TAG,e);
            }
        }

        record = new Record461();
        record.setCall_type(119);
        recordList.add(record);

        return recordList;
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEventMainThread(BaseEvent event) {
        CustomLog.d(TAG,"------onEventMainThread----" + event.getMsg()+" of type "+event.getType()+" aka "+event.getTypeAsString());
        int type = event.getType();
        String msg = event.getMsg();

        if (type == BaseEvent.MSGTYPE_6___WHITE_TO_FRONT) {
            CustomLog.e(TAG,"我收到了 - I received");
            showNewView();
        }
    }
}
