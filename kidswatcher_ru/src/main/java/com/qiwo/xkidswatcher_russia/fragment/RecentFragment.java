package com.qiwo.xkidswatcher_russia.fragment;

//import net.oschina.app.AppContext;
//import net.oschina.app.R;
//import net.oschina.app.base.BaseFragment;
//import net.oschina.app.bean.NotebookData;
//import net.oschina.app.bean.SimpleBackPage;
//import net.oschina.app.db.NoteDatabase;
//import net.oschina.app.ui.SimpleBackActivity;
//import net.oschina.app.ui.dialog.CommonDialog;
//import net.oschina.app.ui.dialog.DialogHelper;
//import net.oschina.app.util.KJAnimations;
//import net.oschina.app.util.StringUtils;
//import net.oschina.app.util.UIHelper;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.qiwo.xkidswatcher_russia.AppContext;
import com.qiwo.xkidswatcher_russia.KidsWatConfig;
import com.qiwo.xkidswatcher_russia.R;
import com.qiwo.xkidswatcher_russia.adapter.ChatMsgViewAdapter;
import com.qiwo.xkidswatcher_russia.adapter.Record461Adapter;
import com.qiwo.xkidswatcher_russia.api.remote.KidsWatApiUrl;
import com.qiwo.xkidswatcher_russia.base.BaseFragment;
import com.qiwo.xkidswatcher_russia.bean.ChatMsgEntity;
import com.qiwo.xkidswatcher_russia.bean.Record461;
import com.qiwo.xkidswatcher_russia.bean.beanForDb___Family;
import com.qiwo.xkidswatcher_russia.db.SqlDb;
import com.qiwo.xkidswatcher_russia.event.BaseEvent;
import com.qiwo.xkidswatcher_russia.thread.CommonThreadMethod;
import com.qiwo.xkidswatcher_russia.thread.ThreadParent;
import com.qiwo.xkidswatcher_russia.ui.QrcodeActivity;
import com.qiwo.xkidswatcher_russia.util.SoundMeter;
import net.intari.CustomLogger.CustomLog;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.ButterKnife;
import butterknife.BindView;;

/**
 *
 */
@Deprecated
public class RecentFragment extends BaseFragment {
    public static final String TAG = RecentFragment.class.getSimpleName();

    // implements OnTouchListener {

    //TODO
    //----------461 新版 ---------- 2015-12-31 18:04:55
    public static final int setMonitorCode = 15999; //开启监听电话模式

    // @BindView(R.id.btn_bottom)
    // private RelativeLayout mBottom;
    public static final List<Record461> selections = new ArrayList<Record461>();
    private static final int deleteRecord = 15998;    //删除通话记录
    private static final int POLL_INTERVAL = 300;
    private static final String GETDEVICECALLRECORDACTION = "w461_get_device_call_record_v1";
    private static final int GETDEVICECALLRECORDCODE = 2201;
    //	public static final Map<Integer, Long> subTimes = new HashMap<Integer, Long>();
    public static List<Integer> subTime = new ArrayList<Integer>();
    static boolean isEditMode = false;
    @BindView(R.id.btn_rcd)
    TextView mBtnRcd;
    @BindView(R.id.listview)
    ListView mListView;
    @BindView(R.id.voice_rcd_hint_loading)
    LinearLayout voice_rcd_hint_loading;
    @BindView(R.id.voice_rcd_hint_rcding)
    LinearLayout voice_rcd_hint_rcding;
    @BindView(R.id.voice_rcd_hint_tooshort)
    LinearLayout voice_rcd_hint_tooshort;
    @BindView(R.id.img1)
    ImageView img1;
    @BindView(R.id.sc_img1)
    ImageView sc_img1;

//	@BindView(R.id.iv)
//	ImageView iv;
    // @BindView(R.id.ivPopUp)
    // private ImageView chatting_mode_btn;

    // @BindView(R.id.volume)
    // private ImageView volume;
    @BindView(R.id.rcChat_popup)
    View rcChat_popup;
    @BindView(R.id.del_re)
    LinearLayout del_re;
    @BindView(R.id.lnearLayout_361)
    LinearLayout lnearLayout_361;
    @BindView(R.id.relativeLayout_bottom)
    RelativeLayout relativeLayout_bottom;
    @BindView(R.id.textView_link)
    TextView textView_link;
    @BindView(R.id.callwatch)
    LinearLayout callwatch;
    @BindView(R.id.monitorwatch)
    LinearLayout monitorwatch;
    @BindView(R.id.call_monitor_watch)
    LinearLayout call_momitor_watch;
    View.OnClickListener view_ll_listener = new View.OnClickListener() {
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.textView_link:
                    Uri uri = Uri.parse("http://www.misafes.com");
                    Intent it = new Intent(Intent.ACTION_VIEW, uri);
                    startActivity(it);
                    break;
                case R.id.button1:
                    SqlDb db = SqlDb.get(getActivity());
                    beanForDb___Family bb = db.getFamilyBy_fid(KidsWatConfig
                            .getDefaultFamilyId());
                    db.closeDb();

                    Intent intent = new Intent(getActivity(), QrcodeActivity.class);
                    intent.putExtra("qrcode", bb.qrcode);
                    intent.putExtra("sim", bb.sim);
                    startActivity(intent);
                    break;
            }
            // menu.showContent();
        }
    };
    OnClickListener callwatchOnClick = new OnClickListener() {

        @Override
        public void onClick(View v) {

            SqlDb db = SqlDb.get(getActivity());
            final beanForDb___Family bb = db.getFamilyBy_fid(KidsWatConfig
                    .getDefaultFamilyId());
            db.closeDb();
            showConfirmDialog(getActivity().getResources().getString(R.string.call), "+" + bb.sim, getActivity().getResources().getString(R.string.call), getActivity().getResources().getString(R.string.cancle), new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface dialog, int which) {
                    Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:"
                            + "+" + bb.sim));
                    startActivity(intent);
                }
            });
        }
    };
    List<Record461> record = null;
    Record461Adapter adapter = null;
    private SoundMeter mSensor;
    private int flag = 1;
    private Handler mxHandler = new Handler();
    private String voiceName;
    private long startVoiceT, endVoiceT;
    private ChatMsgViewAdapter mAdapter;
    private List<ChatMsgEntity> mDataArrays = new ArrayList<ChatMsgEntity>();
    private boolean isShosrt = false;
    private View rootView;
    private Runnable mPollTask = new Runnable() {
        public void run() {
            double amp = mSensor.getAmplitude();
            mxHandler.postDelayed(mPollTask, POLL_INTERVAL);

        }
    };
    private Runnable mSleepTask = new Runnable() {
        public void run() {
            stop();
        }
    };
    private TextView tv_edit;
    private ImageView iv_baby;
    private LinearLayout ll_cancel;
    @SuppressLint("HandlerLeak")
    private Handler handler = new Handler() {

        @Override
        public void handleMessage(Message msg) {

            String mess = msg.obj.toString();
            int code = msg.arg1;
            CustomLog.e(TAG, "mess:" + mess);

            if (mess == null || mess.equals(""))
                return;
            JSONObject json = null;
            try {
                json = new JSONObject(mess);

                int errorCode = json.getInt("error");

                switch (code) {
                    case GETDEVICECALLRECORDCODE:
                        JSONObject infoJson = json.getJSONObject("info");
                        if (errorCode == 0) {
                            record = getRecordlist(infoJson
                                    .getJSONArray("record_list"));

                            adapter = new Record461Adapter(getActivity(), record,
                                    infoJson.getString("kid_name"));
                            isEditMode = true;
                            mListView.setAdapter(adapter);
                            mListView.setSelection(adapter.getCount() - 1);
                            long temp = 0;
                            for (int i = 0; i < record.size() - 1; i++) {
							/*if(record.get(i).getBegin_time() == null){
								CustomLog.d(TAG,"---------- begin time == null , position == "+i+" ---------------");
							}else */
                                if (Long.parseLong(record.get(i).getBegin_time()) - temp > 10 * 60) {
                                    temp = Long.parseLong(record.get(i).getBegin_time());
                                    CustomLog.d(TAG,"time : " + temp);
                                } else {
                                    subTime.add(i);
                                    //		CustomLog.d(TAG,"subTime : "+i +"temp : "+temp);
                                }
                            }
                        }
                        hideWaitDialog();
                        break;
                    case setMonitorCode:
                        if (errorCode == 0) {
                            //获取monitor的状态

                            //打电话
                            SqlDb db = SqlDb.get(getActivity());
                            beanForDb___Family bb = db.getFamilyBy_fid(KidsWatConfig
                                    .getDefaultFamilyId());
                            db.closeDb();
                            Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:"
                                    + "+" + bb.sim));
                            startActivity(intent);
                        }
                        hideWaitDialog();
                        break;
                    case deleteRecord:
                        //TODO
                        if (errorCode == 0) {
                            CustomLog.d(TAG,"delete finish!");
                            selections.clear();
                            subTime.clear();
                            GetRecord();
                            //文字变为Edit
                            tv_edit.setText("Править");
                            //tv_cancel.setVisibility(8);
                            ll_cancel.setVisibility(8);
                            call_momitor_watch.setVisibility(0);
                            isEditMode = true;
                            adapter.notifyDataSetChanged();
                        } else if (errorCode == 1 || errorCode == 101) {
                            //no record selected
                            subTime.clear();
                            GetRecord();
                            //文字变为Edit
                            tv_edit.setText("Править");
                            //tv_cancel.setVisibility(8);
                            ll_cancel.setVisibility(8);
                            call_momitor_watch.setVisibility(0);
                            isEditMode = true;
                            adapter.notifyDataSetChanged();
                        }
                        break;
                }
            } catch (JSONException e) {
                CustomLog.logException(TAG,e);
            }
        }
    };
    //----------461 新版 ---------- 2015-12-30 18:57:47  添加监听按钮
    android.view.View.OnClickListener monitorwatchOnclick = new OnClickListener() {

        @Override
        public void onClick(View v) {
            //点击monitor 发送打开monitor指令
            SqlDb db = SqlDb.get(getActivity());
            beanForDb___Family bb = db.getFamilyBy_fid(KidsWatConfig
                    .getDefaultFamilyId());
//			beanFor___get_watch_info bb = db.getFamilyBy_fid(KidsWatConfig.getDefaultFamilyId());
            showConfirmDialog(getActivity().getResources().getString(R.string.monitor), "+" + bb.sim, getActivity().getResources().getString(R.string.monitor), getActivity().getResources().getString(R.string.cancle), new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface dialog, int which) {
                    //收到手表的monitor为打开，拨打电话监听
                    start_monitor();
                }

            });
        }
    };

    public static boolean isEditMode() {
        return isEditMode;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        if (rootView == null) {
//			rootView = inflater.inflate(R.layout.fragment_record, container,
//					false);
            rootView = inflater.inflate(R.layout.fragment_record2, container,
                    false);
            ButterKnife.bind(this, rootView);
            initData();
            initView(rootView);
        }

        ViewGroup parent = (ViewGroup) rootView.getParent();
        if (parent != null) {
            parent.removeView(rootView);
        }

        return rootView;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        CustomLog.d(TAG,"--------onStart--------");
        setHasOptionsMenu(true);

//		dlImg(AppContext.getInstance().getCurrentFamily().img_path, AppContext.getInstance().getCurrentFamily().family_id);
    }

    @Override
    public void onResume() {
        super.onResume();
        subTime.clear();
        selections.clear();
        tv_edit.setVisibility(View.VISIBLE);
        CustomLog.d(TAG,"--------onResume--------");
        /**
         * 白浩 如果是461
         */
        showView();
    }

    @Override
    public void onPause() {
        super.onPause();
        isEditMode = true;
        tv_edit.setText("Править");
        ll_cancel.setVisibility(View.GONE);
        tv_edit.setVisibility(View.GONE);
        selections.clear();
        subTime.clear();
        CustomLog.d(TAG,"--------onPause--------");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        CustomLog.d(TAG,"--------onDestroy--------");
    }

    @Override
    public void initData() {

    }

    @Override
    public void initView(View view) {
        textView_link.setText(Html.fromHtml("<u>www.lexand.ru</u>"));
        textView_link.setOnClickListener(view_ll_listener);
        tv_edit = (TextView) getActivity().findViewById(R.id.textView_edit);
        ll_cancel = (LinearLayout) getActivity().findViewById(R.id.ll_cancel);
        showView();
        mSensor = new SoundMeter();
        mBtnRcd.setOnTouchListener(new OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                // 按下语音录制按钮时返回false执行父类OnTouch
                return false;
            }
        });

        if (!isEditMode) {
            mListView.setOnItemClickListener(new OnItemClickListener() {

                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    //在编辑模式下
                    if (!isEditMode) {
                        if (selections.contains(record.get(position))) {
                            selections.remove(record.get(position));
                            ImageView iv_left = (ImageView) view.findViewById(R.id.recordchoice_left);
                            ImageView iv_right = (ImageView) view.findViewById(R.id.recordchoice_right);
                            iv_left.setImageResource(R.drawable.recordchoice);
                            iv_right.setImageResource(R.drawable.recordchoice);
                        } else {
                            ImageView iv_left = (ImageView) view.findViewById(R.id.recordchoice_left);
                            ImageView iv_right = (ImageView) view.findViewById(R.id.recordchoice_right);
                            iv_left.setImageResource(R.drawable.recordchoice_s);
                            iv_right.setImageResource(R.drawable.recordchoice_s);
                            selections.add(record.get(position));
                        }
                    }
                }

            });
        }

        ll_cancel.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                if (!isEditMode) {
                    //文字变为Edit
                    tv_edit.setText("Править");
                    isEditMode = true;
                    //tv_cancel.setVisibility(8);
                    ll_cancel.setVisibility(8);
                    selections.clear();
                    adapter.notifyDataSetChanged();
                    call_momitor_watch.setVisibility(0);
                }
            }
        });
        tv_edit.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                CustomLog.d(TAG,"----------onclick---------");
                CharSequence text = tv_edit.getText();
                if ("Править".equalsIgnoreCase(text.toString())) {
                    //文字变为Delete
                    call_momitor_watch.setVisibility(View.GONE);
                    tv_edit.setText("Удалить");
                    //tv_cancel.setVisibility(View.VISIBLE);
                    ll_cancel.setVisibility(0);
                    isEditMode = false;
                    adapter.notifyDataSetChanged();

                } else if ("Удалить".equalsIgnoreCase(text.toString())) {

                    //删除选中的记录
                    String URL = KidsWatApiUrl.getApiUrl();
                    String StrJson = CommonThreadMethod.JsonFormat(SetStateParameter("w461_delete_device_call_record_by_record_id_v1"));
                    if (selections.size() == 0) {
                        showConfirmInformation("Please select Record", null);
                    } else {
                        new Thread(new ThreadParent(TAG,URL, StrJson, handler, deleteRecord)).start();
                        selections.clear();
                        showWaitDialog();
                    }

                }

            }
        });

        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
    }

    public void showConfirmInformation(String title, String message) {
        if (title == null)
            message = "\n" + message;
        new AlertDialog.Builder(getActivity()).setMessage(message).setTitle(title)
                .setPositiveButton("OK", null).show();
    }

    private void showView() {
//		AppContext.getInstance().getCurrentFamily().version = 361+"";
        CustomLog.d(TAG,"version:" + AppContext.getInstance().getCurrentFamily());
        if (AppContext.getInstance().getCurrentFamily() != null) {
            String version = AppContext.getInstance().getCurrentFamily().version;
            if ("361".equalsIgnoreCase(version)) {
                mListView.setVisibility(View.GONE);
                relativeLayout_bottom.setVisibility(View.GONE);
                lnearLayout_361.setVisibility(View.VISIBLE);
//				callwatch.setVisibility(8);
                tv_edit.setVisibility(8);
                ll_cancel.setVisibility(8);
                call_momitor_watch.setVisibility(8);
            } else if (version.equalsIgnoreCase("461") || version.startsWith("561")) {

                TextView tv_title = (TextView) getActivity().findViewById(R.id.textView_title);
                if (tv_title.getText().toString().equals(getApplication().getResources().getString(R.string.main_tab_name_recent))) {
                    tv_edit.setVisibility(0);
                }
                call_momitor_watch.setVisibility(0);
                if (AppContext.getInstance().getLoginUser_kid() != null)
                    set461Data();
            }
        } else {
            mListView.setVisibility(View.GONE);
            relativeLayout_bottom.setVisibility(View.GONE);
            lnearLayout_361.setVisibility(View.VISIBLE);
        }
    }

    private String getDate() {
        Calendar c = Calendar.getInstance();

        String year = String.valueOf(c.get(Calendar.YEAR));
        String month = String.valueOf(c.get(Calendar.MONTH));
        String day = String.valueOf(c.get(Calendar.DAY_OF_MONTH) + 1);
        String hour = String.valueOf(c.get(Calendar.HOUR_OF_DAY));
        String mins = String.valueOf(c.get(Calendar.MINUTE));

        StringBuffer sbBuffer = new StringBuffer();
        sbBuffer.append(year + "-" + month + "-" + day + " " + hour + ":"
                + mins);

        return sbBuffer.toString();
    }

    // 按下语音录制按钮时
    public boolean onTouchEvent(MotionEvent event) {

        if (!Environment.getExternalStorageDirectory().exists()) {
            showLongToast("No SDCard");
            return false;
        }

        CustomLog.d(TAG,"1");
        int[] location = new int[2];
        mBtnRcd.getLocationInWindow(location); // 获取在当前窗口内的绝对坐标
        int btn_rc_Y = location[1];
        int btn_rc_X = location[0];
        int[] del_location = new int[2];
        del_re.getLocationInWindow(del_location);
        int del_Y = del_location[1];
        int del_x = del_location[0];
        if (event.getAction() == MotionEvent.ACTION_DOWN && flag == 1) {
            if (!Environment.getExternalStorageDirectory().exists()) {
                showLongToast("No SDCard");
                return false;
            }
            CustomLog.d(TAG,"2");
            if (event.getY() > btn_rc_Y && event.getX() > btn_rc_X) {// 判断手势按下的位置是否是语音录制按钮的范围内
                CustomLog.d(TAG,"3");
                mBtnRcd.setBackgroundResource(R.drawable.voice_rcd_btn_pressed);
                rcChat_popup.setVisibility(View.VISIBLE);
                voice_rcd_hint_loading.setVisibility(View.VISIBLE);
                voice_rcd_hint_rcding.setVisibility(View.GONE);
                voice_rcd_hint_tooshort.setVisibility(View.GONE);
                mxHandler.postDelayed(new Runnable() {
                    public void run() {
                        if (!isShosrt) {
                            voice_rcd_hint_loading.setVisibility(View.GONE);
                            voice_rcd_hint_rcding.setVisibility(View.VISIBLE);
                        }
                    }
                }, 300);
                img1.setVisibility(View.VISIBLE);
                del_re.setVisibility(View.GONE);
                startVoiceT = System.currentTimeMillis();// SystemClock.currentThreadTimeMillis();
                voiceName = startVoiceT + ".amr";
                start(voiceName);
                flag = 2;
            }
        } else if (event.getAction() == MotionEvent.ACTION_UP && flag == 2) {// 松开手势时执行录制完成
            CustomLog.d(TAG,"4");
            mBtnRcd.setBackgroundResource(R.drawable.voice_rcd_btn_nor);
            if (event.getY() >= del_Y
                    && event.getY() <= del_Y + del_re.getHeight()
                    && event.getX() >= del_x
                    && event.getX() <= del_x + del_re.getWidth()) {
                rcChat_popup.setVisibility(View.GONE);
                img1.setVisibility(View.VISIBLE);
                del_re.setVisibility(View.GONE);
                stop();
                flag = 1;
                File file = new File(
                        android.os.Environment.getExternalStorageDirectory()
                                + "/" + voiceName);
                if (file.exists()) {
                    file.delete();
                }
            } else {

                voice_rcd_hint_rcding.setVisibility(View.GONE);
                stop();
                endVoiceT = System.currentTimeMillis();// SystemClock.currentThreadTimeMillis();
                flag = 1;
                int time = (int) ((endVoiceT - startVoiceT) / 1000);
                if (time < 5) {
                    isShosrt = true;
                    voice_rcd_hint_loading.setVisibility(View.GONE);
                    voice_rcd_hint_rcding.setVisibility(View.GONE);
                    voice_rcd_hint_tooshort.setVisibility(View.VISIBLE);
                    mxHandler.postDelayed(new Runnable() {
                        public void run() {
                            voice_rcd_hint_tooshort.setVisibility(View.GONE);
                            rcChat_popup.setVisibility(View.GONE);
                            isShosrt = false;
                        }
                    }, 500);
                    return false;
                }
                ChatMsgEntity entity = new ChatMsgEntity();
                entity.setDate(getDate());
                entity.setName("高富帅");
                entity.setMsgType(false);
                entity.setTime(time + "\"");
                entity.setText(voiceName);
                mDataArrays.add(entity);
                mAdapter.notifyDataSetChanged();
                mListView.setSelection(mListView.getCount() - 1);
                rcChat_popup.setVisibility(View.GONE);

            }
        }
        if (event.getY() < btn_rc_Y) {// 手势按下的位置不在语音录制按钮的范围内
            CustomLog.d(TAG,"5");
            Animation mLitteAnimation = AnimationUtils.loadAnimation(
                    getActivity(), R.anim.cancel_rc);
            Animation mBigAnimation = AnimationUtils.loadAnimation(
                    getActivity(), R.anim.cancel_rc2);
            img1.setVisibility(View.GONE);
            del_re.setVisibility(View.VISIBLE);
            del_re.setBackgroundResource(R.drawable.voice_rcd_cancel_bg);
            if (event.getY() >= del_Y
                    && event.getY() <= del_Y + del_re.getHeight()
                    && event.getX() >= del_x
                    && event.getX() <= del_x + del_re.getWidth()) {
                del_re.setBackgroundResource(R.drawable.voice_rcd_cancel_bg_focused);
                sc_img1.startAnimation(mLitteAnimation);
                sc_img1.startAnimation(mBigAnimation);
            }
        } else {

            img1.setVisibility(View.VISIBLE);
            del_re.setVisibility(View.GONE);
            del_re.setBackgroundResource(0);
        }
        return false;// super.onTouchEvent(event);
    }

    private void start(String name) {
        mSensor.start(name);
        mxHandler.postDelayed(mPollTask, POLL_INTERVAL);
    }

    private void stop() {
        mxHandler.removeCallbacks(mSleepTask);
        mxHandler.removeCallbacks(mPollTask);
        mSensor.stop();
    }

    public void set461Data() {

        mListView.setVisibility(0);
        lnearLayout_361.setVisibility(8);
        call_momitor_watch.setVisibility(0);
        //----------461 新版 ---------- 2015-12-30 18:56:15 添加监听按钮
        //callwatch.removeAllViews();
        //callwatch.setBackgroundResource(R.drawable.call_btn);
        callwatch.setOnClickListener(callwatchOnClick);
        monitorwatch.setOnClickListener(monitorwatchOnclick);
        GetRecord();
    }

    private void start_monitor() {
        // TODO Auto-generated method stub
        String url = null;
        String action = "active_monitor_mode";
        new Thread(new ThreadParent(TAG,KidsWatApiUrl.getApiUrl(),
                CommonThreadMethod.JsonFormat(SetStateParameter(action)),
                handler, setMonitorCode)).start();
    }

    private Map<String, String> SetStateParameter(String action) {
        Map hash = null;
        switch (action) {
            case "active_monitor_mode":
                Map<String, String> monitorhash = new HashMap<String, String>();
                monitorhash.put("uid", KidsWatConfig.getUserUid());
                monitorhash.put("device_id", KidsWatConfig.getUserDeviceId());
                monitorhash.put("access_token", KidsWatConfig.getUserToken());
                KidsWatApiUrl.addExtraPara(action, monitorhash);
                hash = monitorhash;
                break;
            case "w461_delete_device_call_record_by_record_id_v1":
                Map<String, String> delete = new HashMap<String, String>();
                delete.put("uid", KidsWatConfig.getUserUid());
                delete.put("device_id", KidsWatConfig.getUserDeviceId());
                delete.put("access_token", KidsWatConfig.getUserToken());
                StringBuffer sbDelete = new StringBuffer();
                for (Record461 bean : selections) {
                    sbDelete.append(bean.getMessage_id() + " ");
                }
                String delete_recode = sbDelete.toString().trim().replace(" ", ",");
                CustomLog.d(TAG,"deleterecorde : " + delete_recode);
                delete.put("record_id_list", delete_recode);

                KidsWatApiUrl.addExtraPara(action, delete);
                hash = delete;
                break;
            default:
                break;
        }
        return hash;
    }

//	private TextView tv_cancel;

    /**
     * 获取记录
     */
    public void GetRecord() {

        showWaitDialog();

        new Thread(new ThreadParent(TAG,KidsWatApiUrl.getApiUrl(),
                CommonThreadMethod.JsonFormat(GetRecordParameter()), handler,
                GETDEVICECALLRECORDCODE)).start();
    }

    /*
     * 参数
     */
    public Map<String, String> GetRecordParameter() {
        Map<String, String> hash = new HashMap<String, String>();
        hash.put("uid", KidsWatConfig.getUserUid());
        hash.put("device_id", KidsWatConfig.getUserDeviceId());
        hash.put("access_token", KidsWatConfig.getUserToken());

        //
        hash.put("page", 0 + "");
        hash.put("page_size", 0 + "");

        KidsWatApiUrl.addExtraPara(GETDEVICECALLRECORDACTION, hash);

        return hash;
    }

    /*
     * 格式化记录数据
     */

    public List<Record461> getRecordlist(JSONArray StrArray) {
        List<Record461> recordList = new ArrayList<Record461>();
        if (StrArray.length() == 0)
            return recordList;
        Record461 record = null;
        for (int i = 0; i < StrArray.length(); i++) {
            record = new Record461();
            JSONObject json;
            try {
                json = StrArray.getJSONObject(i);

                record.setBegin_time(json.getString("begin_time"));
                record.setCall_type(json.getInt("call_type"));
                record.setLast_time(json.getString("last_time"));
                record.setMessage_id(json.getString("message_id"));
                record.setTime(json.getString("time"));
                record.setRelation(json.getString("relation"));
                record.setType(0);
                recordList.add(record);
            } catch (JSONException e) {
                CustomLog.logException(TAG,e);
            }
        }

        record = new Record461();
        record.setCall_type(119);
        recordList.add(record);

        return recordList;
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEventMainThread(BaseEvent event) {
        CustomLog.d(TAG,"------onEventMainThread----" + event.getMsg()+" of type "+event.getType()+" aka "+event.getTypeAsString());

        int type = event.getType();
        String msg = event.getMsg();

        if (type == BaseEvent.MSGTYPE_6___WHITE_TO_FRONT) {
            CustomLog.e(TAG, "我收到了 - 我收到了");
            showView();
        }
    }
}
