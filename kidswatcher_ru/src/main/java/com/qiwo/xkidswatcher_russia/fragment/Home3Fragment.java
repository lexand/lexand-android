package com.qiwo.xkidswatcher_russia.fragment;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.media.MediaPlayer.OnPreparedListener;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import android.util.DisplayMetrics;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.qiwo.xkidswatcher_russia.AnalyticsUtils.AnalyticsUtils;
import com.qiwo.xkidswatcher_russia.AppContext;
import com.qiwo.xkidswatcher_russia.KidsWatConfig;
import com.qiwo.xkidswatcher_russia.R;
import com.qiwo.xkidswatcher_russia.Utils;
import com.qiwo.xkidswatcher_russia.api.ApiHttpClient;
import com.qiwo.xkidswatcher_russia.api.remote.KidsWatApiUrl;
import com.qiwo.xkidswatcher_russia.base.BaseFragment;
import com.qiwo.xkidswatcher_russia.bean.Constants;
import com.qiwo.xkidswatcher_russia.bean.beanForDb___Family;
import com.qiwo.xkidswatcher_russia.bean.beanFor___get_watch_data;
import com.qiwo.xkidswatcher_russia.bean.beanFor___get_watch_data.CRows;
import com.qiwo.xkidswatcher_russia.bean.beanFor___get_watch_data_latest;
import com.qiwo.xkidswatcher_russia.db.SqlDb;
import com.qiwo.xkidswatcher_russia.event.BaseEvent;
import com.qiwo.xkidswatcher_russia.thread.CommonThreadMethod;
import com.qiwo.xkidswatcher_russia.thread.ThreadParent;
import com.qiwo.xkidswatcher_russia.ui.MainActivity;
import com.qiwo.xkidswatcher_russia.ui.SleepModeSpeciActivity;
import com.qiwo.xkidswatcher_russia.util.Base64Encode;
import com.qiwo.xkidswatcher_russia.util.KidsWatUtils;
import com.qiwo.xkidswatcher_russia.util.StringUtils;
import com.yandex.mapkit.Animation;
import com.yandex.mapkit.GeoObjectCollection;
import com.yandex.mapkit.MapKitFactory;
import com.yandex.mapkit.geometry.Point;
import com.yandex.mapkit.map.CameraPosition;
import com.yandex.mapkit.map.MapObjectCollection;
import com.yandex.mapkit.mapview.MapView;

import net.intari.CustomLogger.CustomLog;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONException;
import org.json.JSONObject;
import org.kymjs.kjframe.http.HttpCallBack;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.ButterKnife;
import butterknife.BindView;;
import ru.yandex.yandexmapkit.MapController;
import ru.yandex.yandexmapkit.MapView;
import ru.yandex.yandexmapkit.OverlayManager;
import ru.yandex.yandexmapkit.map.MapEvent;
import ru.yandex.yandexmapkit.map.OnMapListener;
import ru.yandex.yandexmapkit.overlay.Overlay;
import ru.yandex.yandexmapkit.overlay.OverlayItem;
import ru.yandex.yandexmapkit.utils.GeoPoint;
import ru.yandex.yandexmapkit.utils.ScreenPoint;

import static com.qiwo.xkidswatcher_russia.Constants.ADDRESS;
import static com.qiwo.xkidswatcher_russia.Constants.CHARGE;
import static com.qiwo.xkidswatcher_russia.Constants.DEVICE_ID;
import static com.qiwo.xkidswatcher_russia.Constants.ERRORCODE;
import static com.qiwo.xkidswatcher_russia.Constants.EVENT_EXCEPTION;
import static com.qiwo.xkidswatcher_russia.Constants.EVENT_GET_LOCATION_ERROR;
import static com.qiwo.xkidswatcher_russia.Constants.EVENT_GET_LOCATION_ERROR_DEVICE_SLEEPS;
import static com.qiwo.xkidswatcher_russia.Constants.EVENT_GET_LOCATION_EXCEPTION;
import static com.qiwo.xkidswatcher_russia.Constants.EVENT_GET_LOCATION_EX_ERROR;
import static com.qiwo.xkidswatcher_russia.Constants.EVENT_GET_LOCATION_EX_ERROR_DEVICE_SLEEPS;
import static com.qiwo.xkidswatcher_russia.Constants.EVENT_GET_LOCATION_EX_EXCEPTION;
import static com.qiwo.xkidswatcher_russia.Constants.EVENT_GET_LOCATION_EX_SERVER_ERROR;
import static com.qiwo.xkidswatcher_russia.Constants.EVENT_GET_LOCATION_EX_START;
import static com.qiwo.xkidswatcher_russia.Constants.EVENT_GET_LOCATION_EX_SUCCESS;
import static com.qiwo.xkidswatcher_russia.Constants.EVENT_GET_LOCATION_SERVER_ERROR;
import static com.qiwo.xkidswatcher_russia.Constants.EVENT_GET_LOCATION_START;
import static com.qiwo.xkidswatcher_russia.Constants.EVENT_GET_LOCATION_SUCCESS;
import static com.qiwo.xkidswatcher_russia.Constants.EVENT_GET_WATCH_DATA_ERROR;
import static com.qiwo.xkidswatcher_russia.Constants.EVENT_GET_WATCH_DATA_EXCEPTION;
import static com.qiwo.xkidswatcher_russia.Constants.EVENT_GET_WATCH_DATA_NO_RESULT;
import static com.qiwo.xkidswatcher_russia.Constants.EVENT_GET_WATCH_DATA_START;
import static com.qiwo.xkidswatcher_russia.Constants.EVENT_GET_WATCH_DATA_SUCCESS;
import static com.qiwo.xkidswatcher_russia.Constants.EVENT_STRANGE_INTERNAL_ISSUE;
import static com.qiwo.xkidswatcher_russia.Constants.FAMILIYID;
import static com.qiwo.xkidswatcher_russia.Constants.GEOPOINT;
import static com.qiwo.xkidswatcher_russia.Constants.LOCATION;
import static com.qiwo.xkidswatcher_russia.Constants.LOCATION_TYPE;
import static com.qiwo.xkidswatcher_russia.Constants.LOG_GET_LOCATION;
import static com.qiwo.xkidswatcher_russia.Constants.LOG_GET_LOCATION_EX;
import static com.qiwo.xkidswatcher_russia.Constants.LOG_GET_WATCH_DATA;
import static com.qiwo.xkidswatcher_russia.Constants.LTIME;
import static com.qiwo.xkidswatcher_russia.Constants.LTIME_FORMATTED;
import static com.qiwo.xkidswatcher_russia.Constants.MESSAGE;
import static com.qiwo.xkidswatcher_russia.Constants.TIME;
import static com.qiwo.xkidswatcher_russia.Constants.UID;
import static com.qiwo.xkidswatcher_russia.Constants.UTCTODAY;
import static com.qiwo.xkidswatcher_russia.Constants.YANDEX_MAP_KIT_KEY;

//import com.qiwo.xkidswatcher_russia.fragment.beanFor___get_watch_data_latest.CInfo.CData;

public class Home3Fragment extends BaseFragment implements OnMapListener {

    final String TAG = Home3Fragment.class.getSimpleName();

    public static final int setMonitorCode = 15989; // 开启监听电话模式
    @BindView(R.id.textView_time)
    TextView mTvTime;
    @BindView(R.id.textView_electricity)
    TextView mTvElectricity;
    @BindView(R.id.textView_recrod)
    TextView mTvRecord;
    @BindView(R.id.textView_location)
    TextView mTvLocation;
    @BindView(R.id.textView_address)
    TextView mTvAddress;
    @BindView(R.id.textView_jd)
    TextView mTvJd;
    @BindView(R.id.textView_range)
    TextView textView_range;
    @BindView(R.id.content_xx)
    FrameLayout frameLayout_conent;
    @BindView(R.id.linearLayout_record)
    LinearLayout linearLayout_record;
    @BindView(R.id.lnearLayout_audio_play)
    LinearLayout lnearLayout_audio_play;
    @BindView(R.id.linearLayout_location)
    LinearLayout linearLayout_location;
    @BindView(R.id.lnearLayout_address)
    LinearLayout lnearLayout_address;
    @BindView(R.id.linearLayout_sleepmode)
    LinearLayout linearLayout_sleepmode;
    @BindView(R.id.linearLayout_electric)
    LinearLayout linearLayout_electric;
    @BindView(R.id.imageView_electricity)
    ImageView imageView_electricity;
    @BindView(R.id.linearLayout_call)
    LinearLayout linearLayout_call;
    @BindView(R.id.linearLayout_monitor)
    LinearLayout linearLayout_monitor;
    @BindView(R.id.mapview)
    MapView mapview;
    MediaPlayer mp = new MediaPlayer();
    boolean isPlaying = false;
    beanFor___get_watch_data bean___watch_data;
    boolean flag;

    long lastLocationUpdate=0L;
    boolean needOneForcedUpdate=false;

    // 缓存 fragment view
    private View rootView;

    private MapObjectCollection mapObjects;
    private GeoObjectCollection geoObjects;

    private MapController mapController;
    private OverlayManager overlayManager;
    private Overlay overlay;
    @SuppressLint("HandlerLeak")
    private Handler handler = new Handler() {

        @Override
        public void handleMessage(Message msg) {

            String mess = msg.obj.toString();
            int code = msg.arg1;
            CustomLog.e(TAG,"mess:" + mess);

            if (mess == null || mess.equals("")) {
                hideWaitDialog();
                return;
            }
            JSONObject json = null;
            try {
                json = new JSONObject(mess);
                int errorCode = json.getInt("error");
                switch (code) {
                    case setMonitorCode:
                        if (errorCode == 0) {
                            // 获取monitor的状态
                            // 打电话
                            SqlDb db = SqlDb.get(getActivity());
                            beanForDb___Family bb = db.getFamilyBy_fid(KidsWatConfig.getDefaultFamilyId());
                            db.closeDb();
                            Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + "+" + bb.sim));
                            startActivity(intent);
                        } else if (errorCode == KidsWatApiUrl.SESSIONTIMEOUT) {
                            KidsWatApiUrl.setTokenTimeOut(getActivity());
                        } else {
                            // showShortToast(json.getJSONObject("info").getString("message"));
                            String message=json.getJSONObject("info").getString("message");
                            CustomLog.w(TAG,"message:"+message+". full json:"+mess);
                            showConfirmInformation(null,
                                    getActivity().getResources().getString(R.string.tip_monitor_fail));
                        }
                        hideWaitDialog();
                        break;
                }
            } catch (JSONException e) {
                CustomLog.logException(TAG,e);
            }
        }
    };
    private long fetch_time_start = 0;
    private AnimationDrawable mAnimationDrawable;

    //	public static int tempCount = 0;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);



    }


    @Override
    public void onDetach() {
        super.onDetach();
        CustomLog.d(TAG,"onDetach");
        if (EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this);
        }
    }

    @Override
    public boolean onBackPressed() {
        CustomLog.d(TAG,"onBackPressed");
        return false;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        CustomLog.d(TAG,"onCreateView");

        if (rootView == null) {
            rootView = inflater.inflate(R.layout.fragment_home2, container,
                    false);
            ButterKnife.bind(this, rootView);
            initData();
            initView(rootView);
        }

        // 缓存的rootView需要判断是否已经被加过parent，
        // 如果有parent需要从parent删除，要不然会发生这个rootview已经有parent的错误。
        ViewGroup parent = (ViewGroup) rootView.getParent();
        if (parent != null) {
            parent.removeView(rootView);
        }

        return rootView;
    }


    @Override
    public void initData() {
        CustomLog.d(TAG,"initData");

    }

    @Override
    public void initView(View view) {
        CustomLog.d(TAG,"initView");
        //注册eventbus
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }

        CustomLog.d(TAG,"Setting MapKit API Key "+Constants.YANDEX_MAP_KIT_KEY);
        MapKitFactory.setApiKey(Constants.YANDEX_MAP_KIT_KEY);
        CustomLog.d(TAG,"Initing MapKit");
        MapKitFactory.initialize(this.getContext());

        //设置监听事件
        linearLayout_record.setOnClickListener(this);
        linearLayout_location.setOnClickListener(this);
        lnearLayout_audio_play.setOnClickListener(this);
        linearLayout_sleepmode.setOnClickListener(this);
        linearLayout_call.setOnClickListener(this);
        linearLayout_monitor.setOnClickListener(this);
        //mapController = mapview.getMapController();
        //overlayManager = mapController.getOverlayManager();
        //overlay = new Overlay(mapController);
        //mapController.setZoomCurrent(12);
        //overlayManager.getMyLocation().setEnabled(true);
        //overlayManager.getMyLocation().clearOverlayItems();
        setMapListener();
        //init map if we arleady have data
        initInitialLocationFromCache();
        initMapOverlay();

        //CustomLog.d(TAG,"initView:mapkit key:"+mapview.getApiKey());



    }


    private void initInitialLocationFromCache() {
        //try to get cached data
        beanFor___get_watch_data cached=AppContext.getInstance().getInternal_bean___watch_data();
        if (cached!=null) {
            try {
                int rows = cached.info.ddata.rows.size();
                CustomLog.d(TAG,String.format("initInitialLocationFromCache:We have %d rows in cached data. using it",rows));

                //let's see if we can found MORE recent one. just for fun at this time.
                CRows latest=cached.info.ddata.rows.get(0);
                for (int i=0;i<rows;i++) {
                    CRows current=cached.info.ddata.rows.get(i);
                    if (current.time>latest.time) {
                        CustomLog.d(TAG, String.format("initInitialLocationFromCache:  cached data %d is from %d aka %s, lat:%f, lon:%f, address:%s, " +
                                        "previous was %d is from %d aka %s, lat:%f, lon:%f, address:%s, so updating ",
                                current.time,
                                Utils.getTimeFromTimestamp(current.time),
                                current.latitude,
                                current.longitude,
                                current.address,
                                latest.time,
                                Utils.getTimeFromTimestamp(latest.time),
                                latest.latitude,
                                latest.longitude,
                                latest.address

                                )

                        );
                        latest=current;
                    }

                }
                bean___watch_data=cached;
                CRows cRows=cached.info.ddata.rows.get(0);

                //do not set initial value - will be set by 'shouldUpdateLocation'
                //lastLocationUpdate=cRows.time;

                CustomLog.d(TAG, String.format("initInitialLocationFromCache: cached data zeroth is from %d aka %s, lat:%f, lon:%f, address:%s, ",
                        cRows.time,
                        Utils.getTimeFromTimestamp(cRows.time),
                        cRows.latitude,
                        cRows.longitude,
                        cRows.address));


                CustomLog.d(TAG, String.format("initInitialLocationFromCache: cached data:  latest is from %d aka %s, lat:%f, lon:%f, address:%s, ",
                        latest.time,
                        Utils.getTimeFromTimestamp(latest.time),
                        latest.latitude,
                        latest.longitude,
                        latest.address));

                if (shouldUpdateLocation("initInitialLocationFromCache",cRows.time)) {
                    updateMarkerInfo(cRows);
                    if (overlay!=null) {
                        add_map_marker(cRows);
                    } else {
                        CustomLog.e(TAG,"overlay is null. shouldn't be!");
                    }

                } else {
                    //TODO:WTF happens? Somebody updated? Well, let's get this 'somebody' update other things. TODO:throw exception?


                    CustomLog.e(TAG,"We should NOT update gui here. Strange. Very strange");
                    String ts=Utils.getTimeFromTimestamp(cRows.time);
                    //String otherText = String.format("%s",
                    //        cRows.type == 1 ? "GPS"
                    //                : "GSM/WIFI");
                    String locationDetails=String.format("lat:%f, lon:%f, %s, charge:%d",cRows.latitude,cRows.longitude,cRows.address,cRows.electricity);
                    String geoPoint=String.format("lat:%f, lon:%f", cRows.latitude, cRows.longitude);

                    String strdate = new java.text.SimpleDateFormat("HH:mm")
                            .format(new java.util.Date(cRows.time * 1000));
                    Map<String,Object> eventProperties=new HashMap<>();

                    eventProperties.put(LOCATION_TYPE,cRows.type);
                    eventProperties.put(TIME,strdate);
                    eventProperties.put(CHARGE,cRows.electricity);
                    eventProperties.put(ADDRESS,cRows.address);
                    eventProperties.put(GEOPOINT,geoPoint);
                    eventProperties.put(LOCATION,locationDetails);
                    eventProperties.put(LTIME,cRows.time);
                    eventProperties.put(LTIME_FORMATTED, ts);
                    eventProperties.put(MESSAGE,"initInitialLocationFromCache:app thinks it should NOT allow gui update - somebody did it arleady");

                    //This event should ONLY be send if I considered sending throw new RuntimeError("...") insted
                    AnalyticsUtils.reportAnalyticsEvent(EVENT_STRANGE_INTERNAL_ISSUE,eventProperties);
                    showLongToast(getActivity().getApplicationContext().getResources().getString(R.string.internal_issue_gui));


                }

            } catch (Exception ex) {
                CustomLog.e(TAG,"initInitialLocationFromCache:Error processing cached data. zeroing");
                CustomLog.logException(TAG,ex);
                bean___watch_data=null;
                resetLastUpdateTime();
                //lastLocationUpdate=0L;
            }

        } else {
            CustomLog.w(TAG,"initInitialLocationFromCache:Cached bean watch data is null and of no help to us");
        }
    }
    private void updateMarkerInfo(CRows row) {
        String otherText = String.format("%s",
                row.type == 1 ? "GPS"
                        : "GSM/WIFI");

        String strdate = new java.text.SimpleDateFormat("HH:mm")
                .format(new java.util.Date(row.time * 1000));
        mTvAddress.setText(row.address);
        mTvTime.setText(strdate);
        textView_range.setText(otherText);
        mTvElectricity.setText(row.electricity + "%");
        setDL(row.electricity);
        if (row.electricity == 0) {
            mTvAddress.setText(getResources().getString(
                    R.string.tip_available));
        }
    }
    @SuppressWarnings("deprecation")
    private void setMapListener() {
        final GestureDetector mGestureDetector = new GestureDetector(new GestureDetector.OnGestureListener() {

            @Override
            public boolean onDown(MotionEvent e) {
                return false;
            }

            @Override
            public void onShowPress(MotionEvent e) {
            }

            @Override
            public boolean onSingleTapUp(MotionEvent e) {

                if (flag) {
                    return false;
                }

                CustomLog.d(TAG,"click listener");

                float screenX = e.getX();
                float screenY = e.getY();

                DisplayMetrics dm = new DisplayMetrics();
                //获取屏幕信息
                getActivity().getWindowManager().getDefaultDisplay().getMetrics(dm);
                int screenHeigh = dm.heightPixels;

                if (screenY < 300 || screenY > screenHeigh - 300) {
                    return false;
                }

                float top = 300;
                ScreenPoint screenPoint = new ScreenPoint(screenX, screenY - top);
                CustomLog.d(TAG,"显示圆 - Display circle");
                setPointMarker();

                GeoPoint geopoint = mapController.getGeoPoint(screenPoint);
                CustomLog.d(TAG,"lat=" + geopoint.getLat() + ",lon=" + geopoint.getLon());

                List<Double> mDistanseList = new ArrayList<Double>();
                double mMinDistanse = 0;
                int i = 0;
                if (bean___watch_data == null || bean___watch_data.info == null || bean___watch_data.info.ddata == null || bean___watch_data.info.ddata.rows == null || bean___watch_data.info.ddata.rows.size() == 0) {
                    return false;
                }
                final CRows marker = bean___watch_data.info.ddata.rows.get(0);
                double lat = marker.latitude;
                double lon = marker.longitude;
                if (Math.abs(lat - geopoint.getLat()) < 0.02 && Math.abs(lon - geopoint.getLon()) < 0.02) {
                    Bitmap drawable;
                    drawable = KidsWatUtils.getBabyImg_v3(getActivity(), AppContext.getInstance().getCurrentFamily().family_id,
                            AppContext.getInstance().getCurrentFamily().sex, AppContext.getInstance().getCurrentFamily());
                    OverlayItem overlayItem = (OverlayItem) overlay.getOverlayItems().get(0);
                    mapController.setPositionAnimationTo(overlayItem.getGeoPoint());
                    overlayItem.setDrawable(new BitmapDrawable(getActivity().getResources(), drawable));

                    //显示信息在linearlayout中
                    long ltime = marker.time;
                    int location_type = marker.type;
                    final String strdate = new java.text.SimpleDateFormat("HH:mm")
                            .format(new java.util.Date(ltime * 1000));
                    final String otherText = String.format("%s",
                            location_type == 1 ? "GPS"
                                    : "GSM/WIFI");
                    getActivity().runOnUiThread(new Runnable() {
                        public void run() {
                            CustomLog.d(TAG,"on marker" + "temp" + " click");
                            mTvAddress.setText(marker.address);
                            mTvTime.setText(strdate);
                            textView_range.setText(otherText);
                            mTvElectricity.setText(marker.electricity + "%");
                            setDL(marker.electricity);
                            if (marker.electricity == 0) {
                                mTvAddress.setText(getResources().getString(
                                        R.string.tip_available));
                            }
                        }
                    });
                } else {
                    if (overlay.getOverlayItems().size()>0) {
                        OverlayItem overlayItem = (OverlayItem) overlay.getOverlayItems().get(0);
                        mapController.setPositionAnimationTo(overlayItem.getGeoPoint());
                    } else {
                        CustomLog.w(TAG,"clicked on map but no overlay items were present. just NOT CRASH. lastLocationUpdate:"+lastLocationUpdate+", aka:"+Utils.getTimeFromTimestamp(lastLocationUpdate));
                    }
                }

				/*//得到距离点击位置最近的点的距离集合
                if(bean___watch_data == null){
					return false;
				}

				for (final beanFor___get_watch_data.CRows row : bean___watch_data.info.ddata.rows) {
					GeoPoint scnPoint = new GeoPoint(row.latitude,row.longitude);
					double scnX = mapController.getScreenPoint(scnPoint).getX();
					double scnY = mapController.getScreenPoint(scnPoint).getY();
					double difX = Math.abs(scnX - screenX);
					double difY = Math.abs(scnY - screenY) - 250;
					if(difX < 100 && difY < 100){
						double distanse = Math.sqrt(Math.pow(difX, 2.0)+Math.pow(difY,2.0));
						mDistanseList.add(distanse);
					}

				}

				if(mDistanseList.size()!=0){
					Collections.sort(mDistanseList);
					mMinDistanse  = mDistanseList.get(0);
				}

				//点击标注点
				for (final beanFor___get_watch_data.CRows row : bean___watch_data.info.ddata.rows) {
					final double lat = row.latitude;
					final double lon = row.longitude;
//					double difX = lat-geoPoint.getLat();
//					double difY = lon-geoPoint.getLon();
					GeoPoint scnPoint = new GeoPoint(row.latitude,row.longitude);
					double scnX = mapController.getScreenPoint(scnPoint).getX();
					double scnY = mapController.getScreenPoint(scnPoint).getY();
					double difX = Math.abs(scnX - screenX);
					double difY = Math.abs(scnY - screenY) - 250;

					if(mMinDistanse == Math.sqrt(Math.pow(difX, 2.0)+Math.pow(difY,2.0))){
						//显示圆
						//显示头像
						List<OverlayItem> overlayItems = overlay.getOverlayItems();
						for(OverlayItem tempOverlayItem : overlayItems){
							if(tempOverlayItem.getGeoPoint().getLat() == lat && tempOverlayItem.getGeoPoint().getLon()==lon){
//								showCircle(scnX,scnY);
								int sex = KidsWatUtils.getBabySex();
								int resSex = sex == 1 ? R.drawable.icon_boy : R.drawable.icon_girl;
								tempOverlayItem.setDrawable(new BitmapDrawable(getApplication().getResources(),KidsWatUtils.getBabyImg_v2(getActivity(), AppContext.getInstance().getCurrentFamily().family_id, AppContext.getInstance().getCurrentFamily().sex)));
								mapController.setPositionAnimationTo(tempOverlayItem.getGeoPoint());
							}
						}
						//显示信息在linearlayout中
						long ltime = row.time;
						final int location_type = row.type;
						final String strdate = new java.text.SimpleDateFormat("HH:mm")
								.format(new java.util.Date(ltime * 1000));
						final String otherText = String.format("%s",
								location_type == 1 ? "GPS"
										: "GSM/WIFI");
						getActivity().runOnUiThread(new Runnable() {
							public void run() {
								mTvAddress.setText(row.address);

								mTvTime.setText(strdate);
								textView_range.setText(otherText);
								mTvElectricity.setText(row.electricity + "%");
								setDL(row.electricity);
								if (row.electricity == 0) {
									mTvAddress.setText(getResources().getString(
											R.string.tip_available));
								}
							}
						});
						break;
					}else{
						GeoPoint center = mapController.getMapCenter();
						mapController.setPositionAnimationTo(mapController.getMapCenter());
					}
				}*/
                return false;
            }

            @Override
            public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
                return false;
            }

            @Override
            public void onLongPress(MotionEvent e) {
            }

            @Override
            public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
                return false;
            }
        });
        MainActivity.MyOnTouchListener myOnTouchListener = new MainActivity.MyOnTouchListener() {
            @Override
            public boolean onTouch(MotionEvent ev) {
                boolean result = mGestureDetector.onTouchEvent(ev);
                return result;
            }
        };

        ((MainActivity) getActivity()).registerMyOnTouchListener(myOnTouchListener);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEventMainThread(BaseEvent event) {
        CustomLog.d(TAG,"------onEventMainThread----" + event.getMsg()+" of type "+event.getType()+" aka "+event.getTypeAsString());

        int type = event.getType();
        String msg = event.getMsg();

        if (type == BaseEvent.MSGTYPE_3___LOAD_FAMILY_FIRST_ITEM) {
            CustomLog.d(TAG,"Load Family First Item message");
            get_device_runing_status();
            get_watch_data();
            get_location_ex(true);//true
        } else if (type == BaseEvent.MSGTYPE_3___BACKGROUND_TO_FRONT) {
            CustomLog.d(TAG,"Background To Front (TODO:WTF?) message");
            get_watch_data();
            get_location(true);
        } else if (type == BaseEvent.MSGTYPE_3___CHANGE_DEVICE_MODE) {
            showSleepMode();
        } else if (type == BaseEvent.MSGTYPE_6___WHITE_HOME_VOICE_PUSH) {
            String[] str = event.getMsg().split(",");
            if (str.length == 2)
                if (str[1].equals(KidsWatConfig.getUserDeviceId())) {
                    fetch_voice_by_voice_id(str[0], 7, str[1], 1);
                }
        } else if (type == BaseEvent.MSGTYPE_3___CHANGE_USER) {
            //Смена текущего юзера (в смысле ребенка)
            CustomLog.d(TAG,"Change user message...will reset last time marker");
            lnearLayout_audio_play.setVisibility(View.GONE);
            //lastLocationUpdate=0L;
            resetLastUpdateTime();

            CustomLog.d(TAG,"Change user message...asking for device status");

            get_device_runing_status();
            CustomLog.d(TAG,"Change user message...asking for watch data");
            get_watch_data();
//			get_location(true);
            //форсмируем не-фоновое обновление полное
            //а не надо - надо было get_watch_data вправить мозги
            //CustomLog.d(TAG,"Change user message...asking for non-background forced full location update");
            //makeOneForcedUpdate();//нам надо и карту тоже обновить, это -
            //get_location_ex(false);

        } else if (type == BaseEvent.MSGTYPE_3___GCM_LOCATION) {
            CustomLog.d(TAG, "GCM_LOCATION");
            try {
                // 收到定位成功的push
                get_watch_data_lastest_step();
                dismissDialog(getString(R.string.home_fragment_hint), 2000);

                JSONObject xjson = new JSONObject(msg);
                JSONObject detail = xjson.getJSONObject("detail");
                String address = detail.getString("address");
                double latitude = detail.getDouble("latitude");
                double longitude = detail.getDouble("longitude");
                long time = detail.getLong("time");
                int location_type = detail.getInt("location_type");

                int electricityxx = 100;
                if (bean___watch_data != null
                        && bean___watch_data.info.ddata.rows.size() > 0) {
                    electricityxx = bean___watch_data.info.ddata.rows.get(bean___watch_data.info.ddata.rows.size() - 1).electricity;
                }

                if (shouldUpdateLocation("GCM_Location",time)) {

                    beanFor___get_watch_data.CRows row = new beanFor___get_watch_data.CRows();
                    row.address = address;
                    row.time = time;
                    row.latitude = latitude;
                    row.longitude = longitude;
                    row.electricity = electricityxx;
                    row.type = location_type;


                    String otherText = String.format("%s",
                            location_type == 1 ? "GPS"
                                    : "GSM/WIFI");

                    String strdate = new java.text.SimpleDateFormat("HH:mm")
                            .format(new java.util.Date(time * 1000));


                    String ts=Utils.getTimeFromTimestamp(row.time);


                    String locationDetails=String.format("lat:%f, lon:%f, %s, charge:%d",latitude,longitude,address,row.electricity);
                    String geoPoint=String.format("lat:%f, lon:%f",latitude,longitude);
                    CustomLog.d(TAG,StringUtils.unescapeString("gcm_location:, strdate:"+strdate+",ltime:"+row.time+", time_formatted:"+ts+
                            String.format(", lat:%f, lon:%f, adddr:%s",row.latitude,row.longitude,row.address)));

                    Map<String,Object> eventProperties=new HashMap<>();

                    eventProperties.put(LOCATION_TYPE,otherText);
                    eventProperties.put(TIME,strdate);
                    eventProperties.put(CHARGE,row.electricity);
                    eventProperties.put(ADDRESS,address);
                    eventProperties.put(GEOPOINT,geoPoint);
                    eventProperties.put(LOCATION,locationDetails);
                    eventProperties.put(LTIME,row.time);
                    eventProperties.put(LTIME_FORMATTED, ts);


                    if (LOG_GET_LOCATION) {
                        AnalyticsUtils.reportAnalyticsEvent(EVENT_GET_LOCATION_SUCCESS,eventProperties);
                    }


                    mTvAddress.setText(address);
                    mTvTime.setText(strdate);
                    textView_range.setText(otherText);
                    mTvElectricity.setText(electricityxx + "%");
                    setDL(electricityxx);
                    if (electricityxx == 0) {
                        mTvAddress.setText(getResources().getString(
                                R.string.tip_available));
                    }

                    CustomLog.d(TAG,"set / show lastLocation");
                    setLastLocation(row);
                    showLastLocation();
                }


            } catch (JSONException e) {
                CustomLog.logException(TAG,e);
            }
        } else if (type == BaseEvent.MSGTYPE_3_SHOW_RECORD) {
            CustomLog.d(TAG,"--msg-->:" + msg);
            if (msg.equals("361")) {
                linearLayout_call.setVisibility(View.GONE);
                linearLayout_monitor.setVisibility(View.GONE);
            } /*else if(msg.equals("461") || msg.equals("5612") || msg.equals("5613")){
				linearLayout_record.setVisibility(View.GONE);
			} */ else if (msg.equals("601")) {
                linearLayout_call.setVisibility(View.VISIBLE);
                linearLayout_monitor.setVisibility(View.VISIBLE);

            } else {
                linearLayout_call.setVisibility(View.GONE);
                linearLayout_monitor.setVisibility(View.GONE);
            }
        }
    }

    private void get_device_runing_status() {

        // --------------------
        final String uid = KidsWatConfig.getUserUid();
        final String access_token = KidsWatConfig.getUserToken();
        final String device_id = KidsWatConfig.getUserDeviceId();

        final String request_url = KidsWatApiUrl
                .getUrlFor___get_device_runing_status(uid, access_token,
                        device_id);
        // -------------------------

        ApiHttpClient.get(request_url, new HttpCallBack() {

            @Override
            public void onPreStart() {
                // showWaitDialog("...loading...");
            }

            @Override
            public void onSuccess(String t) {
                super.onSuccess(t);
                CustomLog.d(TAG,"get_device_runing_status.response=" +  t);
                try {
                    JSONObject response = new JSONObject(t);

                    int code = response.getInt("error");

                    // 返回值:
                    // -1 --- 参数错误 Parameter error
                    // 0 --- 正常模式 normal mode
                    // 2015001 --- 飞行模式 Flight mode


                    //if (code == 0) {
                    //TODO:решить как обрабатывать!
                    if (((code == 0) ||(code==-1))) {
                        SqlDb dbx = SqlDb.get(getActivity());
                        dbx.setDeviceModeBy_did(device_id, 1);
                        AppContext.getInstance().setCurrentFamily(dbx
                                .getFamilyBy_did(device_id));
                        dbx.closeDb();

                        showSleepMode();
                    } else if (code == 2015001) {
                        SqlDb dbx = SqlDb.get(getActivity());
                        dbx.setDeviceModeBy_did(device_id, 2);
                        AppContext.getInstance().setCurrentFamily( dbx
                                .getFamilyBy_did(device_id));
                        dbx.closeDb();
                        showSleepMode();
                    }

                } catch (JSONException e) {
                    CustomLog.logException(TAG,e);
                }
            }

            @Override
            public void onFailure(int errorNo, String strMsg) {
                super.onFailure(errorNo, strMsg);
                String msg = String.format("errorNo:%s\n%s", errorNo, strMsg);
                CustomLog.d(TAG,"error=" + msg);
            }

            @Override
            public void onFinish() {
                dismissDialog(null, 10);
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.linearLayout_record:
                // showToast("linearLayout_record");
                exec_voice_record();
                break;
            case R.id.linearLayout_sleepmode:
                // showToast("linearLayout_record");
                Intent mIntent = new Intent();
                mIntent.setClass(getActivity(), SleepModeSpeciActivity.class);
                startActivity(mIntent);
                break;
            case R.id.lnearLayout_audio_play:
                String audio_file = lnearLayout_audio_play.getTag() == null ? ""
                        : lnearLayout_audio_play.getTag().toString();
                if (audio_file.length() > 0 && !isPlaying) {
                    lnearLayout_audio_play
                            .setBackgroundResource(R.anim.record_play_animations);
                    mAnimationDrawable = (AnimationDrawable) lnearLayout_audio_play
                            .getBackground();
                    mAnimationDrawable.start();

                    doPlayVoice(audio_file);
                } else if (audio_file.length() > 0 && isPlaying) {
                    mAnimationDrawable.stop();

                    mp.stop();
                    mp.release();
                    mp = null;
                    isPlaying = false;
                } else {
                    showLongToast(getActivity().getApplicationContext().getResources().getString(R.string.tip_norecord));
                }
                break;
            case R.id.linearLayout_monitor:

                warpperCallPhonePermission();
                monitor();
                break;
            case R.id.linearLayout_call:

                warpperCallPhonePermission();
                call();
                break;
            case R.id.linearLayout_location:
                get_location_ex(false);
                break;
        }
    }

    private void call() {
        // TODO Auto-generated method stub
        SqlDb db2 = SqlDb.get(getActivity());
        final beanForDb___Family bb2 = db2.getFamilyBy_fid(KidsWatConfig.getDefaultFamilyId());
        db2.closeDb();
        showConfirmDialog(getActivity().getResources().getString(R.string.Call), "+" + bb2.sim,
                getActivity().getResources().getString(R.string.Call_hint),
                getActivity().getResources().getString(R.string.cancle), new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // TODO Auto-generated method stub
                        Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + "+" + bb2.sim));
                        startActivity(intent);
                    }
                });
    }

    private void monitor() {
        // TODO Auto-generated method stub
        SqlDb db = SqlDb.get(getActivity());
        final beanForDb___Family bb = db.getFamilyBy_fid(KidsWatConfig.getDefaultFamilyId());
        db.closeDb();
        showConfirmDialog(getActivity().getResources().getString(R.string.Monitor_hint), "+" + bb.sim,
                getActivity().getResources().getString(R.string.Call_hint),
                getActivity().getResources().getString(R.string.cancle), new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // TODO Auto-generated method stub
                        start_monitor();
                    }
                });
    }

    private void warpperCallPhonePermission() {
        // TODO Auto-generated method stub
        if (Build.VERSION.SDK_INT >= 23) {
            if (ContextCompat.checkSelfPermission(getActivity(),
                    Manifest.permission.CALL_PHONE) == PackageManager.PERMISSION_GRANTED) {
                // 拥有权限
            } else {
                // 申请权限
                ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.CALL_PHONE},
                        Constants.REQUEST_PHONE_REQUEST_PERMISSION);
                return;
            }
        } else {

        }
    }

    private void start_monitor() {
        // TODO Auto-generated method stub
        String action = "active_monitor_mode";
        new Thread(new ThreadParent(TAG,KidsWatApiUrl.getApiUrl(), CommonThreadMethod.JsonFormat(SetStateParameter(action)),
                handler, setMonitorCode)).start();
    }

    private Map<String, String> SetStateParameter(String action) {
        CustomLog.d(TAG,"setStateParameter(action="+action+")");
        CustomLog.d(TAG,"uid:"+KidsWatConfig.getUserUid());
        CustomLog.d(TAG,"token:"+KidsWatConfig.getUserToken());
        CustomLog.d(TAG,"family:"+AppContext.getInstance().getCurrentBeanFamily());

        Map hash = null;
        try {
            switch (action) {
                case "active_monitor_mode":
                    Map<String, String> monitorhash = new HashMap<String, String>();
                    monitorhash.put("uid", KidsWatConfig.getUserUid());
                    monitorhash.put("device_id", AppContext.getInstance().getCurrentFamily().device_id);
                    monitorhash.put("access_token", KidsWatConfig.getUserToken());
                    KidsWatApiUrl.addExtraPara(action, monitorhash);
                    hash = monitorhash;
                    break;

                default:
                    break;
            }

        } catch (Exception e) {
            //TODO:report issue to user!
            String deviceId="";
            AppContext appContext=AppContext.getInstance();
            if (appContext!=null) {
                beanForDb___Family family=appContext.getCurrentFamily();
                if (family!=null) {
                     deviceId=family.device_id;
                } else{
                    CustomLog.d(TAG,"Failed to set  SetStateParamenter. null currentFamily.");
                }
            } else {
                CustomLog.d(TAG,"Failed to set  SetStateParamenter. family was ok?. deviceId="+deviceId+"|");

            }
            CustomLog.d(TAG,"Failed to set  SetStateParamenter. deviceId="+deviceId+"|");


        }
        return hash;
    }

    private void doPlayVoice(String src) {
        if (mp == null) {
            mp = new MediaPlayer();
        }
        // 这里就直接用mp.isPlaying()，因为不可能再报IllegalArgumentException异常了
        if (mp.isPlaying()) {
            mp.stop();
            mp.release();
            mp = null;
            mp = new MediaPlayer();
        }
        try {
            mp.setDataSource(src);
            mp.setOnPreparedListener(new OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                    mp.start();
                }
            });
            // Prepare to async playing
            mp.prepareAsync();
            isPlaying = true;
        } catch (IllegalArgumentException e) {
            CustomLog.logException(TAG,e);
        } catch (SecurityException e) {
            CustomLog.logException(TAG,e);
        } catch (IllegalStateException e) {
            CustomLog.logException(TAG,e);
        } catch (IOException e) {
            CustomLog.logException(TAG,e);
        }

        mp.setOnCompletionListener(new OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mpxx) {
                mp.release();
                mp = null;
                isPlaying = false;
                // -----------
                lnearLayout_audio_play
                        .setBackgroundResource(R.drawable.icon_record3);
            }
        });
    }

    private void updateLocationBasedOnCachedData() {
        beanFor___get_watch_data watch_data=AppContext.getInstance().getInternal_bean___watch_data();

        if (watch_data==null) {
            CustomLog.w(TAG,"No cached watch data!!!!");
            return;
        }
        if (overlay.getOverlayItems() != null) {
            overlay.clearOverlayItems();
        }
        if (watch_data.error == 0) {

            if (watch_data.info.ddata.total_rows > 0) {
                // -------add marker to map--------
                if (overlay.getOverlayItems() != null) {
                    overlay.clearOverlayItems();
                    int i = 0;
                    int rows = watch_data.info.ddata.rows.size();
                    {
                        CRows row = watch_data.info.ddata.rows.get(0);
                        int type = row.type;
                        String address = row.address;
                        long time = row.time;
                        // int reason = row.reason;
                        double latitude = row.latitude;
                        double longitude = row.longitude;
                        int electricity = row.electricity;
                        int location_type = row.type;

                        if (shouldUpdateLocation("updateLocationBasedOnCachedData",row.time)) {

                            add_map_marker(row);
                            /*
                            GeoPoint point = new GeoPoint(latitude, longitude);
                            AppContext.getInstance().setLng(point);

                            Drawable image = location_type == 2 ? getResources().getDrawable(R.drawable.point_select) : getResources().getDrawable(R.drawable.ing);
                            OverlayItem overlayItem = new OverlayItem(point, image);
                            overlay.addOverlayItem(overlayItem);
                            */


                            String otherText = String.format("%s",
                                    location_type == 1 ? "GPS"
                                            : "GSM/WIFI");

                            CustomLog.d(TAG,String.format("updateLocationBasedOnCachedData:Last Location update:%d aka %s, new one %d aka %s",
                                    lastLocationUpdate,
                                    Utils.getTimeFromTimestamp(lastLocationUpdate),
                                    row.time,
                                    Utils.getTimeFromTimestamp(row.time)));
                            lastLocationUpdate=row.time;


                            String strdate = new java.text.SimpleDateFormat("HH:mm")
                                    .format(new java.util.Date(time * 1000));

                            setDL(electricity);
                            mTvElectricity.setText(electricity + "%");
                            mTvAddress.setText(address);
                            mTvTime.setText(strdate);
                            textView_range.setText(otherText);
                            ((MainActivity) getActivity()).setStepNumber(row.step_number);

                            if (electricity == 0) {
                                mTvAddress.setText(getResources().getString(
                                        R.string.tip_available));
                                mTvTime.setText("No locating results");
                            }

                        }
                        //initMapOverlay arleady does this if needed
                        /*

                        overlayManager.addOverlay(overlay);
                        mapController.setZoomCurrent(12);
                        mapController.setPositionAnimationTo(((OverlayItem) (overlay.getOverlayItems().get(0))).getGeoPoint());

                        */

                    }


                }
            } else {
                //no cached - just do regular
                //mapController.setPositionAnimationTo(new GeoPoint(0f, 0f), 2f);
                CustomLog.d(TAG,"updateLocationBasedOnCachedData:should be showing 'no location result'");
                //showNoLocationResult();
            }
        } else {
            //no cached - use regular
            //showLongToast(getActivity().getApplicationContext().getResources().getString(R.string.tip_loadtrack_faild));
        }

    }
    //get_watch_data: upload the location info ( encrypted in base64 and a other custom encryped method ), and get the location ( lat,lng ) and other information
    //get_watch_data - получить все данные какие есть с устройства?
    //и получить и загрузить на сервер
    private void get_watch_data() {
        final String uid = KidsWatConfig.getUserUid();
        final String access_token = KidsWatConfig.getUserToken();
        final String family_id = KidsWatConfig.getDefaultFamilyId();

        if (StringUtils.isEmpty(uid))
            return;

        Calendar a = Calendar.getInstance();
        final int year = a.get(Calendar.YEAR);
        final int month = a.get(Calendar.MONTH);
        final int dayOfMonth = a.get(Calendar.DAY_OF_MONTH);

        final long utcToday = KidsWatUtils.getUtcTimeAt0Time(year, month, dayOfMonth);

        final String request_url = KidsWatApiUrl.getUrlFor___get_watch_data(
                uid, access_token, family_id, utcToday);

        CustomLog.d(TAG,"get_watch_data(). uid:"+uid+",date:"+year+"/"+month+"/"+dayOfMonth+", utcToday:"+utcToday+", family_id:"+family_id);

        Map<String,Object> eventProperties=new HashMap<>();
         //eventProperties.put(URL,request_url);
        eventProperties.put(FAMILIYID,family_id);
        eventProperties.put(UTCTODAY,utcToday);
        eventProperties.put(UID,uid);
        if (LOG_GET_WATCH_DATA) {
            AnalyticsUtils.reportAnalyticsEvent(EVENT_GET_WATCH_DATA_START,eventProperties);
        }

        ApiHttpClient.get(request_url, new HttpCallBack() {

            @Override
            public void onPreStart() {
                CustomLog.d(TAG,"get_watch_data(). uid:"+uid+",date:"+year+"/"+month+"/"+dayOfMonth+", utcToday:"+utcToday+", family_id:"+family_id+". Pre-start");

                showWaitDialog("...Загрузка...");
            }

            @Override
            public void onSuccess(String t) {
                super.onSuccess(t);
                CustomLog.d(TAG,"get_watch_data(). uid:"+uid+",date:"+year+"/"+month+"/"+dayOfMonth+", utcToday:"+utcToday+", family_id:"+family_id+". onSuccess:"+ StringUtils.unescapeString(String.format("url:%s\nt:%s", request_url, t)));

                beanFor___get_watch_data response = AppContext.getInstance()
                        .getGson().fromJson(t, beanFor___get_watch_data.class);
                bean___watch_data = response;
                AppContext.getInstance().setInternal_bean___watch_data(bean___watch_data);

                /*
                if (overlay.getOverlayItems() != null) {
                    overlay.clearOverlayItems();
                }
                */


                Map<String,Object> eventProperties=new HashMap<>();
                 //eventProperties.put(URL,request_url);
                eventProperties.put(FAMILIYID,family_id);
                eventProperties.put(UID,uid);
                 //eventProperties.put(URL,request_url);
                eventProperties.put(ERRORCODE,response.error);
                eventProperties.put(MESSAGE,StringUtils.unescapeString(response.info.message));


                //重新得到map数据
                if (response.error == 0) {

                    if (response.info.ddata.total_rows > 0) {
                        // -------replace marker on map--------
                        if (overlay.getOverlayItems() != null) {
                            //overlay.clearOverlayItems();
                            //int i = 0;
                            //WHY?! it's not in loop. TODO:findout if it's ok. maybe use loop to find out real last row?
                            int rows = response.info.ddata.rows.size();
                            {
                                //let's see if we can found MORE recent one. just for fun at this time.
                                CRows latest=response.info.ddata.rows.get(0);
                                for (int i=0;i<rows;i++) {
                                    CRows current=response.info.ddata.rows.get(i);
                                    if (current.time>latest.time) {
                                        CustomLog.d(TAG, String.format("get_watch_data:onSuccess:  response data %d is from %d aka %s, lat:%f, lon:%f, address:%s, " +
                                                        "previous was %d is from %d aka %s, lat:%f, lon:%f, address:%s, so updating ",
                                                current.time,
                                                Utils.getTimeFromTimestamp(current.time),
                                                current.latitude,
                                                current.longitude,
                                                current.address,
                                                latest.time,
                                                Utils.getTimeFromTimestamp(latest.time),
                                                latest.latitude,
                                                latest.longitude,
                                                latest.address

                                                )

                                        );
                                        latest=current;
                                    }

                                }

                                CRows row = response.info.ddata.rows.get(0);
                                CustomLog.d(TAG, String.format("get_watch_data:onSuccess: data zeroth is from %d aka %s, lat:%f, lon:%f, address:%s, ",
                                        row.time,
                                        Utils.getTimeFromTimestamp(row.time),
                                        row.latitude,
                                        row.longitude,
                                        row.address));

                                CustomLog.d(TAG, String.format("get_watch_data:onSuccess: data latest is from %d aka %s, lat:%f, lon:%f, address:%s, ",
                                        latest.time,
                                        Utils.getTimeFromTimestamp(latest.time),
                                        latest.latitude,
                                        latest.longitude,
                                        latest.address));

                                int type = row.type;
                                String address = row.address;
                                long time = row.time;
                                // int reason = row.reason;
                                double latitude = row.latitude;
                                double longitude = row.longitude;
                                int electricity = row.electricity;
                                int location_type = row.type;

                                if (shouldUpdateLocation("get_watch_data:onSuccess",row.time)) {

                                    add_map_marker(row);
                                    /*
                                    GeoPoint point = new GeoPoint(latitude, longitude);
                                    AppContext.getInstance().setLng(point);

                                    Drawable image = location_type == 2 ? getResources().getDrawable(R.drawable.point_select) : getResources().getDrawable(R.drawable.ing);
                                    OverlayItem overlayItem = new OverlayItem(point, image);
                                    overlay.addOverlayItem(overlayItem);
                                    */


                                    String otherText = String.format("%s",
                                            location_type == 1 ? "GPS"
                                                    : "GSM/WIFI");

                                    String strdate = new java.text.SimpleDateFormat("HH:mm")
                                            .format(new java.util.Date(time * 1000));
                                    String ts=Utils.getTimeFromTimestamp(row.time);
                                    eventProperties.put(LTIME_FORMATTED, ts);

                                    CustomLog.d(TAG,"get_watch_data(). uid:"+uid+",date:"+year+"/"+month+"/"+dayOfMonth+", utcToday:"+utcToday+", family_id:"+family_id+". onSuccess:"+String.format("url:%s\nt:%s", request_url, t)+". strdate:"+strdate+", ltime:"+row.time+",ltime_conv:"+ts);

                                    String locationDetails=String.format("lat:%f, lon:%f, %s, charge:%d",latitude,longitude,address,electricity);
                                    String geoPoint=String.format("lat:%f, lon:%f",latitude,longitude);
                                    eventProperties.put(LOCATION_TYPE,otherText);
                                    eventProperties.put(TIME,strdate);
                                    eventProperties.put(CHARGE,electricity);
                                    eventProperties.put(ADDRESS,address);
                                    eventProperties.put(GEOPOINT,geoPoint);
                                    eventProperties.put(LOCATION,locationDetails);
                                    eventProperties.put(LTIME,row.time);

                                    if (LOG_GET_WATCH_DATA) {
                                        AnalyticsUtils.reportAnalyticsEvent(EVENT_GET_WATCH_DATA_SUCCESS,eventProperties);
                                    }

                                    setDL(electricity);
                                    mTvElectricity.setText(electricity + "%");
                                    mTvAddress.setText(address);
                                    mTvTime.setText(strdate);
                                    textView_range.setText(otherText);
                                    ((MainActivity) getActivity()).setStepNumber(row.step_number);

                                    if (electricity == 0) {
                                        mTvAddress.setText(getResources().getString(
                                                R.string.tip_available));
                                        mTvTime.setText("No locating results");
                                    }
                                    //обновить и карту?
                                    //TODO:
                                    setLastLocation(row);
                                    showLastLocation();



                                    //initMapOverlay arleady does this if needed

                                    /*
                                    overlayManager.addOverlay(overlay);
                                    mapController.setZoomCurrent(12);
                                    if (overlay.getOverlayItems().size()>0) {
                                        mapController.setPositionAnimationTo(((OverlayItem) (overlay.getOverlayItems().get(0))).getGeoPoint());
                                    } else {
                                        CustomLog.w(TAG,"get_watch_data. tryng to animate but no overlay items were present. just NOT CRASH. lastLocationUpdate:"+lastLocationUpdate+", aka:"+Utils.getTimeFromTimestamp(lastLocationUpdate));
                                    }
                                    */

                                }


                            }
							/*int rows = response.info.ddata.rows.size();
							for (beanFor___get_watch_data.CRows row : response.info.ddata.rows) {
								String address = row.address;
								long time = row.time;
								// int reason = row.reason;
								double latitude = row.latitude;
								double longitude = row.longitude;
								int electricity = row.electricity;
								int location_type = row.type;
								String strdate = new java.text.SimpleDateFormat("HH:mm")
										.format(new java.util.Date(time * 1000));
								String otherText = String.format("%s",
										location_type == 1 ? "GPS"
												: "GSM/WIFI");
								GeoPoint point = new GeoPoint(latitude,longitude);

								if(i == 0){
									setDL(electricity);
									mTvElectricity.setText(electricity + "%");
									mTvAddress.setText(address);
									mTvTime.setText(strdate);
									textView_range.setText(otherText);
									((MainActivity)getActivity()).setStepNumber(row.step_number);
								}

								Drawable image = location_type == 2 ? getResources().getDrawable(R.drawable.point_select) : getResources().getDrawable(R.drawable.ing);
								OverlayItem overlayItem = new OverlayItem(point, image);

								overlay.addOverlayItem(overlayItem);
								i++;
							}*/

                        }
                    } else {
                        if (LOG_GET_WATCH_DATA) {
                            AnalyticsUtils.reportAnalyticsEvent(EVENT_GET_WATCH_DATA_NO_RESULT,eventProperties);
                        }
                        CustomLog.d(TAG,"get_watch_data:Will be showing 'no location result'");
                        showNoLocationResult();

                    }
                } else {
                    showLongToast(getActivity().getApplicationContext().getResources().getString(R.string.tip_loadtrack_faild));

                    if (LOG_GET_WATCH_DATA) {
                        AnalyticsUtils.reportAnalyticsEvent(EVENT_GET_WATCH_DATA_ERROR, eventProperties);
                    }
                    CustomLog.d(TAG, "get_watch_data(). uid:" + uid + ",date:" + year + "/" + month + "/" + dayOfMonth + ", utcToday:" + utcToday + ", family_id:" + family_id + ". onSuccess:" + String.format("url:%s\nt:%s", request_url, t));
                }

            }

            @Override
            public void onFailure(int errorNo, String strMsg) {
                super.onFailure(errorNo, strMsg);
                String msg =  String.format("errorNo:%s\n%s", errorNo, strMsg);
                CustomLog.d(TAG, "get_watch_data(). uid:" + uid + ",date:" + year + "/" + month + "/" + dayOfMonth + ", utcToday:" + utcToday + ", family_id:" + family_id + ". error:"+msg);

                Map<String,Object> eventProperties=new HashMap<>();
                 //eventProperties.put(URL,request_url);
                eventProperties.put(ERRORCODE,errorNo);
                eventProperties.put(MESSAGE,StringUtils.unescapeString(strMsg));
                if (LOG_GET_WATCH_DATA) {
                    AnalyticsUtils.reportAnalyticsEvent(EVENT_GET_WATCH_DATA_EXCEPTION,eventProperties);
                }
            }

            @Override
            public void onFinish() {
                dismissDialog(null, 300);
            }

        });

    }

    private void exec_voice_record() {
        final String uid = KidsWatConfig.getUserUid();
        final String access_token = KidsWatConfig.getUserToken();
        final String device_id = KidsWatConfig.getUserDeviceId();
        // final String family_id = AppContext.getDefaultFamilyId();

        final String request_url = KidsWatApiUrl.getUrlFor___exec_voice_record(
                uid, access_token, device_id);

        ApiHttpClient.get(request_url, new HttpCallBack() {

            @Override
            public void onPreStart() {
                mTvRecord.setText("запись\n...");
                linearLayout_record
                        .setBackgroundResource(R.drawable.icon_record_2);
                showWaitDialog(getActivity().getResources().getString(R.string.locating));
            }

            @Override
            public void onSuccess(String t) {
                super.onSuccess(t);
                CustomLog.d(TAG,String.format("url:%s\nt:%s", request_url, t));

                try {
                    JSONObject jso = new JSONObject(t);
                    int code = jso.getInt("error");
                    if (code == 0) {
                        final String command_no = jso.getJSONObject("info")
                                .getString("cmdno");
                        // -----------------
                        SqlDb db = SqlDb.get(getActivity());
                        db.saveFamily___last_record_no(device_id, command_no);
                        db.closeDb();
                        // --------------------
                        // showWaitDialog("...Recording...");
                        mHandler.postDelayed(new Runnable() {

                            @Override
                            public void run() {
                                fetch_time_start = System.currentTimeMillis() / 1000;

                                showWaitDialog( AppContext.getInstance().getApplicationContext().getResources().getString(R.string.transmiting));
                                fetch_voice_by_voice_id(command_no, 7,
                                        KidsWatConfig.getUserDeviceId(), -1);
                            }
                        }, 10 * 1000);

//						KidsWatUtils.get_notice_list_ex();
                    } else if (code == 2015001) {
                        showLongToast( AppContext.getInstance().getApplicationContext().getResources().getString(R.string.tip_operate_sleep_mode));
                        setRecordFailed();
                    } else {
                        setRecordFailed();
                    }
                } catch (JSONException e) {
                    CustomLog.logException(TAG,e);
                    setRecordFailed();
                }
            }

            @Override
            public void onFailure(int errorNo, String strMsg) {
                super.onFailure(errorNo, strMsg);
                String msg = String.format("errorNo:%s\n%s", errorNo, strMsg);
                CustomLog.d(TAG,"error=" + msg);
                setRecordFailed();
            }

            @Override
            public void onFinish() {
                //
            }
        });

    }

    private void fetch_voice_by_voice_id(final String command_no,
                                         final int total_request_number, final String Device_id, int i) {

        if (i == -1) {
            long timespan = System.currentTimeMillis() / 1000
                    - fetch_time_start;

            if (timespan > 40.0) {
                fetch_time_start = System.currentTimeMillis() / 1000;
                setRecordFailed();
                return;
            }
        }

        final String uid = KidsWatConfig.getUserUid();
        final String access_token = KidsWatConfig.getUserToken();

        final String request_url = KidsWatApiUrl
                .getUrlFor___fetch_voice_by_voice_id(uid, access_token,
                        Device_id, command_no, total_request_number);

        ApiHttpClient.get(request_url, new HttpCallBack() {

            @Override
            public void onSuccess(String t) {
                super.onSuccess(t);
                CustomLog.d(TAG,String.format("url:%s\nt:%s", request_url, t));
                try {
                    JSONObject jso = new JSONObject(t);
                    int code = jso.getInt("error");
                    CustomLog.d(TAG,"errorcode=" + code);
                    if (code == 0) {
                        String filepath = String.format("%saudio_%s_%s.amr",
                                KidsWatConfig.getTempFilePath(), Device_id,
                                command_no);
                        String voice_stream = jso.getJSONObject("info")
                                .getString("voice_stream");
                        Base64Encode.decoderBase64FileForAudio(voice_stream,
                                filepath);
                        CustomLog.d(TAG,"save ok");
                        lnearLayout_audio_play.setVisibility(View.VISIBLE);
                        lnearLayout_audio_play.setTag(filepath);
                        // ----------
                        mTvRecord.setText("запись");
                        linearLayout_record
                                .setBackgroundResource(R.drawable.icon_record_1);
                        dismissDialog(getActivity().getResources().getString(R.string.record_successful), 800);
                    } else {
                        if (code == -1) {
                            hideWaitDialog();
                        }

                        fetch_voice_by_voice_id(command_no,
                                total_request_number, Device_id, -1);
                    }
                } catch (JSONException e) {
                    CustomLog.logException(TAG,e);
                    setRecordFailed();
                } catch (Exception e) {
                    CustomLog.logException(TAG,e);
                    fetch_voice_by_voice_id(command_no, total_request_number,
                            Device_id, -1);
                    dismissDialog(null);
                }
            }

            @Override
            public void onFailure(int errorNo, String strMsg) {
                super.onFailure(errorNo, strMsg);
                String msg = String.format("errorNo:%s\n%s", errorNo, strMsg);
                CustomLog.d(TAG,"error=" + msg);
                fetch_voice_by_voice_id(command_no, total_request_number,
                        Device_id, -1);
                if (!KidsWatUtils.isNetworkAvailable(getActivity())) {
                    Toast.makeText(getActivity(), "network unaviable.", Toast.LENGTH_SHORT).show();
                    dismissDialog("");
                    setRecordFailed();
                }
            }

            @Override
            public void onFinish() {
                //
            }
        });
    }


    private void makeOneForcedUpdate() {
        CustomLog.d(TAG,"Got request for one-time forced location update");
        needOneForcedUpdate=true;
    }

    private void resetLastUpdateTime() {

        CustomLog.d(TAG,String.format("Resetting last update time...old one was %d",lastLocationUpdate));
        lastLocationUpdate=0L;

    }

    private boolean shouldUpdateLocation(String func,long time) {

        boolean needUpdate=false;
        if ((time>lastLocationUpdate)||(needOneForcedUpdate)) {
            needUpdate=true;
        }

        CustomLog.d(TAG,String.format("%s:Last Location update:%d aka %s, new one %d aka %s, should update:%s, forced update:%s",
                func,
                lastLocationUpdate,
                Utils.getTimeFromTimestamp(lastLocationUpdate),
                time,
                Utils.getTimeFromTimestamp(time),
                needUpdate?"YES":"NO",
                needOneForcedUpdate?"YES":"NO"
                ));

        if (needOneForcedUpdate) {
            needUpdate=true;
            needOneForcedUpdate=false;
        }

        if (needUpdate) {
            lastLocationUpdate=time;
        }
        return needUpdate;
    }
    private void showLastLocation() {
        if (bean___watch_data!=null) {
            CustomLog.d(TAG,"showLastLocation:will show last location");
            showLastLocationInternal(bean___watch_data);
        }
    }

    private void showLastLocationPossibleFromCache() {
        if (bean___watch_data!=null) {
            CustomLog.d(TAG,"showLastLocationPossibleFromCache:location data is available (not cached)");
            showLastLocationInternal(bean___watch_data);
        } else {
            CustomLog.d(TAG,"showLastLocationPossibleFromCache:location data is available(?) (only cached):"+AppContext.getInstance().getInternal_bean___watch_data());
            showLastLocationInternal(AppContext.getInstance().getInternal_bean___watch_data());
        }
    }

    private void showLastLocationInternal(beanFor___get_watch_data watchData) {
        if ((watchData != null) && (watchData.info!=null) && (watchData.info.ddata!=null) && (watchData.info.ddata.rows!=null)) {
            int rows = watchData.info.ddata.rows.size();
            if (rows > 0) {
                if (overlay != null && overlay.getOverlayItems().size() > 0) {
                    double latitude = watchData.info.ddata.rows.get(0).latitude;
                    double longitude = watchData.info.ddata.rows.get(0).longitude;
                    CustomLog.d(TAG,String.format("Will add point at lat:%f lat:%f",latitude,longitude));

                    replace_map_marker(new GeoPoint(latitude, longitude));

                    final String family_id = KidsWatConfig.getDefaultFamilyId();
                    final int sex = KidsWatUtils.getBabySex();
                    setPointMarker();

                    List<OverlayItem> overlayItems = overlay.getOverlayItems();
                    GeoPoint lastPoint = overlayItems.get(0).getGeoPoint();

                    int resSex = sex == 1 ? R.drawable.icon_boy : R.drawable.icon_girl;
                    Drawable drawable = new BitmapDrawable(getApplication().getResources(), KidsWatUtils.getBabyImg_v3(getActivity(), family_id, sex, AppContext.getInstance().getCurrentFamily()));

                    overlayItems.get(0).setDrawable(drawable);
                    mapController.setZoomCurrent(12);

                    // wtf 调用两次才能移动地图 dont no why,先这样弄
                    // TODO
                    mapController.setPositionAnimationTo(lastPoint);
                    //mapController.setPositionAnimationTo(lastPoint);
                } else {
                    CustomLog.w(TAG,"error showing:overlay:"+overlay);

                }
            } else {
                CustomLog.w(TAG,"rows list is zero-sized");
            }
        } else {
            CustomLog.e(TAG,"No valid data in bean___watch_data.info.ddata.rows. bean___watch_data:"+watchData);
            //try to
        }
    }

    private void setPointMarker() {
        List<OverlayItem> overlayItems = overlay.getOverlayItems();

        for (OverlayItem m : overlayItems) {
            m.setDrawable(getResources().getDrawable(R.drawable.point_select));
        }
    }


    //get_location_ex: get the device 's last point and send a location command to the device
    //иными словами - get_location_ex еще и просит устройство само определится и пуш прислать ?
    //да
    private void get_location_ex(final boolean isBackground) {
        //mZoomCurrent = mapController.getZoomCurrent();
        final String uid = KidsWatConfig.getUserUid();
        final String access_token = KidsWatConfig.getUserToken();
        final String device_id = KidsWatConfig.getUserDeviceId();
        final String family_id = KidsWatConfig.getDefaultFamilyId();
        CustomLog.d(TAG,"get_location_ex(isBackground:"+isBackground+"). uid:"+uid+", device_id:"+device_id+", family_id:"+family_id);


        final String request_url = KidsWatApiUrl.getUrlFor___get_location_ex(
                uid, access_token, device_id, family_id);

        Map<String,Object> eventProperties=new HashMap<>();
         //eventProperties.put(URL,request_url);
        eventProperties.put(FAMILIYID,family_id);
        eventProperties.put(DEVICE_ID,device_id);
        eventProperties.put(UID,uid);
        if (LOG_GET_LOCATION_EX) {
            AnalyticsUtils.reportAnalyticsEvent(EVENT_GET_LOCATION_EX_START,eventProperties);
        }

        ApiHttpClient.get(request_url, new HttpCallBack() {

            @Override
            public void onPreStart() {
                CustomLog.d(TAG,"get_location_ex(isBackground:"+isBackground+"). uid:"+uid+", device_id:"+device_id+", family_id:"+family_id+". Pre-start.");
                if (!isBackground) {
                    mTvLocation.setText(getString(R.string.home_fragment_hint1));
                    linearLayout_location
                            .setBackgroundResource(R.drawable.icon_location_2);
                    showWaitDialog(getString(R.string.home_fragment_hint2));
                }
            }

            @Override
            public void onSuccess(String t) {
                super.onSuccess(t);
                CustomLog.d(TAG,"get_location_ex(isBackground:"+isBackground+"). uid:"+uid+", device_id:"+device_id+", family_id:"+family_id+". onSuccess:"+String.format("url:%s\nt:%s", request_url, t));



                try {
                    JSONObject response = new JSONObject(t);

                    int code = response.getInt("error");



                    /*
                    String message;
                    try {
                        message = response.getJSONObject("info")
                                .getString("message");
                    }   catch (Exception ex) {
                        CustomLog.logException(ex);
                        message=String.format("Error parsing message. Server's error code is %d",code);
                    }
                    */
                    Map<String,Object> eventProperties=new HashMap<>();
                     //eventProperties.put(URL,request_url);
                    eventProperties.put(FAMILIYID,family_id);
                    eventProperties.put(DEVICE_ID,device_id);
                    eventProperties.put(UID,uid);
                     //eventProperties.put(URL,request_url);
                    eventProperties.put(ERRORCODE,code);
                    //eventProperties.put(MESSAGE, StringUtils.unescapeString(message));


                    //TODO:решить как -1 отрабатывать ПРАВИЛЬНО. Пока что вроде там правильно приходит поэтому считаем что ок
                    if ((code == 0) ||(code==-1)) {
                        lnearLayout_audio_play.setVisibility(View.GONE);
                        // int content_type = response.getJSONObject("data")
                        // .getInt("content_type");
                        long ltime = response.getJSONObject("data")
                                .getJSONObject("parameter")
                                .getInt("location_time");
                        String address = response.getJSONObject("data")
                                .getJSONObject("parameter")
                                .getString("address");
                        double longitude = response.getJSONObject("data")
                                .getJSONObject("parameter")
                                .getJSONObject("location")
                                .getDouble("longitude");
                        double latitude = response.getJSONObject("data")
                                .getJSONObject("parameter")
                                .getJSONObject("location")
                                .getDouble("latitude");
                        int electricity = response.getJSONObject("data")
                                .getJSONObject("parameter")
                                .getInt("electricity");
                        int location_type = response.getJSONObject("data")
                                .getJSONObject("parameter")
                                .getInt("location_type");// 1基站,0,gps
                        // ----------------





                        //check if we should update location in GUI
                        if (shouldUpdateLocation("get_location_ex:onSuccess",ltime)) {
                            String strdate = new java.text.SimpleDateFormat("HH:mm")
                                    .format(new java.util.Date(ltime * 1000));
                            String otherText = String.format("%s",
                                    location_type == 1 ? "GPS"
                                            : "GSM/WIFI");
                            String ts=Utils.getTimeFromTimestamp(ltime);
                            eventProperties.put(LTIME_FORMATTED, ts);

                            CustomLog.d(TAG, "get_location_ex(isBackground:"+isBackground+"). uid:"+uid+", device_id:"+device_id+", family_id:"+family_id+". onSuccess:"+String.format("url:%s\nt:%s", request_url, t)+". ltime:"+ltime+",ltime_conv:"+ts+", json:"+ response.getJSONObject("data"));

                            String locationDetails=String.format("lat:%f, lon:%f, %s, charge:%d",latitude,longitude,address,electricity);
                            String geoPoint=String.format("lat:%f, lon:%f",latitude,longitude);
                            eventProperties.put(LOCATION_TYPE,otherText);
                            eventProperties.put(TIME,strdate);
                            eventProperties.put(CHARGE,electricity);
                            eventProperties.put(ADDRESS,address);
                            eventProperties.put(GEOPOINT,geoPoint);
                            eventProperties.put(LOCATION,locationDetails);
                            eventProperties.put(LTIME,ltime);


                            if (LOG_GET_LOCATION_EX) {
                                AnalyticsUtils.reportAnalyticsEvent(EVENT_GET_LOCATION_EX_SUCCESS,eventProperties);
                            }
                            mTvAddress.setText(address);
                            mTvTime.setText(strdate);
                            textView_range.setText(otherText);
                            mTvElectricity.setText(electricity + "%");

                            if (electricity == 0) {
                                showNoLocationResultZeroElectricity();
                            }

                            setDL(electricity);

                            beanFor___get_watch_data.CRows row = new beanFor___get_watch_data.CRows();
                            row.address = address;
                            row.time = ltime;
                            row.latitude = latitude;
                            row.longitude = longitude;
                            row.electricity = electricity;
                            row.type = location_type;

                            setLastLocation(row);
                            showLastLocation();
                        }




                        // 获取最新步数
                        get_watch_data_lastest_step();

                        // ---------------
                        if (!isBackground) {
                            showWaitDialog(getString(R.string.home_fragment_hint4));
                        }

                    } else if (code == 2015001) {
                        CustomLog.d(TAG, "get_location_ex(isBackground:"+isBackground+"). uid:"+uid+", device_id:"+device_id+", family_id:"+family_id+". onSuccess: device_sleeps");
                        if (LOG_GET_LOCATION_EX) {
                            AnalyticsUtils.reportAnalyticsEvent(EVENT_GET_LOCATION_EX_ERROR_DEVICE_SLEEPS,eventProperties);
                        }
                        showLongToast(getActivity().getApplicationContext().getResources().getString(R.string.tip_operate_sleep_mode));

                    } else if (code == -2) {
                        int electricity = response.getJSONObject("data")
                                .getJSONObject("parameter")
                                .getInt("electricity");
                        eventProperties.put(CHARGE,electricity);

                        if (LOG_GET_LOCATION_EX) {
                            AnalyticsUtils.reportAnalyticsEvent(EVENT_GET_LOCATION_EX_ERROR,eventProperties);
                        }
                        mTvElectricity.setText(electricity + "%");
                        CustomLog.d(TAG, "get_location_ex(isBackground:"+isBackground+"). uid:"+uid+", device_id:"+device_id+", family_id:"+family_id+". onSuccess: electricity:"+electricity+"%");

						/*if (electricity == 0) {
							mTvAddress.setText(getResources().getString(
									R.string.tip_available));
							mTvTime.setText("Нет результатов местоположения");
						}*/
                        setDL(electricity);

                        /** 2017.12.26修改
                         showShortToast("Местоположение не определено!");
                         */
                    } else {
                        CustomLog.d(TAG, "get_location_ex(isBackground:"+isBackground+"). uid:"+uid+", device_id:"+device_id+", family_id:"+family_id+". onSuccess: errorCode:"+code);
                        if (LOG_GET_LOCATION_EX) {
                            AnalyticsUtils.reportAnalyticsEvent(EVENT_GET_LOCATION_EX_ERROR,eventProperties);
                        }
                        /** 2017.12.26修改
                         if (!isBackground) {
                         showWaitDialog("Местоположение не определено!");
                         }
                         */
                    }
                } catch (JSONException e) {
                    /** 2017.12.26修改
                     if (!isBackground) {
                     showWaitDialog("Местоположение не определено!");
                     }
                     */
                    Map<String,Object> eventProperties=new HashMap<>();
                    // //eventProperties.put(URL,request_url);
                    eventProperties.put(EVENT_EXCEPTION,e);
                    if (LOG_GET_LOCATION_EX) {
                        AnalyticsUtils.reportAnalyticsEvent(EVENT_GET_LOCATION_EX_EXCEPTION,eventProperties);
                    }
                    CustomLog.logException(TAG,e);
                }

            }

            @Override
            public void onFailure(int errorNo, String strMsg) {
                super.onFailure(errorNo, strMsg);
                String msg =  String.format("errorNo:%s\n%s", errorNo, strMsg);
                CustomLog.d(TAG,"get_location_ex(isBackground:"+isBackground+"). uid:"+uid+", device_id:"+device_id+", family_id:"+family_id+". onError: "+msg);
                Map<String,Object> eventProperties=new HashMap<>();
                 //eventProperties.put(URL,request_url);
                eventProperties.put(ERRORCODE,errorNo);
                eventProperties.put(MESSAGE,StringUtils.unescapeString(strMsg));
                if (LOG_GET_LOCATION_EX) {
                    AnalyticsUtils.reportAnalyticsEvent(EVENT_GET_LOCATION_EX_SERVER_ERROR,eventProperties);
                }
                /** 2017.12.26修改
                 if (!isBackground) {
                 showWaitDialog("Местоположение не определено!");
                 }
                 */
            }

            @Override
            public void onFinish() {
                if (!isBackground) {
                    linearLayout_location
                            .setBackgroundResource(R.drawable.icon_location_1);
                    mTvLocation.setText(getString(R.string.home_fragment_hint5));

                    dismissDialog(null, 800);
                }
            }
        });
    }

    //get_location: Just get the device 's last point
    private void get_location(final boolean isBackground) {
        final String uid = KidsWatConfig.getUserUid();
        final String access_token = KidsWatConfig.getUserToken();
        final String device_id = KidsWatConfig.getUserDeviceId();
        final String family_id = KidsWatConfig.getDefaultFamilyId();

        CustomLog.d(TAG,"get_location(isBackground:"+isBackground+"). uid:"+uid+", device_id:"+device_id+", family_id:"+family_id);


        // KidsWatApi.get_location_ex(uid, access_token, device_id, family_id,
        final String request_url = KidsWatApiUrl.getUrlFor___get_location(
                uid, access_token, device_id, family_id);


        Map<String,Object> eventProperties=new HashMap<>();
         //eventProperties.put(URL,request_url);
        eventProperties.put(FAMILIYID,family_id);
        eventProperties.put(DEVICE_ID,device_id);
        eventProperties.put(UID,uid);
        if (LOG_GET_LOCATION) {
            AnalyticsUtils.reportAnalyticsEvent(EVENT_GET_LOCATION_START,eventProperties);
        }

        ApiHttpClient.get(request_url, new HttpCallBack() {

            @Override
            public void onPreStart() {
                CustomLog.d(TAG,"get_location(isBackground:"+isBackground+"). uid:"+uid+", device_id:"+device_id+", family_id:"+family_id+". Pre-start.");

                if (!isBackground) {
                    mTvLocation.setText("Location\n...");
                    linearLayout_location
                            .setBackgroundResource(R.drawable.icon_location_2);
                    showWaitDialog();
                }
            }

            @Override
            public void onSuccess(String t) {
                super.onSuccess(t);
                CustomLog.d(TAG, "get_location(isBackground:"+isBackground+"). uid:"+uid+", device_id:"+device_id+", family_id:"+family_id+". onSuccess:"+String.format("url:%s\nt:%s", request_url, t));
                try {
                    JSONObject response = new JSONObject(t);

                    int code = response.getInt("error");

                    String message;
                    try {
                        message = response.getJSONObject("info")
                                .getString("message");
                    }   catch (Exception ex) {
                        CustomLog.logException(ex);
                        message=String.format("Error parsing message. Server's error code is %d",code);
                    }
                    Map<String,Object> eventProperties=new HashMap<>();
                     //eventProperties.put(URL,request_url);
                    eventProperties.put(FAMILIYID,family_id);
                    eventProperties.put(DEVICE_ID,device_id);
                    eventProperties.put(UID,uid);
                     //eventProperties.put(URL,request_url);
                    eventProperties.put(ERRORCODE,code);
                    eventProperties.put(MESSAGE, StringUtils.unescapeString(message));


                    //TODO: решить (узнать) как -1 отрабатывать
                    if ((code == 0) ||(code==-1)) {
                        lnearLayout_audio_play.setVisibility(View.GONE);
                        // int content_type = response.getJSONObject("data")
                        // .getInt("content_type");
                        long ltime = response.getJSONObject("data")
                                .getJSONObject("parameter")
                                .getInt("location_time");
                        String address = response.getJSONObject("data")
                                .getJSONObject("parameter")
                                .getString("address");
                        double longitude = response.getJSONObject("data")
                                .getJSONObject("parameter")
                                .getJSONObject("location")
                                .getDouble("longitude");
                        double latitude = response.getJSONObject("data")
                                .getJSONObject("parameter")
                                .getJSONObject("location")
                                .getDouble("latitude");
                        int electricity = response.getJSONObject("data")
                                .getJSONObject("parameter")
                                .getInt("electricity");
                        int location_type = response.getJSONObject("data")
                                .getJSONObject("parameter")
                                .getInt("location_type");// 1基站,0,gps
                        // ----------------

                        if (shouldUpdateLocation("get_location:onSuccess",ltime)) {
                            String strdate = new java.text.SimpleDateFormat("HH:mm")
                                    .format(new java.util.Date(ltime * 1000));
                            String otherText = String.format("%s",
                                    location_type == 1 ? "GPS"
                                            : "GSM/WIFI");
                            String ts=Utils.getTimeFromTimestamp(ltime);
                            eventProperties.put(LTIME_FORMATTED, ts);

                            CustomLog.d(TAG, "get_location(isBackground:"+isBackground+"). uid:"+uid+", device_id:"+device_id+", family_id:"+family_id+". onSuccess:"+String.format("url:%s\nt:%s", request_url, t)+". ltime:"+ltime+",ltime_conv:"+ts+", json:"+ response.getJSONObject("data"));

                            String locationDetails=String.format("lat:%f, lon:%f, %s, charge:%d",latitude,longitude,address,electricity);
                            String geoPoint=String.format("lat:%f, lon:%f",latitude,longitude);
                            eventProperties.put(LOCATION_TYPE,otherText);
                            eventProperties.put(TIME,strdate);
                            eventProperties.put(CHARGE,electricity);
                            eventProperties.put(ADDRESS,address);
                            eventProperties.put(GEOPOINT,geoPoint);
                            eventProperties.put(LOCATION,locationDetails);
                            eventProperties.put(LTIME,ltime);


                            if (LOG_GET_LOCATION) {
                                AnalyticsUtils.reportAnalyticsEvent(EVENT_GET_LOCATION_SUCCESS,eventProperties);
                            }

                            mTvAddress.setText(address);
                            mTvTime.setText(strdate);
                            textView_range.setText(otherText);
                            mTvElectricity.setText(electricity + "%");

                            // Please make sure the watch is on and the network is
                            // available.

                            if (electricity == 0) {
                                showNoLocationResultZeroElectricity();
                            }

                            setDL(electricity);
                            // -----------------

                            beanFor___get_watch_data.CRows row = new beanFor___get_watch_data.CRows();
                            row.address = address;
                            row.time = ltime;
                            row.latitude = latitude;
                            row.longitude = longitude;
                            row.electricity = electricity;
                            row.type = location_type;

                            setLastLocation(row);
                            showLastLocation();
                        }


                        // ---------------
                        /** 2017.12.26修改
                         if (!isBackground) {
                         showWaitDialog("Местоположение не определено!");
                         }
                         */
//						if(clicked){
//							showWaitDialog("Success. Update Address after 1~2 minutes.");
//						}
                    } else if (code == 2015001) {
                        //安全模式下，点击定位才去显示toast
						/*if(clicked){
							showLongToast(getActivity().getResources().getString(R.string.tip_in_sleep_mode));
						}else{
							mTvAddress.setText(getActivity().getResources().getString(R.string.tip_available));
							mTvTime.setText(getActivity().getResources().getString(R.string.tip_no_location_results));
						}*/
                        CustomLog.d(TAG, "get_location(isBackground:"+isBackground+"). uid:"+uid+", device_id:"+device_id+", family_id:"+family_id+". onSuccess: device_sleeps");
                        if (LOG_GET_LOCATION) {
                            AnalyticsUtils.reportAnalyticsEvent(EVENT_GET_LOCATION_ERROR_DEVICE_SLEEPS,eventProperties);
                        }
                        showLongToast(getActivity().getApplicationContext().getResources().getString(R.string.tip_operate_sleep_mode));
                    } else if (code == -2) {
                        int electricity = response.getJSONObject("data")
                                .getJSONObject("parameter")
                                .getInt("electricity");

                        eventProperties.put(CHARGE,electricity);

                        if (LOG_GET_LOCATION) {
                            AnalyticsUtils.reportAnalyticsEvent(EVENT_GET_LOCATION_ERROR,eventProperties);
                        }
                        mTvElectricity.setText(electricity + "%");
                        CustomLog.d(TAG, "get_location(isBackground:"+isBackground+"). uid:"+uid+", device_id:"+device_id+", family_id:"+family_id+". onSuccess: electricity:"+electricity+"%");
                        if (electricity == 0) {
                            showNoLocationResultZeroElectricity();
                        }
                        setDL(electricity);

                        /** 2017.12.26修改
                         showShortToast("Местоположение не определено!");
                         */
                    } else {
                        /** 2017.12.26修改
                         if (!isBackground) {
                         showWaitDialog("Местоположение не определено!");
                         }
                         */
                        CustomLog.d(TAG, "get_location(isBackground:"+isBackground+"). uid:"+uid+", device_id:"+device_id+", family_id:"+family_id+". onSuccess: errorCode:"+code);
                        if (LOG_GET_LOCATION) {
                            AnalyticsUtils.reportAnalyticsEvent(EVENT_GET_LOCATION_ERROR,eventProperties);
                        }
                    }

                } catch (JSONException e) {
                    /** 2017.12.26修改
                     if (!isBackground) {
                     showWaitDialog("Местоположение не определено!");
                     }
                     */
                    Map<String,Object> eventProperties=new HashMap<>();
                     //eventProperties.put(URL,request_url);
                    eventProperties.put(EVENT_EXCEPTION,e);
                    if (LOG_GET_LOCATION) {
                        AnalyticsUtils.reportAnalyticsEvent(EVENT_GET_LOCATION_EXCEPTION,eventProperties);
                    }
                    CustomLog.logException(TAG,e);
                }
            }

            @Override
            public void onFailure(int errorNo, String strMsg) {
                super.onFailure(errorNo, strMsg);
                String msg =  String.format("errorNo:%s\n%s", errorNo, strMsg);
                CustomLog.d(TAG,"get_location(isBackground:"+isBackground+"). uid:"+uid+", device_id:"+device_id+", family_id:"+family_id+". onError: "+msg);
                Map<String,Object> eventProperties=new HashMap<>();
                 //eventProperties.put(URL,request_url);
                eventProperties.put(ERRORCODE,errorNo);
                eventProperties.put(MESSAGE,StringUtils.unescapeString(strMsg));
                if (LOG_GET_LOCATION) {
                    AnalyticsUtils.reportAnalyticsEvent(EVENT_GET_LOCATION_SERVER_ERROR,eventProperties);
                }

                /** 2017.12.26修改
                 if (!isBackground) {
                 showWaitDialog("Местоположение не определено!");
                 }
                 */
            }

            @Override
            public void onFinish() {
                if (!isBackground) {
                    linearLayout_location
                            .setBackgroundResource(R.drawable.icon_location_1);
                    mTvLocation.setText(getString(R.string.home_fragment_hint5));
                    dismissDialog(null, 800);
                }
            }
        });
    }

    private void setLastLocation(CRows row) {
        if (bean___watch_data != null) {
            int rows = bean___watch_data.info.ddata.rows.size();
            if (rows > 0) {
                bean___watch_data.info.ddata.rows.set(0, row);
            }
        } else {
            CustomLog.e(TAG,"can't set last location - bean___watch_data is null");
        }
    }

    /*
    public void getFocus(CRows row) {
        long ltime = row.time;
        int location_type = row.type;
        String strdate = new java.text.SimpleDateFormat("HH:mm")
                .format(new java.util.Date(ltime * 1000));
        String otherText = String.format("%s",
                location_type == 1 ? "GPS"
                        : "GSM/WIFI");
        mTvAddress.setText(row.address);
        mTvTime.setText(strdate);
        textView_range.setText(otherText);
        mTvElectricity.setText(row.electricity + "%");
        setDL(row.electricity);
        if (row.electricity == 0) {
            mTvAddress.setText(getResources().getString(
                    R.string.tip_available));
        }
    } */

    private void setRecordFailed() {
        mTvRecord.setText("запись");
        linearLayout_record.setBackgroundResource(R.drawable.icon_record_1);
        dismissDialog(getActivity().getResources().getString(R.string.record_faild), 800);
    }

    private void showSleepMode() {
        if (AppContext.getInstance().getIsLogin()) {

            beanForDb___Family f = AppContext.getInstance().getCurrentFamily();
            if (f != null && f.d_mode == 2) {
                linearLayout_sleepmode.setVisibility(View.VISIBLE);
                linearLayout_electric.setVisibility(View.INVISIBLE);
            } else {
                linearLayout_sleepmode.setVisibility(View.INVISIBLE);
                linearLayout_electric.setVisibility(View.VISIBLE);
            }
        } else {
            linearLayout_sleepmode.setVisibility(View.INVISIBLE);
            linearLayout_electric.setVisibility(View.VISIBLE);
        }
    }

    public void setDL(int dl) {

        if (dl > 80)
            imageView_electricity.setBackgroundResource(R.drawable.batteryf);
        else if (dl > 60)
            imageView_electricity.setBackgroundResource(R.drawable.batterye);
        else if (dl > 60)
            imageView_electricity.setBackgroundResource(R.drawable.batteryd);
        else if (dl > 20)
            imageView_electricity.setBackgroundResource(R.drawable.batteryc);
        else if (dl > 10)
            imageView_electricity.setBackgroundResource(R.drawable.batteryb);
        else
            imageView_electricity.setBackgroundResource(R.drawable.batterya);
    }

    @Override
    public void onMapActionEvent(MapEvent event) {
        switch (event.getMsg()) {
            case MapEvent.MSG_LONG_PRESS:
                CustomLog.d(TAG,"long press");

                List<Double> mDistanseList = new ArrayList<Double>();
                double mMinDistanse = 0;

                //点击显示默认图片
                CustomLog.d(TAG,"点击地图,先显示圆 - Click on the map to display the circle first.");
                setPointMarker();

                float x = event.getX();
                float y = event.getY();

                ScreenPoint screenPoint = new ScreenPoint(x, y);
                GeoPoint geopoint = mapController.getGeoPoint(screenPoint);
                CustomLog.d(TAG,"screen lat:" + geopoint.getLat() + "lon:" + geopoint.getLon());
                int i = 0;

                final CRows marker = bean___watch_data.info.ddata.rows.get(0);
                double lat = marker.latitude;
                double lon = marker.longitude;
                if (Math.abs(lat - geopoint.getLat()) < 0.02 && Math.abs(lon - geopoint.getLon()) < 0.02) {
                    final int sex = KidsWatUtils.getBabySex();
                    int resSex = sex == 1 ? R.drawable.icon_boy : R.drawable.icon_girl;
                    Drawable drawable = getResources().getDrawable(resSex);
                    OverlayItem overlayItem = (OverlayItem) overlay.getOverlayItems().get(0);
                    overlayItem.setDrawable(drawable);

                    //显示信息在linearlayout中
                    long ltime = marker.time;
                    int location_type = marker.type;
                    final String strdate = new java.text.SimpleDateFormat("HH:mm")
                            .format(new java.util.Date(ltime * 1000));
                    final String otherText = String.format("%s",
                            location_type == 1 ? "GPS"
                                    : "GSM/WIFI");
                    getActivity().runOnUiThread(new Runnable() {
                        public void run() {
                            CustomLog.d(TAG,"on marker" + "" + " click");
                            mTvAddress.setText(marker.address);
                            mTvTime.setText(strdate);
                            textView_range.setText(otherText);
                            mTvElectricity.setText(marker.electricity + "%");
                            setDL(marker.electricity);
                            if (marker.electricity == 0) {
                                mTvAddress.setText(getResources().getString(
                                        R.string.tip_available));
                            }
                        }
                    });
                }
                //得到距离点击位置最近的点的距离集合
				/*for (final beanFor___get_watch_data.CRows row : bean___watch_data.info.ddata.rows) {
					double lat = row.latitude;
					double lon = row.longitude;
					i++;
					CustomLog.d(TAG,"history : "+ i +"----time : "+new Date(row.time*1000).toLocaleString());
					if(Math.abs(lat-geopoint.getLat()) < 0.02 && Math.abs(lon-geopoint.getLon()) < 0.02){
						double distanse = Math.sqrt(Math.pow(lat, 2.0)+Math.pow(lon,2.0));
						mDistanseList.add(distanse);
					}
				}
				Collections.sort(mDistanseList);
				mMinDistanse  = mDistanseList.get(0);*/

				/*//点击标注点
				for (final beanFor___get_watch_data.CRows row : bean___watch_data.info.ddata.rows) {
					final double lat = row.latitude;
					final double lon = row.longitude;
					if(mMinDistanse == Math.sqrt(Math.pow(lat, 2.0)+Math.pow(lon,2.0))){
						CustomLog.d(TAG,"latest click");
						//显示圆
						//显示头像
						List<OverlayItem> overlayItems = overlay.getOverlayItems();
						for(OverlayItem overlayItem : overlayItems){
							if(overlayItem.getGeoPoint().getLat() == lat && overlayItem.getGeoPoint().getLon()==lon){
								CustomLog.d(TAG,"点击的是标记点，显示头像");
								final int sex = KidsWatUtils.getBabySex();
								int resSex = sex == 1 ? R.drawable.icon_boy : R.drawable.icon_girl;
								Drawable drawable = getResources().getDrawable(resSex);
								overlayItem.setDrawable(drawable);
							}
						}
						//显示信息在linearlayout中
						long ltime = row.time;
						int location_type = row.type;
						final String strdate = new java.text.SimpleDateFormat("HH:mm")
								.format(new java.util.Date(ltime * 1000));
						final String otherText = String.format("%s",
								location_type == 1 ? "GPS"
										: "GSM/WIFI");
						getActivity().runOnUiThread(new Runnable() {
							public void run() {
								CustomLog.d(TAG,"on marker"+""+" click");
								mTvAddress.setText(row.address);
								mTvTime.setText(strdate);
								textView_range.setText(otherText);
								mTvElectricity.setText(row.electricity + "%");
								setDL(row.electricity);
								if (row.electricity == 0) {
									mTvAddress.setText(getResources().getString(
											R.string.tip_available));
								}
							}
						});
					}
				}*/
                break;
        }
    }

    /*
    // 此方法未能实现在地图上画圆，无用
    public void showCircle(double lat, double lon) {
        MyCircleOverlay myCircleOverlay = new MyCircleOverlay(getActivity(), mapController, mapview, (float) lat, (float) lon, 50f);

        overlayManager.addOverlay(myCircleOverlay);
    }
    */

    @Override
    public void onResume() {
        flag = false;
        CustomLog.d(TAG,"on resume");
        //let's use cached data
        showLastLocationPossibleFromCache();
        //let's also update
        boolean updateInBackground=true;//AppContext.getInstance().get
        get_location_ex(updateInBackground);
        super.onResume();
    }

    @Override
    public void onPause() {
        flag = true;
        super.onPause();
    }

    private void showNoLocationResultZeroElectricity() {
        CustomLog.d(TAG,"Will whow NoLocatonResult - zero power,etc");
        mTvAddress.setText(getResources().getString(
                R.string.tip_available));
        mTvTime.setText(getString(R.string.home_fragment_hint3));

    }
    private void showNoLocationResult() {
        CustomLog.d(TAG,"Will whow NoLocatonResult");
        // TODO Auto-generated method stub
        setDL(0);
        mTvElectricity.setText("0%");
        mTvAddress.setText(getString(R.string.home_fragment_hint3));
        mTvTime.setText(getString(R.string.home_fragment_hint6));
    }


    private static boolean initedOverlay=false;
    private void initMapOverlay() {
        if (initedOverlay) {
            CustomLog.w(TAG,"Arleady inited overlay. not doing it again. should NOT happen");
            return;
        }

        //TODO:set Zoom
        //position to first object

        mapview.getMap().
        overlayManager.addOverlay(overlay);
        mapController.setZoomCurrent(12);
        if (overlay.getOverlayItems().size()>0) {
            mapController.setPositionAnimationTo(((OverlayItem) (overlay.getOverlayItems().get(0))).getGeoPoint());
        } else {
            CustomLog.w(TAG,"initMapOverlay. tryng to animate but no overlay items were present. just NOT CRASH. lastLocationUpdate:"+lastLocationUpdate+", aka:"+Utils.getTimeFromTimestamp(lastLocationUpdate));
        }
        initedOverlay=true;

    }


    private void add_map_marker(CRows row) {
        CustomLog.d(TAG,String.format("add_map_marker:Will be adding overlay item. lat:%f, lon:%f, loc_type:%d,addr:%s, time:%d aka %s ",
                row.latitude,
                row.longitude,
                row.type,
                row.address,
                row.time,
                Utils.getTimeFromTimestamp(row.time)));

        GeoPoint point = new GeoPoint(row.latitude, row.longitude);
        AppContext.getInstance().setLng(point);

        Drawable image = row.type == 2 ? getResources().getDrawable(R.drawable.point_select) : getResources().getDrawable(R.drawable.ing);
        OverlayItem overlayItem = new OverlayItem(point, image);

        if (overlay.getOverlayItems() != null) {
            CustomLog.d(TAG,"add_map_marker:overlay have "+overlay.getOverlayItems().size()+" items. clearing");
            overlay.clearOverlayItems();
        } else {
            CustomLog.d(TAG,"add_map_marker:overlay have no items list");
        }
        overlay.addOverlayItem(overlayItem);
        //int size=overlayManager.getOverlays().size();
        //CustomLog.d(TAG,"add_map_marker:resulting size of our overlay:"+overlay.getOverlayItems().size());
    }
    private void replace_map_marker(GeoPoint point) {
        // TODO Auto-generated method stub
        CustomLog.d(TAG,String.format("Will be replacing overlay item.  lat:%f, lon:%f ",point.getLat(),point.getLon()));

        overlay.clearOverlayItems();
        OverlayItem item = new OverlayItem(point, getActivity().getResources().getDrawable(R.drawable.point_select));
        overlay.addOverlayItem(item);
    }

    // 获取最新点的数据
    public void get_watch_data_lastest_step() {
        String uid = KidsWatConfig.getUserUid();
        String access_token = KidsWatConfig.getUserToken();
        String family_id = KidsWatConfig.getDefaultFamilyId();

        if (StringUtils.isEmpty(uid))
            return;

        Calendar a = Calendar.getInstance();
        int year = a.get(Calendar.YEAR);
        int month = a.get(Calendar.MONTH);
        int dayOfMonth = a.get(Calendar.DAY_OF_MONTH);

        long utcToday = KidsWatUtils.getUtcTimeAt0Time(year, month, dayOfMonth);
        final String request_url = KidsWatApiUrl.getUrlFor___get_watch_data_latest(uid, access_token, family_id,
                utcToday);

        ApiHttpClient.get(request_url, new HttpCallBack() {

            @Override
            public void onPreStart() {
                super.onPreStart();
            }

            @Override
            public void onSuccess(String t) {
                super.onSuccess(t);
                // 从服务器获取最新点接口返回数据
                beanFor___get_watch_data_latest response = AppContext.getInstance().getGson().fromJson(t,
                        beanFor___get_watch_data_latest.class);
                // 如果返回没有错误
                if (response.error == 0) {

                    // 如果有数据
                    if (response.info.lastest_point != null) {
                        if (overlay.getOverlayItems() != null) {
                            // 获取最近点
                            beanFor___get_watch_data_latest.CInfo.CData lastest_point = response.info.lastest_point;

                            ((MainActivity) getActivity()).setStepNumber(lastest_point.step_number);
                        }
                    }
                }
            }

            @Override
            public void onFailure(int errorNo, String strMsg) {
                super.onFailure(errorNo, strMsg);
            }

            @Override
            public void onFinish() {
                super.onFinish();
            }
        });
    }


}

