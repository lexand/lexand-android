package com.qiwo.xkidswatcher_russia.yandexmap;

import android.content.Context;

import java.util.Map;

import ru.yandex.yandexmapkit.MapController;
import ru.yandex.yandexmapkit.MapView;
import ru.yandex.yandexmapkit.overlay.Overlay;
import ru.yandex.yandexmapkit.overlay.OverlayItem;

public class MyCircleOverlay extends Overlay {

    MapView mMmapView;
    Map<Long, OverlayItem> mData;

    public MyCircleOverlay(Context context, MapController mapController, MapView mapView, float lat, float lon, float radius) {
        super(mapController);
        mMmapView = mapView;
        this.setIRender(new MyCircleOverlayIRender(lat, lon, radius, this));
    }

    @Override
    public int compareTo(Object arg0) {
        return 0;
    }

}