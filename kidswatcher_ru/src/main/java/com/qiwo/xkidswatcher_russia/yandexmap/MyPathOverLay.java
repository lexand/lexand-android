package com.qiwo.xkidswatcher_russia.yandexmap;

import android.content.Context;

import com.qiwo.xkidswatcher_russia.R;

import java.util.Map;

import ru.yandex.yandexmapkit.MapController;
import ru.yandex.yandexmapkit.MapView;
import ru.yandex.yandexmapkit.overlay.Overlay;
import ru.yandex.yandexmapkit.overlay.OverlayItem;

public class MyPathOverLay extends Overlay {

    MapView mMmapView;
    Map<Long, OverlayItem> mData;

    public MyPathOverLay(Context context, MapController mapController, MapView mapView, Map<Long, OverlayItem> mData) {
        super(mapController);
        //this.context = context;
        mMmapView = mapView;
        this.mData = mData;
        this.setIRender(new MyOverlayIRender(this));
//		this.setIRender(new MyOverlayIRender(this,mData));

        //模拟数据
//		Map<Long, OverlayItem> mDatas = new HashMap<Long, OverlayItem>();
//		mDatas.put(System.currentTimeMillis(), new OverlayItem(new GeoPoint(22,114), context.getResources().getDrawable(R.drawable.ic_launcher)));
//		mDatas.put(System.currentTimeMillis(), new OverlayItem(new GeoPoint(24,116), context.getResources().getDrawable(R.drawable.ic_launcher)));

//		for (Long key : mDatas.keySet()) {
//			OverlayItem overlay = mDatas.get(key);
        for (Long key : mData.keySet()) {
            OverlayItem overlay = mData.get(key);

            double x = overlay.getGeoPoint().getLat();
            double y = overlay.getGeoPoint().getLon();

            OverlayItem m = new OverlayItem(
                    overlay.getGeoPoint(),
                    context.getResources().getDrawable(R.drawable.ing_smail));
            //m.setOffsetY(-23);
            this.addOverlayItem(m);
//			mMmapView.getMapController().setPositionNoAnimationTo(mMmapView.getMapController()
//					.getGeoPoint(new ScreenPoint(x, y)));
        }
        //mMmapView.getMapController().setPositionAnimationTo(new GeoPoint(22,114));
    }

    @Override
    public int compareTo(Object arg0) {
        return 0;
    }
	
	/*Context context;
	@Override
	public boolean onLongPress(float x, float y) {
		OverlayItem m = new OverlayItem(
				mMmapView.getMapController().getGeoPoint(new ScreenPoint(x, y)),
				context.getResources().getDrawable(R.drawable.ic_launcher));
		m.setOffsetY(-23);
		this.addOverlayItem(m);
		mMmapView.getMapController().setPositionNoAnimationTo(mMmapView.getMapController()
				.getGeoPoint(new ScreenPoint(x, y)));
		return true;
	}*/
}