package com.qiwo.xkidswatcher_russia.yandexmap;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;

import ru.yandex.yandexmapkit.overlay.Overlay;
import ru.yandex.yandexmapkit.overlay.OverlayIRender;
import ru.yandex.yandexmapkit.overlay.OverlayItem;

public class MyCircleOverlayIRender extends OverlayIRender {

    private float mLat;
    private float mLon;
    private float mRadius;
    private Overlay mOverlay;

    public MyCircleOverlayIRender(float lat, float lon, float radius, Overlay overlay) {
        super();
        mLat = lat;
        mLon = lon;
        mRadius = radius;
        mOverlay = overlay;
    }

    @Override
    public void draw(Canvas canvas, OverlayItem overlayItem) {
        super.draw(canvas, overlayItem);
        Paint mPaint = new Paint();
        mPaint.setDither(true);
        mPaint.setColor(Color.BLUE);
        mPaint.setStyle(Paint.Style.FILL_AND_STROKE);
        mPaint.setStrokeJoin(Paint.Join.ROUND);
        mPaint.setStrokeCap(Paint.Cap.ROUND);
        mPaint.setStrokeWidth(6);
        mPaint.setAntiAlias(true);

        canvas.drawCircle(mLat, mLon, mRadius, mPaint);
        canvas.save();
    }

}
