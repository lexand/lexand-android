package com.qiwo.xkidswatcher_russia.ui;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.Display;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.data.Contents;
import com.google.zxing.encoder.QRCodeEncoder;
import com.qiwo.xkidswatcher_russia.AppContext;
import com.qiwo.xkidswatcher_russia.R;
import com.qiwo.xkidswatcher_russia.base.BaseActivity;
import net.intari.CustomLogger.CustomLog;

import butterknife.BindView;;

/**
 *
 */
public class QrcodeActivity extends BaseActivity {
    public static final String TAG = QrcodeActivity.class.getSimpleName();

    @BindView(R.id.item_phone_no)
    TextView item_phone_no;

    @BindView(R.id.contents_text_view)
    TextView contents_text_view;

    @BindView(R.id.imageview_l)
    ImageView imageview_back;

    @BindView(R.id.linearLayout_l)
    LinearLayout linearLayout_l;
    // -----------
    String qrcode;
    String simnum;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_qrcode;
    }

    @Override
    protected boolean hasBackButton() {
        return false;
    }

    @Override
    protected boolean hasActionBar() {
        return false;
    }

    @Override
    protected void init(Bundle savedInstanceState) {
        super.init(savedInstanceState);
        qrcode = getIntent().getStringExtra("qrcode");
        simnum = getIntent().getStringExtra("sim");

    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.linearLayout_l:
                finish();
                break;
            default:
                break;
        }
    }

    @Override
    public void initView() {
        linearLayout_l.setOnClickListener(this);
    }

    @Override
    public void initData() {
        WindowManager manager = (WindowManager) getSystemService(WINDOW_SERVICE);
        Display display = manager.getDefaultDisplay();
        int width = display.getWidth();
        int height = display.getHeight();
        int smallerDimension = width < height ? width : height;
        smallerDimension = smallerDimension * 7 / 8;
        try {
            QRCodeEncoder qrCodeEncoder = null;
            qrCodeEncoder = new QRCodeEncoder(qrcode, null, Contents.Type.TEXT,
                    BarcodeFormat.QR_CODE.toString(), smallerDimension);
            Bitmap bitmap = qrCodeEncoder.encodeAsBitmap();
            ImageView v = (ImageView) findViewById(R.id.image_view);
            v.setImageBitmap(bitmap);

            TextView item_phone_no = (TextView) findViewById(R.id.item_phone_no);
            item_phone_no.setText("MSISDN : " + simnum);

            contents_text_view.setText(getString(R.string.qrcode_1) + AppContext.getInstance().getCurrentFamily().device_iccid);
        } catch (Exception e) {
            CustomLog.logException(TAG,e);
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            //
        }
        return super.onKeyDown(keyCode, event);
    }

}
