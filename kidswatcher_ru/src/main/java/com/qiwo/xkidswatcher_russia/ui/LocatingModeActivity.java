package com.qiwo.xkidswatcher_russia.ui;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.qiwo.xkidswatcher_russia.AnalyticsUtils.AnalyticsUtils;
import com.qiwo.xkidswatcher_russia.AppContext;
import com.qiwo.xkidswatcher_russia.Constants;
import com.qiwo.xkidswatcher_russia.KidsWatConfig;
import com.qiwo.xkidswatcher_russia.R;
import com.qiwo.xkidswatcher_russia.api.ApiHttpClient;
import com.qiwo.xkidswatcher_russia.api.remote.KidsWatApiUrl;
import com.qiwo.xkidswatcher_russia.base.BaseActivity;
import com.qiwo.xkidswatcher_russia.bean.beanForDb___Family;
import com.qiwo.xkidswatcher_russia.thread.CommonThreadMethod;
import com.qiwo.xkidswatcher_russia.thread.ThreadParent;
import net.intari.CustomLogger.CustomLog;
import com.qiwo.xkidswatcher_russia.widget.DividerItemDecoration;
import com.qiwo.xkidswatcher_russia.widget.WrapLinearLayoutManager;

import org.json.JSONException;
import org.json.JSONObject;
import org.kymjs.kjframe.http.HttpCallBack;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;;import static com.qiwo.xkidswatcher_russia.Constants.LOG_UPD_MODE_CONFIG_EVENTS;

public class LocatingModeActivity extends BaseActivity {
    public static final String TAG = LocatingModeActivity.class.getSimpleName();

    public static final int setdevicesleepModeCode = 15998;
    public static final int getDeviceSleepModeCode = 15997;
    private final String activesleepmode = "w461_set_device_is_active_sleep_mode_v2";
    private final String getdevicevoicelevelAction = "w461_get_device_voice_level_and_shark_mode_and_call_volume";
    @BindView(R.id.linearLayout_l)
    LinearLayout linearLayout_l;
    // ---------------
    @BindView(R.id.recyclerview)
    RecyclerView recyclerView;
    LinearLayout setting_time_zone_linear;
    TextView setting_time_zone_linear_time;
    LinearLayoutManager layoutManager;
    List<RecycleItem> datas;
    StRecycleAdapter mAdapter;
    private int sleepMode;
    private int timeZoneState;
    private CharSequence sleepMode_tip = "You can not do this when the device is under sleep mode.";
    private Handler handler = new Handler() {

        @Override
        public void handleMessage(Message msg) {

            String mess = msg.obj.toString();
            int code = msg.arg1;

            if (mess == null || mess.equals("")) {
                hideWaitDialog();
                return;
            }
            JSONObject json = null;
            try {
                json = new JSONObject(mess);

                int errorCode = json.getInt("error");
                String message=null;
                try {
                    message = json.getJSONObject("info")
                            .getString("message");

                } catch (Exception ex) {
                    CustomLog.logException(ex);
                    message=String.format("Error parsing message. Server's error code is %d",errorCode);
                }
                Map<String,Object> eventProperties=new HashMap<>();
                eventProperties.put(Constants.ERRORCODE,errorCode);
                eventProperties.put(Constants.MESSAGE,message);

                switch (code) {
                    case setdevicesleepModeCode:

                        if (errorCode == 0) {
                            CustomLog.d(TAG,"change sleep mode success!");

                            AnalyticsUtils.reportAnalyticsEvent(Constants.EVENT_LOCATING_MODE_CHANGE_SUCCESS,eventProperties);

                            GetState();
                            break;
                        } else if (errorCode == 2015001) {
                            CustomLog.e(TAG,"change sleep mode errorr!. device sleeps");

                            AnalyticsUtils.reportAnalyticsEvent(Constants.EVENT_LOCATING_MODE_CHANGE_ERROR_DEVICE_SLEEPS,eventProperties);
                            showConfirmInformation(null, getString(R.string.watch_setting_1));
                        } else {
                            CustomLog.e(TAG,"change sleep mode errorr!. errcode "+errorCode);
                            AnalyticsUtils.reportAnalyticsEvent(Constants.EVENT_LOCATING_MODE_CHANGE_ERROR,eventProperties);
                        }
                        hideWaitDialog();

                        GetState();
                        break;
                    case getDeviceSleepModeCode:
                        if (errorCode == 0) {
                            JSONObject infoJson = json.getJSONObject("info");

                            sleepMode = infoJson.getInt("is_active_sleep_mode");
                            if (!infoJson.isNull("time_zone"))
                                timeZoneState = infoJson.getInt("time_zone");
                            CustomLog.d(TAG,"------sleep mode-----" + sleepMode);

                            /*
                             * sleepmode
                             */
//                            if (sleepMode == 1)
//                                iv_sleepMode.setBackgroundResource(R.drawable.switchs_s);
//                            else
//                                iv_sleepMode.setBackgroundResource(R.drawable.switchs);
                            hideWaitDialog();

                            /**time_zone**/
                            String strTimeZone = timeZoneState >= 0 ? "+" + timeZoneState : timeZoneState + "";
                            setting_time_zone_linear_time.setText("GMT " + strTimeZone);
                        }

                    default:
                        break;
                }

            } catch (JSONException e) {
                CustomLog.logException(TAG,e);
            }
        }
    };
    private OnItemClickListener recyleItemListener = new OnItemClickListener() {

        @Override
        public void onItemClick(int position, Object object) {
            RecycleItem bean = (RecycleItem) object;
            CustomLog.d(TAG,"-----" + bean.type + bean.title);
            for (RecycleItem item : datas) {
                if (item.type == bean.type) {
                    item.selected = true;
                } else {
                    item.selected = false;
                }
            }
            mAdapter.notifyDataSetChanged();
            // ---------------
            int modeType = 3;
            if (bean.type == 1) {
                modeType = 1;
            } else if (bean.type == 2) {
                modeType = 3;
            } else if (bean.type == 3) {
                modeType = 4;
            }

//			int modeType = bean.type == 1 ? 2 : 1;
            CustomLog.d(TAG,"----modeType=" + modeType);
            update_mode_config(modeType);
        }

    };

    @Override
    protected int getLayoutId() {
        return R.layout.activity_locating_mode;
    }

    @Override
    protected boolean hasActionBar() {
        return false;
    }

    @Override
    protected void init(Bundle savedInstanceState) {
        super.init(savedInstanceState);
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.linearLayout_l:
                finish();
                break;
            case R.id.setting_time_zone_linear:
                /**只有管理员才能设置时区**/
                if (AppContext.getInstance().getCurrentFamily() == null) {
                    Toast.makeText(this, getString(R.string.new_setting_fragment_1), Toast.LENGTH_SHORT).show();
                    return;
                }
                Intent mIntent = new Intent();
                mIntent.setClass(this, SettingTimeZoneActivity.class);
                mIntent.putExtra(Watch_SettingsActivity.SETTINGTIMEZONESTATE, timeZoneState);
                mIntent.putExtra(Watch_SettingsActivity.INACTIVITY, 1);
                startActivityForResult(mIntent, Watch_SettingsActivity.SETTINGTIMEZONECODE);
                break;
            case R.id.ll_sleepMode:
                beanForDb___Family f = AppContext.getInstance().getCurrentFamily();
                if (f != null && f.d_mode == 2) {
                    Toast.makeText(LocatingModeActivity.this, sleepMode_tip, Toast.LENGTH_SHORT).show();
                    break;
                }

//                if (sleepMode == 0)
//                    iv_sleepMode.setBackgroundResource(R.drawable.switchs_s);
//                else
//                    iv_sleepMode.setBackgroundResource(R.drawable.switchs);

                upDateSleepMode();
            default:
                break;
        }
    }

    /**
     * 获取状态
     */
    public void GetState() {
        showWaitDialog();
        new Thread(new ThreadParent(TAG,KidsWatApiUrl.getApiUrl(),
                CommonThreadMethod.JsonFormat(GetStateParameter()), handler,
                getDeviceSleepModeCode)).start();
    }

    /*
     * 获取状态的参数
     */
    public Map<String, String> GetStateParameter() {
        Map<String, String> hash = new HashMap<String, String>();
        hash.put("uid", KidsWatConfig.getUserUid());
        hash.put("device_id", KidsWatConfig.getUserDeviceId());
        hash.put("access_token", KidsWatConfig.getUserToken());
        KidsWatApiUrl.addExtraPara(getdevicevoicelevelAction, hash);
        return hash;
    }

    private void upDateSleepMode() {
        showWaitDialog();

        new Thread(new ThreadParent(TAG,KidsWatApiUrl.getApiUrl(),
                CommonThreadMethod.JsonFormat(SetStateParameter(activesleepmode)),
                handler, setdevicesleepModeCode)).start();
    }

    public void getIs_sleepModeState() {
        // if (vibrate_image
        // .getBackground()
        // .getConstantState()
        // .equals(getResources().getDrawable(R.drawable.switchs)
        // .getConstantState())) {
        if (sleepMode == 1) {
            sleepMode = 0;
        } else {
            sleepMode = 1;
        }
    }

    /*
     * 获取状态的参数
     */
    public Map<String, String> SetStateParameter(String action) {
        Map<String, String> sleepHash = new HashMap<String, String>();
        sleepHash.put("uid", KidsWatConfig.getUserUid());
        sleepHash.put("device_id", KidsWatConfig.getUserDeviceId());
        sleepHash.put("access_token", KidsWatConfig.getUserToken());

        getIs_sleepModeState();
        //
        sleepHash.put("is_active_sleep_mode", sleepMode + "");

        KidsWatApiUrl.addExtraPara(action, sleepHash);
        return sleepHash;
    }

    @Override
    public void initView() {
        setting_time_zone_linear_time = (TextView) findViewById(R.id.setting_time_zone_linear_time);
        setting_time_zone_linear = (LinearLayout) findViewById(R.id.setting_time_zone_linear);
//        ll_sleepMode = (LinearLayout) findViewById(R.id.ll_sleepMode);
//        ll_sleepMode.setVisibility(View.INVISIBLE);

        if (AppContext.getInstance().getLoginUser_kid().isAdmin == 1 && !("361".equals(AppContext.getInstance().getCurrentFamily().version) ||
                AppContext.getInstance().getCurrentFamily().version.startsWith("601"))) {
//            ll_sleepMode.setVisibility(View.GONE);
        } else if ("361".equals(AppContext.getInstance().getCurrentFamily().version) && AppContext.getInstance().getLoginUser_kid().isAdmin == 1) {
//            ll_sleepMode.setVisibility(View.GONE);
//            if (AppContext.getInstance().getLoginUser_kid().isAdmin == 1) {
            setting_time_zone_linear.setVisibility(View.VISIBLE);
//            } else {
//                setting_time_zone_linear.setVisibility(View.GONE);
//            }
        } else if ("601".equals(AppContext.getInstance().getCurrentFamily().version) && AppContext.getInstance().getLoginUser_kid().isAdmin == 1) {
//            ll_sleepMode.setVisibility(View.VISIBLE);
        } else {
//            ll_sleepMode.setVisibility(View.GONE);
        }

//        ll_sleepMode.setVisibility(View.GONE);

        linearLayout_l.setOnClickListener(this);
//        ll_sleepMode.setOnClickListener(this);
        setting_time_zone_linear.setOnClickListener(this);
//        iv_sleepMode = (ImageView) findViewById(R.id.sleepmode_image);
    }

    @Override
    public void initData() {
        datas = new ArrayList<RecycleItem>();
        datas.add(new RecycleItem(1, R.drawable.icon_small_mode, getApplicationContext().getResources().getString(R.string.small_mode),
                getApplicationContext().getResources().getString(R.string.tip_small_mode), false));
        datas.add(new RecycleItem(2, R.drawable.icon_safe_mode, getApplicationContext().getResources().getString(R.string.safe_mode),
                getApplicationContext().getResources().getString(R.string.tip_safe_mode), true));
        datas.add(new RecycleItem(3, R.drawable.icon_battery_safing_mode,
                getApplicationContext().getResources().getString(R.string.save_mode), getApplicationContext().getResources().getString(R.string.tip_save_mode),
                false));
        // ---------------
        layoutManager = new WrapLinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);// 设置布局管理器
        mAdapter = new StRecycleAdapter(datas);
        mAdapter.setOnItemClickListener(recyleItemListener);
        recyclerView.addItemDecoration(new DividerItemDecoration(this, null));
        recyclerView.setAdapter(mAdapter);// 设置适配器
        get_mode_config();
        GetState();
    }

//    private ImageView iv_sleepMode;

//    private LinearLayout ll_sleepMode;

    // ----------------
    private void update_mode_config(final int mode) {

        // -----------------
        String uid = KidsWatConfig.getUserUid();
        String access_token = KidsWatConfig.getUserToken();
        String family_id = KidsWatConfig.getDefaultFamilyId();

        final String request_url = KidsWatApiUrl
                .getUrlFor___update_mode_config(uid, access_token, family_id,
                        mode);
        // -------------------------

        Map<String,Object> eventProperties=new HashMap<>();
        eventProperties.put(Constants.URL,request_url);
        if (LOG_UPD_MODE_CONFIG_EVENTS) {
            AnalyticsUtils.reportAnalyticsEvent(Constants.EVENT_UPD_MODE_CONFIG_START,eventProperties);
        }


        ApiHttpClient.get(request_url, new HttpCallBack() {

            @Override
            public void onPreStart() {
                showWaitDialog(getApplicationContext().getResources().getString(R.string.loading) + "...");
            }

            @Override
            public void onSuccess(String t) {
                super.onSuccess(t);
                CustomLog.d(TAG,String.format("url:%s\nt:%s", request_url, t));

                try {
                    JSONObject response = new JSONObject(t);

                    int code = response.getInt("error");
                    String message;
                    String messageToUser=null;
                    try {
                        message = response.getJSONObject("info")
                                .getString("message");
                        messageToUser=message;

                    } catch (Exception ex) {
                        CustomLog.logException(ex);
                        message=String.format("Error parsing message. Server's error code is %d",code);
                    }
                    Map<String,Object> eventProperties=new HashMap<>();
                    eventProperties.put(Constants.ERRORCODE,code);
                    eventProperties.put(Constants.MESSAGE,message);
                    if (code == 0) {
                        // {"error":0,"info":{"message":"Successful operation!"}}
                        AnalyticsUtils.reportAnalyticsEvent(Constants.EVENT_UPD_MODE_CONFIG_SUCCESS,eventProperties);
                        finish();
                    } else if (code == 2015001) {
                        JSONObject Ljs = response.getJSONObject("info");
                        if (LOG_UPD_MODE_CONFIG_EVENTS) {
                            AnalyticsUtils.reportAnalyticsEvent(Constants.EVENT_UPD_MODE_CONFIG_ERROR_DEVICE_SLEEPS, eventProperties);
                        }

                        Toast.makeText(LocatingModeActivity.this, getString(R.string.watch_setting_1), Toast.LENGTH_SHORT).show();
                        get_mode_config();
                        //showConfirmInformation(null, getApplicationContext().getResources().getString(R.string.tip_operation_faild));

                        if (messageToUser!=null) {
                            showConfirmInformation(null, messageToUser);

                        } else {
                            showConfirmInformation(null, getApplicationContext().getResources().getString(R.string.tip_operation_faild));
                        }

                    } else {
                        get_mode_config();
                        if (LOG_UPD_MODE_CONFIG_EVENTS) {
                            AnalyticsUtils.reportAnalyticsEvent(Constants.EVENT_UPD_MODE_CONFIG_ERROR, eventProperties);
                        }
                        if (messageToUser!=null) {
                            showConfirmInformation(null, messageToUser);

                        } else {
                            showConfirmInformation(null, getApplicationContext().getResources().getString(R.string.tip_operation_faild));
                        }

                    }
                } catch (JSONException e) {
                    CustomLog.logException(TAG,e);
                    Map<String,Object> eventProperties=new HashMap<>();
                    eventProperties.put(Constants.EVENT_EXCEPTION,e);
                    if (LOG_UPD_MODE_CONFIG_EVENTS) {
                        AnalyticsUtils.reportAnalyticsEvent(Constants.EVENT_UPD_MODE_CONFIG_EXCEPTION, eventProperties);
                    }
                }
            }

            @Override
            public void onFailure(int errorNo, String strMsg) {
                super.onFailure(errorNo, strMsg);
                String msg = String.format("errorNo:%s\n%s", errorNo, strMsg);
                CustomLog.d(TAG,"error=" + msg);
                Map<String,Object> eventProperties=new HashMap<>();
                eventProperties.put(Constants.MESSAGE,strMsg);
                eventProperties.put(Constants.ERRORCODE,errorNo);
                if (LOG_UPD_MODE_CONFIG_EVENTS) {
                    AnalyticsUtils.reportAnalyticsEvent(Constants.EVENT_UPD_MODE_CONFIG_SERVER_FAILED, eventProperties);
                }
            }

            @Override
            public void onFinish() {
                dismissDialog(null, 10);
            }
        });
    }

    private void get_mode_config() {
        String uid = KidsWatConfig.getUserUid();
        String access_token = KidsWatConfig.getUserToken();
        String family_id = KidsWatConfig.getDefaultFamilyId();
        final String request_url = KidsWatApiUrl.getUrlFor___get_mode_config(
                uid, access_token, family_id);
        // --------------
        ApiHttpClient.get(request_url, new HttpCallBack() {

            @Override
            public void onPreStart() {
                // showWaitDialog("...loading...");
            }

            @Override
            public void onSuccess(String t) {
                super.onSuccess(t);
                CustomLog.d(TAG,String.format("url:%s\nt:%s", request_url, t));

                try {
                    JSONObject response = new JSONObject(t);

                    int code = response.getInt("error");
                    if (code == 0) {
                        // response={"data":{"mode":2},"error":0,"info":{"message":"get success!"}}
                        //{"error":0,"data":{"is_ring":0,"is_shark":0,"call_volume":3,"is_active_sleep_mode":0,"mode":1}}
                        int mode = response.getJSONObject("data")
                                .getInt("mode");
//                        if (mode == 1) {
//                            datas.get(0).selected = false;
//                            datas.get(1).selected = true;
//                            mAdapter.notifyDataSetChanged();
//                        } else if (mode == 2) {
//                            datas.get(0).selected = true;
//                            datas.get(1).selected = false;
//                            mAdapter.notifyDataSetChanged();
//                        }
                        if (mode == 1) {
                            datas.get(0).selected = true;
                            datas.get(1).selected = false;
                            datas.get(2).selected = false;
                            mAdapter.notifyDataSetChanged();
                        } else if (mode == 3) {
                            datas.get(0).selected = false;
                            datas.get(1).selected = true;
                            datas.get(2).selected = false;
                            mAdapter.notifyDataSetChanged();
                        } else if (mode == 4) {
                            datas.get(0).selected = false;
                            datas.get(1).selected = false;
                            datas.get(2).selected = true;
                            mAdapter.notifyDataSetChanged();
                        }
                    }

                } catch (JSONException e) {
                    CustomLog.logException(TAG,e);
                }
            }

            @Override
            public void onFailure(int errorNo, String strMsg) {
                super.onFailure(errorNo, strMsg);
                String msg = String.format("errorNo:%s\n%s", errorNo, strMsg);
                CustomLog.d(TAG,"error=" + msg);
            }

            @Override
            public void onFinish() {
                dismissDialog(null, 10);
            }
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Watch_SettingsActivity.SETTINGTIMEZONECODE) {
            /**time_zone**/
            if (data != null) {
                int tState = data.getIntExtra(Watch_SettingsActivity.SETTINGTIMEZONESTATE, 0);
                timeZoneState = SettingTimeZoneActivity.timeZoneListInt[tState];
                setting_time_zone_linear_time.setText("GMT " + SettingTimeZoneActivity.timeZoneList[tState]);
            }
        }
    }

    /**
     * 内部接口回调方法
     */
    public interface OnItemClickListener {
        void onItemClick(int position, Object object);
    }

    // -------------
    public class RecycleItem {
        public int type;
        public int drawableId;
        public String title;
        public String des;
        public boolean selected;

        private RecycleItem(int _type, int _drawableId, String _title,
                            String _des, boolean _selected) {
            type = _type;
            drawableId = _drawableId;
            title = _title;
            des = _des;
            selected = _selected;
        }

    }

    public class StRecycleAdapter extends
            RecyclerView.Adapter<StRecycleAdapter.ViewHolder> {

        private List<RecycleItem> list;
        private OnItemClickListener listener;

        public StRecycleAdapter(List<RecycleItem> list) {
            this.list = list;
        }

        @Override
        public int getItemCount() {
            return list.size();
        }

        @Override
        public void onBindViewHolder(ViewHolder holder, final int position) {

            final RecycleItem bean = list.get(position);
            // holder.title.setText(bean.getTitle());
            // holder.imageView.setImageResource(bean.getResId())；

            holder.imageView_img.setBackgroundResource(bean.drawableId);
            holder.imageView_selected
                    .setVisibility(bean.selected ? View.VISIBLE
                            : View.INVISIBLE);
            holder.textView_title.setText(bean.title);
            holder.textView_des.setText(bean.des);
            /**
             * 调用接口回调
             */
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (null != listener)
                        listener.onItemClick(position, bean);
                }
            });
        }

        /**
         * 设置监听方法
         *
         * @param listener
         */
        public void setOnItemClickListener(OnItemClickListener listener) {
            this.listener = listener;
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int position) {
            View view = LayoutInflater.from(viewGroup.getContext()).inflate(
                    R.layout.view_locating_mode_item, viewGroup, false);
            ViewHolder holder = new ViewHolder(view);
            return holder;
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            public ImageView imageView_img;
            public ImageView imageView_selected;
            public TextView textView_title;
            public TextView textView_des;

            public ViewHolder(View itemView) {
                super(itemView);
                imageView_img = (ImageView) itemView
                        .findViewById(R.id.imageView_img);
                imageView_selected = (ImageView) itemView
                        .findViewById(R.id.imageView_selected);
                textView_title = (TextView) itemView
                        .findViewById(R.id.textView_title);
                textView_des = (TextView) itemView
                        .findViewById(R.id.textView_des);
            }
        }
    }

}
