package com.qiwo.xkidswatcher_russia.ui;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.qiwo.xkidswatcher_russia.AppContext;
import com.qiwo.xkidswatcher_russia.KidsWatConfig;
import com.qiwo.xkidswatcher_russia.R;
import com.qiwo.xkidswatcher_russia.api.ApiHttpClient;
import com.qiwo.xkidswatcher_russia.api.remote.KidsWatApiUrl;
import com.qiwo.xkidswatcher_russia.base.BaseActivity;
import com.qiwo.xkidswatcher_russia.bean.beanFor___check_qrcode;
import com.qiwo.xkidswatcher_russia.util.Contanst;
import net.intari.CustomLogger.CustomLog;

import org.json.JSONException;
import org.json.JSONObject;
import org.kymjs.kjframe.http.HttpCallBack;

import butterknife.BindView;;

/**
 *
 */
public class ScanGuideAActivity extends BaseActivity {


    public static final String PHONESIM = "phone_sim";
    public static final String QRCODE = "qrcode";
    final String TAG = ScanGuideAActivity.class.getSimpleName();
    final int SHOW_SCAN_QRCODE = 100;
    @BindView(R.id.button_next)
    Button button_next;
    @BindView(R.id.imageview_l)
    ImageView imageview_back;
    @BindView(R.id.linearLayout_l)
    LinearLayout linearLayout_l;
    @BindView(R.id.linearLayout_r)
    LinearLayout linearLayout_r;
    String action;
    String qrcode;
    String simnum;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_scan_guide_a;
    }

    @Override
    protected boolean hasActionBar() {
        return false;
    }

    @Override
    protected void init(Bundle savedInstanceState) {
        super.init(savedInstanceState);
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.linearLayout_l:
                finish();
                break;
            case R.id.linearLayout_r:
                // AppManager.getAppManager().AppExit(ScanGuideAActivity.this);
                KidsWatConfig.cleanLoginInfo();
                Intent login_intent = new Intent(this, UserLogin2Activity.class);
                startActivity(login_intent);
                finish();
                break;
            case R.id.button_next:
                startScan();
//			Intent intent = new Intent(this, ScanGuideCameraActivity.class);
//			startActivityForResult(intent, SHOW_SCAN_QRCODE);
                break;
            default:
                break;

        }
    }

    private void startScan() {
        // TODO Auto-generated method stub
        /**
         * 获取摄像头的权限
         */
        if (Build.VERSION.SDK_INT >= 23) {
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
                //拥有权限
                Intent intent = new Intent(this, ScanGuideCameraActivity.class);
                startActivityForResult(intent, SHOW_SCAN_QRCODE);
            } else {
                //申请权限
                ActivityCompat.requestPermissions(ScanGuideAActivity.this, new String[]{Manifest.permission.CAMERA}, Contanst.REQUEST_CODE_REQUEST_PERMISSION);
                return;
            }
        } else {
            Intent intent = new Intent(this, ScanGuideCameraActivity.class);
            startActivityForResult(intent, SHOW_SCAN_QRCODE);
        }
    }

    @SuppressLint("Override")
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == Contanst.REQUEST_CODE_REQUEST_PERMISSION) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                //permission granted
                //TODO 向SD卡写数据
                Intent intent = new Intent(this, ScanGuideCameraActivity.class);
                startActivityForResult(intent, SHOW_SCAN_QRCODE);
            } else {
                //permission denied
                //TODO 显示对话框告知用户必须打开权限
                Toast.makeText(ScanGuideAActivity.this, getApplicationContext().getResources().getString(R.string.camera_permission), Toast.LENGTH_LONG).show();
            }
        }
    }

    @Override
    public void initView() {
        linearLayout_l.setOnClickListener(this);
        linearLayout_r.setOnClickListener(this);
        button_next.setOnClickListener(this);
    }

    @Override
    public void initData() {
        // intent.putExtra("action", "add_watcher");
        action = getIntent().getStringExtra("action");
        qrcode = getIntent().getStringExtra("qrcode");
        simnum = getIntent().getStringExtra("sim");
        if (action != null && action.equalsIgnoreCase("add_watcher")) {
            linearLayout_l.setVisibility(View.INVISIBLE);
        } else {
            linearLayout_r.setVisibility(View.INVISIBLE);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == SHOW_SCAN_QRCODE
                && resultCode == ScanGuideCameraActivity.SCAN_QRCODE_OK) {
            Bundle bundle = data.getExtras();
            String scanResult = bundle.getString("result");
            Log.d(TAG, "qrcode=" + scanResult);
            check_qrcode(scanResult);
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void check_qrcode(final String qrcode) {
        final String uid = KidsWatConfig.getUserUid();
        final String access_token = KidsWatConfig.getUserToken();
        // String device_id = AppContext.getInstance().getProperty(
        // "user.device_id");
        // String family_id = AppContext.getDefaultFamilyId();

        final String request_url = KidsWatApiUrl.getUrlFor___check_qrcode(uid,
                access_token, qrcode);

        ApiHttpClient.get(request_url, new HttpCallBack() {

            @Override
            public void onPreStart() {
                showWaitDialog(getString(R.string.new_setting_fragment_6));
            }

            @Override
            public void onSuccess(String t) {
                super.onSuccess(t);
                CustomLog.d(TAG,String.format("url:%s\nt:%s", request_url, t));
                beanFor___check_qrcode b = AppContext.getInstance().getGson()
                        .fromJson(t, beanFor___check_qrcode.class);

                if (b.error == 0) {

                    Intent mintent = new Intent();
                    try {
                        /*if (new JSONObject(t).getJSONObject("info").getInt(
								"version") == 461) {

							AppContext.getInstance().getCodess() = 461;

							mintent = new Intent(ScanGuideAActivity.this,
									SettingCountryOrPhoneActivity.class);
						} else {
							
							AppContext.getInstance().getCodess() = 361;
							
							mintent = new Intent(ScanGuideAActivity.this,
									SetRelationActivity2.class);
						}*/
                        JSONObject json = new JSONObject(t);
                        int version = json.getJSONObject("info").getInt("version");
                        String sim = "";
                        if (!json.getJSONObject("info").isNull("sim"))
                            sim = new JSONObject(t).getJSONObject("info").getString("sim");

                        AppContext.getInstance().setCodess(version);// = version;
                        if (version == 361 || version == 4620) {
                            mintent = new Intent(ScanGuideAActivity.this,
                                    SetRelationActivity2.class);
                        } else {
                            mintent = new Intent(ScanGuideAActivity.this,
                                    SettingCountryOrPhoneActivity.class).putExtra(PHONESIM, sim).putExtra(QRCODE, qrcode);
                        }
                    } catch (JSONException e) {
                        CustomLog.logException(TAG,e);
                    }
                    mintent.putExtra("action", "add_device");
                    mintent.putExtra("qrcode", qrcode);
                    startActivity(mintent);

                    ScanGuideAActivity.this.finish();
                } else if (b.error == 1043) {
                    showConfirmInformation(getApplicationContext().getResources().getString(R.string.device_has_admin));
                } else if (b.error == -1) {
                    showConfirmInformation(getApplicationContext().getResources().getString(R.string.device_not_shaped));
                } else if (b.error == -2) {
                    // :{"error":-2,"info":{"message":"This qrcode is not validate!"}}
                    showConfirmInformation(getApplicationContext().getResources().getString(R.string.canot_read));
                } else {
                    showConfirmInformation(getApplicationContext().getResources().getString(R.string.device_has_admin));
                }

            }

            @Override
            public void onFailure(int errorNo, String strMsg) {
                super.onFailure(errorNo, strMsg);
                String msg = String
                        .format("%s(error=%s)",
                                errorNo == -1 ? "Connect to the server failed"
                                        : strMsg, errorNo);
                CustomLog.d(TAG,msg);
                showLongToast(getApplicationContext().getResources().getString(R.string.check_conn));
            }

            @Override
            public void onFinish() {
                dismissDialog(null, 50);
            }
        });

    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            finish();
        }
        return false;
    }

}
