package com.qiwo.xkidswatcher_russia.ui;

import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.qiwo.xkidswatcher_russia.R;
import com.qiwo.xkidswatcher_russia.Utils;
import com.qiwo.xkidswatcher_russia.util.KidsWatUtils;
import net.intari.CustomLogger.CustomLog;
import com.qiwo.xkidswatcher_russia.widget.EditTextWithDel;
import com.qiwo.xkidswatcher_russia.widget.sortlistview.CharacterParser;
import com.qiwo.xkidswatcher_russia.widget.sortlistview.PinyinComparator;
import com.qiwo.xkidswatcher_russia.widget.sortlistview.SideBar;
import com.qiwo.xkidswatcher_russia.widget.sortlistview.SideBar.OnTouchingLetterChangedListener;
import com.qiwo.xkidswatcher_russia.widget.sortlistview.SortAdapter;
import com.qiwo.xkidswatcher_russia.widget.sortlistview.SortModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class CountryActivity extends AppCompatActivity {
    public static final String TAG = CountryActivity.class.getSimpleName();

    private ListView sortListView;
    private SideBar sideBar;
    private TextView dialog;
    private SortAdapter adapter;
    private EditTextWithDel mClearEditText;
    private ImageView back_imageview;

    private CharacterParser characterParser;
    private List<SortModel> SourceDateList;

    private PinyinComparator pinyinComparator;

    private String action = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //handled
        //AnalyticsUtils.processActivityOnCreate(TAG,this);

        setContentView(R.layout.activity_select_country);
        action = getIntent().getStringExtra("action");
        //getActionBar().hide();
        initViews();
    }

    private void initViews() {
        characterParser = CharacterParser.getInstance();

        pinyinComparator = new PinyinComparator();

        sideBar = (SideBar) findViewById(R.id.sidrbar);
        dialog = (TextView) findViewById(R.id.dialog);
        sideBar.setTextView(dialog);

        back_imageview = (ImageView) findViewById(R.id.back_imageview);

        back_imageview.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                CountryActivity.this.finish();
            }
        });

        sideBar.setOnTouchingLetterChangedListener(new OnTouchingLetterChangedListener() {

            @Override
            public void onTouchingLetterChanged(String s) {
                int position = adapter.getPositionForSection(s.charAt(0));
                if (position != -1) {
                    sortListView.setSelection(position);
                }

            }
        });

        sortListView = (ListView) findViewById(R.id.country_lvcountry);
        sortListView.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                SortModel m_model = (SortModel) adapter.getItem(position);

                String showtip = m_model.getName() + "("
                        + m_model.getMobilePrefix() + ")";

                if (action != null && action.equalsIgnoreCase("invite")) {
                    setResult(
                            RESULT_OK,
                            (new Intent()).setAction(m_model.getName() + "("
                                    + m_model.getMobilePrefix() + ")"));
                } else {
                    setResult(RESULT_OK,
                            (new Intent()).setAction(m_model.getMobilePrefix()));
                }
                finish();
            }
        });

        String countrydata = KidsWatUtils.getTxtFromAssets(
                CountryActivity.this, "country.json");
        SourceDateList = filledData(countrydata);

        Collections.sort(SourceDateList, pinyinComparator);
        adapter = new SortAdapter(this, SourceDateList);
        sortListView.setAdapter(adapter);

        mClearEditText = (EditTextWithDel) findViewById(R.id.filter_edit);

        mClearEditText.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) {
                filterData(s.toString());
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {

            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
    }

    /**
     * @param data
     * @return
     */
    private List<SortModel> filledData(String data) {
        try {
            JSONObject json = new JSONObject(data);

            JSONArray jsonarray = json.getJSONArray("country");
            int jsonlength = jsonarray.length();
            if (jsonlength > 0) {
                // ---------

                List<SortModel> mSortList = new ArrayList<SortModel>();

                for (int i = 0; i < jsonlength; i++) {
                    JSONObject jsonbject = (JSONObject) jsonarray.get(i);
                    String name = jsonbject.getString("name");
                    String code = jsonbject.getString("code");

                    SortModel sortModel = new SortModel();
                    sortModel.setName(name);
                    sortModel.setMobilePrefix(code);
                    String pinyin = characterParser.getSelling(name);
                    String sortString = pinyin.substring(0, 1).toUpperCase();

                    if (sortString.matches("[A-Z]")) {
                        sortModel.setSortLetters(sortString.toUpperCase());
                    } else {
                        sortModel.setSortLetters("#");
                    }

                    mSortList.add(sortModel);
                }

                return mSortList;
            }

        } catch (JSONException e) {
            CustomLog.logException(TAG,e);
        }

        return null;
    }

    /**
     * @param filterStr
     */
    private void filterData(String filterStr) {
        List<SortModel> filterDateList = new ArrayList<SortModel>();

        if (TextUtils.isEmpty(filterStr)) {
            filterDateList = SourceDateList;
        } else {
            filterDateList.clear();
            for (SortModel sortModel : SourceDateList) {
                String name = sortModel.getName();
                if (name.indexOf(filterStr.toString()) != -1
                        || characterParser.getSelling(name).startsWith(
                        filterStr.toString())) {
                    filterDateList.add(sortModel);
                }
            }
        }

        Collections.sort(filterDateList, pinyinComparator);
        adapter.updateListView(filterDateList);
    }

}
