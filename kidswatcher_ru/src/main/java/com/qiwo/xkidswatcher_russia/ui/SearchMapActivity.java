package com.qiwo.xkidswatcher_russia.ui;

import android.content.Context;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.os.AsyncTask;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.qiwo.xkidswatcher_russia.R;
import net.intari.CustomLogger.CustomLog;

import java.io.IOException;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.BindView;;

//public class SearchMapActivity extends BaseActivity {
public class SearchMapActivity extends AppCompatActivity implements View.OnClickListener {
    public static final String TAG = SearchMapActivity.class.getSimpleName();

    @BindView(R.id.linearLayout_l)
    LinearLayout linearLayout_l;

    @BindView(R.id.editText_add)
    EditText editText_add;

    @BindView(R.id.listView)
    ListView listView;

    @BindView(R.id.button_ok)
    ImageView button_ok;

    @BindView(R.id.textView_cancel)
    TextView textView_cancel;

    xAdapter adapter;
    List<Address> addresses = null;

    // -----------
    private Geocoder geocoder;

    // @Override
    // protected int getLayoutId() {
    // return R.layout.layout_map_search_list;
    // }
    //
    // @Override
    // protected boolean hasActionBar() {
    // return false;
    // }
    //
    // @Override
    // protected void init(Bundle savedInstanceState) {
    // super.init(savedInstanceState);
    // // -----------
    // }
    private searchMapTask mTask;
    TextWatcher textChangeListener = new TextWatcher() {
        @Override
        public void afterTextChanged(Editable s) {
            // TODO Auto-generated method stub
            if (mTask != null && mTask.getStatus() == AsyncTask.Status.RUNNING) {
                mTask.cancel(true); // 如果Task还在运行，则先取消它
            }
            mTask = new searchMapTask();
            mTask.execute(editText_add.getText().toString());
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count,
                                      int after) {
            // TODO Auto-generated method stub
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before,
                                  int count) {
            // TODO Auto-generated method stub
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.layout_map_search_list);
        ButterKnife.bind(this);
        initView();
        initData();
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.linearLayout_l:
            case R.id.textView_cancel:
                finish();
                break;
            case R.id.back_imageview:
                finish();
                break;
            case R.id.button_ok:
                mTask = new searchMapTask();
                mTask.execute(editText_add.getText().toString());
                break;
            default:
                break;
        }
    }

    private void initView() {
        linearLayout_l.setOnClickListener(this);
        textView_cancel.setOnClickListener(this);
        button_ok.setOnClickListener(this);
        // ---------
        // editText_add.addTextChangedListener(watcher)
        editText_add.addTextChangedListener(textChangeListener);
    }

    private void initData() {
        geocoder = new Geocoder(this);

        listView.setOnItemClickListener(new LvItemClickListener());
        adapter = new xAdapter(SearchMapActivity.this);
        listView.setAdapter(adapter);
    }

    @Override
    protected void onResume() {
        super.onResume();

    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // ------------
        super.onActivityResult(requestCode, resultCode, data);
    }

    class LvItemClickListener implements AdapterView.OnItemClickListener {
        public void onItemClick(AdapterView<?> parent, View view, int position,
                                long id) {
            Address address = addresses.get(position);

            Intent resultIntent = new Intent();
            Bundle bundle = new Bundle();
            bundle.putDouble("longitude", address.getLongitude());
            bundle.putDouble("latitude", address.getLatitude());
            resultIntent.putExtras(bundle);
            SearchMapActivity.this.setResult(RESULT_OK, resultIntent);// RESULT_OK

            finish();
        }
    }

    private class searchMapTask extends AsyncTask<String, Integer, String> {
        // onPreExecute方法用于在执行后台任务前做一些UI操作
        @Override
        protected void onPreExecute() {
            CustomLog.d(TAG,"onPreExecute() called");
        }

        // doInBackground方法内部执行后台任务,不可在此方法内修改UI
        @Override
        protected String doInBackground(String... params) {
            if (isCancelled())
                return null;
            try {
                addresses = geocoder.getFromLocationName(params[0], 20);
            } catch (IOException e) {
                // TODO Auto-generated catch block
                CustomLog.logException(TAG,e);
            }
            return null;
        }

        // onProgressUpdate方法用于更新进度信息
        @Override
        protected void onProgressUpdate(Integer... progresses) {
            CustomLog.d(TAG,"onProgressUpdate(Progress... progresses) called");
        }

        // onPostExecute方法用于在执行完后台任务后更新UI,显示结果
        @Override
        protected void onPostExecute(String result) {
            if (isCancelled())
                return;
            CustomLog.d(TAG,"onPostExecute(Result result) called");
            adapter.notifyDataSetChanged();
        }

        // onCancelled方法用于在取消执行中的任务时更改UI
        @Override
        protected void onCancelled() {
            CustomLog.d(TAG,"onCancelled() called");
            adapter.notifyDataSetChanged();
        }
    }

    class xAdapter extends BaseAdapter {

        private Context context;
        private LayoutInflater mInflater;

        private ViewHolder holder;

        public xAdapter(Context c) {
            super();
            this.context = c;
            mInflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        }

        @Override
        public int getCount() {
            // TODO Auto-generated method stub
            return addresses == null ? 0 : addresses.size();
        }

        @Override
        public Object getItem(int index) {

            return addresses.get(index);
        }

        @Override
        public long getItemId(int index) {
            return index;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                convertView = mInflater.inflate(
                        R.layout.layout_map_search_list_item, null);
                holder = new ViewHolder();
                holder.l_title = (TextView) convertView
                        .findViewById(R.id.textView_title);
                holder.l_des = (TextView) convertView
                        .findViewById(R.id.textView_des);

                convertView.setTag(holder);

            } else {
                holder = (ViewHolder) convertView.getTag();
            }

            Address address = addresses.get(position);
            if (address != null) {
                // int maxLine = address.getMaxAddressLineIndex();

                String addressText = "";

                String addressline0 = address.getAddressLine(0) == null ? ""
                        : address.getAddressLine(0);
                String addressline1 = address.getAddressLine(1) == null ? ""
                        : address.getAddressLine(1);
                addressText = addressline0 + addressline1;

                holder.l_title.setText(address.getFeatureName());
                holder.l_des.setText(addressText);
            }
            return convertView;
        }

        private class ViewHolder {
            TextView l_title;
            TextView l_des;

        }

    }

    // ----------------

}