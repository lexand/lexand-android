package com.qiwo.xkidswatcher_russia.ui;

import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;

import com.qiwo.xkidswatcher_russia.R;
import com.qiwo.xkidswatcher_russia.base.BaseActivity;

import butterknife.BindView;;

public class ScanGuideRecoverQrcodeActivity extends BaseActivity {

    @BindView(R.id.linearLayout_l)
    LinearLayout linearLayout_l;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_scan_guide_receover_qr;
    }

    @Override
    protected boolean hasActionBar() {
        return false;
    }

    @Override
    protected void init(Bundle savedInstanceState) {
        super.init(savedInstanceState);
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.linearLayout_l:
                finish();
                break;
            default:
                break;
        }
    }

    @Override
    public void initView() {
        linearLayout_l.setOnClickListener(this);
    }

    @Override
    public void initData() {
        //
    }
    // ----------------

}
