package com.qiwo.xkidswatcher_russia.ui;

import android.content.Intent;
import android.os.Bundle;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.qiwo.xkidswatcher_russia.R;
import com.qiwo.xkidswatcher_russia.base.BaseActivity;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;;

/**
 *
 */
public class SetRelationActivity extends BaseActivity {

    final String TAG = SetRelationActivity.class.getSimpleName();
    @BindView(R.id.linearLayout_l)
    LinearLayout linearLayout_l;
    @BindView(R.id.recyclerview)
    RecyclerView recyclerView;
    LinearLayoutManager layoutManager;
    List<RecycleItem> datas;
    StRecycleAdapter mAdapter;
    // -----------
    String qrcode;
    String action;
    private OnItemClickListener recyleItemListener = new OnItemClickListener() {
        @Override
        public void onItemClick(int position, Object object) {
            RecycleItem bean = (RecycleItem) object;

            String relation = bean.text;

            if ("add_device".equalsIgnoreCase(action)) {
                Intent intent = new Intent(SetRelationActivity.this,
                        KidsProfileActivity.class);
                intent.putExtra("relation", relation);
                startActivity(intent);
            } else {
                Intent resultIntent = new Intent();
                Bundle bundle = new Bundle();
                bundle.putString("relation", relation);
                resultIntent.putExtras(bundle);
                setResult(RESULT_OK, resultIntent);
                finish();
            }

        }
    };

    @Override
    protected int getLayoutId() {
        return R.layout.activity_set_relation;
    }

    @Override
    protected boolean hasActionBar() {
        return false;
    }

    @Override
    protected void init(Bundle savedInstanceState) {
        super.init(savedInstanceState);
        // -----------
        qrcode = getIntent().getStringExtra("qrcode");
        action = getIntent().getStringExtra("action");
        // ----------
        datas = new ArrayList<RecycleItem>();
        datas.add(new RecycleItem(1, R.drawable.bg_item_s1, "Dad"));
        datas.add(new RecycleItem(2, R.drawable.bg_item_s2, "Mum"));
        datas.add(new RecycleItem(3, R.drawable.bg_item_s3, "Grandpa"));
        datas.add(new RecycleItem(4, R.drawable.bg_item_s4, "Grandma"));

        // ---------------
        layoutManager = new GridLayoutManager(this, 2);
        recyclerView.setLayoutManager(layoutManager);// 设置布局管理器
        mAdapter = new StRecycleAdapter(datas);
        mAdapter.setOnItemClickListener(recyleItemListener);
        recyclerView.setAdapter(mAdapter);// 设置适配器
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.linearLayout_l:
                finish();
                break;
            case R.id.button_next:
                //
                break;
            default:
                break;
        }
    }

    @Override
    public void initView() {
        linearLayout_l.setOnClickListener(this);
    }

    @Override
    public void initData() {
        //
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            finish();
        }
        return false;
    }

    /**
     * 内部接口回调方法
     */
    public interface OnItemClickListener {
        void onItemClick(int position, Object object);
    }

    public class RecycleItem {
        public int type;
        public int drawableId;
        public int bg_drawableid;
        public String text;

        private RecycleItem(int _type, int _drawableId, int _bg_drawableid,
                            String _text) {
            type = _type;
            drawableId = _drawableId;
            bg_drawableid = _bg_drawableid;
            text = _text;
        }

        public RecycleItem(int _type, int _bg_drawableid, String _text) {
            this(_type, 0, _bg_drawableid, _text);
        }
    }

    public class StRecycleAdapter extends
            RecyclerView.Adapter<StRecycleAdapter.ViewHolder> {

        private List<RecycleItem> list;
        private OnItemClickListener listener;

        public StRecycleAdapter(List<RecycleItem> list) {
            this.list = list;
        }

        @Override
        public int getItemCount() {
            return list.size();
        }

        @Override
        public void onBindViewHolder(ViewHolder holder, final int position) {

            final RecycleItem bean = list.get(position);
            holder.mImageView.setBackgroundResource(bean.bg_drawableid);
            holder.mTextView.setText(bean.text);
            /**
             * 调用接口回调
             */
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (null != listener)
                        listener.onItemClick(position, bean);
                }
            });
        }

        /**
         * 设置监听方法
         *
         * @param listener
         */
        public void setOnItemClickListener(OnItemClickListener listener) {
            this.listener = listener;
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int position) {
            View view = LayoutInflater.from(viewGroup.getContext()).inflate(
                    R.layout.view_settings_item, viewGroup, false);
            ViewHolder holder = new ViewHolder(view);
            return holder;
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            public TextView mTextView;
            public ImageView mImageView;

            public ViewHolder(View itemView) {
                super(itemView);
                mTextView = (TextView) itemView.findViewById(R.id.txt);
                mImageView = (ImageView) itemView.findViewById(R.id.img);
            }
        }
    }

}
