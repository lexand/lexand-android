package com.qiwo.xkidswatcher_russia.ui;

import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.LinearLayout;

import com.qiwo.xkidswatcher_russia.R;
import com.qiwo.xkidswatcher_russia.base.BaseActivity;

import butterknife.BindView;;

public class SystemSettingHelpActivity extends BaseActivity {

    @BindView(R.id.linearLayout_l)
    LinearLayout linearLayout_l;

    @BindView(R.id.linearLayout_helpitem1)
    LinearLayout linearLayout_help1;

    @BindView(R.id.linearLayout_helpitem2)
    LinearLayout linearLayout_help2;

    @BindView(R.id.web_help)
    WebView web_help;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_systemsettinghelp;
    }

    @Override
    protected boolean hasActionBar() {
        return false;
    }

    @Override
    protected void init(Bundle savedInstanceState) {
        super.init(savedInstanceState);
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.linearLayout_l:
                if (web_help.canGoBack()) {
                    web_help.goBack();
                } else {
                    finish();
                }
                break;

            default:
                break;
        }
    }

    @Override
    public void initView() {
        // linearLayout_l.setOnClickListener(this);
        linearLayout_l.setOnClickListener(this);
        linearLayout_help1.setOnClickListener(this);
        linearLayout_help2.setOnClickListener(this);
    }

    @Override
    public void initData() {
        //
        //web_help.loadUrl("http://misafes1.qiwocloud1.com/view/ru/index.html");
        web_help.loadUrl("https://kidsradar.lexand.ru/faq/");
        web_help.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                return true;
            }
        });
    }

    // ----------------
    @Override
    //设置回退
    //覆盖Activity类的onKeyDown(int keyCoder,KeyEvent event)方法
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK) && web_help.canGoBack()) {
            web_help.goBack(); //goBack()表示返回WebView的上一页面
            return true;
        } else if ((keyCode == KeyEvent.KEYCODE_BACK) && !web_help.canGoBack()) {
            SystemSettingHelpActivity.this.finish();
            return true;
        }
        return false;
    }

}
