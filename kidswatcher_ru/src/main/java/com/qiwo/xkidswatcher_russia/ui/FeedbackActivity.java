package com.qiwo.xkidswatcher_russia.ui;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;

import com.qiwo.xkidswatcher_russia.R;
import com.qiwo.xkidswatcher_russia.base.BaseActivity;

import butterknife.BindView;;

public class FeedbackActivity extends BaseActivity {

    //    	@BindView(R.id.linearLayout_l)
//        LinearLayout linearLayout_l;
//
//	@BindView(R.id.editText_api_url)
//	EditText editText_api_url;
//
//	@BindView(R.id.button_ok)
//	Button button_ok;
//
//	@BindView(R.id.listview)
//	ListView listview;
//    private FeedbackViewAdapter mAdapter;
//    private List<ChatMsgEntity> mDataArrays = new ArrayList<ChatMsgEntity>();
    @BindView(R.id.question_progressBar)
    ProgressBar question_progressBar;
    @BindView(R.id.webView)
    WebView webView;
    private String tip;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_feedback;
    }

    @Override
    protected boolean hasActionBar() {
        return false;
    }

    @Override
    protected void init(Bundle savedInstanceState) {
        super.init(savedInstanceState);
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
//            case R.id.linearLayout_l:
//                finish();
//                break;
//            case R.id.button_ok:
//                String content = editText_api_url.getText().toString();
//                if (TextUtils.isEmpty(content)) {
//                    break;
//                }
//                addFeedback(content);
//                editText_api_url.setText("");
//                break;
            default:
                break;
        }
    }

    public void collapseSoftInputMethod() {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(0, InputMethodManager.HIDE_NOT_ALWAYS);
    }

    @Override
    public void initView() {
//        linearLayout_l.setOnClickListener(this);
//        button_ok.setOnClickListener(this);
    }

    @Override
    public void initData() {
//        // -----------
//        ChatMsgEntity entity = new ChatMsgEntity();
//        entity.setDate("2015-09-25");
//        entity.setMsgType(true);
//
//        entity.setText(tip);
//        mDataArrays.add(entity);
//
//        mAdapter = new FeedbackViewAdapter(FeedbackActivity.this, mDataArrays);
//        listview.setAdapter(mAdapter);

        tip = getString(R.string.feedback_1);
        webView.loadUrl("https://kidsradar.lexand.ru/feedback/app");
        //设置可自由缩放网页
        WebSettings settings = webView.getSettings();
//        settings.setTextSize(WebSettings.TextSize.LARGER);
        settings.setJavaScriptEnabled(true);
        settings.setSupportZoom(true);
        settings.setBuiltInZoomControls(true);

        // 如果页面中链接，如果希望点击链接继续在当前browser中响应，
        // 而不是新开Android的系统browser中响应该链接，必须覆盖webview的WebViewClient对象
        webView.setWebViewClient(new WebViewClient() {
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                //  重写此方法表明点击网页里面的链接还是在当前的webview里跳转，不跳到浏览器那边
                view.loadUrl(url);
                return true;
            }
        });


        webView.setWebChromeClient(new WebChromeClient() {

            @Override
            public void onReceivedTitle(WebView view, String title) {
                super.onReceivedTitle(view, title);
                //这里可以用来处理我们的页面标题
            }

            @Override
            public void onProgressChanged(WebView view, int newProgress) {
                super.onProgressChanged(view, newProgress);
                if (question_progressBar == null) return;
                question_progressBar.setProgress(newProgress);
                //处理网页面加载进度
                if (newProgress == 100) {
                    question_progressBar.setVisibility(View.GONE);
                }
            }
        });

    }

    @Override
    public void onBackPressed() {
        if (webView.canGoBack()) {
            webView.goBack();//返回上一页面
        } else {
            finish();
        }
    }

//    private void addFeedback(String content) {
//
//        ChatMsgEntity entity = new ChatMsgEntity();
//        entity.setDate(content);
//        entity.setMsgType(false);
//        entity.setText(content);
//        mDataArrays.add(entity);
//
//        mAdapter.notifyDataSetChanged();
//
//        editText_api_url.clearFocus();
//    }
    // ----------------

}
