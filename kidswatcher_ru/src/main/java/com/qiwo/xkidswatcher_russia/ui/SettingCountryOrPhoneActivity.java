package com.qiwo.xkidswatcher_russia.ui;

import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.qiwo.xkidswatcher_russia.AnalyticsUtils.AnalyticsUtils;
import com.qiwo.xkidswatcher_russia.R;
import com.qiwo.xkidswatcher_russia.Utils;

public class SettingCountryOrPhoneActivity extends AppCompatActivity implements
        OnClickListener {

    public static final String TAG = SettingCountryOrPhoneActivity.class.getSimpleName();

    /**
     * 设置国家和手机
     */
    final private int GET_CODE = 1000;

    private TextView title;
    private TextView in_number;

    private LinearLayout linearLayout_l;
    private EditText Country_number;
    private Button change_ok, Country_btn;
    private String phoneSim;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //AnalyticsUtils.processActivityOnCreate(TAG,this);

        //getActionBar().hide();
        setContentView(R.layout.activity_change_sim);
        init();
    }

    public void init() {

        //
        phoneSim = getIntent().getStringExtra(ScanGuideAActivity.PHONESIM);

        linearLayout_l = (LinearLayout) findViewById(R.id.linearLayout_l);
        title = (TextView) findViewById(R.id.title);
        in_number = (TextView) findViewById(R.id.in_number);
        Country_number = (EditText) findViewById(R.id.Country_number);

        Country_btn = (Button) findViewById(R.id.Country_btn);
        change_ok = (Button) findViewById(R.id.change_ok);

        Country_btn.setOnClickListener(this);
        change_ok.setOnClickListener(this);
        linearLayout_l.setOnClickListener(this);

        title.setText(getString(R.string.setting_country_or_phone_1));
        if (phoneSim != null)
            in_number.setText("+" + phoneSim);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.Country_btn:
                startActivityForResult(new Intent(this, CountryActivity.class),
                        GET_CODE);
                break;
            case R.id.change_ok:

//                String editnum = Country_number.getText().toString().trim();
//                if (!editnum.equals("")) {
                Intent mintent = new Intent(SettingCountryOrPhoneActivity.this,
                        SetRelationActivity2.class);
                Intent intent = getIntent();
                mintent.putExtra("action", "add_device");
                mintent.putExtra("qrcode", intent.getStringExtra(ScanGuideAActivity.QRCODE));
                mintent.putExtra("country", "");
                mintent.putExtra("phone", phoneSim);
                startActivity(mintent);
                finish();
//                } else {
//                    Toast.makeText(SettingCountryOrPhoneActivity.this,
//                            "Country code is empty!", 1).show();
//                }

                break;
            case R.id.linearLayout_l:

                this.finish();
                break;
            default:
                break;
        }
    }

    /**
     * This method is called when the sending activity has finished, with the
     * result it supplied.
     */
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == GET_CODE) {
            if (resultCode != RESULT_CANCELED) {
                if (data != null) {
                    Country_btn.setText(data.getAction());
                }
            }
        }
    }

}
