package com.qiwo.xkidswatcher_russia.ui;

import android.Manifest;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.qiwo.xkidswatcher_russia.AppContext;
import com.qiwo.xkidswatcher_russia.R;
import com.qiwo.xkidswatcher_russia.base.BaseActivity;
import com.qiwo.xkidswatcher_russia.bean.beanForDb___Family;
import com.qiwo.xkidswatcher_russia.event.BleDisconnect;
import com.qiwo.xkidswatcher_russia.event.BleWritePwdSuccess;
import com.qiwo.xkidswatcher_russia.service.BluetoothLeService;
import com.qiwo.xkidswatcher_russia.util.Contanst;
import com.qiwo.xkidswatcher_russia.util.KidsWatUtils;
import com.qiwo.xkidswatcher_russia.util.StringUtils;
import net.intari.CustomLogger.CustomLog;
import com.qiwo.xkidswatcher_russia.widget.CircleImageView;
import com.qiwo.xkidswatcher_russia.widget.WrapLinearLayoutManager;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;;

@TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR2)
public class BleAccompanyActivity extends BaseActivity {
    public static final String TAG = BleAccompanyActivity.class.getSimpleName();

    private static final int REQUEST_ENABLE_BT = 1;
    @BindView(R.id.linearLayout_l)
    LinearLayout linearLayout_l;
    @BindView(R.id.imageView_ble_phone)
    ImageView imageView_ble_phone;
    @BindView(R.id.textView_PhoneBle)
    TextView textView_PhoneBle;
    @BindView(R.id.recyclerview)
    RecyclerView recyclerView;
    LinearLayoutManager layoutManager;
    List<RecycleItem> datas;
    StRecycleAdapter mAdapter;
    List<String> linkBlueAddress = new ArrayList<String>();
    private BluetoothLeService mBluetoothLeService;
    private final ServiceConnection mServiceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName componentName,
                                       IBinder service) {
            mBluetoothLeService = ((BluetoothLeService.LocalBinder) service)
                    .getService();

            for (RecycleItem item : datas) {
                boolean bleConnected = mBluetoothLeService
                        .getBleIsConnect(item.bleAddress)
                        && (mBluetoothLeService
                        .getBleConnectionState(item.bleAddress) == 3 || mBluetoothLeService
                        .getBleConnectionState(item.bleAddress) == 4);
                item.opened = bleConnected;
                item.msg = bleConnected ? getString(R.string.ble_accompany_2)
                        : getString(R.string.ble_accompany_3);
            }
            mAdapter.notifyDataSetChanged();

            if (!mBluetoothLeService.initialize()) {
                CustomLog.d(TAG,"Unable to initialize Bluetooth");
            }
        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {
            mBluetoothLeService = null;
        }
    };
    private BluetoothAdapter mBluetoothAdapter;
    // Device scan callback.
    private BluetoothAdapter.LeScanCallback mLeScanCallback = new BluetoothAdapter.LeScanCallback() {
        @Override
        public void onLeScan(final BluetoothDevice device, int rssi,
                             byte[] scanRecord) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (device != null && device.getName() != null) {

                        CustomLog.d(TAG,"Device address is "+device.getAddress());

                        for (RecycleItem item : datas) {
                            if (item.opened) {
                                if (item.bleAddress.equalsIgnoreCase(device
                                        .getAddress())) {
                                    if (!StringUtils.isEmpty(item.blePassword)) {
                                        final boolean result = mBluetoothLeService
                                                .connect(item.bleAddress,
                                                        item.blePassword,
                                                        item.babyName);

                                        if (mBluetoothAdapter != null) {
                                            mBluetoothAdapter
                                                    .stopLeScan(mLeScanCallback);
                                        }

                                    } else {
                                        CustomLog.d(TAG,"no ble pwd");
                                    }
                                }
                            }
                        }
                    }
                }
            });
        }
    };
    private OnItemClickListener recyleItemListener = new OnItemClickListener() {

        @Override
        public void onItemClick(View v, int position, Object object) {
            RecycleItem bean = (RecycleItem) object;
            // ---------------
            if (v.getId() == R.id.imageView_ble_watcher) {
                String mmAddress = bean.bleAddress;
                String msg = String.format("%s,%s,%s,%s", bean.babyName,
                        bean.familyid, bean.bleAddress, bean.blePassword);
                CustomLog.d(TAG,msg);

                if (StringUtils.isEmpty(mmAddress))
                    return;

                final BluetoothAdapter adapter = BluetoothAdapter
                        .getDefaultAdapter();

                if (bean.opened)// 选中情况下关闭连接
                {
                    if (adapter != null) {
                        mBluetoothLeService.disconnect(mmAddress);

                        // imageView_ble_watcher.setSelected(false);
                        // // textView_PhoneBle.setText("BlueTooth:OFF");
                        // textView_Msg
                        // .setText("Accompanying Mode has been turned off");

                        for (RecycleItem item : datas) {
                            if (item.familyid == bean.familyid) {
                                item.opened = false;
                                item.msg = getString(R.string.ble_accompany_3);
                                boolean isok = linkBlueAddress.remove(item);
                                CustomLog.d(TAG,"isok=" + isok);
                            }
                        }

                        mAdapter.notifyDataSetChanged();
                        // ------------
                        // scanLeDevice(false);
                        // }
                    } else {
                        CustomLog.d(TAG,"此设备不存在蓝牙设备。。。");
                    }
                } else {// 未选中情况下是连接蓝牙
                    if (adapter != null && adapter.isEnabled()) {
                        if (!StringUtils.isEmpty(bean.blePassword)) {
                            // imageView_ble_watcher.setSelected(true);
                            // textView_PhoneBle.setText("BlueTooth:ON");
                            for (RecycleItem item : datas) {
                                if (item.familyid == bean.familyid) {
                                    linkBlueAddress.add(item.bleAddress);
                                    item.msg = getString(R.string.ble_accompany_4);
                                    item.opened = true;
                                }
                            }
                            mAdapter.notifyDataSetChanged();
                            // ------------
                            CustomLog.d(TAG,"蓝牙已打开 - Bluetooth is On.");

                            scanLeDevice(true, bean.bleAddress);
                        } else {
                            showLongToast("Bluetooth password empty.");
                        }
                    } else {
                        Toast.makeText(BleAccompanyActivity.this,
                                getString(R.string.ble_accompany_1), Toast.LENGTH_LONG)
                                .show();
                    }
                }

            }
        }

    };

    @Override
    protected int getLayoutId() {
        return R.layout.activity_ble_accompany;
    }

    @Override
    protected boolean hasActionBar() {
        return false;
    }

    @Override
    protected void init(Bundle savedInstanceState) {
        super.init(savedInstanceState);
        locationPermissionWrapper();
    }

    // ----------------
    private void locationPermissionWrapper() {
        // TODO Auto-generated method stub
        /**
         * 获取摄像头的权限
         */
        if (Build.VERSION.SDK_INT >= 23) {
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                //拥有权限
            } else {
                //申请权限
                ActivityCompat.requestPermissions(BleAccompanyActivity.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, Contanst.REQUEST_LOCATION_REQUEST_PERMISSION);
                return;
            }
        } else {
        }
    }

    @SuppressLint("Override")
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == Contanst.REQUEST_LOCATION_REQUEST_PERMISSION) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                //permission granted
                //TODO 向SD卡写数据
            } else {
                //permission denied
                //TODO 显示对话框告知用户必须打开权限
                finish();
                Toast.makeText(BleAccompanyActivity.this, getApplicationContext().getResources().getString(R.string.location_permission), Toast.LENGTH_LONG).show();
            }
        }
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.linearLayout_l:
                finish();
                break;
            case R.id.imageView_ble_phone:
                openOrCloseBle();
                break;
            case R.id.button_ok:
                break;
            default:
                break;
        }
    }

    @Override
    public void initView() {
        EventBus.getDefault().register(this);
        linearLayout_l.setOnClickListener(this);
        imageView_ble_phone.setOnClickListener(this);
    }

    public void initData() {

        datas = new ArrayList<RecycleItem>();
        for (String key : AppContext.getInstance().getMapFamilyInfo().keySet()) {
            beanForDb___Family bf = AppContext.getInstance().getMapFamilyInfo() //.map_familyinfo
                    .get(key);
            String mMsg = getString(R.string.ble_accompany_1);

            String msg = String.format("---%s,%s,%s,%s--", bf.nickname,
                    bf.bt_addr_ex, bf.bt_addr_pwd, bf.sex);
            CustomLog.d(TAG,msg);

            if ("5613".equals(bf.version) || "601".equals(bf.version)) {
                continue;
            }

            RecycleItem item = new RecycleItem(bf.nickname, mMsg, key,
                    bf.bt_addr_ex, bf.bt_addr_pwd, bf.base64_img, bf.sex, bf.img_path, false);
            datas.add(item);
        }
        // ---------------
        layoutManager = new WrapLinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);// 设置布局管理器
        mAdapter = new StRecycleAdapter(datas);
        mAdapter.setOnItemClickListener(recyleItemListener);
        recyclerView.setAdapter(mAdapter);// 设置适配器
        // -------------------------

        // 检查当前手机是否支持ble 蓝牙,如果不支持退出程序
        if (!getPackageManager().hasSystemFeature(
                PackageManager.FEATURE_BLUETOOTH_LE)) {
            Toast.makeText(this, "你的设备不支持蓝牙", Toast.LENGTH_SHORT).show();
        }

        // 初始化 Bluetooth adapter, 通过蓝牙管理器得到一个参考蓝牙适配器(API必须在以上android4.3或以上和版本)
        final BluetoothManager bluetoothManager = (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
        mBluetoothAdapter = bluetoothManager.getAdapter();

        // 检查设备上是否支持蓝牙
        if (mBluetoothAdapter == null) {
            Toast.makeText(this, "你的设备不支持蓝牙", Toast.LENGTH_SHORT).show();
            return;
        } else {
            final BluetoothAdapter adapter = BluetoothAdapter
                    .getDefaultAdapter();
            if (adapter != null) {
                if (adapter.isEnabled()) {// 蓝牙已打开,则关闭
                    textView_PhoneBle.setText("Bluetooth:ON");
                    imageView_ble_phone.setSelected(true);
                } else {
                    // textView_PhoneBle.setText("BlueTooth:OFF");
                }
            }
        }

        int currentapiVersion = android.os.Build.VERSION.SDK_INT;
        CustomLog.d(TAG,"version=" + currentapiVersion);
        // ----------
        Intent gattServiceIntent = new Intent(this, BluetoothLeService.class);
        bindService(gattServiceIntent, mServiceConnection, BIND_AUTO_CREATE);
    }

    private void scanLeDevice(final boolean enable, final String xx_blueAddress) {
        if (enable) {
            CustomLog.d(TAG,"正在搜索蓝牙...");

            mHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (mBluetoothAdapter != null) {
                        mBluetoothAdapter.stopLeScan(mLeScanCallback);
                        // ------未搜到指定蓝牙,更新ui--------
                        if (mBluetoothLeService != null) {
                            for (RecycleItem item : datas) {
                                if (item.bleAddress
                                        .equalsIgnoreCase(xx_blueAddress)) {
                                    boolean bleConnected = mBluetoothLeService
                                            .getBleIsConnect(item.bleAddress)
                                            && (mBluetoothLeService
                                            .getBleConnectionState(item.bleAddress) == 3 || mBluetoothLeService
                                            .getBleConnectionState(item.bleAddress) == 4);
                                    item.opened = bleConnected;
                                    item.msg = bleConnected ? getString(R.string.ble_accompany_2)
                                            : getString(R.string.ble_accompany_3);

                                    // -------在连接列表中删除-----
                                    for (int i = 0; i < linkBlueAddress.size(); i++) {
                                        if (linkBlueAddress.get(i)
                                                .equalsIgnoreCase(
                                                        item.bleAddress)) {
                                            linkBlueAddress.remove(i);
                                        }
                                    }
                                }
                            }
                            mAdapter.notifyDataSetChanged();
                        }
                        // -----------------
                    }
                }
            }, 30 * 1000);

            if (mBluetoothAdapter != null) {
                mBluetoothAdapter.startLeScan(mLeScanCallback);
            }
        } else {
            if (mBluetoothAdapter != null) {
                mBluetoothAdapter.stopLeScan(mLeScanCallback);
            }
        }
    }




    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEventMainThread(BleWritePwdSuccess event) {

        CustomLog.d(TAG,"onEventMainThread：" + event.getClass().getSimpleName());

        for (RecycleItem item : datas) {
            if (item.bleAddress.equalsIgnoreCase(event.getBluetoothAddress())) {
                item.msg = getString(R.string.ble_accompany_2);
                item.opened = true;
            }
        }
        mAdapter.notifyDataSetChanged();
        if (mBluetoothAdapter != null) {
            mBluetoothAdapter.stopLeScan(mLeScanCallback);
        }

    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEventMainThread(BleDisconnect event) {

        CustomLog.d(TAG,"onEventMainThread：" + event.getClass().getSimpleName());

        for (RecycleItem item : datas) {
            if (item.bleAddress.equalsIgnoreCase(event.getBleAddress())) {
                item.msg = getString(R.string.ble_accompany_3);
                item.opened = false;
            }
        }
        mAdapter.notifyDataSetChanged();
    }


    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }



    //@Override
    public void onDetach() {
        //super.onDetach();
        if (EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
        if (mBluetoothAdapter != null) {
            mBluetoothAdapter.stopLeScan(mLeScanCallback);
        }
        // unregisterReceiver(mGattUpdateReceiver);
        unbindService(mServiceConnection);
        mBluetoothLeService = null;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        // ApplicationHelper.getInstance().IsAccompanyModeOpen = true;
        // imageView_ble_phone.setSelected(true);
        // textView_PhoneBle.setText("BlueTooth:ON");
        // ------------

        // ------------
        super.onActivityResult(requestCode, resultCode, data);

        // User chose not to enable Bluetooth.
        if (requestCode == 1) {
            if (resultCode == RESULT_OK) {
                textView_PhoneBle.setText("Bluetooth:ON");
            } else if (resultCode == RESULT_CANCELED) {
                return;
            }
        }

        imageView_ble_phone.setSelected(true);
    }

    // ----------------

    private void openOrCloseBle() {
        final BluetoothAdapter adapter = BluetoothAdapter.getDefaultAdapter();
        if (imageView_ble_phone.isSelected())// 选中情况下是关闭蓝牙
        {
            if (adapter != null) {
                if (adapter.isEnabled()) {// 蓝牙已打开,则关闭

                    new AlertDialog.Builder(this)
                            .setTitle(getApplicationContext().getResources().getString(R.string.turn_off))
                            .setMessage(getApplicationContext().getResources().getString(R.string.turn_off_bluetooth))
                            .setIcon(android.R.drawable.ic_dialog_info)
                            .setPositiveButton(getApplicationContext().getResources().getString(R.string.ok),
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(
                                                DialogInterface dialog,
                                                int which) {
                                            // 点击“确认”后的操作
                                            adapter.disable();
                                            imageView_ble_phone
                                                    .setSelected(false);
                                            textView_PhoneBle
                                                    .setText("Bluetooth:OFF");

                                            for (RecycleItem item : datas) {
                                                item.opened = false;
                                                item.msg = getString(R.string.ble_accompany_3);
                                            }
                                            mAdapter.notifyDataSetChanged();
                                            // ------------
                                            scanLeDevice(false, null);
                                            dialog.dismiss();
                                        }
                                    })
                            .setNegativeButton(getApplicationContext().getResources().getString(R.string.cancle),
                                    new DialogInterface.OnClickListener() {

                                        @Override
                                        public void onClick(
                                                DialogInterface dialog,
                                                int which) {
                                            // 点击“返回”后的操作,这里不设置没有任何操作
                                            dialog.dismiss();
                                        }
                                    }).show();

                }
            }
        } else {
            Intent enableBtIntent = new Intent(
                    BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
            CustomLog.d(TAG,"正在打开蓝牙");
        }
    }

    /**
     * 内部接口回调方法
     */
    public interface OnItemClickListener {
        void onItemClick(View v, int position, Object object);
    }

    // -------------
    public class RecycleItem {
        public String babyName;
        public String msg;

        public String familyid;
        public String bleAddress;
        public String blePassword;
        public String base64_img;
        public int sex;
        public boolean opened;
        public String img_path;

        private RecycleItem(String _babyName, String _msg, String _familyid,
                            String _bleAddress, String _blePassword, String base64_img,
                            int _sex, String _img_path, boolean _opened) {
            babyName = _babyName;
            msg = _msg;

            familyid = _familyid;
            bleAddress = _bleAddress;
            blePassword = _blePassword;
            this.base64_img = base64_img;
            sex = _sex;
            opened = _opened;
            img_path = _img_path;
        }

    }

    public class StRecycleAdapter extends
            RecyclerView.Adapter<StRecycleAdapter.ViewHolder> {

        private List<RecycleItem> list;
        private OnItemClickListener listener;

        public StRecycleAdapter(List<RecycleItem> list) {
            this.list = list;
        }

        @Override
        public int getItemCount() {
            return list.size();
        }

        @Override
        public void onBindViewHolder(ViewHolder holder, final int position) {

            final RecycleItem bean = list.get(position);
            // holder.title.setText(bean.getTitle());
            // holder.imageView.setImageResource(bean.getResId())；
            // CustomLog.d(TAG,"position=" + position);
            // CustomLog.d(TAG,"photoPath=" + bean.photoPath);

            holder.textView_baby.setText(bean.babyName);
            holder.textView_Msg.setText(bean.msg);
            holder.imageView_ble_watcher.setSelected(bean.opened);

            String familyid = bean.familyid;
            int sex = bean.sex;
            Bitmap img;
            if (!TextUtils.isEmpty(bean.img_path)) {
                img = KidsWatUtils.getBabyImg_v2(BleAccompanyActivity.this, familyid, sex);
            } else {
                img = sex == 0 ? BitmapFactory.decodeResource(getResources(), R.drawable.icon_girl) :
                        BitmapFactory.decodeResource(getResources(), R.drawable.icon_boy);
            }
            holder.imageView_img.setImageBitmap(img);

            /**
             * 调用接口回调
             */
            holder.imageView_ble_watcher
                    .setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (null != listener)
                                listener.onItemClick(v, position, bean);
                        }
                    });
        }

        /**
         * 设置监听方法
         *
         * @param listener
         */
        public void setOnItemClickListener(OnItemClickListener listener) {
            this.listener = listener;
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int position) {
            View view = LayoutInflater.from(viewGroup.getContext()).inflate(
                    R.layout.view_ble_item, viewGroup, false);
            ViewHolder holder = new ViewHolder(view);
            return holder;
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            public CircleImageView imageView_img;
            public ImageView imageView_ble_watcher;
            public TextView textView_baby;
            public TextView textView_Msg;

            public ViewHolder(View itemView) {
                super(itemView);
                imageView_img = (CircleImageView) itemView
                        .findViewById(R.id.imageView_img);
                imageView_ble_watcher = (ImageView) itemView
                        .findViewById(R.id.imageView_ble_watcher);
                textView_baby = (TextView) itemView
                        .findViewById(R.id.textView_baby);
                textView_Msg = (TextView) itemView
                        .findViewById(R.id.textView_Msg);
            }
        }
    }

}
