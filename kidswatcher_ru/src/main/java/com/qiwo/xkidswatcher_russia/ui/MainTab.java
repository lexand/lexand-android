package com.qiwo.xkidswatcher_russia.ui;

import com.qiwo.xkidswatcher_russia.R;
import com.qiwo.xkidswatcher_russia.fragment.Home3Fragment;
import com.qiwo.xkidswatcher_russia.fragment.Message2Fragment;
import com.qiwo.xkidswatcher_russia.fragment.NewSettingFragment;

public enum MainTab {
    //
    HOME(0, R.string.main_tab_name_map,
            R.drawable.tab_bar_icon_home, Home3Fragment.class),

//	CHAT(1, R.string.main_tab_name_recent, R.drawable.tab_bar_icon_chat,
//			Recent1Fragment.class),

    NOTIFICATION(2, R.string.main_tab_name_notifications,
            R.drawable.tab_bar_icon_notification, Message2Fragment.class),

    SETTINGS(3, R.string.main_tab_name_settings, R.drawable.tab_bar_icon_settings,
            NewSettingFragment.class);

    private int idx;
    private int resName;
    private int resIcon;
    private Class<?> clz;

    private MainTab(int idx, int resName, int resIcon, Class<?> clz) {
        this.idx = idx;
        this.resName = resName;
        this.resIcon = resIcon;
        this.clz = clz;
    }

    public int getIdx() {
        return idx;
    }

    public void setIdx(int idx) {
        this.idx = idx;
    }

    public int getResName() {
        return resName;
    }

    public void setResName(int resName) {
        this.resName = resName;
    }

    public int getResIcon() {
        return resIcon;
    }

    public void setResIcon(int resIcon) {
        this.resIcon = resIcon;
    }

    public Class<?> getClz() {
        return clz;
    }

    public void setClz(Class<?> clz) {
        this.clz = clz;
    }
}
