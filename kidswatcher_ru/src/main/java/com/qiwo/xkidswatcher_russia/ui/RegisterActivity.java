package com.qiwo.xkidswatcher_russia.ui;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.UnderlineSpan;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.qiwo.xkidswatcher_russia.R;
import com.qiwo.xkidswatcher_russia.api.ApiHttpClient;
import com.qiwo.xkidswatcher_russia.api.remote.KidsWatApiUrl;
import com.qiwo.xkidswatcher_russia.base.BaseActivity;
import net.intari.CustomLogger.CustomLog;
import com.qiwo.xkidswatcher_russia.widget.EditTextWithDel;

import org.json.JSONException;
import org.json.JSONObject;
import org.kymjs.kjframe.http.HttpCallBack;

import butterknife.BindView;;

public class RegisterActivity extends BaseActivity {
    final String TAG = RegisterActivity.class.getSimpleName();

    final private int GET_CODE = 1000;

    @BindView(R.id.mobile_num_tv)
    EditTextWithDel mobile_num_tv;

    @BindView(R.id.linearLayout_l)
    LinearLayout linearLayout_l;

    @BindView(R.id.country_mobile_prefix_llayout)
    LinearLayout country_mobile_prefix_llayout;

    @BindView(R.id.button_next)
    Button button_next;

    @BindView(R.id.tv_register_agreenment)
    TextView tv_register_agreenment;

    @BindView(R.id.btn_agreement)
    ImageView btn_agreement;

    @BindView(R.id.country_mobile_prefix_text)
    TextView country_mobile_prefix_text;
    boolean isAgree = false;
    private String mobile;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_register;
    }

    @Override
    protected boolean hasActionBar() {
        return false;
    }

    @Override
    protected void init(Bundle savedInstanceState) {
        super.init(savedInstanceState);
    }

    @Override
    public void initView() {
        country_mobile_prefix_llayout.setOnClickListener(this);
        linearLayout_l.setOnClickListener(this);
        button_next.setOnClickListener(this);
        btn_agreement.setOnClickListener(this);

        String str = getApplicationContext().getResources().getString(R.string.tip_register_agreement);
        String str_sub = getApplicationContext().getResources().getString(R.string.tip_register_agreement_sub);

        SpannableStringBuilder builder = new SpannableStringBuilder(str);
        int start = str.indexOf(str_sub);
        int end = start + str_sub.length();

        builder.setSpan(new ClickAble(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                startActivity(new Intent(RegisterActivity.this, RegisterAgreementActivity.class));
            }
        }), start, end, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        builder.setSpan(new UnderlineSpan(), start, end, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        tv_register_agreenment.setText(builder);
        tv_register_agreenment.setMovementMethod(LinkMovementMethod.getInstance());
    }

    @Override
    public void initData() {

    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.linearLayout_l:
                finish();
                break;
            case R.id.country_mobile_prefix_llayout:
                startActivityForResult(new Intent(this, CountryActivity.class),
                        GET_CODE);
                break;
            case R.id.button_next:

                if (!button_next.isEnabled()) {
                    return;
                }

                String country_code = country_mobile_prefix_text.getText()
                        .toString().trim();

                if (country_code.length() < 2) {
                    showConfirmInformation(getApplicationContext().getResources().getString(R.string.select_country),
                            getApplicationContext().getResources().getString(R.string.tip_select_country));
                    return;
                }
                country_code = country_code.replace("+", "");
                final String countryCode_x = country_code;
                mobile = mobile_num_tv.getText().toString();
                if (mobile.length() < 2) {
                    showConfirmInformation(null, getApplicationContext().getResources().getString(R.string.tip_mobile_empty));
                    // Toast.makeText(RegisterActivity.this, "mobile is empty.",
                    // Toast.LENGTH_LONG).show();
                } else {
                    CustomLog.d(TAG, "phone=" + mobile);
                    check_identifier(countryCode_x, mobile);
                }
                break;
            case R.id.btn_agreement:
                if (isAgree) {
                    btn_agreement.setImageResource(R.drawable.use_agreement_unchecked);
                    button_next.setEnabled(false);
                } else {
                    btn_agreement.setImageResource(R.drawable.use_agreement_checked);
                    button_next.setEnabled(true);
                }

                isAgree = !isAgree;
                break;
            default:
                break;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        // getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    /**
     * This method is called when the sending activity has finished, with the
     * result it supplied.
     */
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == GET_CODE) {
            if (resultCode != RESULT_CANCELED) {
                if (data != null) {
                    country_mobile_prefix_text.setText(data.getAction());
                }
            }

        }
    }

    private void check_identifier(final String country_code,
                                  final String identifier) {
        final String request_url = KidsWatApiUrl.getUrlFor___check_identifier(
                country_code, identifier);
        // -------------------------

        ApiHttpClient.get(request_url, new HttpCallBack() {

            @Override
            public void onPreStart() {
                showWaitDialog("...Загрузка...");
            }

            @Override
            public void onSuccess(String t) {
                super.onSuccess(t);
                CustomLog.d(TAG,"check_identifier.response=" + t);
                try {
                    JSONObject response = new JSONObject(t);

                    int code = response.getInt("error");
                    if (code == 0) {

                        DialogInterface.OnClickListener positiveListener = new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                requre_sms_code(country_code, mobile);
                            }
                        };
                        String title = getApplicationContext().getResources().getString(R.string.tip_title_confirm_phone);
                        String message = getApplicationContext().getResources().getString(R.string.tip_msg_confirm_phone) + "\n+"
                                + country_code + " " + mobile;
                        String positiveText = getApplicationContext().getResources().getString(R.string.ok);
                        String negativeText = getApplicationContext().getResources().getString(R.string.cancle);

                        showConfirmDialog(title, message, positiveText, negativeText,
                                positiveListener);

                    } else if (code == 3201) {
                        // 已注册 - Registered
                        DialogInterface.OnClickListener positiveListener = new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog,
                                                int which) {
                                Intent login_intent = new Intent(
                                        RegisterActivity.this,
                                        UserLoginActivity.class);
                                startActivity(login_intent);
                                finish();
                            }
                        };
                        String title = getApplicationContext().getResources().getString(R.string.phone_number_confirm);
                        String message = getApplicationContext().getResources().getString(R.string.tip_number_registed);
                        String positiveText = getApplicationContext().getResources().getString(R.string.Login_text);
                        String negativeText = getApplicationContext().getResources().getString(R.string.cancle);

                        showConfirmDialog(title, message, positiveText,
                                negativeText, positiveListener);

                        // showConfirmInformation("Phone Number Confirmation",
                        // "The number has been registered.");
                        // Intent mIntent = new Intent();
                        // mIntent.putExtra("country_code", country_code);
                        // mIntent.putExtra("mobile", identifier);
                        // mIntent.setClass(RegisterActivity.this,
                        // UserLoginActivity.class);
                        // startActivity(mIntent);
                    } else {
                        String message = response.getJSONObject("info")
                                .getString("message");
                        Toast.makeText(getApplicationContext(), message,
                                Toast.LENGTH_SHORT).show();

                    }

                } catch (JSONException e) {
                    CustomLog.logException(TAG,e);
                }

            }

            @Override
            public void onFailure(int errorNo, String strMsg) {
                super.onFailure(errorNo, strMsg);
                String msg = String.format("errorNo:%s\n%s", errorNo, strMsg);
                CustomLog.d(TAG,"error=" + msg);
                Toast.makeText(RegisterActivity.this, msg, Toast.LENGTH_LONG)
                        .show();
            }

            @Override
            public void onFinish() {
                dismissDialog(null, 10);
            }
        });

    }

    private void requre_sms_code(final String country_code, String phone) {
        final String request_url = KidsWatApiUrl.getUrlFor___requre_sms_code(
                country_code, phone);
        // -------------------------

        ApiHttpClient.get(request_url, new HttpCallBack() {

            @Override
            public void onPreStart() {
                showWaitDialog("...Загрузка...");
            }

            @Override
            public void onSuccess(String t) {
                super.onSuccess(t);
                CustomLog.d(TAG,"requre_sms_code.response=" + t);
                try {
                    JSONObject response = new JSONObject(t);

                    int code = response.getInt("error");
                    CustomLog.d(TAG, "code=" + code);
                    if (code == 0) {
                        // --------------
                        Intent mIntent = new Intent();
                        mIntent.setClass(getApplicationContext(),
                                RegisterNewAccountActivity.class);
                        mIntent.putExtra("country_code", country_code);
                        mIntent.putExtra("mobile", mobile);
                        RegisterActivity.this.startActivity(mIntent);

                        RegisterActivity.this.finish();
                    } else {
                        Toast.makeText(RegisterActivity.this, getApplicationContext().getResources().getString(R.string.tip_send_sms_error),
                                Toast.LENGTH_SHORT).show();
                    }

                } catch (JSONException e) {
                    CustomLog.logException(TAG,e);
                }

            }

            @Override
            public void onFailure(int errorNo, String strMsg) {
                super.onFailure(errorNo, strMsg);
                String msg = String.format("errorNo:%s\n%s", errorNo, strMsg);
                CustomLog.d(TAG,"error=" + msg);
                Toast.makeText(RegisterActivity.this, msg, Toast.LENGTH_LONG)
                        .show();
            }

            @Override
            public void onFinish() {
                dismissDialog(null, 10);
            }
        });

    }

    class ClickAble extends ClickableSpan implements View.OnClickListener {

        OnClickListener mListener;

        public ClickAble(OnClickListener listener) {
            mListener = listener;
        }

        @Override
        public void onClick(View widget) {
            // TODO Auto-generated method stub
            mListener.onClick(widget);
        }

    }

    // ----------------

}
