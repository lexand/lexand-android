package com.qiwo.xkidswatcher_russia.ui;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.qiwo.xkidswatcher_russia.AppContext;
import com.qiwo.xkidswatcher_russia.KidsWatConfig;
import com.qiwo.xkidswatcher_russia.R;
import com.qiwo.xkidswatcher_russia.api.ApiHttpClient;
import com.qiwo.xkidswatcher_russia.api.remote.KidsWatApiUrl;
import com.qiwo.xkidswatcher_russia.base.BaseActivity;
import com.qiwo.xkidswatcher_russia.bean.beanFor___get_watch_info;
import com.qiwo.xkidswatcher_russia.db.SqlDb;
import com.qiwo.xkidswatcher_russia.thread.CommonThreadMethod;
import com.qiwo.xkidswatcher_russia.thread.ThreadParent;
import net.intari.CustomLogger.CustomLog;

import org.json.JSONException;
import org.json.JSONObject;
import org.kymjs.kjframe.http.HttpCallBack;

import java.util.HashMap;
import java.util.Map;

@SuppressLint("ShowToast")
public class ChangeSIMActivity extends BaseActivity implements OnClickListener {

    /**
     * 换卡
     */

    public static final String TAG = ChangeSIMActivity.class.getSimpleName();

    private static final String ACTIONCHANGESIM = "w461_change_sim_no_v1";
    private static final int CHANGESIMCODE = 2011;

    final private int GET_CODE = 1000;

    private Button change_ok, Country_btn;
    private EditText Country_number;
    private LinearLayout linearLayout_l;
    @SuppressLint("HandlerLeak")
    private Handler handler = new Handler() {

        @Override
        public void handleMessage(Message msg) {

            String mess = msg.obj.toString();
            int code = msg.arg1;

            if (mess == null || mess.equals(""))
                return;
            JSONObject json = null;
            try {
                json = new JSONObject(mess);

                int errorCode = json.getInt("error");

                switch (code) {
                    case CHANGESIMCODE:
                        JSONObject infoJson = json.getJSONObject("info");
                        if (errorCode == 0) {
                            get_watch_info(AppContext.getInstance().getCurrentFamily().device_id);
                        }

                        Toast.makeText(ChangeSIMActivity.this,
                                infoJson.getString("message"), 1).show();
                        break;
                }
                hideWaitDialog();
            } catch (JSONException e) {
                CustomLog.logException(TAG,e);
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_sim);
        //getActionBar().hide();
        init();
    }

    public void init() {
        change_ok = (Button) findViewById(R.id.change_ok);
        Country_btn = (Button) findViewById(R.id.Country_btn);
        Country_number = (EditText) findViewById(R.id.Country_number);
        linearLayout_l = (LinearLayout) findViewById(R.id.linearLayout_l);

        change_ok.setOnClickListener(this);
        Country_btn.setOnClickListener(this);
        linearLayout_l.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.change_ok:
                String editnum = Country_number.getText().toString().trim();

                if (!editnum.equals("")) {
                    Submit();
                } else {
                    Toast.makeText(ChangeSIMActivity.this,
                            getString(R.string.chang_sim_1), 1).show();
                }

                break;

            case R.id.Country_btn:
                startActivityForResult(new Intent(this, CountryActivity.class),
                        GET_CODE);
                break;

            case R.id.linearLayout_l:

                this.finish();
                break;

            default:
                break;
        }
    }

    /**
     * 设置状态
     */
    public void Submit() {
        showWaitDialog();
        new Thread(new ThreadParent(TAG,KidsWatApiUrl.getApiUrl(),
                CommonThreadMethod.JsonFormat(SetSubmitParameter()), handler,
                CHANGESIMCODE)).start();
    }

    /*
     * 参数
     */
    public Map<String, String> SetSubmitParameter() {
        Map<String, String> hash = new HashMap<String, String>();
        hash.put("uid", KidsWatConfig.getUserUid());
        hash.put("device_id", KidsWatConfig.getUserDeviceId());
        hash.put("access_token", KidsWatConfig.getUserToken());

        //
        hash.put("cur_country_code", KidsWatConfig.getUseCountryCode());
        hash.put("cur_sim_no", KidsWatConfig.getUserPhone());
        hash.put("new_country_code", Country_btn.getText().toString().trim()
                .replace("+", ""));
        hash.put("new_sim_no", Country_number.getText().toString().trim());
        hash.put("customer_no", "1");

        KidsWatApiUrl.addExtraPara(ACTIONCHANGESIM, hash);

        return hash;
    }

    private void get_watch_info(final String device_id) {

        final String uid = KidsWatConfig.getUserUid();
        final String access_token = KidsWatConfig.getUserToken();

        final String req_url = KidsWatApiUrl.getUrlFor___get_watch_info(uid,
                access_token, device_id);

        ApiHttpClient.get(req_url, new HttpCallBack() {

            @Override
            public void onSuccess(String t) {
                super.onSuccess(t);
                CustomLog.d(TAG,String.format("url:%s\nt:%s", req_url, t));
                // gson.fromJson("{"user":"admin","pwd":"123456"}",
                beanFor___get_watch_info b = AppContext.getInstance().getGson()
                        .fromJson(t, beanFor___get_watch_info.class);

                if (b.error == 0) {
                    SqlDb db = SqlDb.get(AppContext.getInstance());
                    db.saveFamily(device_id, b);
                    db.closeDb();

                    ChangeSIMActivity.this.finish();
                }

            }

            @Override
            public void onFailure(int errorNo, String strMsg) {
                super.onFailure(errorNo, strMsg);
                String xmsg = String.format("errorNo:%s\n%s", errorNo, strMsg);
                CustomLog.d(TAG,String.format("url:%s\nt:%s", req_url, xmsg));
            }

        });

    }

    /**
     * This method is called when the sending activity has finished, with the
     * result it supplied.
     */
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == GET_CODE) {
            if (resultCode != RESULT_CANCELED) {
                if (data != null) {
                    Country_btn.setText(data.getAction());
                }
            }
        }
    }

    @Override
    public void initView() {

    }

    @Override
    public void initData() {

    }

}
