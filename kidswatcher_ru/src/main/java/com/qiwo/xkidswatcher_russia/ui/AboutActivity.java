package com.qiwo.xkidswatcher_russia.ui;

import android.annotation.SuppressLint;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.qiwo.xkidswatcher_russia.R;
import com.qiwo.xkidswatcher_russia.base.BaseActivity;

import net.intari.CustomLogger.CustomLog;

import butterknife.BindView;;

public class AboutActivity extends BaseActivity {
    public static final String TAG = AboutActivity.class.getSimpleName();

    @BindView(R.id.linearLayout_l)
    LinearLayout linearLayout_l;

    @BindView(R.id.about_version)
    TextView mVersion;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_about;
    }

    @Override
    protected boolean hasActionBar() {
        return false;
    }

    @Override
    protected void init(Bundle savedInstanceState) {
        super.init(savedInstanceState);
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.linearLayout_l:
                finish();
                break;
            case R.id.button_ok:
                finish();
                break;
            default:
                break;
        }
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void initView() {
        // linearLayout_l.setOnClickListener(this);
        linearLayout_l.setOnClickListener(this);
        // -------
        try {
            PackageInfo info = getPackageManager().getPackageInfo(
                    getPackageName(), 0);
            mVersion = findViewById(R.id.about_version);
            mVersion.setText(getResources().getString(R.string.app_name) + " V" + info.versionName);
        } catch (NameNotFoundException e) {
            CustomLog.logException(e);
        }
    }

    @Override
    public void initData() {
        //
    }
    // ----------------

}
