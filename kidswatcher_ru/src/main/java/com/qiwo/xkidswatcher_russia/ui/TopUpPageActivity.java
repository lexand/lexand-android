package com.qiwo.xkidswatcher_russia.ui;

import android.view.KeyEvent;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.LinearLayout;

import com.qiwo.xkidswatcher_russia.AppContext;
import com.qiwo.xkidswatcher_russia.KidsWatConfig;
import com.qiwo.xkidswatcher_russia.R;
import com.qiwo.xkidswatcher_russia.base.BaseActivity;
import net.intari.CustomLogger.CustomLog;

import com.qiwo.xkidswatcher_russia.bean.beanForDb___Family;
import com.qiwo.xkidswatcher_russia.utils.NetworkUtils;
import com.qiwo.xkidswatcher_russia.widget.ProgressWebView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import butterknife.BindView;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.Headers;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class TopUpPageActivity extends BaseActivity {
    public static final String TAG = TopUpPageActivity.class.getSimpleName();

    @BindView(R.id.webview)
    ProgressWebView webview;

    @BindView(R.id.linearLayout_l)
    LinearLayout linearLayout_l;

    @Override
    public void onClick(View v) {
        // TODO Auto-generated method stub
        switch (v.getId()) {
            case R.id.linearLayout_l:
                TopUpPageActivity.this.finish();
                break;

            default:
                break;
        }
    }

    @Override
    public void initView() {
        // TODO Auto-generated method stub
        linearLayout_l.setOnClickListener(this);
    }

    @Override
    protected int getLayoutId() {
        // TODO Auto-generated method stub
        return R.layout.activity_totup_page;
    }

    @Override
    protected boolean hasActionBar() {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public void initData() {

        String sim = KidsWatConfig.getUseCountryCode() + KidsWatConfig.getUserPhone();
        beanForDb___Family family=AppContext.getInstance().getCurrentFamily();
        if (family!=null) {
            String iccid = family.device_iccid;
            CustomLog.d(TAG,"phone number >>> " + sim);
            CustomLog.d(TAG,"iccid >>> " + iccid);
            loadWebView(sim, iccid);
        } else {
            CustomLog.e(TAG,"Current Family is null for sim:"+sim);
            AppContext.showToast(R.string.topup_no_family);
        }
    }

    private void get_device_timeleft(String sim, String iccid) {
//		final String url = "http://kids_radar.clients4.arealidea.ru/api/v1/mobile_service/balance/";
        final String url = "https://kidsradar.lexand.ru/api/v1/mobile_service/balance/";
        getData(url);
    }

    private void getData(String url) {

        String iccid = AppContext.getInstance().getCurrentFamily().device_iccid;
        String sim = KidsWatConfig.getUseCountryCode() + KidsWatConfig.getUserPhone();
        String token = "4teGENskDv2AXepVstwj";
        CustomLog.d(TAG,"Loading data from url with token:"+token+",sim(userPhone):"+sim+", and watchICCID:"+iccid);
        RequestBody requestBody = new FormBody.Builder()
                .add("watchICCID",String.format("[\"%s\"]", iccid))
                .add("phoneNumber",sim)
                .add("token",token)
                /*

                new MultipartBuilder()
                .type(MultipartBuilder.FORM)
                .addPart(Headers.of(
                        "Content-Disposition",
                        "form-data; name=\"watchICCID\""),
                        RequestBody.create(null, String.format("[\"%s\"]", iccid)))
                .addPart(Headers.of(
                        "Content-Disposition",
                        "form-data; name=\"phoneNumber\""),
                        RequestBody.create(null, sim))
                .addPart(Headers.of(
                        "Content-Disposition",
                        "form-data; name=\"token\""),
                        RequestBody.create(null, token)) */
                .build();

        Request request = new Request.Builder()
                .url(url)
                .post(requestBody)
                .build();

        Call call = NetworkUtils.getOkHttpClient().newCall(request);
        call.enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                CustomLog.d(TAG,"Failed to process request "+request.url());
                CustomLog.logException(TAG,e);
                String sim = null;
                int timeleft = 0;

                webview.loadUrl(String.format("http://misafes1.qiwocloud2.com/view/pay/index.html?sim=%s&timeleft=%d", sim, timeleft));
                webview.getSettings().setJavaScriptEnabled(true);

                webview.setWebViewClient(new WebViewClient() {
                    @Override
                    public boolean shouldOverrideUrlLoading(WebView view, String url) {
                        view.loadUrl(url);
                        return true;
                    }
                });
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                try {

                    String resp_str = response.body().string();
                    CustomLog.d(TAG,resp_str);
                    JSONObject json = new JSONObject(resp_str);

                    if (json.getInt("success") == 1) {
                        String sim = AppContext.getInstance().getCurrentFamily().sim;
                        JSONArray arrays = (JSONArray) json.get("balance");

                        if (arrays == null || arrays.length() <= 0) {
                            return;
                        }

                        int timeleft = ((JSONObject) arrays.get(0)).getInt("useDays");
                        webview.loadUrl(String.format("http://misafes1.qiwocloud2.com/view/pay/index.html?sim=%s&timeleft=%d", sim, timeleft));
                        webview.getSettings().setJavaScriptEnabled(true);

                        webview.setWebViewClient(new WebViewClient() {
                            @Override
                            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                                view.loadUrl(url);
                                return true;
                            }
                        });
                    } else {
                        String sim = null;
                        int timeleft = 0;

                        webview.loadUrl(String.format("http://misafes1.qiwocloud2.com/view/pay/index.html?sim=%s&timeleft=%d", sim, timeleft));
                        webview.getSettings().setJavaScriptEnabled(true);

                        webview.setWebViewClient(new WebViewClient() {
                            @Override
                            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                                view.loadUrl(url);
                                return true;
                            }
                        });
                    }

                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    CustomLog.logException(TAG,e);

                    String sim = null;
                    int timeleft = 0;

                    webview.loadUrl(String.format("http://misafes1.qiwocloud2.com/view/pay/index.html?sim=%s&timeleft=%d", sim, timeleft));
                    webview.getSettings().setJavaScriptEnabled(true);

                    webview.setWebViewClient(new WebViewClient() {
                        @Override
                        public boolean shouldOverrideUrlLoading(WebView view, String url) {
                            view.loadUrl(url);
                            return true;
                        }
                    });
                }

            }
        });
        /*
        call.enqueue(new Callback() {
            @Override
            public void onFailure(Request request, IOException e) {
                CustomLog.d(TAG,"Failed to process request "+request.url());
                CustomLog.logException(TAG,e);
                String sim = null;
                int timeleft = 0;

                webview.loadUrl(String.format("http://misafes1.qiwocloud2.com/view/pay/index.html?sim=%s&timeleft=%d", sim, timeleft));
                webview.getSettings().setJavaScriptEnabled(true);

                webview.setWebViewClient(new WebViewClient() {
                    @Override
                    public boolean shouldOverrideUrlLoading(WebView view, String url) {
                        view.loadUrl(url);
                        return true;
                    }
                });
            }

            @Override
            public void onResponse(final Response response) throws IOException {
                try {

                    String resp_str = response.body().string();
                    CustomLog.d(TAG,resp_str);
                    JSONObject json = new JSONObject(resp_str);

                    if (json.getInt("success") == 1) {
                        String sim = AppContext.getInstance().getCurrentFamily().sim;
                        JSONArray arrays = (JSONArray) json.get("balance");

                        if (arrays == null || arrays.length() <= 0) {
                            return;
                        }

                        int timeleft = ((JSONObject) arrays.get(0)).getInt("useDays");
                        webview.loadUrl(String.format("http://misafes1.qiwocloud2.com/view/pay/index.html?sim=%s&timeleft=%d", sim, timeleft));
                        webview.getSettings().setJavaScriptEnabled(true);

                        webview.setWebViewClient(new WebViewClient() {
                            @Override
                            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                                view.loadUrl(url);
                                return true;
                            }
                        });
                    } else {
                        String sim = null;
                        int timeleft = 0;

                        webview.loadUrl(String.format("http://misafes1.qiwocloud2.com/view/pay/index.html?sim=%s&timeleft=%d", sim, timeleft));
                        webview.getSettings().setJavaScriptEnabled(true);

                        webview.setWebViewClient(new WebViewClient() {
                            @Override
                            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                                view.loadUrl(url);
                                return true;
                            }
                        });
                    }

                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    CustomLog.logException(TAG,e);

                    String sim = null;
                    int timeleft = 0;

                    webview.loadUrl(String.format("http://misafes1.qiwocloud2.com/view/pay/index.html?sim=%s&timeleft=%d", sim, timeleft));
                    webview.getSettings().setJavaScriptEnabled(true);

                    webview.setWebViewClient(new WebViewClient() {
                        @Override
                        public boolean shouldOverrideUrlLoading(WebView view, String url) {
                            view.loadUrl(url);
                            return true;
                        }
                    });
                }
            }
        }); */
    }

    // ----------------
    //设置回退
    //覆盖Activity类的onKeyDown(int keyCoder,KeyEvent event)方法
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK) && webview.canGoBack()) {
            webview.goBack(); //goBack()表示返回WebView的上一页面
            return true;
        } else if (keyCode == KeyEvent.KEYCODE_BACK && !webview.canGoBack()) {
            TopUpPageActivity.this.finish();
            return true;
        }
        return false;
    }

    public void loadWebView(String phone_number, String iccid) {
//		String pay_url = String.format("http://kids_radar.clients4.arealidea.ru/api/v1/mobile_service/pay/"
//				+ "?watchICCID=%s&phoneNumber=%s&token=4teGENskDv2AXepVstwj",
//				iccid, phone_number);
        //换新的url
        String pay_url = String.format("https://kidsradar.lexand.ru/api/v1/mobile_service/pay/"
                        + "?watchICCID=%s&phoneNumber=%s&token=4teGENskDv2AXepVstwj",
                iccid, phone_number);
        CustomLog.d(TAG,"pay_url >>> " + pay_url);
        webview.getSettings().setJavaScriptEnabled(true);
        webview.loadUrl(pay_url);

        webview.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                return true;
            }
        });
    }
}
