package com.qiwo.xkidswatcher_russia.ui;

import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import androidx.core.view.ViewCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewFlipper;

import com.prolificinteractive.materialcalendarview.CalendarDay;
import com.prolificinteractive.materialcalendarview.MaterialCalendarView;
import com.prolificinteractive.materialcalendarview.OnDateSelectedListener;
import com.qiwo.xkidswatcher_russia.AppContext;
import com.qiwo.xkidswatcher_russia.KidsWatConfig;
import com.qiwo.xkidswatcher_russia.R;
import com.qiwo.xkidswatcher_russia.api.ApiHttpClient;
import com.qiwo.xkidswatcher_russia.api.remote.KidsWatApiUrl;
import com.qiwo.xkidswatcher_russia.base.BaseActivity;
import com.qiwo.xkidswatcher_russia.bean.beanForDb___Family;
import com.qiwo.xkidswatcher_russia.bean.beanFor___get_watch_data;
import com.qiwo.xkidswatcher_russia.util.KidsWatUtils;
import net.intari.CustomLogger.CustomLog;
import com.qiwo.xkidswatcher_russia.util.TimeHelper;
import com.qiwo.xkidswatcher_russia.widget.SeekBarHint;
import com.qiwo.xkidswatcher_russia.widget.SeekBarHint.OnSeekBarHintProgressChangeListener;
import com.qiwo.xkidswatcher_russia.yandexmap.MyPathOverLay;
import com.yandex.mapkit.mapview.MapView;

import org.kymjs.kjframe.http.HttpCallBack;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import butterknife.BindView;;
import ru.yandex.yandexmapkit.MapController;
import ru.yandex.yandexmapkit.MapView;
import ru.yandex.yandexmapkit.OverlayManager;
import ru.yandex.yandexmapkit.overlay.Overlay;
import ru.yandex.yandexmapkit.overlay.OverlayItem;
import ru.yandex.yandexmapkit.overlay.balloon.BalloonItem;
import ru.yandex.yandexmapkit.utils.GeoPoint;
import ru.yandex.yandexmapkit.utils.ScreenPoint;

public class HistoryActivity_v2 extends BaseActivity implements
        OnTouchListener, OnDateSelectedListener {

    final String TAG = HistoryActivity_v2.class.getSimpleName();

    @BindView(R.id.linearLayout_l_a)
    LinearLayout linearLayout_l_a;

    @BindView(R.id.linearLayout_m_a)
    LinearLayout linearLayout_m_a;

    @BindView(R.id.linearLayout_r_a)
    LinearLayout linearLayout_r_a;

    @BindView(R.id.imageView_arr_a)
    ImageView imageView_arr_a;

    @BindView(R.id.textView_title_a)
    TextView textView_title_a;

    @BindView(R.id.imageview_r_a)
    ImageView imageview_r_a;

    @BindView(R.id.linearLayout_l_b)
    LinearLayout linearLayout_l_b;

    @BindView(R.id.linearLayout_m_b)
    LinearLayout linearLayout_m_b;

    @BindView(R.id.linearLayout_r_b)
    LinearLayout linearLayout_r_b;

    @BindView(R.id.imageView_arr_b)
    ImageView imageView_arr_b;

    @BindView(R.id.textView_title_b)
    TextView textView_title_b;

    @BindView(R.id.imageview_r_b)
    ImageView imageview_r_b;

    @BindView(R.id.linearLayout_cal)
    LinearLayout linearLayout_cal;

    @BindView(R.id.linearlayout_c1)
    LinearLayout linearlayout_c1;

    @BindView(R.id.linearlayout_c2)
    LinearLayout linearlayout_c2;

    @BindView(R.id.linearLayout_walk)
    LinearLayout linearLayout_walk;

    @BindView(R.id.linearLayout_energy)
    LinearLayout linearLayout_energy;

    //	@BindView(R.id.calendar_view)
//	DayPickerView calendarView;
    @BindView(R.id.calendar_view)
    MaterialCalendarView calendarview;

    @BindView(R.id.textView_num)
    TextView textView_num;

    @BindView(R.id.textView_unit)
    TextView textView_unit;

    @BindView(R.id.textView_distance)
    TextView textView_distance;

    @BindView(R.id.textView_step)
    TextView textView_step;

    @BindView(R.id.textView_cal)
    TextView textView_cal;

    @BindView(R.id.textView_time)
    TextView textView_time;

    @BindView(R.id.textView_stime)
    TextView textView_stime;

    @BindView(R.id.linearlayout_tv_last)
    TextView linearlayout_tv_last;

    @BindView(R.id.linearlayout_tv_next)
    TextView linearlayout_tv_next;

    @BindView(R.id.linearlayout_time)
    LinearLayout linearlayout_time;

    @BindView(R.id.linearlayout_ct)
    LinearLayout linearlayout_ct;

    @BindView(R.id.linearlayout_title)
    TextView linearlayout_title;

    @BindView(R.id.seekbar_hint)
    SeekBarHint seekbar_hint;

    @BindView(R.id.viewFlipper)
    ViewFlipper viewFlipper;

    @BindView(R.id.history_mapview)
    MapView mMapView;
    // ---------------
    @BindView(R.id.recyclerview)
    RecyclerView recyclerView;
    LinearLayoutManager layoutManager;
    List<beanFor___get_watch_data.CRows> datas;
    StRecycleAdapter mAdapter;
    int panel_select_index = 0;
    int screenWidth;
    int screenHeight;
    // int linearWidth;
    // int linearHeight;
    int lastX;
    int lastY;
    int year, month, dayOfMonth;
    Map<Long, OverlayItem> markerHistoryList;
    // ----------------
    private View mSavedView;// 当前操作的菜单项
    private MyPathOverLay mMyPathOverLay;
    // ----------------
    private OnItemClickListener recyleItemListener = new OnItemClickListener() {

        @Override
        public void onItemClick(int position, Object object) {
            beanFor___get_watch_data.CRows bean = (beanFor___get_watch_data.CRows) object;
            CustomLog.d(TAG,bean.type + bean.address);
        }
    };
    private Overlay mOverlay;
    private MapController mMapController;
    private GestureDetector mGestureDetector;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_history2;
    }

    @Override
    protected boolean hasActionBar() {
        return false;
    }

    @Override
    protected void init(Bundle savedInstanceState) {
        super.init(savedInstanceState);
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.linearLayout_l_a:
            case R.id.linearLayout_l_b:
                finish();
                break;
            case R.id.linearLayout_r_a:
            case R.id.linearLayout_r_b:
                boolean showListview = linearlayout_c2.getVisibility() == View.GONE ? true
                        : false;
                showHistoryListView(showListview);
                break;
            case R.id.linearLayout_m_a:
            case R.id.linearLayout_m_b:
                boolean showCalview = linearLayout_cal.getVisibility() == View.GONE ? true
                        : false;
                showCalView(showCalview);
                break;
            case R.id.button_ok:
                finish();
                break;
            case R.id.linearLayout_walk:
                highlightSelectedItem(v);
                panel_select_index = 0;
                updateStepCalc();
                break;
            case R.id.linearLayout_energy:
                highlightSelectedItem(v);
                panel_select_index = 1;
                updateStepCalc();
                break;
            case R.id.linearlayout_tv_last:
                getBeforeDate(textView_title_a.getText().toString().trim());
                break;
            case R.id.linearlayout_tv_next:
                getAfterDate(textView_title_a.getText().toString().trim());
                break;
            default:
                break;
        }
    }

    /**
     * 时间字符串获取前一天日期
     *
     * @param dateStr
     */
    private void getBeforeDate(String dateStr) {
        if (dateStr != null && !dateStr.equals("")) {
            if (dateStr.equals(getApplicationContext().getResources().getString(R.string.today))) {
                //日期是当天
                dateStr = String.format("%02d-%02d-%04d", dayOfMonth, month + 1, year);
            }

            Calendar ca = Calendar.getInstance();
            ca.setTime(TimeHelper.getStringtoDateDMY(dateStr));
            ca.add(Calendar.DAY_OF_MONTH, -1);

            onDateSelected(calendarview, new CalendarDay(ca.get(Calendar.YEAR),
                    ca.get(Calendar.MONTH), ca.get(Calendar.DAY_OF_MONTH)), true);
            calendarview.setSelectedDate(ca);
        }
    }

    /**
     * 根据时间字符串获取下一天的日期
     */
    private void getAfterDate(String dateStr) {
        if (dateStr != null && !dateStr.equals("")) {
            Calendar ca = Calendar.getInstance();
            ca.setTime(TimeHelper.getStringtoDateDMY(dateStr));
            ca.add(Calendar.DAY_OF_MONTH, 1);

            onDateSelected(calendarview, new CalendarDay(ca.get(Calendar.YEAR),
                    ca.get(Calendar.MONTH), ca.get(Calendar.DAY_OF_MONTH)), true);
            calendarview.setSelectedDate(ca);
        }
    }

    @Override
    public void onDateSelected(MaterialCalendarView widget,
                               CalendarDay date, boolean selected) {
        // TODO Auto-generated method stub
        int year = date.getYear();
        int month = date.getMonth();
        int day = date.getDay();

        String dateStr = String.format("%d-%02d-%02d", day, month + 1, year);

        Calendar a = Calendar.getInstance();
        String tipText;
        if (year == a.get(Calendar.YEAR) && month == a.get(Calendar.MONTH)
                && day == a.get(Calendar.DATE)) {
            textView_title_a.setText(getApplicationContext().getResources().getString(R.string.today));
            textView_title_b.setText(getApplicationContext().getResources().getString(R.string.today));
            linearlayout_tv_next.setVisibility(View.INVISIBLE);

            Calendar ca = Calendar.getInstance();
            int xhour = ca.get(Calendar.HOUR_OF_DAY) + 1;

            tipText = String.format("00-%02d", xhour);
            textView_stime.setText(tipText);

            seekbar_hint.setProgress(xhour);

        } else if (year < a.get(Calendar.YEAR) || (month < a.get(Calendar.MONTH) && year == a.get(Calendar.YEAR)) || (year == a.get(Calendar.YEAR) && month == a.get(Calendar.MONTH)
                && day < a.get(Calendar.DATE))) {
            textView_title_a.setText(dateStr);
            textView_title_b.setText(dateStr);
            linearlayout_tv_next.setVisibility(View.VISIBLE);

            tipText = String.format("00-%02d", 24);
            textView_stime.setText(tipText);
            seekbar_hint.setProgress(24);
        } else {
            //未来的某天
            Toast.makeText(HistoryActivity_v2.this, getApplicationContext().getResources().getString(R.string.tip_select_correct_date), Toast.LENGTH_SHORT).show();
            return;
        }
        // showLongToast(dateStr);
        showCalView(false);
        long utcTime = KidsWatUtils.getUtcTimeAt0Time(year, month, day);
        get_watch_data(utcTime);
    }

    @Override
    public void initView() {
        mMapController = mMapView.getMapController();
        OverlayManager overlayManager = mMapController.getOverlayManager();
        mOverlay = new Overlay(mMapController);
        overlayManager.addOverlay(mOverlay);
        overlayManager.getMyLocation().setEnabled(false);
        overlayManager.getMyLocation().clearOverlayItems();

//		mMapView.showBuiltInScreenButtons(true);
//		mMapView.showJamsButton(false);
//		mMapView.showFindMeButton(false);
        //mMapController.addMapListener(this);
        //setGesture();
        // ------------------------------------------
        linearLayout_l_a.setOnClickListener(this);
        linearLayout_r_a.setOnClickListener(this);
        linearLayout_m_a.setOnClickListener(this);

        linearLayout_l_b.setOnClickListener(this);
        linearLayout_r_b.setOnClickListener(this);
        linearLayout_m_b.setOnClickListener(this);

        linearlayout_tv_last.setOnClickListener(this);
        linearlayout_tv_next.setOnClickListener(this);

        // MaterialCalendarView 设置
        calendarview.setArrowColor(Color.BLUE);
        calendarview.setOnDateChangedListener(this);
        calendarview.setSelectedDate(Calendar.getInstance());
        calendarview.state().edit().setFirstDayOfWeek(Calendar.MONDAY).commit();

        linearlayout_ct.setOnTouchListener(this);
        Calendar a = Calendar.getInstance();
        int hour = a.get(Calendar.HOUR_OF_DAY) + 1;
        seekbar_hint.setProgress(hour);

        String tipText = String.format("00-%02d", hour);
        textView_stime.setText(tipText);
        seekbar_hint
                .setOnProgressChangeListener(new OnSeekBarHintProgressChangeListener() {
                    @Override
                    public String onHintTextChanged(SeekBarHint seekBarHint,
                                                    int progress) {
                        CustomLog.d(TAG,"-------------hinttextchange---------------");
                        //------change------
                        //onMapClick(null);
                        setPointMarker();
                        Map<Long, OverlayItem> traceData = new TreeMap<Long, OverlayItem>();
                        for (long xtime : markerHistoryList.keySet()) {
                            Calendar a = Calendar.getInstance();
                            a.setTimeInMillis(xtime * 1000);
                            int hour = a.get(Calendar.HOUR_OF_DAY);
                            if (hour < progress) {
                                //隐藏画线层，显示marker层
                                mMapController.getOverlayManager().removeOverlay(mMyPathOverLay);
                                traceData.put(xtime, markerHistoryList.get(xtime));
                                //new MyPathOverLay(HistoryActivity2.this, mMapController, mMapView, traceData);
                                markerHistoryList.get(xtime).setVisible(true);
                                mMapController.setPositionAnimationTo(mMapController.getMapCenter());
                            } else {
                                mMapController.getOverlayManager().removeOverlay(mMyPathOverLay);
                                markerHistoryList.get(xtime).setVisible(false);
                                mMapController.setPositionAnimationTo(mMapController.getMapCenter());
                            }
                        }
                        String tipText = String.format("00-%02d", progress);
                        textView_stime.setText(tipText);
                        mMyPathOverLay = new MyPathOverLay(HistoryActivity_v2.this, mMapController, mMapView, traceData);
                        mMapController.getOverlayManager().addOverlay(mMyPathOverLay);
                        return tipText;
                    }
                });

        seekbar_hint
                .setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                    /**
                     * 拖动条停止拖动的时候调用
                     */
                    @Override
                    public void onStopTrackingTouch(SeekBar seekBar) {
                        CustomLog.d(TAG,"拖动停止 - Stop Dragging(drag to stop)");

                        if (textView_title_a.getText().toString().trim()
                                .equalsIgnoreCase("History")) {

                            Calendar a = Calendar.getInstance();
                            int hour = a.get(Calendar.HOUR_OF_DAY) + 1;
                            if (seekBar.getProgress() > hour) {
                                seekbar_hint.setProgress(hour);
                            }
                        }

                        if (textView_title_b.getText().toString().trim()
                                .equalsIgnoreCase(getApplicationContext().getResources().getString(R.string.today))) {

                            Calendar a = Calendar.getInstance();
                            int hour = a.get(Calendar.HOUR_OF_DAY) + 1;
                            if (seekBar.getProgress() > hour) {
                                seekbar_hint.setProgress(hour);
                            }
                        }
                    }

                    /**
                     * 拖动条开始拖动的时候调用
                     */
                    @Override
                    public void onStartTrackingTouch(SeekBar seekBar) {
                        CustomLog.d(TAG,"开始拖动 - Start Dragging");
                    }

                    /**
                     * 拖动条进度改变的时候调用
                     */
                    @Override
                    public void onProgressChanged(SeekBar seekBar,
                                                  int progress, boolean fromUser) {
                        //
                    }
                });
    }

    @Override
    public void initData() {
        markerHistoryList = new TreeMap<Long, OverlayItem>();

        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);// 设置布局管理器
        DisplayMetrics dm = getResources().getDisplayMetrics();
        screenWidth = dm.widthPixels;
        screenHeight = dm.heightPixels - 50;
        getWathchData();
    }

    public void getWathchData() {
        Calendar a = Calendar.getInstance();
        year = a.get(Calendar.YEAR);
        month = a.get(Calendar.MONTH);
        dayOfMonth = a.get(Calendar.DAY_OF_MONTH);

        CustomLog.d(TAG,"-----Today-----");
        CustomLog.d(TAG,"year=" + year);
        CustomLog.d(TAG,"month=" + month);
        CustomLog.d(TAG,"dayOfMonth=" + dayOfMonth);

        long utcNow = KidsWatUtils.getUtcTimeAt0Time(year, month, dayOfMonth);
        get_watch_data(utcNow);
    }

    //随进度条滑动二改变的textview
    @Override
    public boolean onTouch(View v, MotionEvent event) {
        // TODO Auto-generated method stub
        int action = event.getAction();
        String text = "";
        // CustomLog.d(TAG,"Touch:" + action);
        switch (action) {
            case MotionEvent.ACTION_DOWN:
                lastX = (int) event.getRawX();
                lastY = (int) event.getRawY();
                break;
            /**
             * layout(l,t,r,b) l Left position, relative to parent t Top position,
             * relative to parent r Right position, relative to parent b Bottom
             * position, relative to parent
             * */
            case MotionEvent.ACTION_MOVE:
                int dx = (int) event.getRawX() - lastX;
                int dy = (int) event.getRawY() - lastY;// y轴上移动距离
                dy = 0;

                int left = v.getLeft() + dx;
                int top = v.getTop() + dy;
                int right = v.getRight() + dx;
                int bottom = v.getBottom() + dy;
                if (left < 0) {
                    left = 0;
                    right = left + v.getWidth();
                }
                if (right > screenWidth - 30) {
                    right = screenWidth - 30;
                    left = right - v.getWidth();
                }
                if (top < 0) {
                    top = 0;
                    bottom = top + v.getHeight();
                }
                if (bottom > screenHeight) {
                    bottom = screenHeight;
                    top = bottom - v.getHeight();
                }
                text = String.format("00-%s", (int) left * 24
                        / (screenWidth - 30 - v.getWidth()));
                CustomLog.d(TAG,text);
                textView_time.setText(text);
                textView_time.setFocusable(false);
                v.layout(left, top, right, bottom);
                lastX = (int) event.getRawX();
                lastY = (int) event.getRawY();
                break;
            case MotionEvent.ACTION_UP:
                break;
        }
        return true;
    }

    private void highlightSelectedItem(View v) {
        setSelected(null, false);
        setSelected(v, true);
    }

//	// ----------------
//	@Override
//	public int getMaxYear() {
//		return 0;
//	}
//
//	@Override
//	public void onDayOfMonthSelected(int year, int month, int day) {
//		String dateStr = String.format("%d-%02d-%02d", year, month + 1, day);
//		Calendar a = Calendar.getInstance();
//		String tipText;
//		if (year == a.get(Calendar.YEAR) && month == a.get(Calendar.MONTH)
//				&& day == a.get(Calendar.DATE)) {
//			textView_title_a.setText(getApplicationContext().getResources().getString(R.string.today));
//			textView_title_b.setText(getApplicationContext().getResources().getString(R.string.today));
//			linearlayout_tv_next.setVisibility(View.INVISIBLE);
//
//			Calendar ca = Calendar.getInstance();
//			int xhour = ca.get(Calendar.HOUR_OF_DAY) + 1;
//
//			tipText = String.format("00-%02d", xhour);
//			textView_stime.setText(tipText);
//
//			seekbar_hint.setProgress(xhour);
//
//		} else if(year < a.get(Calendar.YEAR) || (month < a.get(Calendar.MONTH) && year == a.get(Calendar.YEAR)) ||  (year == a.get(Calendar.YEAR) && month == a.get(Calendar.MONTH)
//				&& day < a.get(Calendar.DATE))){
//			textView_title_a.setText(dateStr);
//			textView_title_b.setText(dateStr);
//			linearlayout_tv_next.setVisibility(View.VISIBLE);
//
//			tipText = String.format("00-%02d", 24);
//			textView_stime.setText(tipText);
//			seekbar_hint.setProgress(24);
//		}else{
//			//未来的某天
//		}
//		// showLongToast(dateStr);
//		showCalView(false);
//		CustomLog.d(TAG,"-----Today-----");
//		CustomLog.d(TAG,"year=" + year);
//		CustomLog.d(TAG,"month=" + month);
//		CustomLog.d(TAG,"dayOfMonth=" + day);
//		long utcTime = KidsWatUtils.getUtcTimeAt0Time(year, month, day);
//		get_watch_data(utcTime);
//	}
//
//	@Override
//	public void onDateRangeSelected(
//			SimpleMonthAdapter.SelectedDays<SimpleMonthAdapter.CalendarDay> selectedDays) {
//	}

    private void setSelected(View v, boolean selected) {
        View view;
        if (v == null && mSavedView == null) {
            return;
        }
        if (v != null) {
            mSavedView = v;
            view = mSavedView;

        } else {
            view = mSavedView;
        }

        if (selected) {
            ViewCompat.setHasTransientState(view, false);
            view.setBackgroundResource(R.drawable.bg_track_history_ll_press);
        } else {
            ViewCompat.setHasTransientState(view, true);
            view.setBackgroundColor(Color.TRANSPARENT);
        }
    }

    private void setRecycleAdapter(List<beanFor___get_watch_data.CRows> xdatas) {
        CustomLog.d(TAG,"Setting adapter - we have "+xdatas.size()+" items ");
        mAdapter = new StRecycleAdapter();
        mAdapter.setList(xdatas);
        mAdapter.setOnItemClickListener(recyleItemListener);
        if (recyclerView!=null) {
            recyclerView.setAdapter(mAdapter);// 设置适配器 Setting up the adapter
        } else {
            CustomLog.e(TAG,"Can't set adapter - recylerView is null at this time");
        }
    }

    private void showCalView(boolean b) {
        linearLayout_cal.setVisibility(b ? View.VISIBLE : View.GONE);
//		imageView_arr_a.setImageResource(b ? R.drawable.icon_up_x
//				: R.drawable.icon_down_x);
        imageView_arr_b.setImageResource(b ? R.drawable.icon_up_x
                : R.drawable.icon_down_x);
    }

    private void showHistoryListView(boolean b) {
        float halfWidth = viewFlipper.getWidth() / 2.0f;
        float halfHeight = viewFlipper.getHeight() / 2.0f;
        int duration = 500;
        int depthz = 0;// viewFlipper.getWidth()/2;

        if (linearLayout_cal.getVisibility() == View.VISIBLE)
            showCalView(false);
        if (b) {
            imageview_r_a.setImageResource(R.drawable.icon_history_foot_x);
            imageview_r_b.setImageResource(R.drawable.icon_location_x);
            // linearlayout_c1.setVisibility(View.GONE);
            // linearlayout_c2.setVisibility(View.VISIBLE);

//			Rotate3dAnimation in = new Rotate3dAnimation(-90, 0, halfWidth,
//					halfHeight, depthz, false);
//			in.setDuration(duration);
//			in.setStartOffset(duration);
//			viewFlipper.setInAnimation(in);
//
//			Rotate3dAnimation out = new Rotate3dAnimation(0, 90, halfWidth,
//					halfHeight, depthz, false);
//			out.setDuration(duration);
//			viewFlipper.setOutAnimation(out);
            viewFlipper.showNext();
        } else {
            imageview_r_a.setImageResource(R.drawable.icon_history_foot_x);
            imageview_r_b.setImageResource(R.drawable.icon_location_x);
            // linearlayout_c2.setVisibility(View.GONE);
            // linearlayout_c1.setVisibility(View.VISIBLE);
//			Rotate3dAnimation in = new Rotate3dAnimation(90, 0, halfWidth,
//					halfHeight, depthz, false);
//			in.setDuration(duration);
//			in.setStartOffset(duration);
//			viewFlipper.setInAnimation(in);
//			Rotate3dAnimation out = new Rotate3dAnimation(0, -90, halfWidth,
//					halfHeight, depthz, false);
//			out.setDuration(duration);
//			viewFlipper.setOutAnimation(out);
            viewFlipper.showPrevious();
        }
    }

    // -------end adapter---------
    private void updateStepCalc() {
        if (datas != null && datas.size() > 0 && AppContext.getInstance().getCurrentFamily()!=null)  {

            int rowtotal = datas.size();
            int step = datas.get(0).step_number;
            int calory = datas.get(0).calory;

            if (panel_select_index == 0) {
                textView_num.setText(String.valueOf(step));
                textView_unit.setText(getApplicationContext().getResources().getString(R.string.unit_step));
            } else {
//				textView_num.setText(String.valueOf(calory));
//				textView_unit.setText("ры");
            }
            float distance = distanceAlgorithm(step);
            textView_step.setText(distance + getApplicationContext().getResources().getString(R.string.unit_distance));
            textView_cal.setText(caloryAlgorithm(distance) + getApplicationContext().getResources().getString(R.string.unit_cal));
        } else {
            textView_step.setText("0 " + getApplicationContext().getResources().getString(R.string.unit_distance));
            textView_cal.setText("0 " + getApplicationContext().getResources().getString(R.string.unit_cal));

            if (panel_select_index == 0) {
                textView_num.setText("0");
                textView_unit.setText(getApplicationContext().getResources().getString(R.string.unit_step));
            } else {
//				textView_num.setText("0");
//				textView_unit.setText("ры");
            }
        }
    }

    private void addTrackHistoryToMap(GeoPoint point, String address, long time,
                                      int electricity, int type, int index) {
        if (mOverlay.getOverlayItems() == null) {
            return;
        }

        String strdate = new java.text.SimpleDateFormat("HH:mm")
                .format(new java.util.Date(time * 1000));

        String xAddress = address.length() > 30 ? address.substring(0, 30)
                + "..." : address;

        String xtitle = String.format("%s", strdate);
        OverlayItem overlayItem = new OverlayItem(point, getResources().getDrawable(R.drawable.point_select_smail));
        BalloonItem balloon = new BalloonItem(getApplicationContext(), point);
        balloon.setText(xtitle + "\n" + xAddress);
        overlayItem.setBalloonItem(balloon);

        if (index == 0)
            overlayItem.setDrawable(getResources().getDrawable(R.drawable.start_smail));
        else if (index == datas.size() - 1)
            overlayItem.setDrawable(getResources().getDrawable(R.drawable.end_smail));
        else
            overlayItem.setDrawable(getResources().getDrawable(R.drawable.ing_smail));

        mOverlay.addOverlayItem(overlayItem);
        mMapController.getOverlayManager().addOverlay(mOverlay);

        markerHistoryList.put(time, overlayItem);
        markerHistoryList.get(time).setVisible(true);
    }

    private void get_watch_data(long date) {
        String uid = KidsWatConfig.getUserUid();
        String access_token = KidsWatConfig.getUserToken();
        // String device_id = KidsWatConfig.getUserDeviceId();
        String family_id = KidsWatConfig.getDefaultFamilyId();

        final String request_url = KidsWatApiUrl.getUrlFor___get_watch_data(
                uid, access_token, family_id, date);

        ApiHttpClient.get(request_url, new HttpCallBack() {

            private List<Double> mLatList;
            private List<Double> mLonList;

            @Override
            public void onPreStart() {
                showWaitDialog("...Загрузка...");
            }

            @Override
            public void onSuccess(String t) {
                super.onSuccess(t);
                CustomLog.d(TAG,String.format("url:%s\nt:%s", request_url, t));

                beanFor___get_watch_data response = AppContext.getInstance()
                        .getGson().fromJson(t, beanFor___get_watch_data.class);

                if (response.error == 0) {
                    if (mOverlay.getOverlayItems() != null) {
                        mOverlay.clearOverlayItems();
                    }
                    CustomLog.d(TAG,"count : " + response.info.ddata.total_rows);
                    if (response.info.ddata.total_rows > 0) {
                        datas = response.info.ddata.rows;
                        //给recycleview设置数据
                        setRecycleAdapter(datas);
                        //更新textview数据:step和calc
                        updateStepCalc();
                        // -------add marker to map--------
                        if (mOverlay.getOverlayItems() != null) {
                            mOverlay.clearOverlayItems();
                            if (mMyPathOverLay != null) {
                                mMyPathOverLay.clearOverlayItems();
                                //mMapController.getOverlayManager().removeOverlay(mMyPathOverLay);
                            }
                            markerHistoryList.clear();
                            mLatList = new ArrayList<Double>();
                            mLonList = new ArrayList<Double>();
                            for (int i = 0; i < datas.size(); i++) {
                                beanFor___get_watch_data.CRows row = datas
                                        .get(i);
                                int type = row.type;
                                String address = row.address;
                                long time = row.time;
                                double latitude = row.latitude;
                                double longitude = row.longitude;
                                int electricity = row.electricity;
                                mLatList.add(latitude);
                                mLonList.add(longitude);
                                GeoPoint point = new GeoPoint(latitude, longitude);
                                addTrackHistoryToMap(point, address, time,
                                        electricity, type, i);
                            }
                        }
                        mMyPathOverLay = new MyPathOverLay(HistoryActivity_v2.this, mMapController, mMapView, markerHistoryList);
                        mMapController.getOverlayManager().addOverlay(mMyPathOverLay);

                        Collections.sort(mLatList);
                        Collections.sort(mLonList);
                        double minLat = mLatList.get(0);
                        double maxLat = mLatList.get(mLatList.size() - 1);
                        double minLon = mLonList.get(0);
                        double maxLon = mLonList.get(mLonList.size() - 1);
                        GeoPoint center = new GeoPoint((minLat + maxLat) / 2, (minLon + maxLon) / 2);
                        mMapController.setZoomCurrent(14);
                        mMapController.setPositionAnimationTo(center);
                    } else {
                        mMapController.setPositionAnimationTo(new GeoPoint(0f, 0f), 2f);
                        showLongToast(getString(R.string.history_v2_1));

                        datas = response.info.ddata.rows;
                        setRecycleAdapter(datas);
                        updateStepCalc();
                        // -------add marker to map--------
                        if (mOverlay.getOverlayItems() != null) {
                            markerHistoryList.clear();
                            mOverlay.clearOverlayItems();
                        }

                        if (mMyPathOverLay != null) {
                            CustomLog.d(TAG,"remove history tracker!");
                            mMapController.getOverlayManager().removeOverlay(mMyPathOverLay);
                            mMapController.setPositionAnimationTo(new GeoPoint(0f, 0f), 2f);
                        }
                    }
                } else {
//					showLongToast("load track history failed.");
                }
            }

            @Override
            public void onFailure(int errorNo, String strMsg) {
                super.onFailure(errorNo, strMsg);
                showLongToast(getApplicationContext().getResources().getString(R.string.check_conn));
            }

            @Override
            public void onFinish() {
                dismissDialog(null, 10);
            }
        });
    }

    private void setPointMarker() {
        for (Long l : markerHistoryList.keySet()) {
            markerHistoryList.get(l).setDrawable(getResources().getDrawable(R.drawable.ing_smail));
        }
    }

    @Deprecated
    private void setGesture() {
        mGestureDetector = new GestureDetector(new GestureDetector.OnGestureListener() {

            @Override
            public boolean onSingleTapUp(MotionEvent e) {
                CustomLog.d(TAG,"onsingle");
                CustomLog.d(TAG,"-----------Y=----------" + e.getY());
                float screenX = e.getX();
                float screenY = e.getY();
                ScreenPoint screenPoint = new ScreenPoint(screenX, screenY);
                CustomLog.d(TAG,"显示圆 - Display circle");
                //showCircle(screenX,screenY);
                setPointMarker();

                GeoPoint geoPoint = mMapController.getGeoPoint(screenPoint);
                CustomLog.d(TAG,"lat=" + geoPoint.getLat() + ",lon=" + geoPoint.getLon());

                List<Double> mDistanseList = new ArrayList<Double>();
                double mMinDistanse = 0;
                int i = 0;
                //得到距离点击位置最近的点的距离集合
                if (markerHistoryList == null) {
                    return false;
                }
                for (Long l : markerHistoryList.keySet()) {
                    OverlayItem marker = markerHistoryList.get(l);
                    double lat = marker.getGeoPoint().getLat();
                    double lon = marker.getGeoPoint().getLat();
                    double difX = lat - geoPoint.getLat();
                    double difY = lon - geoPoint.getLon();

                    System.out.println(difX + "");
                    if (Math.abs(difX) < 0.04 && Math.abs(difY) < 0.04) {
                        i++;
                        CustomLog.d(TAG,"在点击位置的范围内");
                        double distanse = Math.sqrt(Math.pow(difX, 2.0) + Math.pow(difY, 2.0));
                        mDistanseList.add(distanse);
                    }
                }

                if (mDistanseList.size() != 0) {
                    Collections.sort(mDistanseList);
                    mMinDistanse = mDistanseList.get(0);
                }

                //点击标注点
                for (Long l : markerHistoryList.keySet()) {
                    OverlayItem marker = markerHistoryList.get(l);
                    double lat = marker.getGeoPoint().getLat();
                    double lon = marker.getGeoPoint().getLat();
                    double difX = lat - geoPoint.getLat();
                    double difY = lon - geoPoint.getLon();

                    if (mMinDistanse == Math.sqrt(Math.pow(difX, 2.0) + Math.pow(difY, 2.0))) {
                        //显示圆
                        //显示头像
                        CustomLog.d(TAG,"点击的是标记点，显示头像");
                        int sex = KidsWatUtils.getBabySex();
                        int resSex = sex == 1 ? R.drawable.icon_boy : R.drawable.icon_girl;
                        Drawable drawable = getResources().getDrawable(resSex);
                        marker.setDrawable(drawable);
                        CustomLog.d(TAG,"更新UI");
                        mMapController.setPositionAnimationTo(marker.getGeoPoint());
                        System.out.println(marker.getGeoPoint().getLat() + "," + marker.getGeoPoint().getLon());
                        CustomLog.d(TAG,"镜头移动");
                    } else {
                        GeoPoint center = mMapController.getMapCenter();
                        CustomLog.d(TAG,"镜头不移动");
                        //(temp % 2 == 0) ? mapController.setPositionAnimationTo(new GeoPoint(center.getLat()+0.00000000001,center.getLon()+0.00000000001):mapController.setPositionAnimationTo(new GeoPoint(center.getLat()-0.00000000001,center.getLon()-0.00000000001);
                        mMapController.setPositionAnimationTo(mMapController.getMapCenter());
                    }
                }

                return false;
            }

            @Override
            public void onShowPress(MotionEvent e) {

            }

            @Override
            public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
                return false;
            }

            @Override
            public void onLongPress(MotionEvent e) {

            }

            @Override
            public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
                return false;
            }

            @Override
            public boolean onDown(MotionEvent e) {
                return false;
            }
        });
    }

    public int distanceAlgorithm(int step) {
        beanForDb___Family family = AppContext.getInstance().getCurrentFamily();
        if (family!=null) {
            int distance = Math.round((family.height / 4 + 0.37f) * step);
            return distance;
        } else {
            return -1;
        }

    }

    public int caloryAlgorithm(float distance) {
        beanForDb___Family family = AppContext.getInstance().getCurrentFamily();
        int calory = Math.round((0.5f * family.weight * distance / 1000));
        return calory;
    }

    /**
     * 内部接口回调方法
     */
    public interface OnItemClickListener {
        void onItemClick(int position, Object object);
    }

    private class StRecycleAdapter extends
            RecyclerView.Adapter<StRecycleAdapter.ViewHolder> {

        private List<beanFor___get_watch_data.CRows> list;
        private OnItemClickListener listener;

        public void setList(List<beanFor___get_watch_data.CRows> _list) {
            list = _list;
        }

        @Override
        public int getItemCount() {
            return list.size();
        }

        @Override
        public void onBindViewHolder(ViewHolder holder, final int position) {
            final beanFor___get_watch_data.CRows bean = list.get(position);

            int m_hour = bean.motion_time / (60 * 60);
            int m_minute = (bean.motion_time - m_hour * 60 * 60) / 60;
            int m_second = bean.motion_time - m_hour * 60 * 60 - m_minute * 60;

            String m_timespan = String.format("%02d:%02d:%02d", m_hour,
                    m_minute, m_second);

            long begtime = bean.getLocalTime() - bean.motion_time;

            SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");// MM/dd
            // HH:mm
            // 前面的lSysTime是秒数，先乘1000得到毫秒数，再转为java.util.Date类型
            java.util.Date dt = new Date(bean.getLocalTime() * 1000);
            String sDateTime = sdf.format(dt); // 得到精确到秒的表示：08/31/2006
            // 21:08:00

            String time_begin_end = sdf.format(new Date(begtime * 1000))
                    + "---" + sDateTime;

            // -------------
            holder.textView_address.setText(bean.address);
            holder.textView_time.setText(time_begin_end);
            holder.textView_timespan.setText(m_timespan);

            /**
             * 调用接口回调
             */
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (null != listener)
                        listener.onItemClick(position, bean);
                }
            });
        }

        /**
         * 设置监听方法
         *
         * @param listener
         */
        public void setOnItemClickListener(OnItemClickListener listener) {
            this.listener = listener;
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int position) {
            View view = LayoutInflater.from(viewGroup.getContext()).inflate(
                    R.layout.view_history_listview_item, viewGroup, false);
            ViewHolder holder = new ViewHolder(view);
            return holder;
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            public ImageView imageView_tx;
            public TextView textView_address;
            public TextView textView_time;
            public TextView textView_timespan;

            public ViewHolder(View itemView) {
                super(itemView);
                imageView_tx = (ImageView) itemView
                        .findViewById(R.id.imageView_tx);
                textView_address = (TextView) itemView
                        .findViewById(R.id.textView_address);
                textView_time = (TextView) itemView
                        .findViewById(R.id.textView_time);
                textView_timespan = (TextView) itemView
                        .findViewById(R.id.textView_timespan);
            }
        }
    }
}
