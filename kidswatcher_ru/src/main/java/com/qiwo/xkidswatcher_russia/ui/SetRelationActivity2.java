package com.qiwo.xkidswatcher_russia.ui;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.qiwo.xkidswatcher_russia.R;
import com.qiwo.xkidswatcher_russia.base.BaseActivity;
import com.qiwo.xkidswatcher_russia.util.Contanst;

import java.util.ArrayList;

import butterknife.BindView;;

/**
 *
 */
public class SetRelationActivity2 extends BaseActivity {

    final String TAG = SetRelationActivity2.class.getSimpleName();
    @BindView(R.id.linearLayout_l)
    LinearLayout linearLayout_l;
    @BindView(R.id.textView_r)
    TextView textView_r;
    @BindView(R.id.imageView_dad)
    ImageView imageView_dad;
    @BindView(R.id.imageView_mom)
    ImageView imageView_mom;
    @BindView(R.id.imageView_grandpa)
    ImageView imageView_grandpa;
    @BindView(R.id.imageView_grandma)
    ImageView imageView_grandma;
    @BindView(R.id.imageView_other)
    ImageView imageView_other;
    @BindView(R.id.textView__dad)
    TextView textView_dad;
    @BindView(R.id.textView__mom)
    TextView textView_mom;
    @BindView(R.id.textView__grandpa)
    TextView textView_grandpa;
    @BindView(R.id.textView__grandma)
    TextView textView_grandma;
    @BindView(R.id.editText_other)
    EditText editText_other;
    @BindView(R.id.textView_zs)
    TextView textView_zs;
    @BindView(R.id.imageView_gg)
    ImageView imageView_gg;
    @BindView(R.id.textView__gg)
    TextView textView_gg;
    @BindView(R.id.imageView_jj)
    ImageView imageView_jj;
    @BindView(R.id.textView__jj)
    TextView textView_jj;
    @BindView(R.id.imageView_ll)
    ImageView imageView_ll;
    @BindView(R.id.textView__ll)
    TextView textView_ll;
    @BindView(R.id.imageView_ly)
    ImageView imageView_ly;
    @BindView(R.id.textView__ly)
    TextView textView_ly;
    // -----------
    String qrcode, action, country, phone;
    ArrayList<String> mList;
    String selectRelation = "";
    private TextWatcher textWatcher = new TextWatcher() {
        @Override
        public void afterTextChanged(Editable s) {
            selectRelation = s.toString();
            System.out.println(selectRelation);
            textView_zs.setText(selectRelation.length() + "/9");
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count,
                                      int after) {
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before,
                                  int count) {
//			selectRelation = s.toString();
//			textView_zs.setText(count + "/9");
        }
    };

    @Override
    protected int getLayoutId() {
        return R.layout.activity_set_relation2;
    }

    @Override
    protected boolean hasActionBar() {
        return false;
    }

    @Override
    protected void init(Bundle savedInstanceState) {
        super.init(savedInstanceState);
        // -----------

        Intent intent = getIntent();

        qrcode = intent.getStringExtra("qrcode");
        action = intent.getStringExtra("action");
        country = intent.getStringExtra("country");
        phone = intent.getStringExtra("phone");
        // ----------
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();

        switch (id) {
            case R.id.linearLayout_l:
                finish();
                break;
            case R.id.textView_r:

                String tag = imageView_other.getTag() == null ? "nochecked"
                        : imageView_other.getTag().toString();

                if (selectRelation.length() == 0 && tag.equalsIgnoreCase("checked")) {
                    selectRelation = "Другой";
                }


                if (mList != null && mList.contains(selectRelation)) {
                    showConfirmInformations(getApplicationContext().getResources().getString(R.string.reminder), getApplicationContext().getResources().getString(R.string.tip_same_relation));
                    return;
                }

                if (selectRelation.length() > 0) {
                    if ("add_device".equalsIgnoreCase(action)) {
                        Intent intent = new Intent(SetRelationActivity2.this,
                                KidsProfileActivity.class);
                        intent.putExtra("relation", selectRelation);
                        intent.putExtra("qrcode", qrcode);
                        intent.putExtra(Contanst.KEY_ACTION, Contanst.ACTION_ADD);
                        intent.putExtra("country", country);
                        intent.putExtra("phone", phone);

                        startActivity(intent);
                        finish();
                    } else {
                        Intent resultIntent = new Intent();
                        Bundle bundle = new Bundle();
                        bundle.putString("relation", selectRelation);
                        resultIntent.putExtras(bundle);
                        setResult(RESULT_OK, resultIntent);
                        finish();
                    }
                } else {
                    showLongToast("please select relation.");
                }
                break;
            case R.id.imageView_dad:
            case R.id.imageView_mom:
            case R.id.imageView_grandpa:
            case R.id.imageView_grandma:
            case R.id.imageView_other:
            case R.id.imageView_gg:
            case R.id.imageView_jj:
            case R.id.imageView_ly:
            case R.id.imageView_ll:
                selectRelation(v);
                break;
            default:
                break;
        }
    }

    @Override
    public void initView() {
        linearLayout_l.setOnClickListener(this);
        textView_r.setOnClickListener(this);
        imageView_dad.setOnClickListener(this);
        imageView_mom.setOnClickListener(this);
        imageView_grandpa.setOnClickListener(this);
        imageView_grandma.setOnClickListener(this);
        imageView_other.setOnClickListener(this);
        imageView_gg.setOnClickListener(this);
        imageView_jj.setOnClickListener(this);
        imageView_ll.setOnClickListener(this);
        imageView_ly.setOnClickListener(this);
        editText_other.addTextChangedListener(textWatcher);
    }

    @Override
    public void initData() {
        //
        Intent intent = getIntent();
        String action = intent.getStringExtra("action");

        if ("add_family".equalsIgnoreCase(action)) {
            mList = intent.getStringArrayListExtra("exist_relationships");
        }
    }

    private void selectRelation(View v) {
        int id = v.getId();
        String tag = v.getTag() == null ? "nochecked" : v.getTag().toString();
        String newTag = tag.equalsIgnoreCase("nochecked") ? "checked"
                : "nochecked";
        int srcid = 0;
        cancelRelationSelect(v);
        v.setTag(newTag);
        if (id == R.id.imageView_dad) {
            srcid = newTag.equalsIgnoreCase("checked") ? R.drawable.icon_dad_pre
                    : R.drawable.icon_dad;
            v.setBackgroundResource(srcid);
            String relation = textView_dad.getText().toString().trim();
            selectRelation = newTag.equalsIgnoreCase("checked") ? relation : "";
        }
        if (id == R.id.imageView_mom) {
            srcid = newTag.equalsIgnoreCase("checked") ? R.drawable.icon__mom_pre
                    : R.drawable.icon_mom;
            v.setBackgroundResource(srcid);
            String relation = textView_mom.getText().toString().trim();
            selectRelation = newTag.equalsIgnoreCase("checked") ? relation : "";
        }
        if (id == R.id.imageView_grandpa) {
            srcid = newTag.equalsIgnoreCase("checked") ? R.drawable.icon__grandpa_pre
                    : R.drawable.icon_grandpa;
            v.setBackgroundResource(srcid);
            String relation = textView_grandpa.getText().toString().trim();
            selectRelation = newTag.equalsIgnoreCase("checked") ? relation : "";
        }
        if (id == R.id.imageView_grandma) {
            srcid = newTag.equalsIgnoreCase("checked") ? R.drawable.icon__grandma_pre
                    : R.drawable.icon_grandma;
            v.setBackgroundResource(srcid);
            String relation = textView_grandma.getText().toString().trim();
            selectRelation = newTag.equalsIgnoreCase("checked") ? relation : "";
        }
        if (id == R.id.imageView_ly) {
            srcid = newTag.equalsIgnoreCase("checked") ? R.drawable.icon_ly_pre
                    : R.drawable.icon_ly;
            v.setBackgroundResource(srcid);
            String relation = textView_ly.getText().toString().trim();
            selectRelation = newTag.equalsIgnoreCase("checked") ? relation : "";
        }
        if (id == R.id.imageView_ll) {
            srcid = newTag.equalsIgnoreCase("checked") ? R.drawable.icon_ll_pre
                    : R.drawable.icon_ll;
            v.setBackgroundResource(srcid);
            String relation = textView_ll.getText().toString().trim();
            selectRelation = newTag.equalsIgnoreCase("checked") ? relation : "";
        }
        if (id == R.id.imageView_gg) {
            srcid = newTag.equalsIgnoreCase("checked") ? R.drawable.icon_gg_pre
                    : R.drawable.icon_gg;
            v.setBackgroundResource(srcid);
            String relation = textView_gg.getText().toString().trim();
            selectRelation = newTag.equalsIgnoreCase("checked") ? relation : "";
        }
        if (id == R.id.imageView_jj) {
            srcid = newTag.equalsIgnoreCase("checked") ? R.drawable.icon_jj_pre
                    : R.drawable.icon_jj;
            v.setBackgroundResource(srcid);
            String relation = textView_jj.getText().toString().trim();
            selectRelation = newTag.equalsIgnoreCase("checked") ? relation : "";
        }
        if (id == R.id.imageView_other) {
            srcid = newTag.equalsIgnoreCase("checked") ? R.drawable.icon__other_pre
                    : R.drawable.icon_other;
            v.setBackgroundResource(srcid);
            boolean editTextIsEnabled = newTag.equalsIgnoreCase("checked") ? true
                    : false;
            int visibility = newTag.equalsIgnoreCase("checked") ? View.VISIBLE
                    : View.INVISIBLE;
            editText_other.setEnabled(editTextIsEnabled);
            textView_zs.setVisibility(visibility);
            String relation = editText_other.getText().toString().trim();
            selectRelation = newTag.equalsIgnoreCase("checked") ? relation : "";
        } else {
            imageView_other.setBackgroundResource(R.drawable.icon_other);
            editText_other.setEnabled(false);
            textView_zs.setVisibility(View.INVISIBLE);
        }

    }

    private void cancelRelationSelect(View v) {
        int id = v.getId();

        if (id != R.id.imageView_dad) {
            imageView_dad.setTag("nochecked");
            imageView_dad.setBackgroundResource(R.drawable.icon_dad);
        }
        if (id != R.id.imageView_mom) {
            imageView_mom.setTag("nochecked");
            imageView_mom.setBackgroundResource(R.drawable.icon_mom);
        }
        if (id != R.id.imageView_grandpa) {
            imageView_grandpa.setTag("nochecked");
            imageView_grandpa.setBackgroundResource(R.drawable.icon_grandpa);
        }
        if (id != R.id.imageView_grandma) {
            imageView_grandma.setTag("nochecked");
            imageView_grandma.setBackgroundResource(R.drawable.icon_grandma);
        }
        if (id != R.id.imageView_ly) {
            imageView_ly.setTag("nochecked");
            imageView_ly.setBackgroundResource(R.drawable.icon_ly);
        }
        if (id != R.id.imageView_ll) {
            imageView_ll.setTag("nochecked");
            imageView_ll.setBackgroundResource(R.drawable.icon_ll);
        }
        if (id != R.id.imageView_gg) {
            imageView_gg.setTag("nochecked");
            imageView_gg.setBackgroundResource(R.drawable.icon_gg);
        }
        if (id != R.id.imageView_jj) {
            imageView_jj.setTag("nochecked");
            imageView_jj.setBackgroundResource(R.drawable.icon_jj);
        }
        if (id != R.id.imageView_other) {
            imageView_other.setTag("nochecked");
            imageView_other.setBackgroundResource(R.drawable.icon_other);
        }

    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            finish();
        }
        return false;
    }

}
