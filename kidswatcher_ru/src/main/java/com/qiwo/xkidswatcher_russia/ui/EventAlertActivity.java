package com.qiwo.xkidswatcher_russia.ui;

import android.app.Activity;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.qiwo.xkidswatcher_russia.AnalyticsUtils.AnalyticsUtils;
import com.qiwo.xkidswatcher_russia.KidsWatConfig;
import com.qiwo.xkidswatcher_russia.R;
import com.qiwo.xkidswatcher_russia.Utils;

import net.intari.CustomLogger.CustomLog;

import java.io.IOException;

import androidx.appcompat.app.AppCompatActivity;
import butterknife.ButterKnife;
import butterknife.BindView;;

public class EventAlertActivity extends AppCompatActivity implements
        View.OnClickListener {

    public static final String TAG = EventAlertActivity.class.getSimpleName();

    // @BindView(R.id.linearLayout_l)
    // LinearLayout linearLayout_l;
    //
    // @BindView(R.id.linearLayout_r)
    // LinearLayout linearLayout_r;

    final MediaPlayer player = new MediaPlayer();
    @BindView(R.id.btn_ok)
    Button btn_ok;
    @BindView(R.id.textView_des)
    TextView textView_des;

    // ----------------XXXXXXXXXXXXXXX--------------------
    @BindView(R.id.pop_layout)
    LinearLayout layout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //AnalyticsUtils.processActivityOnCreate(TAG,this);

        // requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_enent_alert);
        ButterKnife.bind(this);
        initView();
        initData();
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.linearLayout_l:
                finish();
                break;
            case R.id.pop_layout:
                break;
            case R.id.btn_ok:
                if (player != null) {
                    player.stop();
                }
                finish();
                break;
            // ----------
            default:
                break;
        }
    }

    public void initView() {
        // linearLayout_l.setOnClickListener(this);
        // linearLayout_r.setOnClickListener(this);
        layout.setOnClickListener(this);
        btn_ok.setOnClickListener(this);
        // ---------
    }

    // ----------------XXXXXXXXXXXXXXX--------------------

    public void initData() {
        String message = getIntent().getStringExtra("message");
//		String message = getApplicationContext().getResources().getString(R.string.content_sos_text);
        if (getIntent().getIntExtra("bledisconnect", 0) == 1) {
            message = getIntent().getStringExtra("message");
        }
        textView_des.setText(message);
        ringAlert();
    }

    // alarm

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        return true;
    }

    private void ringAlert() {
        try {
            String filename = KidsWatConfig.getTempFilePath() + "alarm.wav";
            player.setLooping(true);
            player.setDataSource(filename);
            player.prepare();
            player.start();

        } catch (IllegalArgumentException e) {
            CustomLog.logException(TAG,e);
        } catch (SecurityException e) {
            CustomLog.logException(TAG,e);
        } catch (IllegalStateException e) {
            CustomLog.logException(TAG,e);
        } catch (IOException e) {
            CustomLog.logException(TAG,e);
        }
    }

    // -------------------

}
