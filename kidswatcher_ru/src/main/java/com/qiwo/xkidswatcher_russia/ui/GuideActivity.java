package com.qiwo.xkidswatcher_russia.ui;

import android.os.Bundle;
import android.os.Parcelable;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;
import androidx.viewpager.widget.ViewPager.OnPageChangeListener;
import androidx.appcompat.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.ImageView;

import com.qiwo.xkidswatcher_russia.AppManager;
import com.qiwo.xkidswatcher_russia.KidsWatConfig;
import com.qiwo.xkidswatcher_russia.R;

import java.util.ArrayList;

public class GuideActivity extends AppCompatActivity {
    private ViewPager viewPager;

    private ArrayList<View> pageViews;
    private ImageView imageView;

    private ImageView[] imageViews;

    private ViewGroup viewPics;

    private ViewGroup viewPoints;
    private Button.OnClickListener Button_OnClickListener = new Button.OnClickListener() {
        public void onClick(View v) {
            KidsWatConfig.setFristStart(false);
            AppManager.AppRestart(GuideActivity.this);
            GuideActivity.this.finish();
        }
    };

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        LayoutInflater inflater = getLayoutInflater();
        //getActionBar().hide();

        pageViews = new ArrayList<>();
        pageViews.add(inflater.inflate(R.layout.view_guide_page_a, null));
        pageViews.add(inflater.inflate(R.layout.view_guide_page_b, null));
        pageViews.add(inflater.inflate(R.layout.view_guide_page_c, null));

        imageViews = new ImageView[pageViews.size()];
        viewPics = (ViewGroup) inflater.inflate(R.layout.activity_guide, null);

        viewPoints = viewPics.findViewById(R.id.viewGroup);
        viewPager = viewPics.findViewById(R.id.guidePages);

        for (int i = 0; i < pageViews.size(); i++) {
            imageView = new ImageView(getApplicationContext());
            imageView.setLayoutParams(new LayoutParams(
                    LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));
            imageView.setPadding(10, 0, 10, 0);
            imageViews[i] = imageView;

            if (i == 0) {
                imageViews[i].setImageResource(R.drawable.point_select2);
            } else {
                imageViews[i].setImageResource(R.drawable.point_normal);
            }

            viewPoints.addView(imageViews[i]);
        }

        setContentView(viewPics);

        viewPager.setAdapter(new GuidePageAdapter());
        viewPager.setOnPageChangeListener(new GuidePageChangeListener());
    }

    class GuidePageAdapter extends PagerAdapter {

        @Override
        public void destroyItem(View v, int position, Object arg2) {
            // TODO Auto-generated method stub
            ((ViewPager) v).removeView(pageViews.get(position));
        }

        @Override
        public void finishUpdate(View arg0) {
            // TODO Auto-generated method stub

        }

        @Override
        public int getCount() {
            // TODO Auto-generated method stub
            return pageViews.size();
        }

        @Override
        public Object instantiateItem(View v, int position) {
            // TODO Auto-generated method stub
            ((ViewPager) v).addView(pageViews.get(position));

            if (position == pageViews.size() - 1) {
                Button btn = (Button) v.findViewById(R.id.button_get_started);
                btn.setOnClickListener(Button_OnClickListener);
            }

            return pageViews.get(position);
        }

        @Override
        public boolean isViewFromObject(View v, Object arg1) {
            // TODO Auto-generated method stub
            return v == arg1;
        }

        @Override
        public void startUpdate(View arg0) {
            // TODO Auto-generated method stub

        }

        @Override
        public int getItemPosition(Object object) {
            // TODO Auto-generated method stub
            return super.getItemPosition(object);
        }

        @Override
        public void restoreState(Parcelable arg0, ClassLoader arg1) {
            // TODO Auto-generated method stub

        }

        @Override
        public Parcelable saveState() {
            // TODO Auto-generated method stub
            return null;
        }
    }

    class GuidePageChangeListener implements OnPageChangeListener {

        @Override
        public void onPageScrollStateChanged(int arg0) {
            // TODO Auto-generated method stub

        }

        @Override
        public void onPageScrolled(int arg0, float arg1, int arg2) {
            // TODO Auto-generated method stub

        }

        @Override
        public void onPageSelected(int position) {
            // TODO Auto-generated method stub
            for (int i = 0; i < imageViews.length; i++) {
                imageViews[position].setImageResource(R.drawable.point_select2);
                if (position != i) {
                    imageViews[i].setImageResource(R.drawable.point_normal);
                }
            }

        }
    }
}