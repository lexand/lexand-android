package com.qiwo.xkidswatcher_russia.ui;

import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.provider.ContactsContract;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.qiwo.xkidswatcher_russia.R;
import com.qiwo.xkidswatcher_russia.base.BaseActivity;
import com.qiwo.xkidswatcher_russia.util.Contanst;
import net.intari.CustomLogger.CustomLog;

import org.kymjs.kjframe.utils.StringUtils;

import butterknife.BindView;

public class SetInvitePhoneNumActivity extends BaseActivity {

    private final static String TAG = SetInvitePhoneNumActivity.class.getSimpleName();

    @BindView(R.id.linearLayout_l)
    LinearLayout linearLayout_l;

    @BindView(R.id.ll_right)
    LinearLayout ll_right;

    @BindView(R.id.ll_contact)
    LinearLayout ll_contact;

    @BindView(R.id.tv_ok)
    TextView tv_ok;

    @BindView(R.id.btn_add_contact)
    TextView btn_add_contact;

    @BindView(R.id.Country_btn)
    Button Country_btn;

    @BindView(R.id.Country_number)
    EditText Country_number;
    String invite_country;
    private TextWatcher textWatcher = new TextWatcher() {
        @Override
        public void afterTextChanged(Editable s) {
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            CustomLog.d(TAG,"s=" + s);
            String phone = Country_number.getText().toString().trim();
            if (StringUtils.isEmpty(phone)) {
                tv_ok.setEnabled(false);
            } else {
                tv_ok.setEnabled(true);
            }
        }
    };

    @Override
    public void onClick(View v) {
        // TODO Auto-generated method stub
        switch (v.getId()) {
            case R.id.ll_right:
                if (tv_ok.isEnabled()) {
                    if (Country_number.getText().toString().trim().matches("[0]+")) {
                        showConfirmInformations(getApplicationContext().getResources().getString(R.string.reminder), getApplicationContext().getResources().getString(R.string.invalid_number));
                    } else {
                        Intent intent = new Intent();
                        intent.putExtra("phone", Country_number.getText().toString().trim());
                        intent.putExtra("country_code", Country_btn.getText().toString().trim());
                        setResult(300, intent);
                        finish();
                    }
                }
                break;

            case R.id.Country_btn:
                chooseCountryCode();
                break;

            case R.id.linearLayout_l:
                finish();
                break;

            case R.id.ll_contact:
                Intent intent = new Intent(Intent.ACTION_PICK,
                        ContactsContract.Contacts.CONTENT_URI);

                startActivityForResult(intent, 400);
                break;
            default:
                break;
        }
    }

    //选择国家码
    public void chooseCountryCode() {
        Intent intent = new Intent(SetInvitePhoneNumActivity.this, CountryActivity.class);
        intent.putExtra("action", "add_family");
        startActivityForResult(intent, Contanst.REQUEST_CODE_COUNTRY_CODE);
    }

    @Override
    public void initView() {
        // TODO Auto-generated method stub
        linearLayout_l.setOnClickListener(this);
        ll_right.setOnClickListener(this);
        Country_btn.setOnClickListener(this);
        Country_number.addTextChangedListener(textWatcher);

        ll_contact.setVisibility(0);
        ll_contact.setOnClickListener(this);

    }

    @Override
    public void initData() {
        // TODO Auto-generated method stub
//		invite_country = KidsWatConfig.getUseCountryCode();
//		Country_btn.setText("+" + invite_country);
        Country_btn.setText("+7");
    }

    @Override
    protected int getLayoutId() {
        // TODO Auto-generated method stub
        return R.layout.activity_set_invite_phone;
    }

    @Override
    protected boolean hasActionBar() {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    protected void onActivityResult(int arg0, int arg1, Intent arg2) {
        // TODO Auto-generated method stub
        super.onActivityResult(arg0, arg1, arg2);
        if (arg2 == null) {
            return;
        }
        switch (arg0) {
            case Contanst.REQUEST_CODE_COUNTRY_CODE:

                invite_country = arg2.getAction();
                Country_btn.setText(invite_country);
                break;
            case 400:
                Uri contactData = arg2.getData();
                Cursor cursor = managedQuery(contactData, null, null, null,
                        null);
                cursor.moveToFirst();
                String phone = "";
                String phone_ = getContactPhone(cursor).replace(" ", "");
                /**去除号码中含有字符为+7
                 *  7
                 *  8
                 *  的字符
                 *  **/
                if (phone_ != null && phone_.length() > 2) {
                    if (phone_.indexOf("+7") != -1) {
                        phone = phone_.substring(2);
                    } else if (phone_.substring(0, 1).equals("7") || phone_.substring(0, 1).equals("8")) {
                        phone = phone_.substring(1);
                    } else {
                        phone = phone_;
                    }
                }
                Country_number.setText(phone);
                break;
            default:
                break;
        }
    }

    private String getContactPhone(Cursor cursor) {
        // TODO Auto-generated method stub
        int phoneColumn = cursor
                .getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER);
        int phoneNum = cursor.getInt(phoneColumn);
        String result = "";
        if (phoneNum > 0) {
            // 获得联系人的ID号
            int idColumn = cursor.getColumnIndex(ContactsContract.Contacts._ID);
            String contactId = cursor.getString(idColumn);
            // 获得联系人电话的cursor
            Cursor phone = getContentResolver().query(
                    ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                    null,
                    ContactsContract.CommonDataKinds.Phone.CONTACT_ID + "="
                            + contactId, null, null);
            if (phone.moveToFirst()) {
                for (; !phone.isAfterLast(); phone.moveToNext()) {
                    int index = phone
                            .getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER);
                    int typeindex = phone
                            .getColumnIndex(ContactsContract.CommonDataKinds.Phone.TYPE);
                    int phone_type = phone.getInt(typeindex);
                    String phoneNumber = phone.getString(index);
                    result = phoneNumber;
                    // switch (phone_type) {//此处请看下方注释
                    // case 2:
                    // result = phoneNumber;
                    // break;
                    //
                    // default:
                    // break;
                    // }
                }
                if (!phone.isClosed()) {
                    phone.close();
                }
            }
        }
        return result;
    }
}
