package com.qiwo.xkidswatcher_russia.ui;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.location.Geocoder;
import android.os.Bundle;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.qiwo.xkidswatcher_russia.AppContext;
import com.qiwo.xkidswatcher_russia.KidsWatConfig;
import com.qiwo.xkidswatcher_russia.R;
import com.qiwo.xkidswatcher_russia.api.ApiHttpClient;
import com.qiwo.xkidswatcher_russia.api.remote.KidsWatApiUrl;
import com.qiwo.xkidswatcher_russia.base.BaseActivity;
import com.qiwo.xkidswatcher_russia.bean.beanForDb___Family;
import com.qiwo.xkidswatcher_russia.bean.beanFor___add_safe_area_config;
import com.qiwo.xkidswatcher_russia.bean.beanFor___get_safe_area_config;
import com.qiwo.xkidswatcher_russia.util.ACache;
import com.qiwo.xkidswatcher_russia.util.Contanst;
import com.qiwo.xkidswatcher_russia.util.KidsWatUtils;
import com.yandex.mapkit.mapview.MapView;

import net.intari.CustomLogger.CustomLog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.kymjs.kjframe.http.HttpCallBack;

import java.util.Locale;

import butterknife.BindView;;
import ru.yandex.yandexmapkit.MapController;
import ru.yandex.yandexmapkit.MapView;
import ru.yandex.yandexmapkit.OverlayManager;
import ru.yandex.yandexmapkit.overlay.Overlay;
import ru.yandex.yandexmapkit.overlay.OverlayItem;
import ru.yandex.yandexmapkit.overlay.balloon.BalloonItem;
import ru.yandex.yandexmapkit.overlay.location.MyLocationItem;
import ru.yandex.yandexmapkit.overlay.location.OnMyLocationListener;
import ru.yandex.yandexmapkit.utils.GeoPoint;
import ru.yandex.yandexmapkit.utils.ScreenPoint;

/**
 * @author Administrator
 */
public class SetSafeZoneActivity_v2 extends BaseActivity implements OnMyLocationListener {

    public static final String TAG = SetSafeZoneActivity_v2.class.getSimpleName();

    @BindView(R.id.linearLayout_l)
    LinearLayout linearLayout_l;

    @BindView(R.id.linearLayout_r)
    LinearLayout linearLayout_r;

    @BindView(R.id.imageview_r)
    ImageView imageview_r;

    @BindView(R.id.linearLayout_safezone_name)
    LinearLayout linearLayout_safezone_name;

    @BindView(R.id.textView_title)
    TextView textView_title;

    @BindView(R.id.textView_address)
    TextView textView_address;

    @BindView(R.id.textView_safezone_name)
    TextView textView_safezone_name;


    @BindView(R.id.editText_add)
    TextView editText_add;

    @BindView(R.id.textView_radius)
    TextView textView_radius;

    @BindView(R.id.button_save)
    Button button_save;

    @BindView(R.id.seekBar_radius)
    SeekBar seekBar_radius;

    @BindView(R.id.safezone_mapview)
    MapView mMapView;
    int index;
    String safezone_jsonText;
    private String action;
    private int action_type = SAFE_ZONE_ACTION.NONE;
    private double mm_latitude = 0f;
    private double mm_longitude = 0f;
    private String mm_safename = "";
    View.OnClickListener view_ll_listener = new View.OnClickListener() {
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.linearLayout_safezone_name:

                    if (action_type == SAFE_ZONE_ACTION.ADD
                            || action_type == SAFE_ZONE_ACTION.EDIT) {
                        String safezone_name = textView_safezone_name.getText()
                                .toString().trim();
                        safezone_name = safezone_name
                                .equalsIgnoreCase(SetSafeZoneActivity_v2.this
                                        .getResources().getString(
                                                R.string.safe_zone_name_tip)) ? ""
                                : safezone_name;
                        final EditText input_text = new EditText(
                                SetSafeZoneActivity_v2.this);
                        input_text.setText(safezone_name);
                        input_text.setFocusable(true);

                        AlertDialog.Builder builder = new AlertDialog.Builder(
                                SetSafeZoneActivity_v2.this);
                        builder.setTitle(getString(R.string.app_name))
                                .setIcon(R.drawable.ic_launcher)
                                .setView(input_text)
                                .setNegativeButton("Cancel", null);
                        builder.setPositiveButton("OK",
                                new DialogInterface.OnClickListener() {

                                    public void onClick(DialogInterface dialog,
                                                        int which) {
                                        String input_str = input_text.getText()
                                                .toString().trim();
                                        if (input_str.length() > 0) {
                                            mm_safename = input_str;
                                            textView_safezone_name
                                                    .setText(input_str);
                                        }

                                    }
                                });
                        builder.show();
                    }
                    break;
                case R.id.linearLayout_search:
                    break;
                // ----------
            }
            // menu.showContent();
        }

    };
    private String mm_address = "";
    private beanFor___get_safe_area_config bean_safezonelist;
    private beanFor___get_safe_area_config.CConfig zoneitem;
    private MapController mMapController;
    private Overlay mOverlay;
    private OverlayManager mOverlayManager;
    private GestureDetector mGestureDetector;

    @Override
    protected int getLayoutId() {
        return R.layout.layout_set_safezone_3;
    }

    @Override
    protected boolean hasActionBar() {
        return false;
    }

    @Override
    protected void init(Bundle savedInstanceState) {
        super.init(savedInstanceState);
        // -----------
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.linearLayout_l:
                finish();
                break;
            case R.id.button_save:
                if ("edit".equalsIgnoreCase(action)
                        && action_type != SAFE_ZONE_ACTION.EDIT) {
                    return;
                }
                mm_safename = textView_safezone_name.getText().toString();
                if (mm_safename.length() == 0) {
                    Toast.makeText(SetSafeZoneActivity_v2.this,
                            getApplicationContext().getResources().getString(R.string.safe_zone_name_tip), Toast.LENGTH_SHORT)
                            .show();
                    return;
                }
                String createuser = AppContext.getInstance().getLoginUser_kid().relation;
                if (action_type == SAFE_ZONE_ACTION.ADD) {
                    CustomLog.d(TAG,"action_type:" + "add");
                    if (prepareForSaveSafeZone(1, mm_latitude, mm_longitude,
                            mm_safename)) {
                        add_safe_area_config(mm_latitude, mm_longitude,
                                seekBar_radius.getProgress(), mm_safename,
                                mm_address.length() > 0 ? mm_address
                                        : textView_address.getText().toString(),
                                createuser);
                    }
                } else if (action_type == SAFE_ZONE_ACTION.EDIT) {
                    CustomLog.d(TAG,"action_type:" + "edit");
                    if (prepareForSaveSafeZone(2, mm_latitude, mm_longitude,
                            mm_safename)) {
                        update_safe_area_config(zoneitem.latitude,
                                zoneitem.longitude,
                                mm_latitude == 0 ? zoneitem.latitude : mm_latitude,
                                mm_longitude == 0 ? zoneitem.longitude
                                        : mm_longitude,
                                seekBar_radius.getProgress(), mm_safename,
                                mm_address.length() > 0 ? mm_address
                                        : zoneitem.address, createuser);
                    }
                }
                // close
                break;
            case R.id.editText_add:
                break;
            case R.id.linearLayout_r:
                Intent dIntent = new Intent(SetSafeZoneActivity_v2.this,
                        SearchMapActivity_v2.class);
                SetSafeZoneActivity_v2.this.startActivityForResult(dIntent, 200);
                break;
            case R.id.linearLayout_safezone_name:
                break;
            case R.id.linearLayout_search:
                break;
            // ----------
            default:
                break;
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
    }

    @Override
    public void initView() {
        linearLayout_l.setOnClickListener(this);
        linearLayout_r.setOnClickListener(this);
        editText_add.setOnClickListener(this);
        button_save.setOnClickListener(this);

        findViewById(R.id.imageView_ss3).setVisibility(View.GONE);
        seekBar_radius.setVisibility(View.VISIBLE);
        // ---------
        seekBar_radius
                .setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                    /**
                     * 拖动条停止拖动的时候调用
                     */
                    @Override
                    public void onStopTrackingTouch(SeekBar seekBar) {
                        CustomLog.d(TAG,"拖动停止 - Drag To Stop");
                        int seekProgress = seekBar.getProgress();
                        int newValue = KidsWatUtils.getSeekBarProgress(
                                seekProgress, 50);
                        if (newValue == 0)
                            newValue = 50;
                        seekBar.setProgress(newValue);
                    }

                    /**
                     * 拖动条开始拖动的时候调用
                     */
                    @Override
                    public void onStartTrackingTouch(SeekBar seekBar) {
                        CustomLog.d(TAG,"开始拖动 - Start dragging");
                    }

                    /**
                     * 拖动条进度改变的时候调用
                     */
                    @Override
                    public void onProgressChanged(SeekBar seekBar,
                                                  int progress, boolean fromUser) {
                        textView_radius.setText(progress + " м");
                    }
                });
    }

    @Override
    public void initData() {

        mMapController = mMapView.getMapController();
        mOverlayManager = mMapController.getOverlayManager();
        mOverlay = new Overlay(mMapController);
        mOverlayManager.getMyLocation().setEnabled(false);
        mOverlayManager.getMyLocation().clearOverlayItems();
        setGesture();
        action = getIntent().getStringExtra(Contanst.KEY_ACTION);
        if ("add".equalsIgnoreCase(action)) {
            action_type = SAFE_ZONE_ACTION.ADD;
            mOverlayManager.getMyLocation().setEnabled(true);
            mOverlayManager.getMyLocation().addMyLocationListener(this);
//			getPointAddress(mMapView.getMapController().getMapCenter());
        } else if ("edit".equalsIgnoreCase(action)) {
            index = getIntent().getIntExtra("index", 0);

            zoneitem = (beanFor___get_safe_area_config.CConfig) getIntent()
                    .getSerializableExtra("zone");

            mm_safename = zoneitem.name;
            seekBar_radius.setProgress(zoneitem.radius);
            action_type = SAFE_ZONE_ACTION.EDIT;
            textView_safezone_name.setText(mm_safename);
            GeoPoint point = new GeoPoint(zoneitem.latitude, zoneitem.longitude);
            showLocation(point);
            textView_address.setText(zoneitem.address);
            mMapController.setPositionAnimationTo(point, 12f);
        }

        ACache mCache = ACache.get(SetSafeZoneActivity_v2.this);
        safezone_jsonText = mCache.getAsString("get_safe_area_config");
        bean_safezonelist = AppContext
                .getInstance()
                .getGson()
                .fromJson(safezone_jsonText,
                        beanFor___get_safe_area_config.class);
        if (safezone_jsonText != null) {
            //
        }
    }

    private void showLocation(GeoPoint point) {
        double lat = point.getLat();
        double lon = point.getLon();
        CustomLog.d(TAG,"show location :" + lat + "," + lon);
        int sex = KidsWatUtils.getBabySex();
        int resSex = sex == 1 ? R.drawable.icon_boy : R.drawable.icon_girl;
        beanForDb___Family family=AppContext.getInstance().getCurrentFamily();
        if (family==null) {
            CustomLog.e(TAG,"Invalid family - it's null!");
            //let's continue - so we WILL crash if someting happens
            //return;
        }
        String familyId=family.family_id!=null?family.family_id:"";
        int familySex=family.sex;
        CustomLog.d(TAG,"Show location:nickName:"+family.nickname+", familyId:"+familyId+",familySex:"+familySex);
        Drawable drawable = new BitmapDrawable(getApplication().getResources(), KidsWatUtils.getBabyImg_v3(SetSafeZoneActivity_v2.this,
                familyId,
                familySex,
                family));
        OverlayItem overlayItem = new OverlayItem(point, drawable);
        BalloonItem balloon = new BalloonItem(this, point);
        balloon.setText(getApplicationContext().getResources().getString(R.string.safe_zones));
        overlayItem.setBalloonItem(balloon);
        mOverlay.addOverlayItem(new OverlayItem(point, drawable));
        mOverlayManager.addOverlay(mOverlay);
        mMapController.setPositionAnimationTo(point, 12f);
    }

    // ----------------XXXXXXXXXXXXXXX--------------------
    private void addCircleToMap(GeoPoint center, double radius) {
        int sex = KidsWatUtils.getBabySex();
        int resSex = sex == 1 ? R.drawable.icon_boy : R.drawable.icon_girl;
        Drawable drawable = getResources().getDrawable(resSex);
        OverlayItem overlayItem = new OverlayItem(center, drawable);
        BalloonItem balloonItem = new BalloonItem(this, center);
        balloonItem.setText(getApplicationContext().getResources().getString(R.string.safe_zones));
        overlayItem.setBalloonItem(balloonItem);
        mOverlay.addOverlayItem(overlayItem);
        mOverlayManager.addOverlay(mOverlay);
//		mMapController.setPositionAnimationTo(center, 12f);
    }

    /**
     * @param type      1为添加用户,2为修改用户.
     * @param latitude
     * @param longitude
     * @param safename
     * @return
     */
    private boolean prepareForSaveSafeZone(int type, double latitude,
                                           double longitude, String safename) {

        if (bean_safezonelist != null
                && bean_safezonelist.info.ddata.config.size() > 0) {
            int i = 0;
            for (beanFor___get_safe_area_config.CConfig b : bean_safezonelist.info.ddata.config) {
                if (type == 1) {
                    if (safename.equalsIgnoreCase(b.name)) {
                        Toast.makeText(
                                SetSafeZoneActivity_v2.this,
                                getString(R.string.set_safe_zone_1),
                                Toast.LENGTH_LONG).show();
                        return false;
                    }
                    if (KidsWatUtils.GetDistance(b.latitude, b.longitude,
                            latitude, longitude) < 100) {
                        Toast.makeText(
                                SetSafeZoneActivity_v2.this,
                                getString(R.string.set_safe_zone_2),
                                Toast.LENGTH_LONG).show();
                        return false;
                    }
                } else {
                    if (i != index) {
                        if (safename.equalsIgnoreCase(b.name)) {
                            Toast.makeText(
                                    SetSafeZoneActivity_v2.this,
                                    getString(R.string.set_safe_zone_1),
                                    Toast.LENGTH_LONG).show();
                            return false;
                        }

                        if (KidsWatUtils.GetDistance(b.latitude, b.longitude,
                                latitude, longitude) < 100) {
                            Toast.makeText(
                                    SetSafeZoneActivity_v2.this,
                                    getString(R.string.set_safe_zone_2),
                                    Toast.LENGTH_LONG).show();
                            return false;
                        }

                    }

                }
                i++;
            }
        }

        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 200 && resultCode == RESULT_OK) {
            Bundle bundle = data.getExtras();
            mm_longitude = bundle.getDouble("longitude");
            mm_latitude = bundle.getDouble("latitude");
            CustomLog.d(TAG,"mm_longitude=" + mm_longitude + ",mm_latitude" + mm_latitude);
            GeoPoint lng = new GeoPoint(mm_latitude, mm_longitude);
            //mMap.clear();
            //清空地图
            mOverlay.clearOverlayItems();
            addCircleToMap(lng, seekBar_radius.getProgress());
            getPointAddress(lng);
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void getPointAddress(final GeoPoint point) {
        // ---------------
        CustomLog.d(TAG,"call---------->getPointAddress()");

        Geocoder geoCoder = new Geocoder(getBaseContext(), Locale.getDefault());
        try {
//			List<Address> addresses = geoCoder.getFromLocation(point.getLat(),
//					point.getLon(), 1);
            String url = "https://geocode-maps.yandex.ru/1.x/?format=json&geocode=" + point.getLon() + "," + point.getLat();
            ApiHttpClient.get(url, new HttpCallBack() {
                @Override
                public void onSuccess(String t) {
                    // TODO Auto-generated method stub
                    super.onSuccess(t);
                    try {
                        CustomLog.d(TAG,"get geopoint address!");
                        JSONArray array = new JSONObject(t).getJSONObject("response").getJSONObject("GeoObjectCollection").getJSONArray("featureMember");
                        if (array.length() == 0) {
                            CustomLog.d(TAG,"no location result!");
                            mm_address = "";
                            if (textView_address != null)
                                textView_address.setText(mm_address);
                        } else {
                            CustomLog.d(TAG,"has location data!");
                            String address = ((JSONObject) array.get(0)).getJSONObject("GeoObject").getJSONObject("metaDataProperty").getJSONObject("GeocoderMetaData").getString("text");
                            mm_address = address;
                            if (textView_address != null)
                                textView_address.setText(mm_address);
                        }
                        mMapController.setPositionAnimationTo(point, 12f);
                    } catch (JSONException e) {
                        // TODO Auto-generated catch block
                        CustomLog.logException(TAG,e);

                    }

                }
            });

			/*String add = "";
			StringBuilder sbAdd;
			if(addresses.isEmpty()){
				CustomLog.d(TAG,"this is not a detail address");
			}
			CustomLog.d(TAG,"addresses.size = "+addresses.size());
			if (addresses.size() > 0) {
				Address address = addresses.get(0);
				CustomLog.d(TAG,"addresses="+addresses);
				CustomLog.d(TAG,"address = "+address);
				for (int i = 0; i < 10; i++) {
					if (i > 4){
						break;
					}
					CustomLog.d(TAG,"-----------"+i+","+address.getAddressLine(i));
					add += address.getAddressLine(i);
				}
				mm_address = add.replace("null", "");
				CustomLog.d(TAG,"mm_address = "+mm_address);
			}
			CustomLog.d(TAG,add);
			final String finalAdd = add.replace("null", "");*/
//			runOnUiThread(new Runnable() {
//				public void run() {
//
//				}
//			});
        } catch (Exception e) {
            CustomLog.logException(TAG,e);
        }
    }

    private void add_safe_area_config(double latitude, double longitude,
                                      int radius, String name, String address, String create_user) {
        // -----------------
        final String uid = KidsWatConfig.getUserUid();
        final String access_token = KidsWatConfig.getUserToken();
        String family_id = KidsWatConfig.getDefaultFamilyId();
        CustomLog.d(TAG,"address=" + address);
        if (address == "" || "правка".equals(address)) {
            Toast.makeText(SetSafeZoneActivity_v2.this, getString(R.string.set_safe_zone_3), Toast.LENGTH_SHORT).show();
            return;
        }

        final String request_url = KidsWatApiUrl
                .getUrlFor___add_safe_area_config(uid, access_token, family_id,
                        latitude, longitude, radius, name, address, create_user);
        // -------------------------
        ApiHttpClient.get(request_url, new HttpCallBack() {

            @Override
            public void onPreStart() {
                showWaitDialog(getString(R.string.new_setting_fragment_6));
            }

            @Override
            public void onSuccess(String t) {
                super.onSuccess(t);
                CustomLog.d(TAG,String.format("url:%s\nt:%s", request_url, t));

                beanFor___add_safe_area_config response = AppContext
                        .getInstance().getGson()
                        .fromJson(t, beanFor___add_safe_area_config.class);

                // {"error":0,"info":{"message":"success option"}}
                if (response.error == 0) {
                    SetSafeZoneActivity_v2.this.setResult(1110);// RESULT_OK

                    finish();
                } else {
                    Toast.makeText(SetSafeZoneActivity_v2.this,
                            response.info.message, Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(int errorNo, String strMsg) {
                super.onFailure(errorNo, strMsg);
                String msg = String
                        .format("%s(error=%s)",
                                errorNo == -1 ? "Connect to the server failed"
                                        : strMsg, errorNo);
                CustomLog.d(TAG,msg);
                showLongToast(msg);
            }

            @Override
            public void onFinish() {
                dismissDialog(null, 10);
            }
        });
    }

    private void update_safe_area_config(double old_latitude,
                                         double old_longitude, double latitude, double longitude,
                                         int radius, String name, String address, String create_user) {
        // -----------------
        final String uid = KidsWatConfig.getUserUid();
        final String access_token = KidsWatConfig.getUserToken();
        String family_id = KidsWatConfig.getDefaultFamilyId();

        if (address == null || "правка".equals(address)) {
            return;
        }

        final String request_url = KidsWatApiUrl
                .getUrlFor___update_safe_area_config(uid, access_token,
                        family_id, latitude, longitude, old_latitude,
                        old_longitude, radius, name, address, create_user);
        // -------------------------

        ApiHttpClient.get(request_url, new HttpCallBack() {

            @Override
            public void onPreStart() {
                showWaitDialog(getString(R.string.new_setting_fragment_6));
            }

            @Override
            public void onSuccess(String t) {
                super.onSuccess(t);
                CustomLog.d(TAG,String.format("url:%s\nt:%s", request_url, t));
                beanFor___add_safe_area_config response = AppContext
                        .getInstance().getGson()
                        .fromJson(t, beanFor___add_safe_area_config.class);

                if (response.error == 0) {
                    SetSafeZoneActivity_v2.this.setResult(1111);// RESULT_OK

                    finish();

                } else {
                    Toast.makeText(SetSafeZoneActivity_v2.this,
                            response.info.message, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(int errorNo, String strMsg) {
                super.onFailure(errorNo, strMsg);
                String msg = String
                        .format("%s(error=%s)",
                                errorNo == -1 ? "Connect to the server failed"
                                        : strMsg, errorNo);
                CustomLog.d(TAG,msg);
                showLongToast(msg);
            }

            @Override
            public void onFinish() {
                dismissDialog(null, 10);
            }
        });
    }

    @Deprecated
    private void setGesture() {
        mGestureDetector = new GestureDetector(new GestureDetector.OnGestureListener() {

            @Override
            public boolean onSingleTapUp(MotionEvent e) {

                float top = 300;
                float buttom = 1300f;
                if (e.getY() > 300 && e.getY() < buttom) {
                    CustomLog.d(TAG,"onsingle");
                    if (action_type == SAFE_ZONE_ACTION.ADD
                            || action_type == SAFE_ZONE_ACTION.EDIT) {
                        CustomLog.d(TAG,"-------Y=------" + e.getY());
                        float screenX = e.getX();
                        float screenY = e.getY() - top;
//					float screenY = e.getY();
                        ScreenPoint scnPoint = new ScreenPoint(screenX, screenY);
                        GeoPoint point = mMapController.getGeoPoint(scnPoint);
                        CustomLog.d(TAG,"lat:" + point.getLat() + ",lon:" + point.getLon());
                        mOverlay.clearOverlayItems();
                        showLocation(point);
                        getPointAddress(point);

                        mm_latitude = point.getLat();
                        mm_longitude = point.getLon();
                    }
                }
                return false;
            }

            @Override
            public void onShowPress(MotionEvent e) {

            }

            @Override
            public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
                return false;
            }

            @Override
            public void onLongPress(MotionEvent e) {

            }

            @Override
            public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
                return false;
            }

            @Override
            public boolean onDown(MotionEvent e) {
                return false;
            }
        });
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        mGestureDetector.onTouchEvent(ev);
        return super.dispatchTouchEvent(ev);
    }

    @Override
    public void onMyLocationChange(MyLocationItem location) {
        // TODO Auto-generated method stub
        CustomLog.d(TAG,"on my location changed!");
        showLocation(location.getGeoPoint());
        getPointAddress(location.getGeoPoint());

        mOverlayManager.getMyLocation().setEnabled(false);
    }

    public interface SAFE_ZONE_ACTION {
        int NONE = -1;
        int ADD = 0;
        int EDIT = 1;
        int DELETE = 3;
    }
}
