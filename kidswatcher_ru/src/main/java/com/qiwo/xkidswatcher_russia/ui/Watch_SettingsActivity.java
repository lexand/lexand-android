package com.qiwo.xkidswatcher_russia.ui;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.qiwo.xkidswatcher_russia.AppContext;
import com.qiwo.xkidswatcher_russia.KidsWatConfig;
import com.qiwo.xkidswatcher_russia.R;
import com.qiwo.xkidswatcher_russia.api.remote.KidsWatApiUrl;
import com.qiwo.xkidswatcher_russia.base.BaseActivity;
import com.qiwo.xkidswatcher_russia.bean.beanForDb___Family;
import com.qiwo.xkidswatcher_russia.thread.CommonThreadMethod;
import com.qiwo.xkidswatcher_russia.thread.ThreadParent;
import com.qiwo.xkidswatcher_russia.util.KidsWatUtils;
import net.intari.CustomLogger.CustomLog;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;


public class Watch_SettingsActivity extends BaseActivity implements
        OnClickListener {

    public static final String TAG = Watch_SettingsActivity.class.getSimpleName();

    public static final int setdevicevoicelevelCode = 15925;
    public static final int SETTINGTIMEZONECODE = 100;
    public static final String SETTINGTIMEZONESTATE = "time_zone_state";
    public static final String INACTIVITY = "in_activity";
    //    private ImageView iv_sleepmode;
    private final String getdevicevoicelevelAction = "w461_get_device_voice_level_and_shark_mode_and_call_volume";
    private final String setdevicevoicelevelAction = "w461_set_device_voice_level_and_shark_mode_and_call_volume";
    private final String set_device_is_ring_v2 = "w461_set_device_is_ring_v2";
    private final String set_device_call_volume_v2 = "w461_set_device_call_volume_v2";
    private final String set_device_is_shark_v2 = "w461_set_device_is_shark_v2";
    private final String active_sleep_mode_v2 = "w461_set_device_is_active_sleep_mode_v2";
    private final int getdevicevoicelevelCode = 15924;
    LinearLayout setting_time_zone_linear;
    TextView setting_time_zone_linear_time;
    int ringState = 0;
    int sharkState = 0;
    int sleepModeState = 0;
    int timeZoneState = 0;
    private ImageView muted_image, vibrate_image, High_image, Medium_image,
            Low_image;
    private LinearLayout linearLayout_l;
    private FrameLayout locationing_mode;
    private FrameLayout High_linear, Medium_linear, Low_linear;
    private String ACTION = setdevicevoicelevelAction;
    @SuppressLint("HandlerLeak")
    private Handler handler = new Handler() {

        @Override
        public void handleMessage(Message msg) {

            String mess = msg.obj.toString();
            int code = msg.arg1;

            if (mess == null || mess.equals(""))
                return;
            JSONObject json = null;
            try {
                json = new JSONObject(mess);

                int errorCode = json.getInt("error");

                switch (code) {
                    case getdevicevoicelevelCode:

                        if (errorCode == 0) {
                            JSONObject infoJson = json.getJSONObject("info");

                            ringState = infoJson.getInt("is_ring");
                            sharkState = infoJson.getInt("is_shark");
                            sleepModeState = infoJson.getInt("is_active_sleep_mode");
                            if (!infoJson.isNull("time_zone"))
                                timeZoneState = infoJson.getInt("time_zone");
                            int call_volume = infoJson.getInt("call_volume");

                            /*
                             * muted
                             */
                            if (ringState == 1)
                                muted_image
                                        .setBackgroundResource(R.drawable.switchs_s);
                            else
                                muted_image
                                        .setBackgroundResource(R.drawable.switchs);
                            /*
                             * vibrate
                             */
                            if (sharkState == 1)
                                vibrate_image
                                        .setBackgroundResource(R.drawable.switchs_s);
                            else
                                vibrate_image
                                        .setBackgroundResource(R.drawable.switchs);
                            /*
                             * sleepmode
                             */
//                            if (sleepModeState == 1)
//                                iv_sleepmode
//                                        .setBackgroundResource(R.drawable.switchs_s);
//                            else
//                                iv_sleepmode
//                                        .setBackgroundResource(R.drawable.switchs);
                            /*
                             * Volume
                             */
                            setDVolume();

                            switch (call_volume) {
                                case 1:
                                    Low_image.setVisibility(0);
                                    break;
                                case 2:
                                    Medium_image.setVisibility(0);
                                    break;
                                case 3:
                                    High_image.setVisibility(0);
                                    break;

                                default:
                                    break;
                            }
                        } else if (errorCode == 2015001) {
                            JSONObject mes = json.getJSONObject("info");

                            Toast.makeText(Watch_SettingsActivity.this,
                                    getString(R.string.watch_setting_1), 1).show();
                        }

                        /**time_zone**/
                        String strTimeZone = timeZoneState >= 0 ? "+" + timeZoneState : timeZoneState + "";
                        setting_time_zone_linear_time.setText("GMT " + strTimeZone);

                        hideWaitDialog();

                        break;

                    case setdevicevoicelevelCode:

                        JSONObject infoJson = json.getJSONObject("info");
                        if (errorCode == 0) {

                            // 09-24 11:08:10.322: I/getResult(27215):
                            // {"error":-1,"info":{"message":" cmd send failed!"}}

                            // High_linear.setEnabled(true);
                            // Medium_linear.setEnabled(true);
                            // Low_linear.setEnabled(true);
                            // muted_image.setEnabled(true);
                            // vibrate_image.setEnabled(true);

                            JSONObject mes = json.getJSONObject("info");

                            // Toast.makeText(Watch_SettingsActivity.this,
                            // mes.getString("message"), 1).show();

                        } else {
                            //	登录超时，请重新登录
                            //	десантный  тайм - аут  ,  пожалуйста,  войдите
                            Toast.makeText(Watch_SettingsActivity.this,
                                    infoJson.getString("message"), 1).show();
                        }

                        GetState();

                        break;

                    default:
                        break;
                }

            } catch (JSONException e) {
                CustomLog.logException(TAG,e);
            }
        }
    };
    private CharSequence sleepMode_tip;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_watch_setting);
        //getActionBar().hide();
        init();
    }

    public void init() {

        setting_time_zone_linear_time = (TextView) findViewById(R.id.setting_time_zone_linear_time);
        muted_image = (ImageView) findViewById(R.id.muted_image);
        vibrate_image = (ImageView) findViewById(R.id.vibrate_image);

        linearLayout_l = (LinearLayout) findViewById(R.id.linearLayout_l);
        setting_time_zone_linear = (LinearLayout) findViewById(R.id.setting_time_zone_linear);
        locationing_mode = (FrameLayout) findViewById(R.id.locationing_mode);

        High_image = (ImageView) findViewById(R.id.High_image);
        Medium_image = (ImageView) findViewById(R.id.Medium_image);
        Low_image = (ImageView) findViewById(R.id.Low_image);

        High_linear = (FrameLayout) findViewById(R.id.High_linear);
        Medium_linear = (FrameLayout) findViewById(R.id.Medium_linear);
        Low_linear = (FrameLayout) findViewById(R.id.Low_linear);

        //----------461/561  新版 ----------
//        LinearLayout ll_sleepMode = (LinearLayout) findViewById(R.id.ll_sleepMode);

        if (AppContext.getInstance().getCurrentFamily().version.equals("461")
                || AppContext.getInstance().getCurrentFamily().version.startsWith("561")
                || AppContext.getInstance().getCurrentFamily().version.equalsIgnoreCase("601")
                || KidsWatUtils.isRussianVersion()) {

        }

        if (AppContext.getInstance().getLoginUser_kid().isAdmin == 1) {
            setting_time_zone_linear.setVisibility(View.VISIBLE);
        }

        // onclick
        linearLayout_l.setOnClickListener(this);
        locationing_mode.setOnClickListener(this);

        High_linear.setOnClickListener(this);
        Medium_linear.setOnClickListener(this);
        Low_linear.setOnClickListener(this);

        muted_image.setOnClickListener(this);
        vibrate_image.setOnClickListener(this);

        setting_time_zone_linear.setOnClickListener(this);

        sleepMode_tip = getString(R.string.watch_setting_3);

        GetState();

        setDVolume();
    }

    /**
     * 获取状态
     */
    public void GetState() {
        new Thread(new ThreadParent(TAG,KidsWatApiUrl.getApiUrl(),
                CommonThreadMethod.JsonFormat(GetStateParameter()), handler,
                getdevicevoicelevelCode)).start();
    }

    /**
     * 设置状态
     */
    public void SetState(String action) {
        showWaitDialog();

        // High_linear.setEnabled(false);
        // Medium_linear.setEnabled(false);
        // Low_linear.setEnabled(false);
        // muted_image.setEnabled(false);
        // vibrate_image.setEnabled(false);

        new Thread(new ThreadParent(TAG,KidsWatApiUrl.getApiUrl(),
                CommonThreadMethod.JsonFormat(SetStateParameter(action)),
                handler, setdevicevoicelevelCode)).start();
    }

    @Override
    public void onClick(View v) {

        boolean Executive = true;

        switch (v.getId()) {

            case R.id.Low_linear:
                /*
                 * 低
                 */
                Executive = true;
                setDVolume();
                Low_image.setVisibility(0);

                ACTION = set_device_call_volume_v2;

                break;

            case R.id.Medium_linear:
                /*
                 * 中
                 */
                Executive = true;
                setDVolume();
                Medium_image.setVisibility(0);

                ACTION = set_device_call_volume_v2;

                break;

            case R.id.High_linear:
                /*
                 * 高
                 */
                Executive = true;
                setDVolume();
                High_image.setVisibility(0);

                ACTION = set_device_call_volume_v2;

                break;

            case R.id.muted_image:
                Executive = true;
                if (ringState == 0)
                    muted_image.setBackgroundResource(R.drawable.switchs_s);
                else
                    muted_image.setBackgroundResource(R.drawable.switchs);

                ACTION = set_device_is_ring_v2;

                break;
            case R.id.vibrate_image:
                Executive = true;
                if (sharkState == 0)
                    vibrate_image.setBackgroundResource(R.drawable.switchs_s);
                else
                    vibrate_image.setBackgroundResource(R.drawable.switchs);

                ACTION = set_device_is_shark_v2;

                break;

            case R.id.linearLayout_l:
                Executive = false;
                this.finish();

                break;
            case R.id.locationing_mode:
                Executive = false;
                Intent intent = new Intent(Watch_SettingsActivity.this,
                        LocatingModeActivity.class);
                startActivity(intent);
                break;

            case R.id.sleepmode_image:
                beanForDb___Family f = AppContext.getInstance().getCurrentFamily();
                if (f != null && f.d_mode == 2) {
                    Toast.makeText(Watch_SettingsActivity.this, sleepMode_tip, Toast.LENGTH_SHORT).show();
                    break;
                }
                Executive = true;
//                if (sleepModeStll_sleepModeф
                ACTION = active_sleep_mode_v2;

                break;
            case R.id.setting_time_zone_linear:
                /**只有管理员才能设置时区**/
                if (AppContext.getInstance().getCurrentFamily() == null) {
                    Toast.makeText(this, getString(R.string.watch_setting_2), Toast.LENGTH_SHORT).show();
                    return;
                }
                Intent mIntent = new Intent();
                mIntent.setClass(this, SettingTimeZoneActivity.class);
                mIntent.putExtra(SETTINGTIMEZONESTATE, timeZoneState);
                mIntent.putExtra(INACTIVITY, 0);
                startActivityForResult(mIntent, SETTINGTIMEZONECODE);
                break;
            default:
                break;
        }

        /*
         * 只要改变了状态就设置状态
         */
        if (Executive)
            SetState(ACTION);
    }

    public void setDVolume() {
        High_image.setVisibility(View.GONE);
        Medium_image.setVisibility(View.GONE);
        Low_image.setVisibility(View.GONE);
    }

    /**
     * ------------------------------------------------------------------------
     * ------------------------------------------------------------------------
     */

    /*
     * 获取状态的参数
     */
    public Map<String, String> GetStateParameter() {
        Map<String, String> hash = new HashMap<String, String>();
        hash.put("uid", KidsWatConfig.getUserUid());
        hash.put("device_id", KidsWatConfig.getUserDeviceId());
        hash.put("access_token", KidsWatConfig.getUserToken());
        KidsWatApiUrl.addExtraPara(getdevicevoicelevelAction, hash);

        return hash;
    }

    /*
     * 获取状态的参数
     */
    public Map<String, String> SetStateParameter(String action) {
        if (active_sleep_mode_v2.equals(action)) {
            Map<String, String> sleepHash = new HashMap<String, String>();
            sleepHash.put("uid", KidsWatConfig.getUserUid());
            sleepHash.put("device_id", KidsWatConfig.getUserDeviceId());
            sleepHash.put("access_token", KidsWatConfig.getUserToken());

            getIs_sleepModeState();
            //
            sleepHash.put("is_active_sleep_mode", sleepModeState + "");

            KidsWatApiUrl.addExtraPara(action, sleepHash);

            return sleepHash;
        }

        Map<String, String> hash = new HashMap<String, String>();
        hash.put("uid", KidsWatConfig.getUserUid());
        hash.put("device_id", KidsWatConfig.getUserDeviceId());
        hash.put("access_token", KidsWatConfig.getUserToken());

        getIs_ringState();
        getIs_sharkState();

        //
        hash.put("is_ring", ringState + "");
        hash.put("is_shark", sharkState + "");
        hash.put("call_volume", getCall_volumeState() + "");

        KidsWatApiUrl.addExtraPara(action, hash);

        return hash;
    }

    /*
     * 确认声音大小
     */
    public int getCall_volumeState() {
        int call_volume = 1;
        if (High_image.getVisibility() == 0)
            call_volume = 3;
        else if (Medium_image.getVisibility() == 0)
            call_volume = 2;
        else if (Low_image.getVisibility() == 0)
            call_volume = 1;

        return call_volume;
    }

    /*
     * 确认打开muted
     */

    public void getIs_ringState() {
        // if (muted_image
        // .getBackground()
        // .getConstantState()
        // .equals(getResources().getDrawable(R.drawable.switchs)
        // .getConstantState())) {
        if (ringState == 1) {
            ringState = 0;
        } else {
            ringState = 1;
        }

    }

    /*
     * 确认打开shark
     */
    public void getIs_sharkState() {
        // if (vibrate_image
        // .getBackground()
        // .getConstantState()
        // .equals(getResources().getDrawable(R.drawable.switchs)
        // .getConstantState())) {
        if (sharkState == 1) {
            sharkState = 0;
        } else {
            sharkState = 1;
        }
    }

    public void getIs_sleepModeState() {
        // if (vibrate_image
        // .getBackground()
        // .getConstantState()
        // .equals(getResources().getDrawable(R.drawable.switchs)
        // .getConstantState())) {
        if (sleepModeState == 1) {
            sleepModeState = 0;
        } else {
            sleepModeState = 1;
        }
    }

    @Override
    public void initView() {

    }

    @Override
    public void initData() {

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == SETTINGTIMEZONECODE) {
            /**time_zone**/
            if (data != null) {
                int tState = data.getIntExtra(Watch_SettingsActivity.SETTINGTIMEZONESTATE, 0);
                timeZoneState = SettingTimeZoneActivity.timeZoneListInt[tState];
                setting_time_zone_linear_time.setText("GMT " + SettingTimeZoneActivity.timeZoneList[tState]);
            }
        }
    }

}