package com.qiwo.xkidswatcher_russia.ui;

import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.qiwo.xkidswatcher_russia.AppContext;
import com.qiwo.xkidswatcher_russia.KidsWatConfig;
import com.qiwo.xkidswatcher_russia.R;
import com.qiwo.xkidswatcher_russia.api.ApiHttpClient;
import com.qiwo.xkidswatcher_russia.api.remote.KidsWatApiUrl;
import com.qiwo.xkidswatcher_russia.base.BaseActivity;
import com.qiwo.xkidswatcher_russia.event.BaseEvent;
import com.qiwo.xkidswatcher_russia.util.ACache;
import net.intari.CustomLogger.CustomLog;
import com.qiwo.xkidswatcher_russia.widget.APopupWindow.ItemPosition;
import com.qiwo.xkidswatcher_russia.widget.APopupWindow.onClickItemListener;
import com.qiwo.xkidswatcher_russia.widget.BottomPopupWindow;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONException;
import org.json.JSONObject;
import org.kymjs.kjframe.http.HttpCallBack;

import butterknife.BindView;;

/**
 *
 */
public class SystemSettingsActivity extends BaseActivity {

    final String TAG = SystemSettingsActivity.class.getSimpleName();
    @BindView(R.id.imageview_l)
    ImageView imageview_back;
    @BindView(R.id.linearLayout_cc)
    LinearLayout linearLayout_cc;
    @BindView(R.id.linearLayout_l)
    LinearLayout linearLayout_l;
    @BindView(R.id.linearLayout_alertNotification)
    LinearLayout linearLayout_alertNotification;
    @BindView(R.id.linearLayout_about)
    LinearLayout linearLayout_about;
    @BindView(R.id.linearLayout_feedback)
    LinearLayout linearLayout_feedback;
    @BindView(R.id.linearLayout_app_update)
    LinearLayout linearLayout_app_update;
    @BindView(R.id.user_mobile)
    TextView user_mobile;
    @BindView(R.id.button_exit)
    Button button_exit;
    @BindView(R.id.linearLayout_help)
    LinearLayout linearlayout_help;

    // -----------
    private BottomPopupWindow mLogoutWindow = null;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_system_settings;
    }

    @Override
    protected boolean hasActionBar() {
        return false;
    }

    @Override
    protected void init(Bundle savedInstanceState) {
        super.init(savedInstanceState);
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.linearLayout_l:
                finish();
                break;
            case R.id.linearLayout_alertNotification:
                Intent xintent = new Intent(this, AlertNotificationActivity.class);
                // intent.putExtra(Contanst.KEY_ACTION, Contanst.ACTION_EDIT);
                startActivity(xintent);
                break;
            case R.id.linearLayout_about:
                Intent intent = new Intent(this, AboutActivity.class);
                // intent.putExtra(Contanst.KEY_ACTION, Contanst.ACTION_EDIT);
                startActivity(intent);
                break;
            case R.id.linearLayout_feedback:
                Intent intentx = new Intent(this, FeedbackActivity.class);
                startActivity(intentx);
                break;
            case R.id.linearLayout_app_update:
                showLongToast(getString(R.string.system_setting_1));
                break;
            case R.id.button_exit:
                showLogoutPopupWindow(linearLayout_cc);
                break;
            case R.id.linearLayout_help:
                startActivity(new Intent(this, SystemSettingHelpActivity.class));
                break;
            default:
                break;
        }
    }

    @Override
    public void initView() {
        linearLayout_l.setOnClickListener(this);
        linearLayout_alertNotification.setOnClickListener(this);
        linearLayout_about.setOnClickListener(this);
        linearLayout_feedback.setOnClickListener(this);
        linearLayout_app_update.setOnClickListener(this);
        button_exit.setOnClickListener(this);
        linearlayout_help.setOnClickListener(this);
    }

    @Override
    public void initData() {
        String phone = KidsWatConfig.getUserPhone();

        user_mobile.setText("(+" + KidsWatConfig.getUseCountryCode() + ") "
                + phone);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    protected void showLogoutPopupWindow(View relyView) {
        if (mLogoutWindow == null) {
            mLogoutWindow = new BottomPopupWindow(this, 0, relyView);

            // addItem(title, listener, bgDrawableId, textColorResId,
            // default_textSize, drawable, pos, 0);

            mLogoutWindow
                    .addItem(
                            getString(R.string.system_setting_2),
                            null, 0, R.color.gray, 15, null, ItemPosition.TOP,
                            15);
            mLogoutWindow.addItem(getString(R.string.exit), new onClickItemListener() {

                @Override
                public void clickItem(View v) {
                    // TODO Auto-generated method stub
                    ACache cache = ACache.get(SystemSettingsActivity.this);
                    cache.clear();

                    if (AppContext.getInstance().getIsLogin()) {
                        logout();
                    } else {
                        KidsWatConfig.cleanLoginInfo();
                        // AppManager.AppRestart(SystemSettingsActivity.this);

                        BaseEvent event = new BaseEvent(
                                BaseEvent.MSGTYPE_1___LOGOUT, "logout");
                        EventBus.getDefault().post(event);
                        SystemSettingsActivity.this.finish();
                    }
                }
            }, 0, R.color.red, 18, null, ItemPosition.BOTTOM, 0);

            // 添加“取消”
            mLogoutWindow.addItem(getApplicationContext().getResources().getString(R.string.cancle), null, 0, R.color.main_blue, 18,
                    null, ItemPosition.OTHER, 10);
        }
        mLogoutWindow.show();
    }

    private void logout() {
        final String uid = KidsWatConfig.getUserUid();
        // String device_id = AppContext.getInstance().getProperty(
        // "user.device_id");
        // String family_id = AppContext.getDefaultFamilyId();

        final String request_url = KidsWatApiUrl.getUrlFor___logout(uid);

        ApiHttpClient.get(request_url, new HttpCallBack() {

            @Override
            public void onPreStart() {
                showWaitDialog(getString(R.string.new_setting_fragment_6));
            }

            @Override
            public void onSuccess(String t) {
                super.onSuccess(t);
                CustomLog.d(TAG,String.format("url:%s\nt:%s", request_url, t));
                try {
                    JSONObject response = new JSONObject(t);

                    int code = response.getInt("error");
                    if (code == 0) {
                        KidsWatConfig.cleanLoginInfo();
                    }

                } catch (JSONException e) {
                    CustomLog.logException(TAG,e);
                }
            }

            @Override
            public void onFailure(int errorNo, String strMsg) {
                super.onFailure(errorNo, strMsg);
                String msg = String.format("errorNo:%s\n%s", errorNo, strMsg);
                CustomLog.d(TAG,"error=" + msg);
            }

            @Override
            public void onFinish() {
                dismissDialog(null, 5);
                // AppManager.getAppManager().AppExit(SystemSettingsActivity.this);
                BaseEvent event = new BaseEvent(BaseEvent.MSGTYPE_1___LOGOUT,
                        "logout");
                EventBus.getDefault().post(event);
                SystemSettingsActivity.this.finish();
            }
        });

    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            finish();
        }
        return false;
    }

}
