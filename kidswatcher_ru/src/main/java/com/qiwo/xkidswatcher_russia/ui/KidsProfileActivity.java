package com.qiwo.xkidswatcher_russia.ui;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.content.DialogInterface.OnShowListener;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.provider.MediaStore.Images;
import androidx.annotation.NonNull;
import com.google.android.material.snackbar.Snackbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;
import android.text.InputFilter;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.qiwo.xkidswatcher_russia.AnalyticsUtils.AnalyticsUtils;
import com.qiwo.xkidswatcher_russia.AppContext;
import com.qiwo.xkidswatcher_russia.AppManager;
import com.qiwo.xkidswatcher_russia.Constants;
import com.qiwo.xkidswatcher_russia.KidsWatConfig;
import com.qiwo.xkidswatcher_russia.R;
import com.qiwo.xkidswatcher_russia.Utils;
import com.qiwo.xkidswatcher_russia.api.ApiHttpClient;
import com.qiwo.xkidswatcher_russia.api.remote.KidsWatApiUrl;
import com.qiwo.xkidswatcher_russia.base.BaseActivity;
import com.qiwo.xkidswatcher_russia.base.BaseDialog;
import com.qiwo.xkidswatcher_russia.bean.beanForDb___Family;
import com.qiwo.xkidswatcher_russia.bean.beanForDb___LoginMember;
import com.qiwo.xkidswatcher_russia.db.SqlDb;
import com.qiwo.xkidswatcher_russia.event.BaseEvent;
import com.qiwo.xkidswatcher_russia.thread.CommonThreadMethod;
import com.qiwo.xkidswatcher_russia.thread.ThreadParent;
import com.qiwo.xkidswatcher_russia.util.Base64Encode;
import com.qiwo.xkidswatcher_russia.util.Contanst;
import com.qiwo.xkidswatcher_russia.util.Encoder;
import com.qiwo.xkidswatcher_russia.util.ImageUtils;
import com.qiwo.xkidswatcher_russia.util.KidsWatUtils;
import com.qiwo.xkidswatcher_russia.util.SimpleDateUtil;
import com.qiwo.xkidswatcher_russia.util.StringUtils;
import net.intari.CustomLogger.CustomLog;
import com.qiwo.xkidswatcher_russia.widget.APopupWindow.ItemPosition;
import com.qiwo.xkidswatcher_russia.widget.APopupWindow.onClickItemListener;
import com.qiwo.xkidswatcher_russia.widget.BottomPopupWindow;
import com.qiwo.xkidswatcher_russia.widget.CircleImageView;
import com.qiwo.xkidswatcher_russia.widget.DatePickerPopupWindow2;
import com.qiwo.xkidswatcher_russia.widget.EditTextWithDel;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONException;
import org.json.JSONObject;

/*
import cz.msebera.android.httpclient.HttpEntity;
import cz.msebera.android.httpclient.HttpResponse;
import cz.msebera.android.httpclient.NameValuePair;
import cz.msebera.android.httpclient.client.CookieStore;
import cz.msebera.android.httpclient.client.entity.UrlEncodedFormEntity;
import cz.msebera.android.httpclient.client.methods.HttpPost;
import cz.msebera.android.httpclient.impl.client.DefaultHttpClient;
import cz.msebera.android.httpclient.params.BasicHttpParams;
import cz.msebera.android.httpclient.params.HttpParams;
*/
import org.kymjs.kjframe.http.HttpCallBack;
import org.kymjs.kjframe.http.HttpParams;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;;

import static com.qiwo.xkidswatcher_russia.Constants.CAMERA_PREFIX;
import static com.qiwo.xkidswatcher_russia.Constants.CAMERA_SUFFIX;
import static com.qiwo.xkidswatcher_russia.Constants.CROP_PREFIX;
import static com.qiwo.xkidswatcher_russia.Constants.DEFAULT_IMAGE_FORMAT;
import static com.qiwo.xkidswatcher_russia.Constants.PHOTO_PROVIDER;
import static com.qiwo.xkidswatcher_russia.util.PermissionUtils.grantPermissionsForUri;
import static com.qiwo.xkidswatcher_russia.util.PhotoUtils.CROP_SIZE;
import static com.qiwo.xkidswatcher_russia.util.PhotoUtils.FILE_SAVEPATH;
import static com.qiwo.xkidswatcher_russia.util.PhotoUtils.GOOGLE_PHOTOS_PACKAGE_NAME;
import static com.qiwo.xkidswatcher_russia.util.PhotoUtils.copyFromStream;
import static com.qiwo.xkidswatcher_russia.util.PhotoUtils.getURIDisplayName;
import static com.qiwo.xkidswatcher_russia.util.PhotoUtils.isGooglePhotosInstalled;

/**
 *
 */
public class KidsProfileActivity extends BaseActivity {

    public static final int REQUSET_CODE_USER_INFO_EX = 1200;
    // ----------------
    private final static int CROP = 120;
    //private final static String FILE_SAVEPATH = KidsWatConfig.getTempFilePath();
    final String TAG = KidsProfileActivity.class.getSimpleName();
    final int SET_RELATION_SHIP = 300;
    @BindView(R.id.isadmin_image)
    ImageView isadmin_image;
    @BindView(R.id.imageview_l)
    ImageView imageview_back;
    @BindView(R.id.imageView_baby)
    CircleImageView imageView_baby;
    @BindView(R.id.linearLayout_cc)
    LinearLayout linearLayout_cc;
    @BindView(R.id.linearLayout_cc2)
    LinearLayout linearLayout_cc2;
    @BindView(R.id.linearLayout_l)
    LinearLayout linearLayout_l;
    @BindView(R.id.relativeLayout_img)
    RelativeLayout relativeLayout_img;
    @BindView(R.id.relativeLayout_relation)
    RelativeLayout relativeLayout_relation;
    @BindView(R.id.relativeLayout_nickname)
    RelativeLayout relativeLayout_nickname;
    @BindView(R.id.relativeLayout_birthday)
    RelativeLayout relativeLayout_birthday;
    @BindView(R.id.relativeLayout_sex)
    RelativeLayout relativeLayout_sex;
    @BindView(R.id.relativeLayout_height)
    RelativeLayout relativeLayout_height;
    @BindView(R.id.relativeLayout_weight)
    RelativeLayout relativeLayout_weight;
    @BindView(R.id.view_line_relation)
    View view_line_relation;
    @BindView(R.id.textView_relation)
    TextView textView_relation;
    @BindView(R.id.textView_nickname)
    TextView textView_nickname;
    @BindView(R.id.textView_birthday)
    TextView textView_birthday;
    @BindView(R.id.textView_sex)
    TextView textView_sex;
    @BindView(R.id.textView_height)
    TextView textView_height;
    @BindView(R.id.textView_weight)
    TextView textView_weight;
    // ---------more icon----------
    @BindView(R.id.imageView_more_photo)
    ImageView imageView_more_photo;
    @BindView(R.id.imageView_more_nickname)
    ImageView imageView_more_nickname;
    // ------------
    @BindView(R.id.imageView_more_gender)
    ImageView imageView_more_gender;
    @BindView(R.id.imageView_more_birthday)
    ImageView imageView_more_birthday;
    @BindView(R.id.imageView_more_height)
    ImageView imageView_more_height;
    @BindView(R.id.imageView_more_weight)
    ImageView imageView_more_weight;
    // --------------
    @BindView(R.id.button_ok)
    Button button_ok;
    beanForDb___Family fBaby = null;
    beanForDb___LoginMember loginUser_kid = null;
    String saveBbImgPath = "";
    private int sex = 0;
    private float height = 1.00f;
    private float weight = 20f;
    private long birthday = System.currentTimeMillis() / 1000;
    private String nickname;
    private String img_base64_d = "";
    Handler handler = new Handler() {
        public void handleMessage(android.os.Message msg) {
            String mess = msg.obj.toString();
            int code = msg.arg1;

            if (mess == null || mess.equals("")) {
                Map<String,Object> eventProperties=new HashMap<>();
                //eventProperties.put(Constants.ERRORCODE,errorCode);
                AnalyticsUtils.reportAnalyticsEvent(Constants.EVENT_PAIRING_FAILED_NETWORK_ERROR,eventProperties);

                return;

            }

            JSONObject json = null;
            try {
                json = new JSONObject(mess);

                int errorCode = json.getInt("error");

                switch (code) {
                    case Constants.PAIRING_MAGIC1:
                        if (errorCode == 0) {
                            String family_id = json.getJSONObject("info")
                                    .getString("family_id");
                            KidsWatConfig.setDefaultFamilyId(family_id);
                            CustomLog.d(TAG,"Pairing success. Familiy id "+family_id);

                            add_baby_info(family_id, img_base64_d);
                        } else if (errorCode == 1233) {
                            showLongToast(getString(R.string.connect_to_watch_not_connected_try_again));

                            button_ok.setEnabled(true);

                            Map<String,Object> eventProperties=new HashMap<>();
                            eventProperties.put(Constants.ERRORCODE,errorCode);
                            AnalyticsUtils.reportAnalyticsEvent(Constants.EVENT_PAIRING_FAILED,eventProperties);

                        } else {
                            String message = json.getJSONObject("info")
                                    .getString("message");

                            Map<String,Object> eventProperties=new HashMap<>();
                            eventProperties.put(Constants.MESSAGE,message);
                            AnalyticsUtils.reportAnalyticsEvent(Constants.EVENT_PAIRING_FAILED_SERVER,eventProperties);
                            showConfirmInformation(getApplicationContext().getResources().getString(R.string.app_name),
                                    message);

                            button_ok.setEnabled(true);
                        }
                        dismissDialog(null, 2);
                        break;
                }
            } catch (Exception e) {
                Map<String,Object> eventProperties=new HashMap<>();
                AnalyticsUtils.reportAnalyticsEvent(Constants.EVENT_EXCEPTION,eventProperties);
                AnalyticsUtils.reportThrowable("Error in handler",e);
                CustomLog.logException(e);
                // TODO: handle exception
                dismissDialog(null, 2);
            }


        }

        ;
    };
    // -----------
    private String action = null;
    private String qrcode, phone, country;
    private String relation_for_add = null;
    private BottomPopupWindow mSelectPhotoPopupWindow = null;
    private DatePickerPopupWindow2 mDatePickerPopupWindow = null;
    private Uri origUri;
    private String origPath;
    private Uri origUriLocal;
    private Uri cropUri;
    private File protraitFile;
    private Bitmap protraitBitmap;
    private String protraitPath;


    private Uri tempPreCropUri=null;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_kids_profile;
    }

    @Override
    protected boolean hasActionBar() {
        return false;
    }

    @Override
    protected void init(Bundle savedInstanceState) {
        super.init(savedInstanceState);
        CustomLog.d(TAG,"init(savedInstanceState="+savedInstanceState+"), called from onCreate()");
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.linearLayout_l:
                finish();
                break;
            case R.id.button_ok:
                // Intent intent = new Intent(this, ScanGuideCameraActivity.class);
                // startActivityForResult(intent, SHOW_SCAN_QRCODE);

                CustomLog.d(TAG,"ok button clicked, disabling");
                button_ok.setEnabled(false);

                if (action.equals(Contanst.ACTION_ADD)) {
                    CustomLog.d(TAG,"Action.Add. will add watch");
                    add_watch(qrcode, relation_for_add);
                }
                break;
            case R.id.relativeLayout_img:
                if (userIsAllowed())
                    showSelecPhotoPopupWindow(linearLayout_cc2);
                break;
            case R.id.relativeLayout_relation:
                Intent intent = new Intent(this, SetRelationActivity2.class);
                startActivityForResult(intent, SET_RELATION_SHIP);
                break;
            case R.id.relativeLayout_nickname:
                if (userIsAllowed())
                    editNickName();
                break;
            case R.id.relativeLayout_birthday:
                if (userIsAllowed()) {
                    DatePickerPopupWindow2.OnDatePickerInfoListener listener = new DatePickerPopupWindow2.OnDatePickerInfoListener() {
                        public void onDateReceiver(String date) {
                            textView_birthday.setText(date);
                            // showShortToast(date);
                            try {
//							SimpleDateFormat sdf = new SimpleDateFormat(
//									"yyyy-MM-dd");
                                SimpleDateFormat sdf = new SimpleDateFormat(
                                        "dd-MM-yyyy");
                                Date xdate = sdf.parse(date);
                                birthday = xdate.getTime() / 1000;
                                if (action.equalsIgnoreCase(Contanst.ACTION_EDIT)) {
                                    update_baby_info();
                                }
                            } catch (ParseException e) {
                                birthday = System.currentTimeMillis() / 1000;
                                showLongToast(e.toString());
                            }
                        }
                    };
                    showSelectBirthdayPopupWindow(linearLayout_cc2, listener);
                }
                break;
            case R.id.relativeLayout_sex:
            case R.id.relativeLayout_height:
            case R.id.relativeLayout_weight:
                if (userIsAllowed())
                    setUserInfoEx();
                break;
            default:
                break;
        }
    }

    @Override
    public void initView() {
        linearLayout_l.setOnClickListener(this);
        relativeLayout_img.setOnClickListener(this);
        relativeLayout_relation.setOnClickListener(this);
        relativeLayout_nickname.setOnClickListener(this);
        relativeLayout_birthday.setOnClickListener(this);
        relativeLayout_sex.setOnClickListener(this);
        relativeLayout_height.setOnClickListener(this);
        relativeLayout_weight.setOnClickListener(this);
        button_ok.setOnClickListener(this);
    }

    @Override
    public void initData() {
        CustomLog.d(TAG,"initData()");
        nickname = getString(R.string.baby);
        action = getIntent().getStringExtra(Contanst.KEY_ACTION);
        if (action.equals(Contanst.ACTION_ADD)) {
            Intent intent = getIntent();
            qrcode = intent.getStringExtra("qrcode");
            relation_for_add = intent.getStringExtra("relation");
            phone = intent.getStringExtra("phone");
            country = intent.getStringExtra("country");

            relativeLayout_relation.setVisibility(View.GONE);
            view_line_relation.setVisibility(View.GONE);

//			textView_birthday.setText(new SimpleDateFormat("yyyy-MM-dd")
//					.format(new Date()));
            textView_birthday.setText(new SimpleDateFormat("dd-MM-yyyy")
                    .format(new Date()));

        } else if (action.equals(Contanst.ACTION_EDIT)) {
            button_ok.setVisibility(View.INVISIBLE);
            showKidsProfile();
        }
    }

    private boolean userIsAllowed() {
        if (action.equals(Contanst.ACTION_ADD)) {
            return true;
        } else if (action.equals(Contanst.ACTION_EDIT)) {
            if (loginUser_kid.isAdmin == 1) {
                return true;
            }

        }
        return false;
    }

    private void showMoreIcon(boolean show) {
        int visible = show ? View.VISIBLE : View.GONE;
        imageView_more_photo.setVisibility(visible);
        imageView_more_nickname.setVisibility(visible);
        imageView_more_gender.setVisibility(visible);
        imageView_more_birthday.setVisibility(visible);
        imageView_more_height.setVisibility(visible);
        imageView_more_weight.setVisibility(visible);
    }

    private void showKidsProfile() {

        fBaby = AppContext.getInstance().getCurrentFamily();
        loginUser_kid = AppContext.getInstance().getLoginUser_kid();

        if (fBaby==null) {
            CustomLog.e(TAG,"showKidsProfile:Baby not found, well, family is not found.AppContext.getInstance().getCurrentFamily() == null,loginUser_kid="+loginUser_kid+"|, so will not show profile");
            AppContext.showToast(R.string.family_not_found);
            return;
        }
        sex = fBaby.sex;
        height = fBaby.height;
        weight = fBaby.weight;
        nickname = fBaby.nickname;
        birthday = fBaby.birthday;

        if (loginUser_kid.isAdmin == 0) {
            showMoreIcon(false);
        }

        textView_relation.setText(loginUser_kid.relation);
        textView_nickname.setText(fBaby.nickname);
//		String birthdayText = fBaby.birthdayText;
//		textView_birthday.setText(birthdayText);

        String b = SimpleDateUtil.convert2String(
                birthday * 1000, SimpleDateUtil.DATE_FORMAT);
        textView_birthday.setText(b);
//		Log.e("----","----birthdayText="+birthdayText+"---"+fBaby.birthday+"----"+b);
        String sexText = fBaby.sexText;

        if ("boy".equalsIgnoreCase(sexText)) {
            sexText = getString(R.string.boy);
        } else {
            sexText = getString(R.string.girl);
        }

        textView_sex.setText(sexText);
        textView_height.setText(fBaby.height + "м");
        textView_weight.setText(fBaby.weight + "кг");
        imageView_baby.setImageBitmap(KidsWatUtils.getBabyImg_v3(this, fBaby.family_id, fBaby.sex, AppContext.getInstance().getCurrentFamily()));

        if (AppContext.getInstance().getLoginUser_kid().isAdmin == 1)
            isadmin_image.setVisibility(0);
        else
            isadmin_image.setVisibility(8);
    }

    private void editNickName() {
        final String nameStr = action.equals(Contanst.ACTION_ADD) ? ""
                : fBaby.nickname;

        View view = mInflater.inflate(R.layout.dialog_edittext_view, null);
        final EditTextWithDel editView = (EditTextWithDel) view
                .findViewById(R.id.edittext);
        editView.setHint(getApplicationContext().getResources().getString(R.string.nickname) + "(0-9)");
        editView.setText(nameStr);
        editView.setSelection(editView.getText().length());
        editView.setFilters(new InputFilter[]{new InputFilter.LengthFilter(9)});

        final BaseDialog b = new BaseDialog(this);
        b.setTitle(getApplicationContext().getResources().getString(R.string.nickname));
        b.setContentView(view);
        b.setOnShowListener(new OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                // inputMethodManager.showSoftInput(editView, 0);
            }
        });
        b.setOnDismissListener(new OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                // inputMethodManager.hideSoftInputFromWindow(
                // editView.getWindowToken(), 0);
            }
        });
        b.setNegativeButton(R.string.cancle, new OnClickListener() {
            @Override
            public void onClick(View v) {
                b.dismiss();
            }
        }, R.style.button_default);
        b.setPositiveButton(R.string.ok, new OnClickListener() {
            @Override
            public void onClick(View v) {

                nickname = editView.getText().toString().trim();
                CustomLog.d(TAG,nickname);
                b.dismiss();
                if (action.equals(Contanst.ACTION_EDIT)) {
                    update_baby_info();
                } else if (action.equals(Contanst.ACTION_ADD)) {
                    textView_nickname.setText(nickname);
                }
                // 设置名字
            }
        }, R.style.button_default);
        b.show();

    }

    private void setUserInfoEx() {
        Intent intent = new Intent(this, BabyInfoActivity.class);
        intent.putExtra("sex", sex);
        intent.putExtra("height", height * 100);
        intent.putExtra("weight", weight);
        startActivityForResult(intent, REQUSET_CODE_USER_INFO_EX);
    }

    protected void showSelecPhotoPopupWindow(View relyView) {
        if (mSelectPhotoPopupWindow == null) {
            mSelectPhotoPopupWindow = new BottomPopupWindow(this, 0, relyView);

            // 添加“拍照”
            mSelectPhotoPopupWindow.addItem(getApplicationContext().getResources().getString(R.string.take_photo),
                    new onClickItemListener() {

                        @Override
                        public void clickItem(View v) {
                            startActionCamera();
                        }
                    }, ItemPosition.TOP);

            // 添加“从相册选择”
            mSelectPhotoPopupWindow.addItem(getApplicationContext().getResources().getString(R.string.choose_from_albulm),
                    new onClickItemListener() {

                        @Override
                        public void clickItem(View v) {
                            startImagePick();
                        }
                    }, ItemPosition.BOTTOM);

            // 添加“取消”
            mSelectPhotoPopupWindow.addCancelItem(getString(R.string.cancle), null, 10);
        }
        mSelectPhotoPopupWindow.show();
    }

    protected DatePickerPopupWindow2 showSelectBirthdayPopupWindow(
            View relyView,
            DatePickerPopupWindow2.OnDatePickerInfoListener listener) {
        if (mDatePickerPopupWindow == null) {
            mDatePickerPopupWindow = new DatePickerPopupWindow2(this, relyView,
                    listener);
        }
        // mDatePickerPopupWindow.se
        mDatePickerPopupWindow.show();
        return mDatePickerPopupWindow;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        CustomLog.i(TAG,"onActivityResult(requestCode="
                +String.valueOf(requestCode)
                +",resultCode="+String.valueOf(resultCode)
                +",intent="+Utils.toUri(data)
                +")");


        if (requestCode == REQUSET_CODE_USER_INFO_EX) {
            if (resultCode == Activity.RESULT_OK && data != null) {
                sex = data.getIntExtra(BabyInfoActivity.KEY_GENDER, sex);
                float xxheight = data.getFloatExtra(
                        BabyInfoActivity.KEY_HEIGHT, height);
                height = xxheight / 100;
                weight = data
                        .getFloatExtra(BabyInfoActivity.KEY_WEIGHT, weight);
                if (sex == 0) {
                    // layout_gender.setContent(R.string.gender_0);
                    textView_sex.setText(getApplicationContext().getResources().getString(R.string.girl));
                } else if (sex == 1) {
                    // layout_gender.setContent(R.string.gender_1);
                    textView_sex.setText(getApplicationContext().getResources().getString(R.string.boy));
                }

                textView_height.setText(height + "м");
                textView_weight.setText(weight + "кг");
                if (action.equals(Contanst.ACTION_EDIT)) {
                    beanForDb___Family f = AppContext.getInstance().getCurrentFamily();
                    // if (f != null) {
                    if (!TextUtils.isEmpty(f.img_path)) {

                    } else {
                        int imgSrcId = sex == 1 ? R.drawable.baby_boy
                                : R.drawable.baby_girl;
                        imageView_baby.setImageResource(imgSrcId);
                    }
                    // } else {
                    // int imgSrcId = sex == 1 ? R.drawable.baby_boy
                    // : R.drawable.baby_girl;
                    // imageView_baby.setImageResource(imgSrcId);
                    // }

                    update_baby_info();
                } else if (action.equalsIgnoreCase(Contanst.ACTION_ADD)) {
                    int imgSrcId = sex == 1 ? R.drawable.baby_boy
                            : R.drawable.baby_girl;
                    imageView_baby.setImageResource(imgSrcId);
                }

            }
            return;
        } else if (requestCode == SET_RELATION_SHIP) {
            if (resultCode == Activity.RESULT_OK && data != null) {
                Bundle bundle = data.getExtras();
                String result = bundle.getString("relation");
                CustomLog.d(TAG, "relation=" + result);
                updateRelationShip(result);

            }
        } else if (requestCode == ImageUtils.REQUEST_CODE_IMAGE_FROM_CAMERA) {
            if (resultCode == RESULT_OK)
                startActionCropV2(origUri);
                //startActionCrop(origUri);

            // 拍照后裁剪
        } else if (requestCode == ImageUtils.REQUEST_CODE_IMAGE_PICK) {
            if (data != null && data.getData() != null)
                startActionCropV2(data.getData());// 选图后裁剪
                //startActionCrop(data.getData());// 选图后裁剪

        } else if (requestCode == ImageUtils.REQUEST_CODE_GETIMAGE_BYSDCARD) {
            if (resultCode != RESULT_OK) {
                CustomLog.w(TAG,"Got REQUEST_CODE_GETIMAGE_BYSDCARD but resultCode is NOT RESULT_OK, it's:"+resultCode);
                Toast.makeText(KidsProfileActivity.this, getApplicationContext().getResources().getString(R.string.error_cropper_not_ok), Toast.LENGTH_LONG).show();
                return;
            }
            if (data!=null) {
                CustomLog.d(TAG,"data:"+Utils.toUri(data));
                if (data.getData()!=null) {
                    CustomLog.d(TAG,"data.getData:"+data.getData());
                }
            }

            // uploadNewPhoto();// 上传新照片`
            //we have data.getData() and can upload
            //generate thumbnail FROM INPUT STREAM
            if (data != null && data.getData() != null) {

                CustomLog.d(TAG,"uploading avatar in regular way from "+data.getData());
                uploadAvatar(KidsProfileActivity.this,data.getData(),Constants.IMAGE_NAME_FOR_AVATAR);

            } else {
                //Let's do manual
                Uri originalUri=data.getParcelableExtra(Constants.SOURCE_URI);
                CustomLog.w(TAG,"Original(Stored) uri:"+originalUri);
                if (tempPreCropUri!=null) {
                    CustomLog.w(TAG,"Using backup source image at "+tempPreCropUri.toString()+" for avatar");
                    Toast.makeText(KidsProfileActivity.this, getApplicationContext().getResources().getString(R.string.error_no_crop_data_manual), Toast.LENGTH_LONG).show();
                    uploadAvatar(KidsProfileActivity.this,tempPreCropUri,Constants.IMAGE_NAME_FOR_AVATAR);

                } else {
                    Toast.makeText(KidsProfileActivity.this, getApplicationContext().getResources().getString(R.string.error_no_crop_data), Toast.LENGTH_LONG).show();

                }

                /*
                //This should never happen. except when it is
                if (data!=null) {
                    if (originalUri!=null) {
                        Toast.makeText(KidsProfileActivity.this, getApplicationContext().getResources().getString(R.string.error_no_crop_data_manual), Toast.LENGTH_LONG).show();
                        uploadAvatar(KidsProfileActivity.this,originalUri,Constants.IMAGE_NAME_FOR_AVATAR);

                    } else {
                        CustomLog.e(TAG,"Failed to unparcel original data");
                        Toast.makeText(KidsProfileActivity.this, getApplicationContext().getResources().getString(R.string.error_no_crop_data), Toast.LENGTH_LONG).show();

                    }



                } else {
                    Toast.makeText(KidsProfileActivity.this, getApplicationContext().getResources().getString(R.string.error_no_data), Toast.LENGTH_LONG).show();

                }
                */

            }

            /* else {
                if (!StringUtils.isEmpty(protraitPath) && protraitFile.exists()) {

                    protraitBitmap = ImageUtils.loadImgThumbnail(protraitPath, 120,
                            120);
                }

            } */


        }

        if (resultCode != Activity.RESULT_OK || data == null) {
            super.onActivityResult(requestCode, resultCode, data);
            return;
        }
    }


    //IMAGE_NAME_FOR_AVATAR
    private void uploadAvatar(Context context, Uri uri,String imageName) {
        CustomLog.i(TAG,"Will (try to) upload avatar from "+uri.toString());
        try {
            CustomLog.d(TAG,"Generating thumbnail based on "+uri.toString());
            InputStream inputStream=getContentResolver().openInputStream(uri);
            protraitBitmap = ImageUtils.loadImgThumbnail(inputStream, CROP_SIZE,CROP_SIZE);
            CustomLog.d(TAG,"Generated thumbnail based on "+uri.toString()+", saving it as "+imageName);

            if (protraitBitmap != null) {
                try {
                    ImageUtils.saveImage(context, imageName,
                            protraitBitmap);
                    CustomLog.d(TAG,"Saved image "+imageName);
                    String imgbase64 = Base64Encode
                            .encodeBase64FileFromContext(context,imageName);
                    img_base64_d = imgbase64;

                    if (action.equals(Contanst.ACTION_EDIT)) {
//						updateBabyImg(imgbase64);
                        CustomLog.d(TAG,"ActionEdit.  updating image");
                        updateBabyImg_v2(imgbase64);
                        // ---------------------------
                    } else if (action.equalsIgnoreCase(Contanst.ACTION_ADD)) {
                        CustomLog.d(TAG,"ActionAdd.  just add image");
                        imageView_baby.setImageBitmap(protraitBitmap);
                    }

                    // msg.obj = res;
                } catch (IOException e) {
                    CustomLog.logException(TAG,e);
                }
            } else {
                CustomLog.e(TAG,"failed to generate and update thumbnail. protraitBitmap was NOT generated");
            }

        } catch (IOException ex) {
            CustomLog.logException(TAG,ex);
        } finally {
            CustomLog.d(TAG,"forcing tempPreCropUri:"+tempPreCropUri.toString()+" to be null");
            tempPreCropUri=null;
        }
    }

    private void startActionCamera() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
                CustomLog.d(TAG,"Arleady  have camera permission");
                //now check storage permission
                if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                    CustomLog.d(TAG,"Arleady have storage permission");
                    startActionCameraInternal();

                } else {
                    //no permission, ask!

                    if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                            Manifest.permission.CAMERA)) {

                        Snackbar.make(linearLayout_cc, R.string.location_permission_storage_rationale, Snackbar.LENGTH_LONG)
                                .setAction(R.string.grant_perm, new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        CustomLog.d(TAG,"User agreed with rationale");
                                        doAskForCameraPermission();
                                    }
                                })
                                .show();
                    } else {
                        CustomLog.d(TAG,"No need to provide rationale for storage permission");
                        doAskForStoragePermission();
                    }
                }
            } else {

                if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                        Manifest.permission.CAMERA)) {

                    Snackbar.make(linearLayout_cc, R.string.location_permission_photos_rationale, Snackbar.LENGTH_LONG)
                            .setAction(R.string.grant_perm, new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    CustomLog.d(TAG,"User agreed with rationale");
                                    doAskForCameraPermission();
                                }
                            })
                            .show();
                } else {
                    CustomLog.d(TAG,"No need to provide rationale for camera permission");
                    doAskForCameraPermission();
                }


            }
        } else {
            CustomLog.d(TAG,"No need to ask for  camera permission");
            startActionCameraInternal();
        }
    }

    private void doAskForStoragePermission(){
        ActivityCompat.requestPermissions(KidsProfileActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, Contanst.REQUEST_CODE_REQUEST_PERMISSION_STORAGE);
        CustomLog.d(TAG,"Asking for storage permission");
    }

    /*
    private void startActionCamera() {
        //TODO:ask for rationale too
         if (Build.VERSION.SDK_INT >= 23) {
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
                CustomLog.d(TAG,"Arleady  have camera permission");
                startActionCameraInternal();
            } else {

                if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                        Manifest.permission.CAMERA)) {

                    Snackbar.make(linearLayout_cc, R.string.location_permission_photos_rationale, Snackbar.LENGTH_LONG)
                            .setAction(R.string.grant_perm, new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    CustomLog.d(TAG,"User agreed with rationale");
                                    doAskForCameraPermission();
                                }
                            })
                            .show();
                } else {
                    CustomLog.d(TAG,"No need to provide rationale");
                    doAskForCameraPermission();
                }


            }
        } else {
            CustomLog.d(TAG,"No need to ask for  camera permission");
            startActionCameraInternal();
        }
    }
    */

    private void doAskForCameraPermission(){
        ActivityCompat.requestPermissions(KidsProfileActivity.this, new String[]{Manifest.permission.CAMERA}, Contanst.REQUEST_CODE_REQUEST_PERMISSION_CAMERA);
        CustomLog.d(TAG,"Asking for camera permission");
    }

    @SuppressLint("Override")
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        CustomLog.i(TAG,"onRequestPermissionsResult(requestCode="
                +String.valueOf(requestCode)
                +",perms="+Utils.formatStringArray(permissions)
                +",grantResult:"+Utils.formatIntArray(grantResults)+")");

        if (requestCode == Contanst.REQUEST_CODE_REQUEST_PERMISSION_STORAGE) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                //permission granted
                CustomLog.d(TAG,"Now have storage permission");
                startActionCamera();//go and ask storage permission if needed
                //startActionCameraInternal();
            } else {
                //permission denied
                //now tell user he was baad
                Toast.makeText(KidsProfileActivity.this, getApplicationContext().getResources().getString(R.string.location_permission_storage), Toast.LENGTH_LONG).show();
            }
        } else if (requestCode == Contanst.REQUEST_CODE_REQUEST_PERMISSION_CAMERA) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                //permission granted
                CustomLog.d(TAG,"Now have camera permission");
                startActionCamera();//go and ask storage permission if needed
                //startActionCameraInternal();
            } else {
                //permission denied
                //now tell user he was baad

                Toast.makeText(KidsProfileActivity.this, getApplicationContext().getResources().getString(R.string.location_permission_photos), Toast.LENGTH_LONG).show();
            }
        }
        /*
        if (requestCode == Contanst.REQUEST_CODE_REQUEST_PERMISSION_CAMERA) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                //permission granted
                CustomLog.d(TAG,"Now have camera permission");
                startActionCameraInternal();
            } else {
                //permission denied
                //now tell user he was baad

                Toast.makeText(KidsProfileActivity.this, getApplicationContext().getResources().getString(R.string.location_permission_photos), Toast.LENGTH_LONG).show();
            }
        }
        */
    }

    /**
     * 选择图片裁剪
     */
    private void startImagePick() {
        Intent intent;
        if (Build.VERSION.SDK_INT < 19) {
            intent = new Intent();
            intent.setAction(Intent.ACTION_GET_CONTENT);
            intent.setType("image/*");
            startActivityForResult(intent,
                    ImageUtils.REQUEST_CODE_IMAGE_PICK);
        } else {
            intent = new Intent(Intent.ACTION_PICK,
                    Images.Media.EXTERNAL_CONTENT_URI);
            intent.setType("image/*");
            startActivityForResult(intent,
                    ImageUtils.REQUEST_CODE_IMAGE_PICK);
        }

    }

    /**
     * 相机拍照
     */
    private void startActionCameraInternal() {

        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        Uri photoURI;
        try {
            photoURI=this.getCameraTempFile();
            CustomLog.d(TAG,"Photo file is "+photoURI.toString());
        } catch (Exception ex) {
            CustomLog.logException(TAG,ex);
            //getCameraTempFile will show it's own message if any errrors
            return;
        }
        grantPermissionsForUri(this,photoURI,takePictureIntent);

        takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);


        // If you call startActivityForResult() using an intent that no app can handle, your app will crash.
        // So as long as the result is not null, it's safe to use the intent.
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            // Start the image capture intent to take photo
            CustomLog.d(TAG,"taking photo using "+takePictureIntent.toURI());
            startActivityForResult(takePictureIntent, ImageUtils.REQUEST_CODE_IMAGE_FROM_CAMERA);
        } else {
            CustomLog.e(TAG,"Can't take photo using "+takePictureIntent.toURI()+" - nobody can handle this one");
            Toast.makeText(KidsProfileActivity.this, R.string.no_camera,
                    Toast.LENGTH_SHORT).show();
        }
        /*

        ComponentName componentName=takePictureIntent.resolveActivity(getPackageManager());
        if ( componentName!= null) {
            // Start the image capture intent to take photo
            CustomLog.d(TAG,"taking photo using "+Utils.toUri(takePictureIntent)+" and componentName:"+componentName);
            startActivityForResult(takePictureIntent, ImageUtils.REQUEST_CODE_IMAGE_FROM_CAMERA);
        } else {
            CustomLog.e(TAG,"Can't take photo using "+takePictureIntent.toURI()+" - nobody can handle this one");
            Toast.makeText(KidsProfileActivity.this, R.string.no_camera,
                    Toast.LENGTH_SHORT).show();
        }
        */



        /*
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, this.getCameraTempFile());
        startActivityForResult(intent,
                ImageUtils.REQUEST_CODE_IMAGE_FROM_CAMERA);
                */
    }

    /**
     * 拍照后裁剪
     *
     * @param data 原始图片
     *             裁剪后图片
     *
     * Using 'old-style' uri
     */
    /*
    private void startActionCrop(Uri data) {
        CustomLog.d(TAG,"Cropping "+data.toString());
        Intent intent = new Intent("com.android.camera.action.CROP");
        intent.setDataAndType(data, "image/*");
        //intent.putExtra("output", this.getUploadTempFile(data));
        intent.putExtra("output", data);//this.getUploadTempFile(data));
        grantPermissionsForUri(this,data,intent);
        startActionCropInternal(intent);
    }
    */


    private void startActionCropV2(Uri data) {

        Utils.logFreeSpaceOn(TAG,Environment.getExternalStorageDirectory().getPath());

        //TODO:modify code using same logic as in ShowDialogActivity
        Uri newUri=makeUriFromUri(data,DEFAULT_IMAGE_FORMAT);
        CustomLog.d(TAG,"CroppingV2 "+data+",newUri:"+newUri+",tempPreCropUri(should be NULL):"+tempPreCropUri);
        tempPreCropUri=newUri;
        //CustomLog.d(TAG,"CroppingV2 "+data+",newUri:"+newUri+",tempPreCropUri(should be NULL):"+tempPreCropUri);
        Intent intent = new Intent("com.android.camera.action.CROP");
        intent.setDataAndType(newUri, "image/*");
        intent.putExtra("output", newUri);//this.getUploadTempFile(newUri));
        intent.putExtra(Constants.SOURCE_URI,newUri);
        //intent.putExtra("output", data);//this.getUploadTempFile(data));
        //grant permissions
        grantPermissionsForUri(this,newUri,intent);

        startActionCropInternal(intent);
    }

    private void startActionCropInternal(Intent intent) {

        boolean googlePhotosInstalled=isGooglePhotosInstalled(this);
        CustomLog.d(TAG,"GooglePhotos is installed:"+googlePhotosInstalled);

        intent.putExtra("crop", "true");
        intent.putExtra("aspectX", 1);// 裁剪框比例
        intent.putExtra("aspectY", 1);
        intent.putExtra("outputX", CROP);// 输出图片大小
        intent.putExtra("outputY", CROP);
        intent.putExtra("scale", true);// 去黑边
        intent.putExtra("scaleUpIfNeeded", true);// 去黑边 - Go to the black side


        //TODO:what if no app to handle? crash again?

        //This code dublicates PhotoUtils.doCrop. Correct this!
        if (googlePhotosInstalled) {
            intent.setPackage(GOOGLE_PHOTOS_PACKAGE_NAME);
            try {
                CustomLog.d(TAG,"Trying to handle crop using Google Photos. Intent:"+intent.toURI());
                startActivityForResult(intent,
                        ImageUtils.REQUEST_CODE_GETIMAGE_BYSDCARD);
            }  catch (ActivityNotFoundException ex) {
                //this makes NO sense. This should NEVER EVER happen
                CustomLog.logException(TAG,ex);
                Toast.makeText(KidsProfileActivity.this, R.string.error_no_cropper,
                        Toast.LENGTH_LONG).show();
            } catch (Exception ex) {
                CustomLog.logException(TAG,ex);
                Toast.makeText(KidsProfileActivity.this, R.string.error_cropper_google_photos,
                        Toast.LENGTH_LONG).show();
            }


        } else {
            try {
                CustomLog.d(TAG,"Trying to handle crop using regular means because Google Photos was not found. Intent:"+intent.toURI());

                startActivityForResult(intent,
                        ImageUtils.REQUEST_CODE_GETIMAGE_BYSDCARD);

            } catch (ActivityNotFoundException ex) {
                CustomLog.logException(TAG,ex);
                Toast.makeText(KidsProfileActivity.this, R.string.error_no_cropper,
                        Toast.LENGTH_LONG).show();
            } catch (Exception ex) {
                CustomLog.logException(TAG,ex);
                Toast.makeText(KidsProfileActivity.this, R.string.error_cropper,
                        Toast.LENGTH_LONG).show();
            }
        }

    }



    private Uri makeUriFromUri(Uri originalUri,String format) {
        CustomLog.d(TAG,"makeUriFromUri(originalUri="+originalUri.toString()+",format="+format);
        String storageState = Environment.getExternalStorageState();
        if (storageState.equals(Environment.MEDIA_MOUNTED)) {
            File savedir = new File(FILE_SAVEPATH);
            if (!savedir.exists()) {
                savedir.mkdirs();
            }
        } else {

            Toast.makeText(KidsProfileActivity.this, R.string.tip_no_access_to_sdcard,
                    Toast.LENGTH_SHORT).show();

            return null;
        }

        String displayName=getURIDisplayName(this,originalUri);
        File f=new File(displayName);
        final String[] split = f.getPath().split(":");//split the path.
        String ext=null;
        if (split.length>1) {
            ext=split[split.length-1];
        }
        format = StringUtils.isEmpty(ext) ? format : ext;
        CustomLog.d(TAG,".Uri:"+originalUri.toString()+".DisplayName:"+displayName+", splits:"+split+", ext:"+ext+",format:"+format);



        String timeStamp = new SimpleDateFormat("yyyyMMddHHmmss")
                .format(new Date());



        String newFileName = CROP_PREFIX + KidsWatConfig.getDefaultFamilyId() + "."
                + format;
        String resultPath = KidsWatConfig.getTempFilePath() + newFileName;

        if ((AppContext.getInstance().getCurrentFamily()!=null) && (AppContext.getInstance().getCurrentFamily().family_id!=null)) {
            resultPath=String.format("%sbb_%s.jpg",
                    KidsWatConfig.getTempFilePath(), AppContext.getInstance().getCurrentFamily().family_id);
        } else {
            CustomLog.w(TAG,"Failed to get current family. using base name version of file instead");
        }

        /*
        String cropFileName = String.format("%sbb_%s.jpg",
                KidsWatConfig.getTempFilePath(), AppContext.getInstance().getCurrentFamily().family_id);

        //String newFileName = CROP_PREFIX + KidsWatConfig.getDefaultFamilyId() + "."
               // + format;

        String resultPath = cropFileName;//FILE_SAVEPATH + newFileName;
        */
        CustomLog.d(TAG,"resultPath:"+resultPath);



        Utils.logFreeSpaceOn(TAG,resultPath);
        File resultFile = new File(resultPath);
        try {
            InputStream inputStream=getContentResolver().openInputStream(originalUri);
            copyFromStream(inputStream,resultFile);
            //update 'full'(?) image
            //MUST be kept in sync with getBabyImg_v3 in KidsWatUtils. TODO:remove this interdepenciy.
            saveBbImgPath=resultPath;//This line is ABSENT from version in PhotoUtils. TODO: make it universal
            CustomLog.d(TAG,"makeUriFromUri generated new file at "+resultPath+", should update baby image");
            KidsWatUtils.setBabyImagePath(TAG,saveBbImgPath);

        } catch (IOException ex) {
            CustomLog.logException(TAG,ex);
        }

        Uri uri = FileProvider.getUriForFile(this,
                PHOTO_PROVIDER,
                resultFile);

        return uri;
    }



    // 拍照保存的绝对路径
    private Uri getCameraTempFile() {
        String storageState = Environment.getExternalStorageState();
        if (storageState.equals(Environment.MEDIA_MOUNTED)) {
            File savedir = new File(FILE_SAVEPATH);
            if (!savedir.exists()) {
                savedir.mkdirs();
            }
        } else {
            Toast.makeText(KidsProfileActivity.this, "无法保存上传的头像，请检查SD卡是否挂载", // dkzm: aka Unable to save uploaded avatar, please check if SD card is mounted"
                    Toast.LENGTH_SHORT).show();
            return null;
        }

        String timeStamp = new SimpleDateFormat("yyyyMMddHHmmss")
                .format(new Date());
        // 照片命名
        String cropFileName = CAMERA_PREFIX + timeStamp + CAMERA_SUFFIX;

        //keep in sync with KidsWatUtil!
        //String cropFileName = String.format("%sbb_%s.jpg",
        //        KidsWatConfig.getTempFilePath(), AppContext.getInstance().getCurrentFamily().family_id);

        //String cropFileName = "bb_camera_"
        // + AppContext.getInstance().getCurrentFamily().family_id + ".jpg";

        // 裁剪头像的绝对路径
        protraitPath = FILE_SAVEPATH + cropFileName;
        protraitFile = new File(protraitPath);

        cropUri = FileProvider.getUriForFile(this,
                 PHOTO_PROVIDER,
                 protraitFile);

        //cropUri = Uri.fromFile(protraitFile);

        this.origUriLocal=Uri.fromFile(protraitFile);
        this.origUri = this.cropUri;
        this.origPath=protraitPath;
        CustomLog.d(TAG,"getCameraTempFile:OrigUri:"+origUri+", protraitPath:"+protraitPath+", origPath:"+origPath+", origUriLocal:"+origUriLocal);
        return this.cropUri;
    }
/*
    private Uri getUploadTempFile(String thePath) {
        String storageState = Environment.getExternalStorageState();
        if (storageState.equals(Environment.MEDIA_MOUNTED)) {
            File savedir = new File(FILE_SAVEPATH);
            if (!savedir.exists()) {
                savedir.mkdirs();
            }
        } else {
            Toast.makeText(KidsProfileActivity.this, "无法保存上传的头像，请检查SD卡是否挂载",
                    Toast.LENGTH_SHORT).show();
            return null;
        }
        String timeStamp = new SimpleDateFormat("yyyyMMddHHmmss")
                .format(new Date());


        String ext = FileUtil.getFileFormat(thePath);
        ext = StringUtils.isEmpty(ext) ? "jpg" : ext;
        // 照片命名
        // String cropFileName = "ks_crop_" + timeStamp + "." + ext;
        String cropFileName = CROP_PREFIX + KidsWatConfig.getDefaultFamilyId() + "."
                + "jpg";
        // 裁剪头像的绝对路径
        protraitPath = FILE_SAVEPATH + cropFileName;
        saveBbImgPath = protraitPath;
        protraitFile = new File(protraitPath);


        cropUri = FileProvider.getUriForFile(this,
                PHOTO_PROVIDER,
                protraitFile);

        //cropUri = Uri.fromFile(protraitFile);
        return this.cropUri;
    }
    */
    // 裁剪头像的绝对路径

    /*
    private Uri getUploadTempFile(Uri uri) {
        String storageState = Environment.getExternalStorageState();
        if (storageState.equals(Environment.MEDIA_MOUNTED)) {
            File savedir = new File(FILE_SAVEPATH);
            if (!savedir.exists()) {
                savedir.mkdirs();
            }
        } else {
            Toast.makeText(KidsProfileActivity.this, "无法保存上传的头像，请检查SD卡是否挂载",
                    Toast.LENGTH_SHORT).show();
            return null;
        }
        String timeStamp = new SimpleDateFormat("yyyyMMddHHmmss")
                .format(new Date());
        String thePath = ImageUtils.getAbsolutePathFromNoStandardUri(uri);

        // 如果是标准Uri
        if (StringUtils.isEmpty(thePath)) {
            thePath = Utils.getUriRealPath(KidsProfileActivity.this,uri);
            //thePath = ImageUtils.getAbsoluteImagePath(KidsProfileActivity.this,uri);
        }
        String ext = FileUtil.getFileFormat(thePath);
        ext = StringUtils.isEmpty(ext) ? "jpg" : ext;
        // 照片命名
        // String cropFileName = "ks_crop_" + timeStamp + "." + ext;
        String cropFileName = CROP_PREFIX + KidsWatConfig.getDefaultFamilyId() + "."
                + "jpg";
        // 裁剪头像的绝对路径
        protraitPath = FILE_SAVEPATH + cropFileName;
        saveBbImgPath = protraitPath;
        protraitFile = new File(protraitPath);

        cropUri = FileProvider.getUriForFile(this,
                PHOTO_PROVIDER,
                protraitFile);
        //cropUri = Uri.fromFile(protraitFile);
        return this.cropUri;
    }

*/
    @Override
    protected void onResume() {
        super.onResume();
        CustomLog.d(TAG,"onResume()");
    }

    @Override
    protected void onPause() {
        super.onPause();
        CustomLog.d(TAG,"onPause()");
    }

    @Override
    protected void onStart() {
        super.onStart();
        CustomLog.d(TAG,"onStart()");
    }
    @Override
    protected void onStop() {
        super.onStop();
        CustomLog.d(TAG,"onStop()");
    }

    private void update_baby_info() {

        // -----------------
        final String uid = KidsWatConfig.getUserUid();
        final String access_token = KidsWatConfig.getUserToken();
        final String family_id = KidsWatConfig.getDefaultFamilyId();

        // beanForDb___Family f = AppContext.getInstance().getCurrentFamily();

        final String request_url = KidsWatApiUrl.getUrlFor___update_baby_info(
                uid, access_token, family_id, nickname, birthday, sex, height,
                weight, 0);

        ApiHttpClient.get(request_url, new HttpCallBack() {

            @Override
            public void onPreStart() {
                showWaitDialog(getString(R.string.new_setting_fragment_6));
            }

            @Override
            public void onSuccess(String t) {
                super.onSuccess(t);
                CustomLog.d(TAG,String.format("url:%s\nt:%s", request_url, t));

                try {
                    JSONObject response = new JSONObject(t);

                    int code = response.getInt("error");
                    if (code == 0) {
                        SqlDb db = SqlDb.get(KidsProfileActivity.this);
                        int rows = db.updateFamily(family_id, nickname,
                                birthday, sex, height, weight);

                        // 同步更新数据
                        AppContext.getInstance().setCurrentFamily( db
                                .getFamilyBy_fid(family_id));

                        // 同步更新数据
                        AppContext.getInstance().getMapFamilyInfo().put(family_id,
                                AppContext.getInstance().getCurrentFamily());

                        db.closeDb();

                        String birthdayText = SimpleDateUtil.convert2String(
                                birthday * 1000, SimpleDateUtil.DATE_FORMAT);
                        CustomLog.d(TAG,"birthday=" + birthday);
                        textView_birthday.setText(birthdayText);
                        textView_nickname.setText(nickname);

                        AppContext.getInstance().getCurrentFamily().nickname = nickname;
                        AppContext.getInstance().getCurrentFamily().birthday = birthday;
                        AppContext.getInstance().getCurrentFamily().birthdayText = birthdayText;

                        BaseEvent event = new BaseEvent(
                                BaseEvent.MSGTYPE_3___CHANGE_KIDS_NICKNAME,
                                "change_nickname");
                        EventBus.getDefault().post(event);

                    } else {
                        String msg = response.toString();
                        showLongToast(msg);
                    }

                } catch (JSONException e) {
                    CustomLog.logException(TAG,e);
                }

            }

            @Override
            public void onFailure(int errorNo, String strMsg) {
                super.onFailure(errorNo, strMsg);
                if (errorNo == -1) {
                    showLongToast(getString(R.string.check_connection_and_try_again));
                } else {
                    String msg = String.format("%s(error=%s)",
                            errorNo == -1 ? "Connect to the server failed"
                                    : strMsg, errorNo);
                    showLongToast(msg);
                }
            }

            @Override
            public void onFinish() {
                dismissDialog(null, 2);
            }
            // --------------

        });

        // -------------------------

    }

    private void updateRelationShip(final String relation) {

        // -----------------
        final String uid = KidsWatConfig.getUserUid();
        final String access_token = KidsWatConfig.getUserToken();
        final String family_id = KidsWatConfig.getDefaultFamilyId();
        final String version = AppContext.getInstance().getCurrentFamily().version;
        final String device_id = AppContext.getInstance().getCurrentFamily().device_id;

        final String request_url = KidsWatApiUrl.getUrlFor___update_relation(
                uid, access_token, family_id, relation, version, device_id);
        // -------------------------
        ApiHttpClient.get(request_url, new HttpCallBack() {

            @Override
            public void onPreStart() {
                showWaitDialog(getString(R.string.new_setting_fragment_6));
            }

            @Override
            public void onSuccess(String t) {
                super.onSuccess(t);
                CustomLog.d(TAG,String.format("url:%s\nt:%s", request_url, t));

                try {
                    JSONObject response = new JSONObject(t);

                    int code = response.getInt("error");
                    if (code == 0) {
                        textView_relation.setText(relation);
                        SqlDb db = SqlDb.get(KidsProfileActivity.this);
                        db.set___relation_by_uid_fid(uid, family_id, relation);
                        AppContext.getInstance().setLoginUser_kid(db
                                .getLoginMemberBy_uid_fid(uid, family_id));
                        db.closeDb();
                    } else {
                        String message = response.getJSONObject("info")
                                .getString("message");
                        showLongToast(message);
                    }

                } catch (JSONException e) {
                    CustomLog.logException(TAG,e);
                }
            }

            @Override
            public void onFailure(int errorNo, String strMsg) {
                super.onFailure(errorNo, strMsg);
                String msg = String
                        .format("%s(error=%s)",
                                errorNo == -1 ? "Connect to the server failed"
                                        : strMsg, errorNo);
                CustomLog.d(TAG,msg);
                showLongToast(msg);
            }

            @Override
            public void onFinish() {
                dismissDialog(null, 10);
            }
        });
        // -------------------------
    }

    private void add_watch(final String qrcode, String relation) {
        // -----------------
//		new Thread(new ThreadParent(KidsWatApiUrl.getApiUrl(),
//				CommonThreadMethod.JsonFormat(GetStateParameter()), handler,
//				getdevicevoicelevelCode)).start();

        final String uid = KidsWatConfig.getUserUid();
        final String access_token = KidsWatConfig.getUserToken();
        String country_code = "", userPhone = "", version = "", device_country_code = "", device_sim_no = "";
        country_code = KidsWatConfig.getUseCountryCode();
        userPhone = KidsWatConfig.getUserPhone();
        String mobile = KidsWatConfig.getUserPhone();
        if (AppContext.getInstance().getCodess() == 461 || AppContext.getInstance().getCodess() == 5612 || AppContext.getInstance().getCodess() == 5613) {
            if (country != null)
                device_country_code = country;
            if (phone != null)
                device_sim_no = phone;
            version = AppContext.getInstance().getCodess() + "";
        } else if (AppContext.getInstance().getCodess() == 4620 || AppContext.getInstance().getCodess() == 361 || AppContext.getInstance().getCodess() == 601) {
            version = AppContext.getInstance().getCodess() + "";
        }

        Map<String,Object> eventProperties=new HashMap<>();
        eventProperties.put(Constants.EVENT_TYPE,Constants.EVENT_PAIRING_STARTED);
        eventProperties.put(Constants.UID,uid);
        eventProperties.put(Constants.USERPHONE,userPhone);
        eventProperties.put(Constants.DEVICESIM,device_sim_no);

        AnalyticsUtils.reportAnalyticsEvent(Constants.EVENT_PAIRING_STARTED,eventProperties);

        showWaitDialog(getString(R.string.connecting_to_watch_wait));
        //TODO:переписать ВОТ ЭТО нормальным образом. Хотя бы на AsyncTasks (а лучше - Rx/Coroutines!!!)
        new Thread(new ThreadParent(TAG,KidsWatApiUrl.getApiUrl(), CommonThreadMethod.JsonFormat(
                get_add_watch_params(country_code, userPhone, version, device_country_code, device_sim_no, qrcode, relation, mobile)), handler, Constants.PAIRING_MAGIC1)).start();

        // final String family_id = AppContext.getDefaultFamilyId();
//		final String request_url = KidsWatApiUrl.getUrlFor___add_watch(uid,
//				access_token, qrcode, country_code, userPhone, relation,
//				version, device_country_code, device_sim_no);
//
//		ApiHttpClient.get(request_url, new HttpCallBack() {
//
//			@Override
//			public void onPreStart() {
//				showWaitDialog("...Связывание устройства, подождите...");
//			}
//
//			@Override
//			public void onSuccess(String t) {
//				super.onSuccess(t);
//				CustomLog.d(TAG,String.format("url:%s\nt:%s", request_url, t));
//
//				try {
//					JSONObject response = new JSONObject(t);
//					int code = response.getInt("error");
//					if (code == 0) {
//						// --------
//						// {"error":0,"info":{"message":"Successful operation!","device_id":"0000000000989688","family_id":10000001}}
//						String family_id = response.getJSONObject("info")
//								.getString("family_id");
//						// String device_id = response.getJSONObject(
//						// "info").getString("device_id");
//
//						// String nickname = textView_nickname.getText()
//						// .toString().trim();
//						add_baby_info(family_id, img_base64_d);
//						// active_watch(device_id);
//					} else if (code == 1233) {
//						showLongToast("Устройство не сети, попробуйте снова!");
//					}
//					// else if (code == 2015002) {
//					// showLongToast("The device is busy, please try again later");
//					// }
//					else {
//						String message = response.getJSONObject("info")
//								.getString("message");
//						showConfirmInformation(message);
//					}
//
//				} catch (JSONException e) {
//					CustomLog.logException(TAG,e);
//				}
//
//			}
//
//			@Override
//			public void onFailure(int errorNo, String strMsg) {
//				super.onFailure(errorNo, strMsg);
//				if (errorNo == -1) {
//					showLongToast("Please check your connection and try again.");
//				} else {
//					String msg = String.format("%s(error=%s)",
//							errorNo == -1 ? "Connect to the server failed"
//									: strMsg, errorNo);
//					showLongToast(msg);
//				}
//
//				button_ok.setEnabled(true);
//			}
//
//			@Override
//			public void onFinish() {
//				dismissDialog(null, 2);
//			}
//		});
    }

    private Map<String, String> get_add_watch_params(String country_code, String userPhone, String version, String device_country_code, String device_sim_no
            , String qrcode, String relation, String mobile) {

        final String uid = KidsWatConfig.getUserUid();
        final String access_token = KidsWatConfig.getUserToken();
//		String country_code = "", userPhone = "", version = "", device_country_code = "", device_sim_no = "";
//		country_code = KidsWatConfig.getUseCountryCode();
//		userPhone = KidsWatConfig.getUserPhone();
//
//		if (AppContext.getInstance().getCodess() == 461 || AppContext.getInstance().getCodess() == 5612 || AppContext.getInstance().getCodess() == 5613) {
//			if (country != null)
//				device_country_code = country;
//			if (phone != null)
//				device_sim_no = phone;
//			version = AppContext.getInstance().getCodess() + "";
//		} else if(AppContext.getInstance().getCodess() == 4620 || AppContext.getInstance().getCodess() == 361){
//			version = AppContext.getInstance().getCodess() + "";
//		}

//		final String action = "add_watch";
//		Map<String, String> inputParams = new HashMap<String, String>();
//		addExtraParaToMap(action, inputParams);
//		addTokenToMap(inputParams, uid, access_token);
//
//		inputParams.put("country_code", country_code);
//		inputParams.put("qrcode", qrcode);
//		inputParams.put("mobile", mobile);
//		inputParams.put("relation", relation);
//		inputParams.put("version", version);
//
//		inputParams.put("device_country_code", device_country_code);
//		inputParams.put("device_sim_no", device_sim_no);
//		// --------------------
//		return makeURL(getApiUrl(), inputParams);

        Map<String, String> hash = new HashMap<String, String>();
        hash.put("uid", uid);
        hash.put("access_token", access_token);
        hash.put("country_code", country_code);
        hash.put("userPhone", userPhone);
        hash.put("version", version);
        hash.put("mobile", mobile);
        hash.put("customer_no", 1 + "");

        hash.put("relation", relation);
        hash.put("qrcode", qrcode);

        hash.put("device_country_code", device_country_code);
        hash.put("device_sim_no", device_sim_no);
        KidsWatApiUrl.addExtraPara("add_watch", hash);

        return hash;
    }

    private void add_baby_info(String family_id, String imgbase64) {
        // --------------------
        final String uid = KidsWatConfig.getUserUid();
        final String access_token = KidsWatConfig.getUserToken();
        // final String family_id = AppContext.getDefaultFamilyId();

        HttpParams params = new HttpParams();

        CustomLog.d(TAG,"img=" + imgbase64);

        long cur_time = System.currentTimeMillis() / 1000;
        String apitoken = Encoder.encode("MD5", String.valueOf(cur_time)
                + KidsWatApiUrl.api_secret);// SHA-1

        params.put("time", String.valueOf(cur_time));
        params.put("api_token", apitoken);

        params.put("uid", uid);
        params.put("access_token", access_token);

        params.put("family_id", family_id);

        params.put("nickname", nickname);
        params.put("birthday", String.valueOf(birthday));
        params.put("sex", String.valueOf(sex));
        params.put("height", String.valueOf(height));
        params.put("weight", String.valueOf(weight));
        params.put("grade", 0);
        params.put("customer_no", 1);
//		params.put("img", imgbase64);

        File imageFile = new File(saveBbImgPath);
        params.put("img", imageFile);

        final String request_url = KidsWatApiUrl.getApiUrl()
                + "?action=add_baby_info";

        ApiHttpClient.getHttpClient().post(request_url, params, false,
                new HttpCallBack() {

                    @Override
                    public void onPreStart() {
                        CustomLog.d(TAG,"PAring - adding on server");
                        Map<String,Object> eventProperties=new HashMap<>();
                        eventProperties.put(Constants.FAMILIYID,family_id);
                        eventProperties.put(Constants.NICKNAME,nickname);
                        AnalyticsUtils.reportAnalyticsEvent(Constants.EVENT_PAIRING_UPDATING_SERVER_WITH_BABY,eventProperties);

                        showWaitDialog(getString(R.string.connecting_to_watch_wait));
                    }

                    @Override
                    public void onSuccess(String t) {
                        super.onSuccess(t);
                        CustomLog.d(TAG,String.format("url:%s\nt:%s", request_url, t));

                        try {
                            JSONObject response = new JSONObject(t);

                            int code = response.getInt("error");
                            if (code == 0) {
                                String message = response.getJSONObject("info")
                                        .getString("message");
                                CustomLog.d(TAG,message);
                                // showLongToast("add device successfully");
                                dismissDialog(null, 10);

                                DialogInterface.OnClickListener positiveListener = new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog,
                                                        int which) {
                                        if (AppContext.getInstance()
                                                .getFamilyListCount() > 0) {
                                            EventBus.getDefault()
                                                    .post(new BaseEvent(
                                                            BaseEvent.MSGTYPE_1___ADD_KIDS_OK,
                                                            "add baby ok"));

                                            KidsProfileActivity.this.finish();

                                        } else {
                                            // Intent intent = new Intent(
                                            // KidsProfileActivity.this,
                                            // MainActivity.class);
                                            // startActivity(intent);
                                            AppManager
                                                    .AppRestart(KidsProfileActivity.this);
                                            KidsProfileActivity.this.finish();
                                        }
                                    }
                                };
                                String title = getApplicationContext().getResources().getString(R.string.app_name);
                                String messagex = getString(R.string.connected_to_watch_success);
                                String positiveText = "OK";
                                String negativeText = null;

                                Map<String,Object> eventProperties=new HashMap<>();
                                eventProperties.put(Constants.MESSAGE,message);
                                AnalyticsUtils.reportAnalyticsEvent(Constants.EVENT_PAIRING_SUCCESS,eventProperties);


                                showConfirmDialog(title, messagex,
                                        positiveText, negativeText,
                                        positiveListener);

                            } else {
                                // {"error":401,"info":{"message":"Parameter Error!"}}
                                String msg = response.toString();
                                CustomLog.d(TAG,msg);
                                // Toast.makeText(getApplicationContext(), msg,
                                // Toast.LENGTH_SHORT).show();
                                String message = response.getJSONObject("info")
                                        .getString("message");
                                Map<String,Object> eventProperties=new HashMap<>();
                                eventProperties.put(Constants.MESSAGE,message);
                                AnalyticsUtils.reportAnalyticsEvent(Constants.EVENT_PAIRING_POSSIBLE_FAIL,eventProperties);

                                showConfirmInformation(getApplicationContext().getResources().getString(R.string.app_name), message);
                                // showConfirmInformation("Kid's Watcher",
                                // "Pairing fails, please try again.");
                            }

                        } catch (JSONException e) {
                            CustomLog.logException(TAG,e);
                            Map<String,Object> eventProperties=new HashMap<>();

                            AnalyticsUtils.reportAnalyticsEvent(Constants.EVENT_PAIRING_FAILED,eventProperties);

                            showConfirmInformation(getApplicationContext().getResources().getString(R.string.app_name),
                                    getString(R.string.connect_to_watch_failed_try_again));
                        }

                        button_ok.setEnabled(true);
                    }

                    @Override
                    public void onFailure(int errorNo, String strMsg) {
                        super.onFailure(errorNo, strMsg);
                        Map<String,Object> eventProperties=new HashMap<>();
                        eventProperties.put(Constants.MESSAGE,strMsg);

                        AnalyticsUtils.reportAnalyticsEvent(Constants.EVENT_PAIRING_FAILED_SERVER,eventProperties);

                        if (errorNo == -1) {
//							showLongToast("Please check your connection and try again.");
                            showLongToast(getApplicationContext().getResources().getString(R.string.check_conn));
                        } else {
							/*String msg = String
									.format("%s(error=%s)",
											errorNo == -1 ? "Connect to the server failed"
													: strMsg, errorNo);
							showLongToast(msg);*/
                            showLongToast(getApplicationContext().getResources().getString(R.string.check_conn));
                        }
                    }

                    @Override
                    public void onFinish() {
                        dismissDialog(null, 2);
                    }
                });
    }

    // TODO 上传头像
    private void updateBabyImg_v2(final String imgbase64) {
        // --------------------
        final String uid = KidsWatConfig.getUserUid();
        final String access_token = KidsWatConfig.getUserToken();
        final String family_id = KidsWatConfig.getDefaultFamilyId();

        CustomLog.d(TAG,"updateBabyImg_v2, familiy_id:"+family_id);
        HttpParams params = new HttpParams();

        long cur_time = System.currentTimeMillis() / 1000;
        String apitoken = Encoder.encode("MD5", String.valueOf(cur_time)
                + KidsWatApiUrl.api_secret);// SHA-1

        params.put("time", String.valueOf(cur_time));
        params.put("api_token", apitoken);

        params.put("customer_no", 1);//俄罗斯标志
        params.put("uid", uid);
        params.put("access_token", access_token);
        params.put("family_id", family_id);
        File imageFile = new File(saveBbImgPath);
        params.put("img", imageFile);


        final String request_url = KidsWatApiUrl.getApiUrl()
                + "?action=update_baby_img_v2";

        ApiHttpClient.getHttpClient().post(request_url, params, false,
                new HttpCallBack() {

                    @Override
                    public void onPreStart() {
                        showWaitDialog(getString(R.string.new_setting_fragment_6));
                    }

                    @Override
                    public void onSuccess(String t) {
                        super.onSuccess(t);
//							imageFile.delete();
                        CustomLog.d(TAG,String.format(" Baby image was successfully uploaded. url:%s\nt:%s", request_url, t));

                        try {
                            JSONObject response = new JSONObject(t);
                            int code = response.getInt("error");
                            CustomLog.d(TAG,"errorcode=" + code);
                            if (code == 0) {

                                SqlDb db = SqlDb.get(KidsProfileActivity.this);
                                db.saveFamily(family_id, imgbase64,
                                        saveBbImgPath);
                                AppContext.getInstance().setCurrentFamily(db
                                        .getFamilyBy_fid(family_id));
                                db.closeDb();

                                try {
                                    CustomLog.d(TAG,"Loading current image from saveBbImgPath="+saveBbImgPath);
                                    File bbimg = new File(saveBbImgPath);

                                    Bitmap bmp = MediaStore.Images.Media
                                            .getBitmap(KidsProfileActivity.this
                                                    .getContentResolver(), Uri
                                                    .fromFile(bbimg));
                                    imageView_baby.setImageBitmap(bmp);

                                    //
                                    AppContext.getInstance().getCurrentFamily().img_path = saveBbImgPath;
                                    AppContext.getInstance().getMapFamilyInfo().put(family_id, AppContext.getInstance().getCurrentFamily());
                                    BaseEvent event = new BaseEvent(
                                            BaseEvent.MSGTYPE_3___WHITE_SETTING_CHANGE_KIDS_PHOTO,
                                            saveBbImgPath);
                                    EventBus.getDefault().post(event);

                                } catch (FileNotFoundException e) {
                                } catch (IOException e) {
                                } catch (Exception e) {
                                    CustomLog.logException(TAG,e);
                                }
                            }
                        } catch (JSONException e) {
                            CustomLog.logException(TAG,e);
                        }

                        // ------------------
                    }

                    @Override
                    public void onFailure(int errorNo, String strMsg) {
                        super.onFailure(errorNo, strMsg);
                        if (errorNo == -1) {
                            showLongToast("Please check your connection and try again.");
                            CustomLog.e(TAG,"No connection for baby image upload");
                        } else {
                            String msg = String
                                    .format("%s(error=%s)",
                                            errorNo == -1 ? "Connect to the server failed"
                                                    : strMsg, errorNo);
                            CustomLog.e(TAG,msg);
                            showLongToast(msg);
                        }
                    }

                    @Override
                    public void onFinish() {
                        dismissDialog(null, 10);
                    }
                });

    }

    private void updateBabyImg(final String imgbase64) {

        // --------------------
        final String uid = KidsWatConfig.getUserUid();
        final String access_token = KidsWatConfig.getUserToken();
        final String family_id = KidsWatConfig.getDefaultFamilyId();

        HttpParams params = new HttpParams();
        // --------------

        long cur_time = System.currentTimeMillis() / 1000;
        String apitoken = Encoder.encode("MD5", String.valueOf(cur_time)
                + KidsWatApiUrl.api_secret);// SHA-1

        params.put("time", String.valueOf(cur_time));
        params.put("api_token", apitoken);

        params.put("uid", uid);
        params.put("access_token", access_token);
        params.put("family_id", family_id);
        params.put("img", imgbase64);
        params.put("customer_no", 1);//俄罗斯标志

        final String request_url = KidsWatApiUrl.getApiUrl()
                + "?action=update_baby_img";

        ApiHttpClient.getHttpClient().post(request_url, params, false,
                new HttpCallBack() {

                    @Override
                    public void onPreStart() {
                        showWaitDialog(getString(R.string.new_setting_fragment_6));
                    }

                    @Override
                    public void onSuccess(String t) {
                        super.onSuccess(t);
                        CustomLog.d(TAG,String.format("url:%s\nt:%s", request_url, t));

                        try {
                            JSONObject response = new JSONObject(t);
                            int code = response.getInt("error");
                            if (code == 0) {

                                SqlDb db = SqlDb.get(KidsProfileActivity.this);
                                db.saveFamily(family_id, imgbase64,
                                        saveBbImgPath);
                                AppContext.getInstance().setCurrentFamily(
                                        db.getFamilyBy_fid(family_id))
                                ;
                                db.closeDb();
                                try {
                                    File bbimg = new File(saveBbImgPath);

                                    Bitmap bmp = MediaStore.Images.Media
                                            .getBitmap(KidsProfileActivity.this
                                                    .getContentResolver(), Uri
                                                    .fromFile(bbimg));
                                    imageView_baby.setImageBitmap(bmp);

                                    //
                                    BaseEvent event = new BaseEvent(
                                            BaseEvent.MSGTYPE_3___WHITE_SETTING_CHANGE_KIDS_PHOTO,
                                            saveBbImgPath);
                                    EventBus.getDefault().post(event);

                                } catch (FileNotFoundException e) {
                                } catch (IOException e) {
                                } catch (Exception e) {
                                    CustomLog.logException(TAG,e);
                                }
                            }
                        } catch (JSONException e) {
                            CustomLog.logException(TAG,e);
                        }

                        // ------------------

                    }

                    @Override
                    public void onFailure(int errorNo, String strMsg) {
                        super.onFailure(errorNo, strMsg);
                        if (errorNo == -1) {
                            showLongToast("Please check your connection and try again.");
                        } else {
                            String msg = String
                                    .format("%s(error=%s)",
                                            errorNo == -1 ? "Connect to the server failed"
                                                    : strMsg, errorNo);
                            showLongToast(msg);
                        }
                    }

                    @Override
                    public void onFinish() {
                        dismissDialog(null, 10);
                    }
                });

    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            finish();
        }
        return false;
    }
}
