package com.qiwo.xkidswatcher_russia.ui;

import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.qiwo.xkidswatcher_russia.R;
import com.qiwo.xkidswatcher_russia.api.ApiHttpClient;
import com.qiwo.xkidswatcher_russia.api.remote.KidsWatApiUrl;
import com.qiwo.xkidswatcher_russia.base.BaseActivity;
import net.intari.CustomLogger.CustomLog;
import com.qiwo.xkidswatcher_russia.widget.EditTextWithDel;

import org.json.JSONException;
import org.json.JSONObject;
import org.kymjs.kjframe.http.HttpCallBack;

import butterknife.BindView;;

public class ForgetPasswordActivity extends BaseActivity {

    private static final int COUNT_INTERVAL = 1000;
    private static final int COUNT_TOTAL = 120 * 1000;
    final String TAG = ForgetPasswordActivity.class.getSimpleName();

    @BindView(R.id.button_next)
    Button button_next;

    // @BindView(R.id.username)
    // EditText user_mobileno;
    @BindView(R.id.send_sms_again)
    Button send_sms_again;
    @BindView(R.id.smscode)
    EditText smscode_text;
    @BindView(R.id.password_1_tv)
    EditTextWithDel password_1_text;
    @BindView(R.id.password_2_tv)
    EditTextWithDel password_2_text;
    @BindView(R.id.back_imageview)
    ImageView back_imageview;
    @BindView(R.id.textView_mobile)
    TextView textView_mobile;
    String country_code;
    // -----------
    String mobile;
    private String password;
    private TimeCount time;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_forget_password;
    }

    @Override
    protected boolean hasActionBar() {
        return false;
    }

    @Override
    protected void init(Bundle savedInstanceState) {
        super.init(savedInstanceState);
    }

    @Override
    public void initView() {
        country_code = getIntent().getStringExtra("country_code");
        mobile = getIntent().getStringExtra("mobile");

        textView_mobile.setText(mobile == null ? "" : "+" + country_code + " "
                + mobile);

        back_imageview.setOnClickListener(this);
        button_next.setOnClickListener(this);

        send_sms_again.setOnClickListener(this);
    }

    @Override
    public void initData() {
        if (mobile.length() > 0) {
            SendSMS_CoundDown(mobile);
        } else {
            // Verify Code is empty
            showConfirmInformation(null, getApplicationContext().getResources().getString(R.string.text_phone_empty));
        }
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.back_imageview:
                ForgetPasswordActivity.this.finish();
                break;

            case R.id.send_sms_again:

                // String mobileno = user_mobileno.getText().toString().trim();
                if (mobile.length() > 0) {
                    SendSMS_CoundDown(mobile);
                } else {
                    // Toast.makeText(getApplicationContext(),
                    // "please input mobile.",
                    // Toast.LENGTH_SHORT).show();
                    showConfirmInformation(null, getApplicationContext().getResources().getString(R.string.text_phone_empty));
                }

                break;
            case R.id.button_next:

                String smscode = smscode_text.getText().toString().trim();
                password = password_1_text.getText().toString();
                String password2 = password_2_text.getText().toString();
                if (smscode.length() == 0) {
                    showConfirmInformation(null, getApplicationContext().getResources().getString(R.string.text_verify_code_empty));
                    return;
                    // Toast.makeText(ForgetPasswordActivity.this,
                    // "pls input sms code and password! ", Toast.LENGTH_SHORT)
                    // .show();
                }
                if (password.length() < 6 || password.length() > 20) {
                    showConfirmInformation(null,
                            getApplicationContext().getResources().getString(R.string.text_password_empty));
                    return;
                    // Toast.makeText(ForgetPasswordActivity.this,
                    // "pls input sms code and password! ", Toast.LENGTH_SHORT)
                    // .show();
                }

                Log.d(TAG, "smdcode=" + smscode);
                Log.d(TAG, "password=" + password);
                if (password.equals(password2)) {
                    validate_code(country_code, mobile, smscode);
                } else {
                    showConfirmInformation(null, getApplicationContext().getResources().getString(R.string.text_pwd_not_match));
                    // Toast.makeText(ForgetPasswordActivity.this,
                    // "Password not match! ", Toast.LENGTH_SHORT).show();
                }

                break;
            default:
                break;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        CustomLog.d(TAG,"--------onDestroy--------");
        time.cancel();
        ApiHttpClient.cancelAll();
    }

    // -----------------

    /*
     * countdown
     */
    public void SendSMS_CoundDown(String mobile) {
        time = new TimeCount(COUNT_TOTAL, COUNT_INTERVAL);// 构造CountDownTimer对象
        time.start();
        requre_sms_code(country_code, mobile);
    }

    // ----------------
    private void requre_sms_code(String country_code, String phone) {
        // -----------------
        final String request_url = KidsWatApiUrl.getUrlFor___requre_sms_code(
                country_code, phone);

        ApiHttpClient.get(request_url, new HttpCallBack() {

            @Override
            public void onPreStart() {
                showWaitDialog(getString(R.string.new_setting_fragment_6));
            }

            @Override
            public void onSuccess(String t) {
                super.onSuccess(t);
                CustomLog.d(TAG,"requre_sms_code.response=" + t);
                try {
                    JSONObject response = new JSONObject(t);

                    int code = response.getInt("error");
                    if (code == 0) {
                        password = password_1_text.getText().toString();
                        Log.d(TAG, "password=" + password);
                        // reset_pwd(mobile, password);
                    } else {
                        // {"error":400,"info":{"message":"Parameter Error!"}}
                        String message = response.getJSONObject("info")
                                .getString("message");
                        Toast.makeText(getApplicationContext(), message,
                                Toast.LENGTH_SHORT).show();
                    }

                } catch (JSONException e) {
                    CustomLog.logException(TAG,e);
                }

            }

            @Override
            public void onFailure(int errorNo, String strMsg) {
                super.onFailure(errorNo, strMsg);
                String msg = String.format("errorNo:%s\n%s", errorNo, strMsg);
                CustomLog.d(TAG,"error=" + msg);
                Toast.makeText(ForgetPasswordActivity.this, msg,
                        Toast.LENGTH_LONG).show();
            }

            @Override
            public void onFinish() {
                dismissDialog(null, 10);
            }
        });

    }

    private void validate_code(final String country_code, String phone,
                               String code) {
        // -----------------
        final String request_url = KidsWatApiUrl.getUrlFor___validate_code(
                country_code, phone, code);

        ApiHttpClient.get(request_url, new HttpCallBack() {

            @Override
            public void onPreStart() {
                showWaitDialog(getString(R.string.new_setting_fragment_6));
            }

            @Override
            public void onSuccess(String t) {
                super.onSuccess(t);
                CustomLog.d(TAG,"validate_code.response=" + t);
                try {
                    JSONObject response = new JSONObject(t);

                    int code = response.getInt("error");
                    if (code == 0) {
                        password = password_1_text.getText().toString();
                        Log.d(TAG, "password=" + password);
                        reset_pwd(country_code, mobile, password);
                    } else {
                        // {"error":400,"info":{"message":"Parameter Error!"}}
                        // String message = response.getJSONObject("info")
                        // .getString("message");
                        // showConfirmInformation(null, message);
                        showConfirmInformation(null,
                                getApplicationContext().getResources().getString(R.string.text_verify_code_wrong));
                    }

                } catch (JSONException e) {
                    CustomLog.logException(TAG,e);
                }

            }

            @Override
            public void onFailure(int errorNo, String strMsg) {
                super.onFailure(errorNo, strMsg);
                String msg = String.format("errorNo:%s\n%s", errorNo, strMsg);
                CustomLog.d(TAG,"error=" + msg);
                Toast.makeText(ForgetPasswordActivity.this, msg,
                        Toast.LENGTH_LONG).show();
            }

            @Override
            public void onFinish() {
                dismissDialog(null, 10);
            }
        });

    }

    // ----------------
    private void reset_pwd(String country_code, String mobile, String password) {
        final String request_url = KidsWatApiUrl.getUrlFor___reset_pwd(
                country_code, mobile, password);
        // -------------------------

        ApiHttpClient.get(request_url, new HttpCallBack() {

            @Override
            public void onPreStart() {
                showWaitDialog(getString(R.string.new_setting_fragment_6));
            }

            @Override
            public void onSuccess(String t) {
                super.onSuccess(t);
                CustomLog.d(TAG,"reset_pwd.response=" + t);
                try {
                    JSONObject response = new JSONObject(t);

                    int code = response.getInt("error");
                    if (code == 0) {
                        Toast.makeText(ForgetPasswordActivity.this,
                                getApplicationContext().getResources().getString(R.string.text_pwd_modify_ok), Toast.LENGTH_SHORT)
                                .show();
                        ForgetPasswordActivity.this.finish();
                    } else {
                        // {"error":400,"info":{"message":"Parameter Error!"}}
                        String message = response.getJSONObject("info")
                                .getString("message");
                        Toast.makeText(getApplicationContext(), message,
                                Toast.LENGTH_SHORT).show();
                    }

                } catch (JSONException e) {
                    CustomLog.logException(TAG,e);
                }

            }

            @Override
            public void onFailure(int errorNo, String strMsg) {
                super.onFailure(errorNo, strMsg);
                String msg = String.format("errorNo:%s\n%s", errorNo, strMsg);
                CustomLog.d(TAG,"error=" + msg);
                Toast.makeText(ForgetPasswordActivity.this, msg,
                        Toast.LENGTH_LONG).show();
            }

            @Override
            public void onFinish() {
                dismissDialog(null, 10);
            }
        });

    }

    /**
     * countdown
     */

    public class TimeCount extends CountDownTimer {
        // Parameters of total length, and time interval
        public TimeCount(long millisInFuture, long countDownInterval) {
            super(millisInFuture, countDownInterval);
            // send_sms_again.setTextColor(getResources().getColor(
            // R.color.login_bg));
            send_sms_again.setEnabled(false);
            send_sms_again.setBackgroundResource(R.drawable.editview_press_bg);
        }

        @Override
        public void onTick(long millisUntilFinished) {
            send_sms_again.setText((millisUntilFinished / 1000) + " s");
        }

        @Override
        public void onFinish() {
            send_sms_again.setText(getResources()
                    .getString(R.string.sent_again));
            // send_sms_again.setTextColor(Color.WHITE);
            send_sms_again.setEnabled(true);
            send_sms_again.setBackgroundResource(R.drawable.editview_bg);
        }
    }

    // -------------

    // ----------------

    // ----------------

}
