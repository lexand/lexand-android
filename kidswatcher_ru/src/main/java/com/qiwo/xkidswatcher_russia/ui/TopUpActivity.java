package com.qiwo.xkidswatcher_russia.ui;

import android.content.Intent;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.android.trivialdrivesample.IabHelper;
import com.example.android.trivialdrivesample.IabResult;
import com.example.android.trivialdrivesample.Inventory;
import com.example.android.trivialdrivesample.Purchase;
import com.example.android.trivialdrivesample.SkuDetails;
import com.qiwo.xkidswatcher_russia.AppContext;
import com.qiwo.xkidswatcher_russia.KidsWatConfig;
import com.qiwo.xkidswatcher_russia.R;
import com.qiwo.xkidswatcher_russia.api.ApiHttpClient;
import com.qiwo.xkidswatcher_russia.api.remote.KidsWatApiUrl;
import com.qiwo.xkidswatcher_russia.base.BaseActivity;
import com.qiwo.xkidswatcher_russia.bean.beanForDb___Family;
import com.qiwo.xkidswatcher_russia.bean.beanForDb___GBilling;
import com.qiwo.xkidswatcher_russia.db.SqlDb;
import com.qiwo.xkidswatcher_russia.util.KidsWatUtils;
import net.intari.CustomLogger.CustomLog;
import com.qiwo.xkidswatcher_russia.widget.DividerItemDecoration;
import com.qiwo.xkidswatcher_russia.widget.WrapLinearLayoutManager;

import org.json.JSONException;
import org.json.JSONObject;
import org.kymjs.kjframe.http.HttpCallBack;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import butterknife.BindView;;

/**
 * @author Administrator
 */
public class TopUpActivity extends BaseActivity {

    final String TAG = TopUpActivity.class.getSimpleName();

    // ----------------
    static final String SKU_6MONTH_SERVICE = "com.qiwo.kidswatcher.service.6month";
    static final String SKU_1YEAR_SERVICE = "com.qiwo.kidswatcher.service.1year";
    // (arbitrary) request code for the purchase flow
    static final int RC_REQUEST = 10001;
    @BindView(R.id.linearLayout_l)
    LinearLayout linearLayout_l;
    @BindView(R.id.recyclerview)
    RecyclerView recyclerView;
    @BindView(R.id.button_recharge)
    Button button_recharge;
    @BindView(R.id.textView_recharge_tip)
    TextView textView_recharge_tip;
    LinearLayoutManager layoutManager;
    List<RecycleItem> datas;
    StRecycleAdapter mAdapter;
    String select_SKU = "";
    boolean getPriceOkFromGooglePlay = false;
    // The helper object
    IabHelper mHelper;
    Inventory mInventorys;
    // Listener that's called when we finish querying the items we own
    IabHelper.QueryInventoryFinishedListener mGotInventoryListener = new IabHelper.QueryInventoryFinishedListener() {
        public void onQueryInventoryFinished(IabResult result,
                                             Inventory inventory) {
            CustomLog.d(TAG,"Query inventory finished.");
            if (result.isFailure()) {
                showError();
                // complain("Failed to query inventory: " + result);
                return;
            }

            if (inventory != null) {
                getPriceOkFromGooglePlay = true;
                mInventorys = inventory;
                SkuDetails details_price_1year = inventory
                        .getSkuDetails(SKU_1YEAR_SERVICE);
                if (details_price_1year != null) {
                    String price_1year = details_price_1year.getPrice();
                    setPrice(1, 1, price_1year + "/Year");
                    CustomLog.d(TAG,"one year service Price" + price_1year);

                } else {
                    showError();
                    Toast.makeText(TopUpActivity.this,
                            " inventory.getSkuDetails is null",
                            Toast.LENGTH_SHORT).show();
                }

                SkuDetails details_price_6month = inventory
                        .getSkuDetails(SKU_6MONTH_SERVICE);
                if (details_price_6month != null) {
                    String price_6month = details_price_6month.getPrice();
                    setPrice(1, 2, price_6month + "/6Month");
                    CustomLog.d(TAG,"6 month Price" + price_6month);

                } else {
                    showError();
                    Toast.makeText(TopUpActivity.this,
                            " inventory.getSkuDetails is null",
                            Toast.LENGTH_SHORT).show();
                }

                mAdapter.notifyDataSetChanged();

                // String servicePay_6month = inventory
                // .getSkuDetails(SKU_6MONTH_SERVICE).getPrice();

                // CustomLog.d(TAG,"bananaPrice" + servicePay_6month);
            } else {
                showError();
            }

        }
    };
    // Called when consumption is complete
    IabHelper.OnConsumeFinishedListener mConsumeFinishedListener = new IabHelper.OnConsumeFinishedListener() {
        public void onConsumeFinished(Purchase purchase, IabResult result) {
            CustomLog.d(TAG,"Consumption finished. Purchase: " + purchase
                    + ", result: " + result);

            // sku, you probably should check...
            if (result.isSuccess()) {
                SkuDetails selectSkuDetails = mInventorys
                        .getSkuDetails(select_SKU);
                // String uid =
                // ApplicationHelper.getInstance().getCurLoginUid();
                String family_id = KidsWatConfig.getDefaultFamilyId();
                final String device_id = KidsWatConfig.getUserDeviceId();
                String policy_sequence = "com.cyberblue.KidsWatcher.EU.price0";

                // if (select_SKU.equalsIgnoreCase(SKU_6MONTH_SERVICE)) {
                // policy_sequence = "com.cyberblue.KidsWatcher.EU.price0";
                // } else if (select_SKU.equalsIgnoreCase(SKU_1YEAR_SERVICE)) {
                // policy_sequence = "com.cyberblue.KidsWatcher.EU.price1";
                // }
                policy_sequence = select_SKU;

                String fullPrice = selectSkuDetails.getPrice();
                CustomLog.d(TAG,purchase.getOriginalJson() + "\n----------------");
                SqlDb db = SqlDb.get(TopUpActivity.this);
                db.saveGoogleBilling(device_id, family_id, fullPrice,
                        purchase.getOrderId(), purchase.getOriginalJson(),
                        policy_sequence);
                db.closeDb();
                // final String device_id, String policy_sequence,
                // String currency_country_code, String currency_symbol, String
                // price,String order_id
                String currency_country_code = fullPrice.substring(0, 0 + 2);
                String currency_symbol = fullPrice.substring(2, 3);
                String price = fullPrice.substring(3);

                charge_money_of_app(device_id, policy_sequence,
                        currency_country_code, currency_symbol, price,
                        purchase.getOrderId());
                // savePayResult(purchase.getOriginalJson());
                // successfully consumed, so we apply the effects of the item in
                // our
                // game world's logic, which in our case means filling the gas
                // tank a bit
                CustomLog.d(TAG,"Consumption successful. Provisioning.");
            } else {
                complain("Error while consuming: " + result);
            }
            CustomLog.d(TAG,"End consumption flow.");
        }
    };

    // reminder
    // please select a product
    // ok
    IabHelper.OnIabPurchaseFinishedListener mPurchaseFinishedListener = new IabHelper.OnIabPurchaseFinishedListener() {
        public void onIabPurchaseFinished(IabResult result, Purchase purchase) {
            CustomLog.d(TAG,"Purchase finished: " + result + ", purchase: " + purchase);
            if (result.isFailure()) {
                // Oh noes!
                return;
            }

            CustomLog.d(TAG,"Purchase successful.");

            // if (purchase.getSku().equals(SKU_1YEAR_SERVICE)
            // || purchase.getSku().equals(SKU_6MONTH_SERVICE)) {
            CustomLog.d(TAG,"JSON=" + purchase.getOriginalJson());

            // bought 1/4 tank of gas. So consume it.
            mHelper.consumeAsync(purchase, mConsumeFinishedListener);
            // }
        }
    };
    // -------------
    private OnItemClickListener recyleItemListener = new OnItemClickListener() {

        @Override
        public void onItemClick(View v, int position, Object object) {
            String tag = v.getTag() != null ? v.getTag().toString() : "";
            CustomLog.d(TAG,"tag=" + tag);
            RecycleItem bean = (RecycleItem) object;
            CustomLog.d(TAG,"Bean type:"+bean.type + ", title:"+ bean.title);
            for (RecycleItem item : datas) {
                if (item.type == bean.type) {
                    item.selectedStatus = tag.equalsIgnoreCase("b1") ? 1 : 2;
                } else {
                    item.selectedStatus = 0;
                }
            }
            // ------设置当前选择项------
            if (bean.type == 1) {
                try {
                    Date utcExpiredDate = new java.text.SimpleDateFormat(
                            "yyyy-MM-dd").parse(bean.expiredText);

                    // .parse(bean.expiredText);
                    if (tag.equalsIgnoreCase("b1")) {
                        select_SKU = SKU_1YEAR_SERVICE;
                        Calendar cd = Calendar.getInstance();
                        cd.setTime(utcExpiredDate);
                        // calendar.add(Calendar.WEEK_OF_YEAR, -1);
                        cd.add(Calendar.YEAR, +1);
                        String newExpiredDateText = new java.text.SimpleDateFormat(
                                "yyyy-MM-dd").format(new java.util.Date(cd
                                .getTimeInMillis()));
                        textView_recharge_tip
                                .setText("After top-up, the service expiration day will be "
                                        + newExpiredDateText);
                    } else if (tag.equalsIgnoreCase("b2")) {
                        select_SKU = SKU_6MONTH_SERVICE;
                        Calendar cd = Calendar.getInstance();
                        cd.setTime(utcExpiredDate);
                        // calendar.add(Calendar.WEEK_OF_YEAR, -1);
                        cd.add(Calendar.MONTH, +6);
                        String newExpiredDateText = new java.text.SimpleDateFormat(
                                "yyyy-MM-dd").format(new java.util.Date(cd
                                .getTimeInMillis()));
                        textView_recharge_tip
                                .setText("After top-up, the service expiration day will be "
                                        + newExpiredDateText);
                    } else {
                        select_SKU = "";
                        textView_recharge_tip.setText("");
                    }

                } catch (ParseException e) {
                    // TODO Auto-generated catch block
                    CustomLog.logException(TAG,e);
                }
            } else if (bean.type == 2) {
                if (tag.equalsIgnoreCase("b1")) {
                    select_SKU = SKU_1YEAR_SERVICE + "xx";
                } else if (tag.equalsIgnoreCase("b2")) {
                    select_SKU = SKU_6MONTH_SERVICE + "xxx";
                } else {
                    select_SKU = "";
                    textView_recharge_tip.setText("");
                }
            } else {
                select_SKU = "";
            }
            // --------------
            mAdapter.notifyDataSetChanged();
            // ---------------
        }

    };

    @Override
    protected int getLayoutId() {
        return R.layout.activity_top_up;
    }

    @Override
    protected boolean hasActionBar() {
        return false;
    }

    @Override
    protected void init(Bundle savedInstanceState) {
        super.init(savedInstanceState);
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.linearLayout_l:
                finish();
                break;
            case R.id.button_recharge:
                if (getPriceOkFromGooglePlay && select_SKU.length() > 2) {
                    mHelper.launchPurchaseFlow(TopUpActivity.this, select_SKU,
                            RC_REQUEST, mPurchaseFinishedListener);
                } else {
                    showConfirmInformation(getString(R.string.top_up_1));
                }
                break;
            default:
                break;
        }
    }

    @Override
    public void initView() {
        // linearLayout_l.setOnClickListener(this);
        linearLayout_l.setOnClickListener(this);
        button_recharge.setOnClickListener(this);
        // -------
    }

    @Override
    public void initData() {
        // int _type, int _drawableId, String _title,String _des, boolean
        // _selected
        SqlDb db = SqlDb.get(this);
        beanForDb___Family bb = db.getFamilyBy_fid(KidsWatConfig
                .getDefaultFamilyId());
        db.closeDb();
        String qrcode = bb.qrcode;
        // AppContext.getInstance().getCurrentFamily().qrcode;
        get_device_validate();
        get_charge_package_policy(qrcode);
        String expiredDate = AppContext.getInstance().getCurrentFamily().valid_date;
        datas = new ArrayList<RecycleItem>();
        datas.add(new RecycleItem(1, getString(R.string.top_up_2), expiredDate, "Nn/Year",
                "Nn/6Month", 0));
        // datas.add(new RecycleItem(6, "Basic Service", expiredDate, "Nn/Year",
        // "Nn/6Month", 0));
        if (AppContext.getInstance().getCurrentFamily().version
                .equalsIgnoreCase("362")) {
            datas.add(new RecycleItem(2, getString(R.string.top_up_3),
                    "Service Expired:2015-08-20", "$10/1800", "$20/3000", 0));
        }

        // ---------------
        // layoutManager = new WrapLinearLayoutManager(this);
        recyclerView.setHasFixedSize(true);
        layoutManager = new WrapLinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);// 设置布局管理器
        mAdapter = new StRecycleAdapter(datas);
        mAdapter.setOnItemClickListener(recyleItemListener);
        recyclerView.addItemDecoration(new DividerItemDecoration(this, null));
        recyclerView.setAdapter(mAdapter);// 设置适配器
        // ------------
        String base64EncodedPublicKey = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAoKq/PZd+rfx8fhLTyRh4MpvUVzIT1U/Q+thaMcY5OA9VRUPs8TDQqsM5d/655mwqXywspC0VSCStumgbqUTiE9kl5PEHP+iMeDtXZ96pAXh7y80dpP8r64191O3fZ1cvkole0aPG9RYvdtI/f/vFu36utQSZwBgdA+gRisQ3h9t/h8UfUtbPcPg6zSMFjeT5LMojgnlNCpJ4eDxnp/gsMavQ+f7mQsMqJj/e3funRzq3X7fjIpz1KDGkdkiwGnmQ8X3e9k2MbCKxhOLPwHii7Efx2a87b/PzGVcgUDOYbI9tNsPtNr35NaTZWOApl1Bkkuaa4g5tN2c9QITvdxyrswIDAQAB";// 此处请填写你应用的appkey
        mHelper = new IabHelper(this, base64EncodedPublicKey);
        // enable debug logging (for a production application, you should set
        // this to false).
        mHelper.enableDebugLogging(true);

        // Start setup. This is asynchronous and the specified listener
        // will be called once setup completes.
        mHelper.startSetup(new IabHelper.OnIabSetupFinishedListener() {
            public void onIabSetupFinished(IabResult result) {

                if (!result.isSuccess()) {
                    // Oh noes, there was a problem.
                    complain(getString(R.string.top_up_4) + result);
                    return;
                }

                // Hooray, IAB is fully set up. Now, let's get an inventory of
                // stuff we own.
                CustomLog.d(TAG,"Setup successful. Querying inventory.");
                // mHelper.queryInventoryAsync(mGotInventoryListener);
                // getPrice();
            }
        });

        postFailedOrder();
    }

    // 获取价格
    private void getPrice() {
        List<String> additionalSkuList = new ArrayList<String>();
        additionalSkuList.add(SKU_1YEAR_SERVICE);
        additionalSkuList.add(SKU_6MONTH_SERVICE);
        // additionalSkuList.add(SKU_TESTPAY_SERVICE);
        try {
            mHelper.queryInventoryAsync(true, additionalSkuList,
                    mGotInventoryListener);
        } catch (Exception e) {
            CustomLog.logException(TAG,e);
            // Toast.makeText(TopUpActivity.this, "", duration)
            CustomLog.e(TAG,"拉取价格失败！ - Pulling price failed!");
        }
    }

    private void showError() {
        showShortToast(getString(R.string.top_up_5));
    }

    void complain(String message) {
        CustomLog.d(TAG,"**** TrivialDrive Error: " + message);
        // alert("Error: " + message);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        CustomLog.d(TAG,"onActivityResult(" + requestCode + "," + resultCode + ","
                + data);

        // Pass on the activity result to the helper for handling
        if (!mHelper.handleActivityResult(requestCode, resultCode, data)) {
            // not handled, so handle it ourselves (here's where you'd
            // perform any handling of activity results not related to in-app
            // billing...
            super.onActivityResult(requestCode, resultCode, data);
        } else {
            CustomLog.d(TAG,"onActivityResult handled by IABUtil.");
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        ApiHttpClient.cancelAll();
    }

    /**
     * @param type      1为basic service,2为Premium Service
     * @param t         ,1为第一栏,2为第二栏
     * @param priceText ,价钱显示文本
     */
    private void setPrice(int type, int t, String priceText) {
        for (RecycleItem item : datas) {
            if (item.type == type) {
                if (t == 1) {
                    item.btnText_1 = priceText;
                } else {
                    item.btnText_2 = priceText;
                }
            }
        }
    }

    private void postFailedOrder() {
        SqlDb db = SqlDb.get(this);
        String device_id = KidsWatConfig.getUserDeviceId();
        List<beanForDb___GBilling> l = db.getFailedGoogleBilling(device_id);
        db.closeDb();
        // final String device_id, String policy_sequence,
        // String currency_country_code, String currency_symbol, String
        // price,String order_id
        if (l.size() == 0)
            return;
        for (beanForDb___GBilling m : l) {
            String fullPrice = m.price;
            String policy_sequence = m.policy_sequence;
            String orderId = m.orderId;

            String currency_country_code = fullPrice.substring(0, 0 + 2);
            String currency_symbol = fullPrice.substring(2, 3);
            String price = fullPrice.substring(3);

            KidsWatUtils.charge_money_of_app(device_id, policy_sequence,
                    currency_country_code, currency_symbol, price, orderId);

        }
    }

    void charge_money_of_app(final String device_id, String policy_sequence,
                             String currency_country_code, String currency_symbol, String price,
                             final String order_id) {

        // --------------------
        final String uid = KidsWatConfig.getUserUid();
        final String access_token = KidsWatConfig.getUserToken();
        // final String family_id = KidsWatConfig.getDefaultFamilyId();
        // String device_id = AppContext.getInstance().getProperty(
        // "user.device_id");

        final String request_url = KidsWatApiUrl
                .getUrlFor___charge_money_of_app_v2(uid, access_token,
                        device_id, policy_sequence, currency_country_code,
                        currency_symbol, price, order_id);
        // -------------------------

        ApiHttpClient.get(request_url, new HttpCallBack() {

            @Override
            public void onPreStart() {
                showWaitDialog(getString(R.string.new_setting_fragment_6));
            }

            @Override
            public void onSuccess(String t) {
                super.onSuccess(t);
                String responseText = String.format("url:%s\nt:%s",
                        request_url, t);
                CustomLog.d(TAG,responseText);
                KidsWatUtils.logDebugTxt(responseText);
                try {
                    JSONObject response = new JSONObject(t);

                    int code = response.getInt("error");
                    if (code == 0) {
                        String valid_date = response.getJSONObject("info")
                                .getString("valid_date");
                        SqlDb db = SqlDb.get(TopUpActivity.this);
                        db.saveFamilyValid_date(device_id, valid_date);
                        db.setGoogleBillingUploadedBy_did(device_id, order_id);
                        db.closeDb();

                        for (RecycleItem item : datas) {
                            if (item.type == 1) {
                                item.expiredText = valid_date;
                            }
                        }
                        mAdapter.notifyDataSetChanged();
                        // textView_valid_date.setText(valid_date);

                    } else if (code == -1) {
                        String message = "Watch is not shipped.";
                        showLongToast(message);
                    } else {
                        String message = "charge money error.";
                        showLongToast(message);
                    }

                } catch (JSONException e) {
                    CustomLog.logException(TAG,e);
                    showLongToast("charge money error!");
                }

            }

            @Override
            public void onFailure(int errorNo, String strMsg) {
                super.onFailure(errorNo, strMsg);
                String msg = String.format("errorNo:%s\n%s", errorNo, strMsg);
                CustomLog.d(TAG,"error="+msg);
                //CustomLog.d(TAG,"error=" + msg);
                showLongToast(msg);
            }

            @Override
            public void onFinish() {
                dismissDialog(null, 10);
            }
        });

    }

    private void get_charge_package_policy(String qrcode) {

        final String uid = KidsWatConfig.getUserUid();
        final String access_token = KidsWatConfig.getUserToken();
        final String device_id = KidsWatConfig.getUserDeviceId();

        final String req_url = KidsWatApiUrl
                .getUrlFor___get_charge_package_policy(uid, access_token,
                        device_id, qrcode);

        ApiHttpClient.get(req_url, new HttpCallBack() {

            @Override
            public void onSuccess(String t) {
                super.onSuccess(t);
                String responseText = String.format("url:%s\nt:%s", req_url, t);
                String deviceVersion = AppContext.getInstance().getCurrentFamily().version;
                CustomLog.d(TAG,"device version=" + deviceVersion);
                CustomLog.d(TAG,responseText);
                KidsWatUtils.logDebugTxt("device_version=" + deviceVersion
                        + "\n" + responseText);

                try {
                    JSONObject response = new JSONObject(t);

                    int code = response.getInt("error");
                    if (code == 0) {
                        getPrice();
                    } else {
                        // String message = "charge money error.";
                        // showLongToast(message);
                        // :{"error":-2,"info":{"message":"the orderNumber of qrcode do not match the orderNumber list of shipment!"}}
                        String msg = response.getJSONObject("info").getString(
                                "message");
                        showConfirmInformation(null, msg);
                    }

                } catch (JSONException e) {
                    CustomLog.logException(TAG,e);
                    showLongToast("charge money error!");
                }
            }

            @Override
            public void onFailure(int errorNo, String strMsg) {
                super.onFailure(errorNo, strMsg);
                String xmsg = String.format("errorNo:%s\n%s", errorNo, strMsg);
                CustomLog.d(TAG,String.format("url:%s\nt:%s", req_url, xmsg));
            }

        });

    }

    // ----------------

    private void get_device_validate() {

        final String uid = KidsWatConfig.getUserUid();
        final String access_token = KidsWatConfig.getUserToken();
        final String device_id = KidsWatConfig.getUserDeviceId();

        final String req_url = KidsWatApiUrl.getUrlFor___get_device_validate(
                uid, access_token, device_id);

        ApiHttpClient.get(req_url, new HttpCallBack() {

            @Override
            public void onSuccess(String t) {
                super.onSuccess(t);
                CustomLog.d(TAG,String.format("url:%s\nt:%s", req_url, t));

                try {
                    JSONObject jso = new JSONObject(t);
                    int code = jso.getInt("error");
                    if (code == 0) {

                        String valid_date;
                        valid_date = jso.getJSONObject("info").getString(
                                "valid_date");

                        for (RecycleItem item : datas) {
                            if (item.type == 1) {
                                item.expiredText = valid_date;
                            }
                        }
                        mAdapter.notifyDataSetChanged();

                    } else {
                        showLongToast(t);
                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    CustomLog.logException(TAG,e);
                }

            }

            @Override
            public void onFailure(int errorNo, String strMsg) {
                super.onFailure(errorNo, strMsg);
                String xmsg = String.format("errorNo:%s\n%s", errorNo, strMsg);
                CustomLog.d(TAG,String.format("url:%s\nt:%s", req_url, xmsg));
            }

        });

    }

    /**
     * 内部接口回调方法
     */
    public interface OnItemClickListener {
        void onItemClick(View v, int position, Object object);
    }

    // -------------
    public class RecycleItem {
        public int type;
        // public int drawableId;
        public String title;
        public String expiredText;
        public String btnText_1;
        public String btnText_2;
        // 选中状态，0，没有一个选中;1,选中第一个,2,选中第二个
        public int selectedStatus;

        private RecycleItem(int _type, String _title, String _expiredText,
                            String _btnText_1, String _btnText_2, int _selectedStatus) {
            type = _type;
            // drawableId = _drawableId;
            title = _title;
            expiredText = _expiredText;
            btnText_1 = _btnText_1;
            btnText_2 = _btnText_2;
            _selectedStatus = selectedStatus;
        }

    }

    public class StRecycleAdapter extends
            RecyclerView.Adapter<StRecycleAdapter.ViewHolder> {

        private List<RecycleItem> list;
        private OnItemClickListener listener;

        public StRecycleAdapter(List<RecycleItem> list) {
            this.list = list;
        }

        @Override
        public int getItemCount() {
            return list.size();
        }

        @Override
        public void onBindViewHolder(ViewHolder holder, final int position) {

            final RecycleItem bean = list.get(position);
            // holder.title.setText(bean.getTitle());
            // holder.imageView.setImageResource(bean.getResId())；
            holder.textView_title.setText(bean.title);
            holder.textView_expired.setText("Service Expired:"
                    + bean.expiredText);

            holder.button_s1.setTag("b1");
            holder.button_s2.setTag("b2");
            holder.button_s1.setText(bean.btnText_1);
            holder.button_s2.setText(bean.btnText_2);

            if (bean.selectedStatus == 1) {
                holder.button_s1
                        .setBackgroundResource(R.drawable.bg_topup_press);
                holder.button_s2
                        .setBackgroundResource(R.drawable.editview_press_bg);
            } else if (bean.selectedStatus == 2) {
                holder.button_s1
                        .setBackgroundResource(R.drawable.editview_press_bg);
                holder.button_s2
                        .setBackgroundResource(R.drawable.bg_topup_press);
            } else {
                holder.button_s1
                        .setBackgroundResource(R.drawable.editview_press_bg);
                holder.button_s2
                        .setBackgroundResource(R.drawable.editview_press_bg);
            }
            /**
             * 调用接口回调
             */

            View.OnClickListener view_listener = new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (null != listener)
                        listener.onItemClick(v, position, bean);
                }
            };
            holder.button_s1.setOnClickListener(view_listener);
            holder.button_s2.setOnClickListener(view_listener);
        }

        /**
         * 设置监听方法
         *
         * @param listener
         */
        public void setOnItemClickListener(OnItemClickListener listener) {
            this.listener = listener;
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int position) {
            View view = LayoutInflater.from(viewGroup.getContext()).inflate(
                    R.layout.view_top_up_item, viewGroup, false);
            ViewHolder holder = new ViewHolder(view);
            return holder;
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            public ImageView imageView_img;

            public TextView textView_title;
            public TextView textView_expired;

            public Button button_s1;
            public Button button_s2;

            public ViewHolder(View itemView) {
                super(itemView);
                imageView_img = (ImageView) itemView
                        .findViewById(R.id.imageView_img);
                textView_title = (TextView) itemView
                        .findViewById(R.id.textView_title);
                textView_expired = (TextView) itemView
                        .findViewById(R.id.textView_expired);

                button_s1 = (Button) itemView.findViewById(R.id.button_s1);
                button_s2 = (Button) itemView.findViewById(R.id.button_s2);
            }
        }
    }

    // --------------------

}
