package com.qiwo.xkidswatcher_russia.ui;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.qiwo.xkidswatcher_russia.AppContext;
import com.qiwo.xkidswatcher_russia.KidsWatConfig;
import com.qiwo.xkidswatcher_russia.R;
import com.qiwo.xkidswatcher_russia.api.ApiHttpClient;
import com.qiwo.xkidswatcher_russia.api.remote.KidsWatApiUrl;
import com.qiwo.xkidswatcher_russia.base.BaseActivity;
import com.qiwo.xkidswatcher_russia.dialog.ShowDialogActivity;
import com.qiwo.xkidswatcher_russia.event.BaseEvent;
import com.qiwo.xkidswatcher_russia.util.ACache;
import com.qiwo.xkidswatcher_russia.util.BaseBitmapUtil;
import com.qiwo.xkidswatcher_russia.util.Encoder;
import com.qiwo.xkidswatcher_russia.util.PhotoUtils;

import net.intari.CustomLogger.CustomLog;
import com.qiwo.xkidswatcher_russia.util.UpLoadUtil;
import com.qiwo.xkidswatcher_russia.widget.APopupWindow.ItemPosition;
import com.qiwo.xkidswatcher_russia.widget.APopupWindow.onClickItemListener;
import com.qiwo.xkidswatcher_russia.widget.BottomPopupWindow;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONException;
import org.json.JSONObject;
import org.kymjs.kjframe.http.HttpCallBack;
import org.kymjs.kjframe.http.HttpParams;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import butterknife.BindView;;

/**
 *
 */
@SuppressLint("SdCardPath")
public class SystemSettingActivity_v2 extends BaseActivity {
    final String TAG = SystemSettingActivity_v2.class.getSimpleName();

    public final static int PHOTO_RESULT = 0x00;
    public final static int PHOTO_RESULT_TOW = 0x02;
    public final static int PHOTO_RESULT_THREE = 0x03;
    public final static int UPDATE_USER = 0x04;
    public final static int UPDATE_SEX = 0x05;
    @BindView(R.id.imageview_l)
    ImageView imageview_back;
    @BindView(R.id.linearLayout_cc)
    LinearLayout linearLayout_cc;
    @BindView(R.id.linearLayout_l)
    LinearLayout linearLayout_l;
    @BindView(R.id.linearLayout_alertNotification)
    LinearLayout linearLayout_alertNotification;
    @BindView(R.id.linearLayout_about)
    LinearLayout linearLayout_about;
    @BindView(R.id.linearLayout_feedback)
    LinearLayout linearLayout_feedback;
    @BindView(R.id.linearLayout_app_update)
    LinearLayout linearLayout_app_update;
    @BindView(R.id.user_mobile)
    TextView user_mobile;
    @BindView(R.id.user_photo)
    ImageView user_photo;
    @BindView(R.id.user_info_photo_lin)
    LinearLayout user_info_photo_lin;
    @BindView(R.id.button_exit)
    Button button_exit;
    //// 照片命名
    private Uri imageUri = Uri.parse("file:///sdcard/photo.jpg");
    private Bitmap bitmap;
    private String strBitmap;

    // -----------
    private BottomPopupWindow mLogoutWindow = null;
    @SuppressLint("HandlerLeak")
    private Handler handler = new Handler() {

        public void handleMessage(android.os.Message msg) {
            dismissDialog(null, 10);
            switch (msg.what) {
                case 4:
                    String content = (String) msg.obj;
                    // CustomLog.d(TAG,"content = "+content);
                    try {
                        if (content == null)
                            return;

                        JSONObject response = new JSONObject(content);
                        // CustomLog.d(TAG,"response = "+response.toString());
                        int code = response.getInt("error");
                        if (code == 0) {

                            CustomLog.d(TAG,"-------content------" + content);
                            String image_path = (String) response.getJSONObject("info").getJSONObject("user_info")
                                    .get("img_path");

                            CustomLog.d(TAG,"image path:" + image_path);
                            CustomLog.d(TAG,response.toString());
                            KidsWatConfig.setUserImagepath(image_path);

                            AppContext.getInstance().getImageLoader().displayImage("http://" + image_path, user_photo,
                                    AppContext.getInstance().getOptions());//options);

                        }
                    } catch (JSONException e) {
                        CustomLog.logException(TAG,e);
                    }
                    break;
            }
        }
    };
    private LinearLayout ll_help;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_system_setting_v2;
    }

    @Override
    protected boolean hasActionBar() {
        return false;
    }

    @Override
    protected void init(Bundle savedInstanceState) {
        super.init(savedInstanceState);
        CustomLog.d(TAG,"init()");
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.linearLayout_l:
                finish();
                break;
            case R.id.user_info_photo_lin:

                /*
                 * if (AppContext.is561) { // 获取自己的头像 showPhotoDialog(); }
                 */
                showPhotoDialog();

                break;
            case R.id.linearLayout_alertNotification:
                Intent xintent = new Intent(this, AlertNotificationActivity.class);
                // intent.putExtra(Contanst.KEY_ACTION, Contanst.ACTION_EDIT);
                startActivity(xintent);
                break;
            case R.id.linearLayout_about:
                Intent intent = new Intent(this, AboutActivity.class);
                // intent.putExtra(Contanst.KEY_ACTION, Contanst.ACTION_EDIT);
                startActivity(intent);
                break;
            case R.id.linearLayout_feedback:
                Intent intentx = new Intent(this, FeedbackActivity.class);
                startActivity(intentx);
                break;
            case R.id.linearLayout_app_update:
                showLongToast("The current version is the latest version.");
                break;
            case R.id.button_exit:
                showLogoutPopupWindow(linearLayout_cc);
                break;
            case R.id.linearLayout_help:
                startActivity(new Intent(this, SystemSettingHelpActivity.class));
                break;
            default:
                break;
        }
    }

    /**
     * 跳转到获取头像界面
     */

    private void showPhotoDialog() {
        Intent photoIntent3 = new Intent(this, ShowDialogActivity.class);
        startActivityForResult(photoIntent3, PHOTO_RESULT);
        // overridePendingTransition(R.anim.avtivity_open, PHOTO_RESULT);
    }

    @Override
    public void initView() {
        linearLayout_l.setOnClickListener(this);
        linearLayout_alertNotification.setOnClickListener(this);
        linearLayout_about.setOnClickListener(this);
        linearLayout_feedback.setOnClickListener(this);
        linearLayout_app_update.setOnClickListener(this);
        button_exit.setOnClickListener(this);
        user_info_photo_lin.setOnClickListener(this);
        ll_help = (LinearLayout) findViewById(R.id.linearLayout_help);
        ll_help.setOnClickListener(this);
    }

    @Override
    public void initData() {
        String phone = KidsWatConfig.getUserPhone();
        String ountryCode = KidsWatConfig.getUseCountryCode();
        user_mobile.setText("+" + ountryCode + " " + phone);

        // String relation = "";
        // if (AppContext.getInstance().currentBeanFamily != null) {
        // relation = AppContext.getInstance().currentBeanFamily.getInfo()
        // .getRelation();
        // account_relation.setText(relation);
        // }
        // strUrl不应该存到sp里边，应该从服务器获取啊
        // 本地加载图片

        String strUrl = KidsWatConfig.getUserImagepath();

        CustomLog.d(TAG,"strUrl : " + strUrl);
        if (strUrl != null && !strUrl.equals("")) {
            // 显示自己设置的图片
            // "img_path":"misafes2.qiwocloud2.com?time=1452063291&api_token=62b2cacdef9edb7e591640ab7ebb6873&action=get_user_img&
            // uid=10000195&return_type=base64_decode_img_byte"
            CustomLog.d(TAG,"加载用户头像 - Load user avatar - "+strUrl);
            AppContext.getInstance().getImageLoader().displayImage("http://" + strUrl, user_photo, AppContext.getInstance().getOptions());//options);
        }

        // else {
        // // 说明没有设置图片
        // // 现在要根据关系选择默认的图片
        // String dad = getString(R.string.family_name_dad);
        // String mom = getString(R.string.family_name_mom);
        // String grandpa = getString(R.string.family_name_grandpa);
        // String grandma = getString(R.string.family_name_grandma);
        //
        // int icon = R.drawable.icon_other;
        // if (relation.equals(dad)) {
        // icon = R.drawable.icon_dad;
        // } else if (relation.equals(mom)) {
        // icon = R.drawable.icon_mom;
        // } else if (relation.equals(grandpa)) {
        // icon = R.drawable.icon_grandpa;
        // } else if (relation.equals(grandma)) {
        // icon = R.drawable.icon_grandma;
        // }
        //
        // user_photo.setImageResource(icon);
        // }

        // // 测试自动上传一个照片给服务器
        // bitmap = BitmapFactory.decodeResource(getResources(),
        // R.drawable.set_logo);
        // strBitmap = BaseBitmapUtil.bitmaptoString(bitmap);
        // String strmd5=getMD5(strBitmap);
        //
        // CustomLog.e(TAG,"===上传图片的md5加密--" + strmd5);
        // CustomLog.e(TAG,"===上传图片的base64--" + strBitmap);
        //
        // updateBabyImg(strBitmap);

    }

    protected void showLogoutPopupWindow(View relyView) {
        if (mLogoutWindow == null) {
            mLogoutWindow = new BottomPopupWindow(this, 0, relyView);

            // addItem(title, listener, bgDrawableId, textColorResId,
            // default_textSize, drawable, pos, 0);

            mLogoutWindow.addItem(getApplicationContext().getResources().getString(R.string.logout_message),
                    null, 0, R.color.gray, 15, null, ItemPosition.TOP, 15);
            mLogoutWindow.addItem("Выйти из учетной записи", new onClickItemListener() {

                @Override
                public void clickItem(View v) {
                    // TODO Auto-generated method stub
                    ACache cache = ACache.get(SystemSettingActivity_v2.this);
                    cache.clear();

                    if (AppContext.getInstance().getIsLogin()) {
                        CustomLog.d(TAG,"Logged in - performing networked logout");
                        logout();
                    } else {
                        CustomLog.d(TAG,"Not logged in somehow - not performing networked logout");
                        KidsWatConfig.cleanLoginInfo();
                        // AppManager.AppRestart(SystemSettingsActivity.this);

                        /*
                        BaseEvent event = new BaseEvent(BaseEvent.MSGTYPE_1___LOGOUT, "logout");
                        EventBus.getDefault().post(event);

                        // manually do that's needed - I'm not at all sure that message WILL reach main activity in time
                        Intent login_intent = new Intent(SystemSettingActivity_v2.this, UserLogin2Activity.class);
                        startActivity(login_intent);

                        SystemSettingActivity_v2.this.finish();
                        */
                        doLogout(SystemSettingActivity_v2.this);

                    }
                }
            }, 0, R.color.red, 18, null, ItemPosition.BOTTOM, 0);

            // 添加“取消”
            mLogoutWindow.addItem(getApplicationContext().getResources().getString(R.string.cancle), null, 0, R.color.main_blue, 18, null, ItemPosition.OTHER, 10);
        }
        mLogoutWindow.show();
    }

    private void logout() {
        final String uid = KidsWatConfig.getUserUid();
        // String device_id = AppContext.getInstance().getProperty(
        // "user.device_id");
        // String family_id = AppContext.getDefaultFamilyId();

        final String request_url = KidsWatApiUrl.getUrlFor___logout(uid);

        ApiHttpClient.get(request_url, new HttpCallBack() {

            @Override
            public void onPreStart() {
                showWaitDialog(getApplicationContext().getResources().getString(R.string.loading));
            }

            @Override
            public void onSuccess(String t) {
                super.onSuccess(t);
                CustomLog.d(TAG,String.format("url:%s\nt:%s", request_url, t));
                try {
                    JSONObject response = new JSONObject(t);

                    int code = response.getInt("error");
                    if (code == 0) {
                        KidsWatConfig.cleanLoginInfo();
                    }

                } catch (JSONException e) {
                    CustomLog.logException(TAG,e);
                }
            }

            @Override
            public void onFailure(int errorNo, String strMsg) {
                super.onFailure(errorNo, strMsg);
                String msg = String.format("errorNo:%s\n%s", errorNo, strMsg);
                CustomLog.d(TAG,"error=" + msg);
            }

            @Override
            public void onFinish() {
                dismissDialog(null, 5);
                //AppManager.getAppManager().AppExit(SystemSettingsActivity.this);
                doLogout(SystemSettingActivity_v2.this);

                //SystemSettingActivity_v2.this.finish();
            }
        });

    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            finish();
        }
        return false;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        CustomLog.w(TAG,"Got OnActivityResult(requestCode="+requestCode+",resultCode="+resultCode);
        super.onActivityResult(requestCode, resultCode, data);

//		01-14 16:33:26.378: I/System.out(17572): /storage/emulated/0--path---
//		01-14 16:33:26.388: I/System.out(17572): imagepath=/storage/emulated/0/photo.png

        switch (requestCode) {
            case PHOTO_RESULT:
//			System.gc();
                CustomLog.d(TAG,"得到数据返回 - Got data back;");
                if (resultCode == Activity.RESULT_OK) {
                    CustomLog.d(TAG,"imagepath=" + PhotoUtils.imagePath);

                    bitmap = BitmapFactory.decodeFile(PhotoUtils.imagePath);
                    bitmap = BaseBitmapUtil.rotateBitmapByDegree(bitmap, ShowDialogActivity.photoMatrix);
                    ShowDialogActivity.photoMatrix = 0;
                } else {
                    if (ShowDialogActivity.photoBitmap != null) {
                        bitmap = ShowDialogActivity.photoBitmap;
                    }
                }

                if (bitmap != null) {
                    strBitmap = BaseBitmapUtil.bitmaptoString(bitmap);
//				strBitmap = BaseBitmapUtil.bitmapToBase64(bitmap);
                    // CustomLog.e(TAG,"====头像字符串=" + strBitmap);
                    showWaitDialog("...loading...");

                    final String uid = KidsWatConfig.getUserUid();
                    final String access_token = KidsWatConfig.getUserToken();

                    long cur_time = System.currentTimeMillis() / 1000;
                    String apitoken = Encoder.encode("MD5", String.valueOf(cur_time) + KidsWatApiUrl.api_secret);// SHA-1

                    final String request_url = KidsWatApiUrl.getApiUrl() + "?action=update_user_imge&time="
                            + String.valueOf(cur_time) + "&api_token=" + apitoken + "&uid=" + uid + "&access_token="
                            + access_token;

                    CustomLog.e(TAG,"---url=" + request_url);
                    AppContext.getInstance().getImageLoader().clearDiscCache();
                    AppContext.getInstance().getImageLoader().clearMemoryCache();
                    new Thread() {
                        public void run() {
                            String content = UpLoadUtil.uploadFile(new File(PhotoUtils.imagePath), request_url);
                            Message message = handler.obtainMessage();
                            message.what = 4;
                            message.obj = content;
                            handler.sendMessage(message);
                        }

                        ;
                    }.start();

//				 updateBabyImg(strBitmap);
//				 user_photo.setImageBitmap(BaseBitmapUtil.stringtoBitmap(strBitmap));
//				 setSavePhoto(strBitmap);
                    ShowDialogActivity.photoBitmap = null;
                }
                break;


            case PHOTO_RESULT_TOW:// 从照相机返回调用
                //will NOT work. why it's needed?
                CustomLog.e(TAG,"WTF!?");
                if (resultCode == Activity.RESULT_OK) {// 点击确认后回掉
                    PhotoUtils.doCrop(this, imageUri);
                }
                break;
            case PHOTO_RESULT_THREE:// 裁剪返回调用
                CustomLog.e(TAG,"WTF!? 2");
                if (resultCode == Activity.RESULT_OK) {
                    bitmap = BitmapFactory.decodeFile(PhotoUtils.imagePath);
                    user_photo.setImageBitmap(bitmap);
                    if (bitmap != null) {
                        strBitmap = BaseBitmapUtil.bitmaptoString(bitmap);
                    }
                }
                break;
        }
    }

    /**
     * 提交个人照片到服务器
     *
     * @param imgbase64
     */
    private void updateBabyImg(final String imgbase64) {

        // --------------------
        final String uid = KidsWatConfig.getUserUid();
        final String access_token = KidsWatConfig.getUserToken();
        final String family_id = KidsWatConfig.getDefaultFamilyId();

        HttpParams params = new HttpParams();
        // --------------

        long cur_time = System.currentTimeMillis() / 1000;
        String apitoken = Encoder.encode("MD5", String.valueOf(cur_time) + KidsWatApiUrl.api_secret);// SHA-1

        params.put("time", String.valueOf(cur_time));
        params.put("api_token", apitoken);

        params.put("uid", uid);
        params.put("access_token", access_token);
        params.put("customer_no", "1");

        // params.put("family_id", family_id);
        params.put("img", imgbase64);

        // final String request_url = KidsWatApiUrl.getApiUrl()
        // + "?action=update_baby_img";

        final String request_url = KidsWatApiUrl.getApiUrl() + "?action=update_user_imge";

        ApiHttpClient.getHttpClient().post(request_url, params, false, new HttpCallBack() {

            @Override
            public void onPreStart() {
                showWaitDialog(getString(R.string.new_setting_fragment_6));
            }

            @Override
            public void onSuccess(String t) {
                super.onSuccess(t);
                CustomLog.d(TAG,String.format("url:%s\nt:%s", request_url, t));

                try {
                    JSONObject response = new JSONObject(t);
                    int code = response.getInt("error");
                    if (code == 0) {

                        // SqlDb db = SqlDb
                        // .get(SystemSettingsActivity2.this);
                        // db.saveFamily(family_id, imgbase64, "");
                        // AppContext.getInstance().currentFamily = db
                        // .getFamilyBy_fid(family_id);
                        // db.closeDb();

                        user_photo.setImageBitmap(bitmap);

                        BaseEvent event = new BaseEvent(BaseEvent.MSGTYPE_3___WHITE_SETTING_CHANGE_KIDS_PHOTO, "");
                        EventBus.getDefault().post(event);

                        // try {
                        // File bbimg = new File(saveBbImgPath);
                        //
                        // Bitmap bmp = MediaStore.Images.Media
                        // .getBitmap(SystemSettingsActivity.this
                        // .getContentResolver(), Uri
                        // .fromFile(bbimg));
                        // imageView_baby.setImageBitmap(bmp);
                        //
                        // //
                        // BaseEvent event = new BaseEvent(
                        // BaseEvent.MSGTYPE_3___WHITE_SETTING_CHANGE_KIDS_PHOTO,
                        // saveBbImgPath);
                        // EventBus.getDefault().post(event);
                        //
                        //
                        // } catch (FileNotFoundException e) {
                        // } catch (IOException e) {
                        // } catch (Exception e) {
                        // CustomLog.logException(TAG,e);
                        // }
                    }
                } catch (JSONException e) {
                    CustomLog.logException(TAG,e);
                }

                // ------------------
            }

            @Override
            public void onFailure(int errorNo, String strMsg) {
                super.onFailure(errorNo, strMsg);
                if (errorNo == -1) {
                    showLongToast("Please check your connection and try again.");
                } else {
                    String msg = String.format("%s(error=%s)", errorNo == -1 ? "Connect to the server failed" : strMsg,
                            errorNo);
                    showLongToast(msg);
                }
            }

            @Override
            public void onFinish() {
                dismissDialog(null, 10);
            }
        });

    }

    /**
     * md5加密
     *
     * @param info
     * @return
     */
    public String getMD5(String info) {
        try {
            MessageDigest md5 = MessageDigest.getInstance("MD5");
            md5.update(info.getBytes("UTF-8"));
            byte[] encryption = md5.digest();

            StringBuffer strBuf = new StringBuffer();
            for (int i = 0; i < encryption.length; i++) {
                if (Integer.toHexString(0xff & encryption[i]).length() == 1) {
                    strBuf.append("0").append(Integer.toHexString(0xff & encryption[i]));
                } else {
                    strBuf.append(Integer.toHexString(0xff & encryption[i]));
                }
            }

            return strBuf.toString();
        } catch (NoSuchAlgorithmException e) {
            return "";
        } catch (UnsupportedEncodingException e) {
            return "";
        }
    }
	
	
	/*@Override
	protected void onResume() {
		AppContext.getInstance().getImageLoader().clearDiscCache();
		AppContext.getInstance().getImageLoader().clearMemoryCache();
		// TODO Auto-generated method stubString strUrl = KidsWatConfig.getUserImagepath();
		
		String strUrl = KidsWatConfig.getUserImagepath();
		CustomLog.d(TAG,"on resume "+"strUrl : " + strUrl);
		if (strUrl != null && !strUrl.equals("")) {
			// 显示自己设置的图片
			// "img_path":"misafes2.qiwocloud2.com?time=1452063291&api_token=62b2cacdef9edb7e591640ab7ebb6873&action=get_user_img&
			// uid=10000195&return_type=base64_decode_img_byte"
			CustomLog.d(TAG,"加载用户头像");
			AppContext.getInstance().getImageLoader().displayImage(Contanst.IMAGE_HEAD + strUrl, user_photo, AppContext.options);
		}

		super.onResume();
	}*/
}
