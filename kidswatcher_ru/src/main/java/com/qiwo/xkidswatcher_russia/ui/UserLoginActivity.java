package com.qiwo.xkidswatcher_russia.ui;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.Html;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.qiwo.xkidswatcher_russia.AppContext;
import com.qiwo.xkidswatcher_russia.AppManager;
import com.qiwo.xkidswatcher_russia.KidsWatConfig;
import com.qiwo.xkidswatcher_russia.R;
import com.qiwo.xkidswatcher_russia.api.ApiHttpClient;
import com.qiwo.xkidswatcher_russia.api.remote.KidsWatApiUrl;
import com.qiwo.xkidswatcher_russia.base.BaseActivity;
import com.qiwo.xkidswatcher_russia.bean.beanFor___login;
import com.qiwo.xkidswatcher_russia.util.Contanst;
import com.qiwo.xkidswatcher_russia.util.KidsWatUtils;
import com.qiwo.xkidswatcher_russia.util.StringUtils;
import net.intari.CustomLogger.CustomLog;
import com.qiwo.xkidswatcher_russia.widget.EditTextWithDel;

import org.json.JSONException;
import org.json.JSONObject;
import org.kymjs.kjframe.http.HttpCallBack;
import org.kymjs.kjframe.utils.PreferenceHelper;

import butterknife.BindView;;

public class UserLoginActivity extends BaseActivity {

    public static final String TAG = UserLoginActivity.class.getSimpleName();

    @BindView(R.id.button_next)
    Button button_next;

    @BindView(R.id.username)
    EditText username_text;

    @BindView(R.id.password_tv)
    EditTextWithDel password_text;

    @BindView(R.id.linearLayout_l)
    LinearLayout linearLayout_l;

    @BindView(R.id.forget_password_textview)
    TextView forget_password_textview;

    @BindView(R.id.new_account_text)
    TextView new_account_text;

    @BindView(R.id.textView_countrycode)
    TextView textView_countrycode;
    private TextWatcher textWatcher = new TextWatcher() {
        @Override
        public void afterTextChanged(Editable s) {
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count,
                                      int after) {
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before,
                                  int count) {
            CustomLog.d(TAG,"s=" + s);
            String phone = username_text.getText().toString().trim();
            String pwd = password_text.getText().toString().trim();
            if (StringUtils.isEmpty(phone) || StringUtils.isEmpty(pwd)) {
                button_next.setBackgroundResource(R.drawable.btn01_disable_bg);
                button_next.setEnabled(false);
            } else {
                button_next.setBackgroundResource(R.drawable.btn01_bg);
                button_next.setEnabled(true);
            }
        }
    };

    @Override
    protected int getLayoutId() {
        return R.layout.activity_userlogin;
    }

    @Override
    protected boolean hasActionBar() {
        return false;
    }

    @Override
    protected void init(Bundle savedInstanceState) {
        super.init(savedInstanceState);
    }

    @Override
    public void initView() {
        forget_password_textview.setText(Html
                .fromHtml("<u>Забыли пароль?</u>"));
        new_account_text.setText(Html.fromHtml("<u>Зарегистрироваться</u>"));

        linearLayout_l.setOnClickListener(this);
        button_next.setOnClickListener(this);

        forget_password_textview.setOnClickListener(this);
        new_account_text.setOnClickListener(this);
        textView_countrycode.setOnClickListener(this);

        username_text.addTextChangedListener(textWatcher);
        password_text.addTextChangedListener(textWatcher);
    }

    @Override
    public void initData() {

        String savedCountryCode = KidsWatConfig.getUseCountryCode();
        if (StringUtils.isEmpty(savedCountryCode)) {
            savedCountryCode = "7";
        }
        String defaultCountryCode = "+" + savedCountryCode;
        textView_countrycode.setText(defaultCountryCode);

        String mobile = PreferenceHelper.readString(this, Contanst.PREF_NAME, "umobileno");
        String country_code = PreferenceHelper.readString(this, Contanst.PREF_NAME, "country_code");

        if (!TextUtils.isEmpty(country_code) && !TextUtils.isEmpty(mobile)) {
            String pref_contrycode = "+" + country_code;
            textView_countrycode.setText(pref_contrycode);
            username_text.setText(mobile);
        }
    }

    @Override
    public void onClick(View v) {

        String country_code = textView_countrycode.getText().toString().trim()
                .replace("+", "");
        String mobileno = username_text.getText().toString().trim();
        String password = password_text.getText().toString().trim();
        int id = v.getId();
        switch (id) {
            case R.id.linearLayout_l:
                UserLoginActivity.this.finish();
                break;
            case R.id.textView_countrycode:
                startActivityForResult(new Intent(this, CountryActivity.class), 200);
                break;
            case R.id.forget_password_textview:

                if (mobileno.length() > 3) {
                    check_identifier(country_code, mobileno);
                } else {
                    // Phone number is empty
                    showConfirmInformation(null, getApplicationContext().getResources().getString(R.string.tip_mobile_empty));
                    // Toast.makeText(UserLoginActivity.this, "pls input mobileno",
                    // Toast.LENGTH_SHORT).show();
                }

                break;
            case R.id.new_account_text:
                startActivity(new Intent(UserLoginActivity.this,
                        RegisterActivity.class));
                break;
            case R.id.button_next:
                if (mobileno.length() > 3) {
                    userLogin(country_code, mobileno, password);
                }
                break;
            default:
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 200) {
            if (resultCode != RESULT_CANCELED) {
                if (data != null) {
                    textView_countrycode.setText(data.getAction());
                }
            }

        }
    }

    private void userLogin(final String country_code, final String umobileno,
                           final String mmpassword) {
        // -----------------
        String pushToken = KidsWatUtils.getGooglePushToken();
        if (pushToken == null) {
            // showShortToast("Connect to Google Play failure, some functions(SOS,Google map) will not be available.");
            pushToken = "jiadetoken";
        }
        CustomLog.d(TAG,"push token = " + pushToken);
        final String request_url = KidsWatApiUrl.getUrlFor___login(
                country_code, umobileno, mmpassword, pushToken);
        // -------------------------

        ApiHttpClient.get(request_url, new HttpCallBack() {

            @Override
            public void onPreStart() {
                showWaitDialog(getString(R.string.new_setting_fragment_6));
            }

            @Override
            public void onSuccess(String t) {
                super.onSuccess(t);
                CustomLog.d(TAG,String.format("url:%s\nt:%s", request_url, t));

                beanFor___login response = AppContext.getInstance().getGson()
                        .fromJson(t, beanFor___login.class);

                if (response.error == 0) {
                    // ApplicationHelper.getInstance().mm_beanFor_login
                    // = response;
                    KidsWatConfig.saveUserInfo(response.info.user, mmpassword);

                    //---------record username
                    PreferenceHelper.write(UserLoginActivity.this, Contanst.PREF_NAME, "umobileno", umobileno);
                    PreferenceHelper.write(UserLoginActivity.this, Contanst.PREF_NAME, "country_code", country_code);

                    CustomLog.d(TAG,"user imge_path : " + response.info.user.img_path);
                    KidsWatConfig.setUserImagepath(response.info.user.img_path);

                    AppManager.AppRestart(UserLoginActivity.this);
                    UserLoginActivity.this.finish();

                } else if (response.error == -1) {
                    showConfirmInformation(getString(R.string.user_login_1));
                } else {
                    showConfirmInformation(getString(R.string.user_login_2),
                            response.info.message);
                }
            }

            @Override
            public void onFailure(int errorNo, String strMsg) {
                super.onFailure(errorNo, strMsg);
                String xmsg = String.format("errorNo:%s\n%s", errorNo, strMsg);
                CustomLog.d(TAG,String.format("url:%s\nt:%s", request_url, xmsg));

                if (errorNo == -1) {
                    dismissDialog(null, 50);
                    showConfirmInformation(getApplicationContext().getResources().getString(R.string.time_out),
                            getApplicationContext().getResources().getString(R.string.check_conn));
                } else {
                    String msg = String.format("%s(error=%s)",
                            errorNo == -1 ? "Connect to the server failed"
                                    : strMsg, errorNo);
                    showLongToast(msg);
                    dismissDialog(msg, 800);
                }
            }

            @Override
            public void onFinish() {
                dismissDialog(null, 10);
            }
        });

    }

    // ----------------

    private void check_identifier(final String country_code,
                                  final String identifier) {
        final String request_url = KidsWatApiUrl.getUrlFor___check_identifier(
                country_code, identifier);
        // -------------------------

        ApiHttpClient.get(request_url, new HttpCallBack() {

            @Override
            public void onPreStart() {
                showWaitDialog(getString(R.string.new_setting_fragment_6));
            }

            @Override
            public void onSuccess(String t) {
                super.onSuccess(t);
                CustomLog.d(TAG,"respond:" + t);
                try {
                    JSONObject response = new JSONObject(t);

                    int code = response.getInt("error");
                    if (code == 0) {
                        // 用户未注册，跳转到注册页面 TODO
                        String title = getApplicationContext().getResources().getString(R.string.tip_title_confirm_phone);
                        String message = getApplicationContext().getResources().getString(R.string.tip_not_regist);

                        showConfirmInformation(title, message);

                    } else if (code == 3201) {
                        // 已注册, 请求验证码
                        String title = getApplicationContext().getString(R.string.forget_password_text);
                        String message = getApplicationContext().getString(R.string.tip_msg_confirm_phone) + " +" + country_code + " " + identifier;

                        showConfirmDialog(title, message, "Ok", getApplicationContext().getResources().getString(R.string.cancle), new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                                String text_country = textView_countrycode.getText().toString().replace("+", "");
                                String text_phone = username_text.getText().toString();
//								requre_sms_code(text_country, text_phone);

                                Intent mIntent = new Intent();
                                mIntent.putExtra("country_code", text_country);
                                mIntent.putExtra("mobile", text_phone);
                                mIntent.setClass(UserLoginActivity.this,
                                        ForgetPasswordActivity.class);
                                startActivity(mIntent);
                            }

                        }, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dismissDialog(null);
                            }

                        });

                    } else {
                        String message = response.getJSONObject("info")
                                .getString("message");
                        Toast.makeText(getApplicationContext(), message,
                                Toast.LENGTH_SHORT).show();
                    }

                } catch (JSONException e) {
                    CustomLog.logException(TAG,e);
                }
            }

            @Override
            public void onFailure(int errorNo, String strMsg) {
                super.onFailure(errorNo, strMsg);
                String msg = String.format("errorNo:%s\n%s", errorNo, strMsg);
                CustomLog.d(TAG,"error=" + msg);
                Toast.makeText(UserLoginActivity.this, msg, Toast.LENGTH_LONG)
                        .show();
            }

            @Override
            public void onFinish() {
                dismissDialog(null, 10);
            }
        });

    }

    private void requre_sms_code(final String country_code, final String phone) {
        final String request_url = KidsWatApiUrl.getUrlFor___requre_sms_code(
                country_code, phone);
        // -------------------------

        ApiHttpClient.get(request_url, new HttpCallBack() {

            @Override
            public void onPreStart() {
                showWaitDialog(getString(R.string.new_setting_fragment_6));
            }

            @Override
            public void onSuccess(String t) {
                super.onSuccess(t);
                CustomLog.d(TAG,"requre_sms_code.response=" + t);
                try {
                    JSONObject response = new JSONObject(t);

                    int code = response.getInt("error");
                    if (code == 0) {
                        // --------------
                        // 跳转到忘记密码页面
                        Intent mIntent = new Intent();
                        mIntent.setClass(getApplicationContext(),
                                ForgetPasswordActivity.class);
                        mIntent.putExtra("country_code", country_code);
                        mIntent.putExtra("mobile", phone);
                        UserLoginActivity.this.startActivity(mIntent);

                        UserLoginActivity.this.finish();
                    } else {
                        Toast.makeText(UserLoginActivity.this, getApplicationContext().getResources().getString(R.string.tip_send_sms_error),
                                Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    CustomLog.logException(TAG,e);
                }

            }

            @Override
            public void onFailure(int errorNo, String strMsg) {
                super.onFailure(errorNo, strMsg);
                String msg = String.format("errorNo:%s\n%s", errorNo, strMsg);
                CustomLog.d(TAG,"error=" + msg);
                Toast.makeText(UserLoginActivity.this, msg, Toast.LENGTH_LONG)
                        .show();
            }

            @Override
            public void onFinish() {
                dismissDialog(null, 10);
            }
        });

    }
}
