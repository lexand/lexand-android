package com.qiwo.xkidswatcher_russia;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.app.ActivityCompat.OnRequestPermissionsResultCallback;
import androidx.core.content.ContextCompat;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.code.microlog4android.config.PropertyConfigurator;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.JsonSyntaxException;
import com.qiwo.xkidswatcher_russia.AnalyticsUtils.AnalyticsUtils;
import com.qiwo.xkidswatcher_russia.api.ApiHttpClient;
import com.qiwo.xkidswatcher_russia.api.remote.KidsWatApiUrl;
import com.qiwo.xkidswatcher_russia.bean.beanFor___login;
import com.qiwo.xkidswatcher_russia.db.SqlDb;
import com.qiwo.xkidswatcher_russia.ui.GuideActivity;
import com.qiwo.xkidswatcher_russia.ui.MainActivity;
import com.qiwo.xkidswatcher_russia.ui.RegisterActivity;
import com.qiwo.xkidswatcher_russia.ui.SettingActivity;
import com.qiwo.xkidswatcher_russia.ui.UserLoginActivity;
import com.qiwo.xkidswatcher_russia.util.Contanst;
import com.qiwo.xkidswatcher_russia.util.KidsWatUtils;
import com.qiwo.xkidswatcher_russia.util.TDevice;
import net.intari.CustomLogger.CustomLog;

import org.json.JSONObject;
import org.kymjs.kjframe.http.HttpCallBack;
import org.kymjs.kjframe.http.KJAsyncTask;
import org.kymjs.kjframe.utils.FileUtils;
import org.kymjs.kjframe.utils.PreferenceHelper;
import org.kymjs.kjframe.utils.StringUtils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * 应用启动界面
 */
public class AppStart extends AppCompatActivity implements OnRequestPermissionsResultCallback {

    public static final String TAG = AppStart.class.getSimpleName();

    private final static int SWITCH_MAINACTIVITY = 1000;
    private final static int SWITCH_GUIDACTIVITY = 1001;
    private static final int DOWNLOADING = 1003;
    private static final int DOWNLOAD_FINISH = 1004;
    AsyncTask<Void, Void, Void> mRegisterTask;
    LinearLayout linearLayout_c;
    @SuppressLint("HandlerLeak")
    public Handler mHandler = new Handler() {
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case SWITCH_MAINACTIVITY:
                    CustomLog.d(TAG,"Will switch to main activity");
                    String userPhone = KidsWatConfig.getUserPhone();
                    String password = KidsWatConfig.getUserPassword();
                    CustomLog.d(TAG,"userphone=" + userPhone);
                    CustomLog.d(TAG,"password=" + password);

                    if (StringUtils.isEmpty(userPhone)
                            || StringUtils.isEmpty(password)) {
                        // 显示登陆注册按钮
                        AlphaAnimation aa = new AlphaAnimation(0.0f, 1.0f);
                        aa.setDuration(1000);
                        linearLayout_c.startAnimation(aa);
                        linearLayout_c.setVisibility(View.VISIBLE);
                    } else {
                        CustomLog.d(TAG,"going to login()");
                        login();
                    }

                    break;
                case SWITCH_GUIDACTIVITY:
                    CustomLog.d(TAG,"Will switch to guide activity");
                    Intent intent = new Intent(AppStart.this, GuideActivity.class);
                    startActivity(intent);
                    CustomLog.d(TAG,"Will switch to guide activity - issued startActivity:"+Utils.toUri(intent));
                    AppStart.this.finish();
                    CustomLog.d(TAG,"Will switch to guide activity - finishing this one");
                    break;
            }
            super.handleMessage(msg);
        }
    };
    Button button_signup;
    Button button_login;
    ImageView imageView_c;
    LinearLayout linearlayoout_splash;
    View.OnClickListener view_ll_listener = new View.OnClickListener() {
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.button_signup:
                    CustomLog.d(TAG,"Will switch to Register activity");
                    Intent reg_intent = new Intent(AppStart.this,
                            RegisterActivity.class);
                    startActivity(reg_intent);
                    break;

                case R.id.button_login:
                    CustomLog.d(TAG,"Will switch to Login activity");
                    Intent login_intent = new Intent(AppStart.this,
                            UserLoginActivity.class);
                    startActivity(login_intent);
                    break;

            }
        }

    };
    boolean isCancelUpdate;
    int mProgress = 0;
    View pub_view;
    private int count = 0;
    private long firstTime = 0;
    private long interval = 500;
    View.OnClickListener view_img_listener = new View.OnClickListener() {
        public void onClick(View v) {
            int viewId = v.getId();
            if (viewId == R.id.linearlayoout_splash) {
                long secondTime = System.currentTimeMillis();
                if (secondTime - firstTime <= interval) {
                    ++count;
                } else {
                    count = 1;
                }
                firstTime = secondTime;
                // ---------------------
                CustomLog.d(TAG,"count=" + count);
                if (count == 10) {
                    count = 0;
                    CustomLog.d(TAG,"Will switch to settings activity");
                    Intent reg_intent = new Intent(AppStart.this,
                            SettingActivity.class);
                    startActivity(reg_intent);
                }
            }
        }

    };
    private ProgressBar mProgressbar;
    private TextView mTextView;
    private AlertDialog mDownloadProgressDialog;

    @SuppressLint("StaticFieldLeak")
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        CustomLog.d(TAG,"onCreate in progress");
        PropertyConfigurator.getConfigurator(this).configure();

        // 防止第三方跳转时出现双实例
        Activity aty = AppManager.getActivity(MainActivity.class);
        if (aty != null && !aty.isFinishing()) {
            CustomLog.e(TAG,"can't complete onCreate in AppStart");
            finish();
        }


        //check if we need to force token regeneration

        if (!KidsWatConfig.getFCMMigrationStarted()) {
            CustomLog.i(TAG,"FCM:Firebase Instance was created at:"+ FirebaseInstanceId.getInstance().getCreationTime());

            CustomLog.i(TAG,"GCM->FCM token migration not started. Doing so. Async mode");
            new Thread(new Runnable() {

                @Override
                public void run() {
                    try {
                        CustomLog.i(TAG,"FCM:Firebase Instance ID:"+ FirebaseInstanceId.getInstance().getId());
                        CustomLog.i(TAG,"GCM->FCM token migration not started. Doing so. Asking Firebase to delete");
                        FirebaseInstanceId.getInstance().deleteInstanceId();
                        CustomLog.i(TAG,"FCM:Reasking:");
                        String token=FirebaseInstanceId.getInstance().getToken();
                        CustomLog.d(TAG,"(new)FCM Push Token should be "+token );

                        CustomLog.d(TAG, "FCM:Device registered: regId = " + token);
                        if (token!=null) {
                            AppContext.getInstance().setGCMRegID(token);
                            KidsWatConfig.setGcmAppid(token);
                            // ServerUtilities.register(this, token);
                        }


                    } catch (Exception e) {
                        CustomLog.logException(TAG,e);
                    }
                }
            }).start();

        }

        //init channels (required for Oreo+)
        MyGcmListenerService.initChannel(AppStart.this);
        //Register with GCM
        //will also take care of Register with Server clls
        //not necessary with FCM
        //RegistrationIntentService.initiateGCMRegistration(AppStart.this);
        //according to https://developers.google.com/cloud-messaging/android/android-migrate-iid-service this is not strictly necessary but just do anyway
        String token=FirebaseInstanceId.getInstance().getToken();
        CustomLog.d(TAG,"FCM Push Token should be "+token );

        if (token==null) {
            CustomLog.e(TAG,"FCM:null appid, migration in process?");

        } else {
            CustomLog.d(TAG, "FCM:Device registered: regId = " + token);

            AppContext.getInstance().setGCMRegID(token);
            KidsWatConfig.setGcmAppid(token);

            ServerUtilities.register(this, token);

        }


        // ------------
        // ------------------
        // GCMRegistrar.onDestroy(this);
        /*

        try {
            CustomLog.d(TAG,"Will Check device with GCM..");
            GCMRegistrar.checkDevice(this);
            CustomLog.d(TAG,"Will check manifest  with GCM..");
            GCMRegistrar.checkManifest(this);
        } catch (Exception e) {
            CustomLog.logException(TAG,e);
        }
        final String regId = GCMRegistrar.getRegistrationId(this);
        // final String regId = "";
        CustomLog.d(TAG,"regId=" + regId);
        if (regId.equals("")) {
            CustomLog.d(TAG,"regId is empty. Starting Push Registration");
            GCMRegistrar.register(AppStart.this, GCMIntentService.SENDER_ID);
        } else {
            if (GCMRegistrar.isRegisteredOnServer(this)) {
                CustomLog.d(TAG,"isRegisteredOnServer");
            } else {
                // Try to register again, but not in the UI thread.
                // It's also necessary to cancel the thread onDestroy(),
                // hence the use of AsyncTask instead of a raw thread.
                final Context context = this;
                mRegisterTask = new AsyncTask<Void, Void, Void>() {

                    @Override
                    protected Void doInBackground(Void... params) {
                        boolean registered = ServerUtilities.register(context,
                                regId);
                        if (!registered) {
                            CustomLog.e(TAG,"Not regstered - should unregister");
                            GCMRegistrar.unregister(context);
//							GCMRegistrar.unregister(context);
                        }
                        return null;
                    }

                    @Override
                    protected void onPostExecute(Void result) {
                        mRegisterTask = null;
                    }

                };
                mRegisterTask.execute(null, null, null);
            }

        }
        */

        // ------------------
        // SystemTool.gc(this); //针对性能好的手机使用，加快应用相应速度

        // final View view = View.inflate(this, R.layout.app_start, null);
        final View view = View.inflate(this, R.layout.activity_splash, null);
        setContentView(view);
        linearLayout_c = findViewById(R.id.linearLayout_c);
        button_signup = findViewById(R.id.button_signup);
        button_login = findViewById(R.id.button_login);
        imageView_c = findViewById(R.id.imageView_c);

        linearlayoout_splash = findViewById(R.id.linearlayoout_splash);
        button_signup.setOnClickListener(view_ll_listener);
        button_login.setOnClickListener(view_ll_listener);
        linearlayoout_splash.setOnClickListener(view_img_listener);

        //Android 6.0申请权限
        requrePhonePermission(view);

        // 渐变展示启动屏
//		AlphaAnimation aa = new AlphaAnimation(0.5f, 1.0f);
//		aa.setDuration(800);
//		view.startAnimation(aa);
//		aa.setAnimationListener(new AnimationListener() {
//			@Override
//			public void onAnimationEnd(Animation arg0) {
//				redirectTo();
//			}
//
//			@Override
//			public void onAnimationRepeat(Animation animation) {
//			}
//
//			@Override
//			public void onAnimationStart(Animation animation) {
//			}
//		});
//
//		// -----------
//		String filename = KidsWatConfig.getTempFilePath() + "alarm.wav";
//		java.io.File f = new java.io.File(filename);
//		if (f.exists()) {
//			//
//		} else {
//			try {
//				KidsWatUtils.copyBigDataToSD(this, "alarm.wav", filename);
//			} catch (IOException e) {
//				CustomLog.logException(TAG,e);
//			}
//		}

        // ------------------------------
    }

    @Override
    protected void onResume() {
        super.onResume();
        int cacheVersion = PreferenceHelper.readInt(this, "first_install",
                "first_install", -1);
        int currentVersion = TDevice.getVersionCode();
        CustomLog.d(TAG,"onResume. Cache Version:"+cacheVersion+", currentVersion:"+currentVersion);
        if (cacheVersion < currentVersion) {
            CustomLog.d(TAG,"Will clear image cache");
            PreferenceHelper.write(this, "first_install", "first_install",
                    currentVersion);
            cleanImageCache();
            CustomLog.d(TAG,"Done clearing image cache");
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Only show items in the action bar relevant to this screen
        // if the drawer is not showing. Otherwise, let the drawer
        // decide what to show in the action bar.
        // getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
//			Intent reg_intent = new Intent(AppStart.this, SettingActivity.class);
//			startActivity(reg_intent);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void cleanImageCache() {
        final File folder = FileUtils.getSaveFolder("kidswatcher/imagecache");
        KJAsyncTask.execute(new Runnable() {
            @Override
            public void run() {
                if (folder.listFiles() != null) {
                    for (File file : folder.listFiles()) {
                        file.delete();
                    }
                }
            }
        });
    }

    /**
     * 跳转到...
     */
    private void redirectTo() {
        boolean mFirst = KidsWatConfig.isFristStart();
        if (mFirst)
            mHandler.sendEmptyMessageDelayed(SWITCH_GUIDACTIVITY, 10);
        else
            mHandler.sendEmptyMessageDelayed(SWITCH_MAINACTIVITY, 1000);
    }

    private void login() {

        CustomLog.d(TAG,"login()");
        if (!KidsWatUtils.isNetworkAvailable(this)) {
            startActivity(new Intent(this, UserLoginActivity.class));
            finish();
            return;
        }

        final String country_code = KidsWatConfig.getUseCountryCode();
        final String userPhone = KidsWatConfig.getUserPhone();
        final String password = KidsWatConfig.getUserPassword();
        String pushToken = KidsWatUtils.getGooglePushToken();

        CustomLog.d(TAG,"pushToken" + pushToken);
        if (pushToken == null) {
            pushToken = "jiadetoken";
        }

        final String req_url = KidsWatApiUrl.getUrlFor___login(country_code,
                userPhone, password, pushToken);

        AppContext.getInstance().getMapFamilyInfo().clear();

        ApiHttpClient.get(req_url, new HttpCallBack() {

            @Override
            public void onSuccess(String t) {
                super.onSuccess(t);
                CustomLog.d(TAG,String.format("Automatic login ok. url:%s.t:%s", req_url, t));
                try {
                    // gson.fromJson("{"user":"admin","pwd":"123456"}",
                    beanFor___login b = AppContext.getInstance().getGson()
                            .fromJson(t, beanFor___login.class);



                    if (b.error == 0) {
                        CustomLog.d(TAG,"No error. Logged in");
                        AppContext.getInstance().setIsLogin(true);//isLogin = true;
                        KidsWatConfig.saveUserInfo(b.info.user, password);
                        SqlDb db = SqlDb.get(AppContext.getInstance());
                        db.saveMember(b);
                        db.deleteMember_family_By_uid(b.info.user.uid);
                        db.closeDb();


                        AnalyticsUtils.analytics_setUserInfo(TAG,b.info.user);


                        //检查更新
                        CustomLog.d(TAG,"Will check for updates..");
                        checkUpdate();
                        CustomLog.d(TAG,"Will check for updates..done");

                    } else {
                        CustomLog.w(TAG,"server responded with error code:"+b.error+", Will transition to UserLoginActivity");
                        Intent intent = new Intent(AppStart.this, UserLoginActivity.class);
                        startActivity(intent);
                        CustomLog.d(TAG,"Finishing this activity");
                        finish();
                        CustomLog.d(TAG,"Finished this activity");

                    }

                } catch (JsonSyntaxException ex) {
                    CustomLog.e(TAG,"Failed to autologin due JSON parsing error:"+ex);
                    CustomLog.logException(TAG,ex);
                    Toast.makeText(AppStart.this,
                            getApplicationContext().getResources().getString(R.string.incorrect_server_response_json), Toast.LENGTH_LONG)
                            .show();

                    CustomLog.w(TAG,"Failed automatic login. Will transition to UserLoginActivity");
                    Intent intent = new Intent(AppStart.this, UserLoginActivity.class);
                    startActivity(intent);
                    CustomLog.d(TAG,"Finishing this activity");
                    finish();
                    CustomLog.d(TAG,"Finished this activity");

                }
            }

            @Override
            public void onFailure(int errorNo, String strMsg) {
                super.onFailure(errorNo, strMsg);

                if (errorNo == -1) {
                    Toast.makeText(AppStart.this,
                            getApplicationContext().getResources().getString(R.string.tip_check_network), Toast.LENGTH_LONG)
                            .show();
                } else {
                    String msg = String.format("%s(error=%s)",
                            errorNo == -1 ? getApplicationContext().getResources().getString(R.string.tip_conect_servise_faild)
                                    : strMsg, errorNo);

                    Toast.makeText(AppStart.this, msg, Toast.LENGTH_LONG).show();
                }

                CustomLog.w(TAG,"Failed automatic login. Will transition to UserLoginActivity");
                Intent intent = new Intent(AppStart.this, UserLoginActivity.class);
                startActivity(intent);
                CustomLog.d(TAG,"Finishing this activity");
                finish();
                CustomLog.d(TAG,"Finished this activity");
            }

            @Override
            public void onFinish() {
                CustomLog.d(TAG,"Finished login request");

            }
        });
    }

    private void checkUpdate() {
        CustomLog.d(TAG,"Checking for updates...");
        // 访问服务器，返回是否有最新更新
        String version = null;
        try {
            version = getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
        } catch (NameNotFoundException e) {
            // TODO Auto-generated catch block
            CustomLog.logException(TAG,e);
        }

        String req_url = KidsWatApiUrl.getUrlFor___check_version(1 + "", 0 + "", version);
        /*
         * HttpParams params = new HttpParams(); params.put("customer_no", 0);
         * params.put("platform", 0); params.put("version", version);
         * params.put("access_token", KidsWatConfig.getUserToken());
         * CustomLog.d(TAG,"check_version");
         */
        CustomLog.d(TAG,"Checking for updates...will query url:"+req_url);

        ApiHttpClient.get(req_url, new HttpCallBack() {

            @Override
            public void onSuccess(String t) {
                super.onSuccess(t);
                CustomLog.d(TAG,"t:" + t);
                try {
                    CustomLog.d(TAG,"Checking for updates...success:"+t);

                    JSONObject json = new JSONObject(t);
                    int error = json.getInt("error");

//					{"error":0,"info":{"message":"Andriod request app version!",
//					"latest_version":"",
//					"app_url":"",
//					"app_update_info":"",
//					"is_latest":1}}

                    String latest_version = json.getJSONObject("info").getString("latest_version");
                    final String app_url = json.getJSONObject("info").getString("app_url");
                    KidsWatApiUrl.UPDATE_URL = app_url;
                    String app_update_info = json.getJSONObject("info").getString("app_update_info");
                    int is_latest = json.getJSONObject("info").getInt("is_latest");
                    int isMustUpdate = 0;
//					int isMustUpdate = json.getJSONObject("info").getInt("isMustUpdate");

                    if (error == 0) {
                        boolean isLastestVersion = is_latest == 1;
                        String versionName = latest_version;

                        String versionInfo = app_update_info;
                        CustomLog.i(TAG,"Update query check ok. isLatestVersion:"+isLastestVersion+", isMustUpdate:"+isMustUpdate);
                        // 如果有最新版本,弹出是否要更新的提示框
                        if (!isLastestVersion) {
                            if (isMustUpdate == 1) {
                                showSelectDialog_v3(getApplicationContext().getResources().getString(R.string.reminder),
                                        versionInfo,
                                        getApplicationContext().getResources().getString(R.string.text_btn_update),
                                        new OnClickListener() {

                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                // TODO Auto-generated method stub
                                                CustomLog.w(TAG,"Terminating app and starting update process via "+app_url);
                                                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(app_url)));
                                                AppStart.this.finish();
                                                AppManager.getAppManager().AppExit(AppStart.this);
                                            }
                                        });
                            } else {

                                long last_time = KidsWatConfig.getLastIgnoreUpdateTime();
                                long ignore_time_span = System.currentTimeMillis() - last_time;
                                CustomLog.d(TAG,"Non critical update available");
                                if (ignore_time_span > 24 * 60 * 60 * 1000) {//每隔24小时显示一次
                                    String title = getApplicationContext().getResources().getString(R.string.new_version) + " "
                                            + versionName;
                                    String positivetext = getApplicationContext().getResources().getString(R.string.update_new);
                                    String negetivetext = getApplicationContext().getResources().getString(R.string.ignore_new);
                                    OnClickListener negetiveListener = new OnClickListener() {

                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            // TODO Auto-generated method stub
                                            dialog.dismiss();
                                            KidsWatConfig.setLastIgnoreUpdateTime(System.currentTimeMillis());
                                            startMainActivity();
                                        }
                                    };

                                    OnClickListener positiveListener = new OnClickListener() {

                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            // TODO Auto-generated method stub
                                            //										update();
                                            final String appPackageName = getPackageName(); // getPackageName() from Context or Activity object
                                            try {
                                                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                                            } catch (android.content.ActivityNotFoundException anfe) {
                                                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                                            }
                                            AppManager.getAppManager().AppExit(AppStart.this);
                                        }
                                    };

                                    showSelectDialog_v2(title, versionInfo, positivetext, negetivetext, negetiveListener,
                                            positiveListener);
                                } else {
                                    CustomLog.d(TAG,"All ok. will start main - 1");
                                    startMainActivity();
                                }
                            }
                        } else {
                            CustomLog.d(TAG,"All ok. will start main - 2");
                            startMainActivity();
                        }
                    } else {
                        Toast.makeText(AppStart.this, "check version error.", Toast.LENGTH_SHORT).show();
                        CustomLog.d(TAG,"All ok. will start main - 3");

                        startMainActivity();
                    }
                } catch (Exception e) {
                    // TODO Auto-generated catch block
                    CustomLog.logException(TAG,e);
                }

            }

            @Override
            public void onLoading(long count, long current) {
                super.onLoading(count, current);
                CustomLog.d(TAG,"update check:loading:"+current+" of "+count);
            }

            @Override
            public void onPreStart() {
                super.onPreStart();
                CustomLog.d(TAG,"Pre-starting update check");
            }

            @Override
            public void onFailure(int errorNo, String strMsg) {
                super.onFailure(errorNo, strMsg);

                CustomLog.w(TAG,"Update check failed. errNo:"+errorNo+",msg:"+strMsg); }

            @Override
            public void onFinish() {
                CustomLog.d(TAG,"Finished update check request");

            }
        });

/*
        ApiHttpClient.getHttpClient().get(url, new HttpCallBack() {
            @Override
            public void onSuccess(String t) {
                CustomLog.d(TAG,"t:" + t);
                super.onSuccess(t);
                try {
                    CustomLog.d(TAG,"Checking for updates...success:"+t);

                    JSONObject json = new JSONObject(t);
                    int error = json.getInt("error");

//					{"error":0,"info":{"message":"Andriod request app version!",
//					"latest_version":"",
//					"app_url":"",
//					"app_update_info":"",
//					"is_latest":1}}

                    String latest_version = json.getJSONObject("info").getString("latest_version");
                    final String app_url = json.getJSONObject("info").getString("app_url");
                    KidsWatApiUrl.UPDATE_URL = app_url;
                    String app_update_info = json.getJSONObject("info").getString("app_update_info");
                    int is_latest = json.getJSONObject("info").getInt("is_latest");
                    int isMustUpdate = 0;
//					int isMustUpdate = json.getJSONObject("info").getInt("isMustUpdate");

                    if (error == 0) {
                        boolean isLastestVersion = is_latest == 1;
                        String versionName = latest_version;

                        String versionInfo = app_update_info;
                        CustomLog.i(TAG,"Update query check ok. isLatestVersion:"+isLastestVersion+", isMustUpdate:"+isMustUpdate);
                        // 如果有最新版本,弹出是否要更新的提示框
                        if (!isLastestVersion) {
                            if (isMustUpdate == 1) {
                                showSelectDialog_v3(getApplicationContext().getResources().getString(R.string.reminder),
                                        versionInfo,
                                        getApplicationContext().getResources().getString(R.string.text_btn_update),
                                        new OnClickListener() {

                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                // TODO Auto-generated method stub
                                                CustomLog.w(TAG,"Terminating app and starting update process via "+app_url);
                                                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(app_url)));
                                                AppStart.this.finish();
                                                AppManager.getAppManager().AppExit(AppStart.this);
                                            }
                                        });
                            } else {

                                long last_time = KidsWatConfig.getLastIgnoreUpdateTime();
                                long ignore_time_span = System.currentTimeMillis() - last_time;
                                CustomLog.d(TAG,"Non critical update available");
                                if (ignore_time_span > 24 * 60 * 60 * 1000) {//每隔24小时显示一次
                                    String title = getApplicationContext().getResources().getString(R.string.new_version) + " "
                                            + versionName;
                                    String positivetext = getApplicationContext().getResources().getString(R.string.update_new);
                                    String negetivetext = getApplicationContext().getResources().getString(R.string.ignore_new);
                                    OnClickListener negetiveListener = new OnClickListener() {

                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            // TODO Auto-generated method stub
                                            dialog.dismiss();
                                            KidsWatConfig.setLastIgnoreUpdateTime(System.currentTimeMillis());
                                            startMainActivity();
                                        }
                                    };

                                    OnClickListener positiveListener = new OnClickListener() {

                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            // TODO Auto-generated method stub
                                            //										update();
                                            final String appPackageName = getPackageName(); // getPackageName() from Context or Activity object
                                            try {
                                                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                                            } catch (android.content.ActivityNotFoundException anfe) {
                                                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                                            }
                                            AppManager.getAppManager().AppExit(AppStart.this);
                                        }
                                    };

                                    showSelectDialog_v2(title, versionInfo, positivetext, negetivetext, negetiveListener,
                                            positiveListener);
                                } else {
                                    CustomLog.d(TAG,"All ok. will start main - 1");
                                    startMainActivity();
                                }
                            }
                        } else {
                            CustomLog.d(TAG,"All ok. will start main - 2");
                            startMainActivity();
                        }
                    } else {
                        Toast.makeText(AppStart.this, "check version error.", Toast.LENGTH_SHORT).show();
                        CustomLog.d(TAG,"All ok. will start main - 3");

                        startMainActivity();
                    }
                } catch (Exception e) {
                    // TODO Auto-generated catch block
                    CustomLog.logException(TAG,e);
                }

            }
        });
        */
    }

    // 单按钮
    protected void showSelectDialog_v3(String title, String message, String positivetext,
                                       android.content.DialogInterface.OnClickListener positiveListener) {
        if (title == null)
            message = "\n" + message;
        View v = getLayoutInflater().inflate(R.layout.dialog_custom, null);
        TextView tv_title = (TextView) v.findViewById(R.id.custom_dialog_title);
        TextView tv_message = (TextView) v.findViewById(R.id.custom_dialog_message);
        tv_message.setGravity(Gravity.CENTER_VERTICAL);

        if (!TextUtils.isEmpty(title)) {
            tv_title.setVisibility(View.VISIBLE);
        } else {
            tv_title.setVisibility(View.GONE);
        }

        (tv_title).setText(title);
        (tv_message).setText(message);

        AlertDialog.Builder builder = new AlertDialog.Builder(AppStart.this, AlertDialog.THEME_HOLO_LIGHT);
        builder.setView(v).setPositiveButton(positivetext, positiveListener).setCancelable(false);
        AlertDialog dialog = builder.create();
        dialog.show();
        dialog.getButton(dialog.BUTTON_POSITIVE)
                .setTextColor(getResources().getColor(R.color.abc_search_url_text_selected));
    }

    private void startMainActivity() {
        CustomLog.d(TAG,"Starting main activity");

        Intent intent = new Intent(AppStart.this,
                MainActivity.class);
        startActivity(intent);
        CustomLog.d(TAG,"Starting main activity - issued startActvity for "+Utils.toUri(intent));
        AppStart.this.finish();
        CustomLog.d(TAG,"Starting main activity - issued finish for this one ");

    }

    /**
     * 安装更新文件
     */
    public void installApk() {
        String apkPath = KidsWatConfig.getTempFilePath() + "papet.apk";
        File apkFile = new File(apkPath);
        if (!apkFile.exists()) {
            return;
        }

        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setDataAndType(Uri.parse("file://" + apkFile.toString()), "application/vnd.android.package-archive");
        AppStart.this.startActivity(intent);
    }

    protected void showSelectDialog_v2(String title, String message, String positivetext, String negetivetext, android.content.DialogInterface.OnClickListener negetiveListener, android.content.DialogInterface.OnClickListener positiveListener) {
        if (title == null)
            message = "\n" + message;
        View v = getLayoutInflater().inflate(R.layout.dialog_custom, null);
        TextView tv_title = (TextView) v.findViewById(R.id.custom_dialog_title);
        TextView tv_message = (TextView) v.findViewById(R.id.custom_dialog_message);
        if (!TextUtils.isEmpty(title)) {
            tv_title.setVisibility(View.VISIBLE);
        } else {
            tv_title.setVisibility(View.GONE);
        }
        (tv_title).setText(title);
        (tv_message).setText(message);

        AlertDialog.Builder builder = new AlertDialog.Builder(AppStart.this, AlertDialog.THEME_HOLO_LIGHT);

        builder.setView(v).setPositiveButton(positivetext, positiveListener).setNegativeButton(negetivetext, negetiveListener);
        AlertDialog dialog = builder.create();
        dialog.show();
        dialog.getButton(dialog.BUTTON_POSITIVE).setTextColor(getResources().getColor(R.color.abc_search_url_text_selected));
        dialog.getButton(dialog.BUTTON_NEGATIVE).setTextColor(getResources().getColor(R.color.abc_search_url_text_selected));
    }

    private void update() {
        // TODO Auto-generated method stub
        // 显示下载进度框
        AlertDialog.Builder builder = new AlertDialog.Builder(this, AlertDialog.THEME_HOLO_LIGHT);
        View view = getLayoutInflater().inflate(R.layout.soft_update_progress, null);
        mProgressbar = (ProgressBar) view.findViewById(R.id.update_progress);

        mTextView = (android.widget.TextView) view.findViewById(R.id.textview_progress);
        builder.setView(view);
        isCancelUpdate = false;
        builder.setNegativeButton(getApplicationContext().getResources().getString(R.string.cancle),
                new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // TODO Auto-generated method stub
                        // 取消更新
                        isCancelUpdate = true;
                        AppManager.AppRestart(AppStart.this);
                    }
                });

        mDownloadProgressDialog = builder.create();
        mDownloadProgressDialog.show();
        // 下载最新版本apk
        downloadApk();
    }

    private void downloadApk() {
        new downloadApkThread().start();
    }

    private void requrePhonePermission(View v) {
        // TODO Auto-generated method stub
        if (Build.VERSION.SDK_INT >= 23) {
            if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.READ_PHONE_STATE) == PackageManager.PERMISSION_GRANTED) {
                //拥有权限
                requreStoragePermission(v);
            } else {
                pub_view = v;
                ActivityCompat.requestPermissions(AppStart.this, new String[]{android.Manifest.permission.READ_PHONE_STATE}, Contanst.REQUEST_PHONE_REQUEST_PERMISSION);
            }
        } else {
            requreStoragePermission(v);
        }
    }

    private void requreStoragePermission(View v) {
        // TODO Auto-generated method stub
        if (Build.VERSION.SDK_INT >= 23) {
            if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                //拥有权限
                showAnimAndDownFile(v);
            } else {
                pub_view = v;
                ActivityCompat.requestPermissions(AppStart.this, new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, Contanst.REQUEST_STORAGE_REQUEST_PERMISSION);
                return;
            }
        } else {
            showAnimAndDownFile(v);
        }
    }

    private void showAnimAndDownFile(View view) {
        // TODO Auto-generated method stub
        // 渐变展示启动屏
        AlphaAnimation aa = new AlphaAnimation(0.5f, 1.0f);
        aa.setDuration(800);
        view.startAnimation(aa);
        aa.setAnimationListener(new AnimationListener() {
            @Override
            public void onAnimationEnd(Animation arg0) {
                redirectTo();
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }

            @Override
            public void onAnimationStart(Animation animation) {
            }
        });

        if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
            int cacheVersion = PreferenceHelper.readInt(this, "first_install", "first_install", -1);
            int currentVersion = TDevice.getVersionCode();
            if (cacheVersion < currentVersion) {
                PreferenceHelper.write(this, "first_install", "first_install", currentVersion);
                cleanImageCache();
            }
        }


        String filename = KidsWatConfig.getTempFilePath() + "alarm.wav";
        java.io.File f = new java.io.File(filename);
        if (f.exists()) {
            //
        } else {
            try {
                KidsWatUtils.copyBigDataToSD(this, "alarm.wav", filename);
            } catch (IOException e) {
                CustomLog.logException(TAG,e);
            }
        }
    }

    @SuppressLint("Override")
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] arg1, int[] grantResults) {

        // TODO Auto-generated method stub
        if (requestCode == Contanst.REQUEST_PHONE_REQUEST_PERMISSION) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                //permission granted
                //TODO 向SD卡写数据
                requreStoragePermission(pub_view);
            } else {
                //permission denied
                //TODO 显示对话框告知用户必须打开权限
                requreStoragePermission(pub_view);
                Toast.makeText(AppStart.this, getApplicationContext().getResources().getString(R.string.phone_permission), Toast.LENGTH_LONG).show();
            }
        } else if (requestCode == Contanst.REQUEST_STORAGE_REQUEST_PERMISSION) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                //permission granted
                //TODO 向SD卡写数据
                showAnimAndDownFile(pub_view);
            } else {
                //permission denied
                //TODO 显示对话框告知用户必须打开权限
                showAnimAndDownFile(pub_view);
                Toast.makeText(AppStart.this, getApplicationContext().getResources().getString(R.string.storage_permission), Toast.LENGTH_LONG).show();
            }
        }
    }

    // 下载文件线程
    public class downloadApkThread extends Thread {


        @Override
        public void run() {
            // 判断SD卡是否挂载，是否开启文件读写权限
            if (Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) {
                // 获取apk文件路径
                String apkDir = KidsWatConfig.getTempFilePath();
                InputStream in = null;
                FileOutputStream out = null;
                try {
                    // 创建连接
                    URL url = new URL("http://download.misafes.com/download/kidswatcher__v1.2.5.apk");
                    HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                    conn.connect();
                    // 获取文件大小
                    int length = conn.getContentLength();
                    // 获取输入流
                    in = conn.getInputStream();
                    File apkDirFile = new File(apkDir);
                    if (!apkDirFile.exists()) {
                        apkDirFile.mkdirs();
                    }
                    File apkFile = new File(apkDir, "kidswatcher.apk");
                    out = new FileOutputStream(apkFile);
                    int len = -1;
                    int count = 0;
                    byte[] bys = new byte[1024];

                    while (!isCancelUpdate && ((len = in.read(bys)) != -1)) {
                        // 下载中
                        out.write(bys, 0, len);
                        // 计算进度条的位置
                        count += len;
                        mProgress = (int) (((float) count / length) * 100);
                        // 更新进度
                        mHandler.sendEmptyMessage(DOWNLOADING);
                    }
                    // 下载完成
                    // 更新进度条
                    try {
                        in.close();
                        out.close();
                    } catch (IOException e) {
                        // TODO Auto-generated catch block
                        CustomLog.logException(TAG,e);
                    }

                    if (!isCancelUpdate) {
                        CustomLog.d(TAG,"download finish");
                        mHandler.sendEmptyMessage(DOWNLOAD_FINISH);
                    }
                } catch (Exception e) {
                    // TODO Auto-generated catch block
                    CustomLog.logException(TAG,e);
                }
            }
        }
    }
}
