package com.qiwo.xkidswatcher_russia;

import android.annotation.TargetApi;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.os.StatFs;
import android.provider.DocumentsContract;
import android.provider.MediaStore;


import net.intari.CustomLogger.CustomLog;



import static android.content.Intent.URI_INTENT_SCHEME;

/**
 * Created by Dmitriy Kazimirov on 26/01/2019.
 */
public class Utils {


    public static String getTimeFromTimestamp(long epoch) {
        return new java.text.SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(new java.util.Date (epoch*1000));
    }

    /**
     * Normalized phone number if it's incorrect.
     * WARNING: Russia-only.
     * @param tag - log tag
     * @param number - number to use
     * @return normalized numer
     */
    public static String normalizePhoneNumber(String tag, String number) {
        CustomLog.d(tag,"Will validate phone number "+number+"");
        if (number.startsWith("+7")) {
            CustomLog.d(tag,"Number "+number+" starts with +7, it's ok");
            return number;
        } else if (number.startsWith("7")) {
            CustomLog.d(tag,"Number "+number+" starts with 7, it's ok");
            return number;
        } else if (number.startsWith("8")) {
            CustomLog.d(tag,"Number "+number+" starts with 8, it's ok");
            return number;
        } else {
            String fixed="+7"+number;
            CustomLog.d(tag,"Number "+number+" now fixed as "+fixed);
            return fixed;
        }

    }



    /* Get uri related content real local file path. */
    public static  String getUriRealPath(Context ctx, Uri uri)
    {
        String ret = "";

        if( isAboveKitKat() )
        {
            // Android OS above sdk version 19.
            ret = getUriRealPathAboveKitkat(ctx, uri);
        }else
        {
            // Android OS below sdk version 19
            ret = getImageRealPath(ctx.getContentResolver(), uri, null);
        }

        return ret;
    }

    public static void logFreeSpaceOn(String tag, String path) {
        try {
            //StatFs stat = new StatFs(Environment.getExternalStorageDirectory().getPath());
            StatFs stat = new StatFs(path);

            long bytesAvailable;
            if (android.os.Build.VERSION.SDK_INT >=
                    android.os.Build.VERSION_CODES.JELLY_BEAN_MR2) {
                bytesAvailable = stat.getBlockSizeLong() * stat.getAvailableBlocksLong();
            }
            else {
                bytesAvailable = (long)stat.getBlockSize() * (long)stat.getAvailableBlocks();
            }
            long megAvailable = bytesAvailable / (1024 * 1024);
            CustomLog.i(tag,path+" has "+megAvailable+" Mb available");

        } catch (Exception ex) {
            //it's ok. it's only internal issue
            CustomLog.e(tag,"Failed to log free space on "+path+" due to "+ex+", NOT logged stacktrace");
        }
    }
    public static String toUri(Intent intent) {
        if (intent!=null)
            return intent.toUri(URI_INTENT_SCHEME|Intent.URI_ALLOW_UNSAFE);
        else
            return "<null intent>";

        /*
        if (intent.getPackage()!=null) {
            return intent.toUri(URI_INTENT_SCHEME|Intent.URI_ALLOW_UNSAFE|Intent.URI_ANDROID_APP_SCHEME);
        } else {

        }
        */
    }

    @TargetApi(Build.VERSION_CODES.KITKAT)
    private static String getUriRealPathAboveKitkat(Context ctx, Uri uri)
    {
        String ret = "";

        if(ctx != null && uri != null) {

            if(isContentUri(uri))
            {
                if(isGooglePhotoDoc(uri.getAuthority()))
                {
                    ret = uri.getLastPathSegment();
                }else {
                    ret = getImageRealPath(ctx.getContentResolver(), uri, null);
                }
            }else if(isFileUri(uri)) {
                ret = uri.getPath();
            }else if(isDocumentUri(ctx, uri)){

                // Get uri related document id.
                String documentId = DocumentsContract.getDocumentId(uri);

                // Get uri authority.
                String uriAuthority = uri.getAuthority();

                if(isMediaDoc(uriAuthority))
                {
                    String idArr[] = documentId.split(":");
                    if(idArr.length == 2)
                    {
                        // First item is document type.
                        String docType = idArr[0];

                        // Second item is document real id.
                        String realDocId = idArr[1];

                        // Get content uri by document type.
                        Uri mediaContentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                        if("image".equals(docType))
                        {
                            mediaContentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                        }else if("video".equals(docType))
                        {
                            mediaContentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                        }else if("audio".equals(docType))
                        {
                            mediaContentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                        }

                        // Get where clause with real document id.
                        String whereClause = MediaStore.Images.Media._ID + " = " + realDocId;

                        ret = getImageRealPath(ctx.getContentResolver(), mediaContentUri, whereClause);
                    }

                }else if(isDownloadDoc(uriAuthority))
                {
                    // Build download uri.
                    Uri downloadUri = Uri.parse("content://downloads/public_downloads");

                    // Append download document id at uri end.
                    Uri downloadUriAppendId = ContentUris.withAppendedId(downloadUri, Long.valueOf(documentId));

                    ret = getImageRealPath(ctx.getContentResolver(), downloadUriAppendId, null);

                }else if(isExternalStoreDoc(uriAuthority))
                {
                    String idArr[] = documentId.split(":");
                    if(idArr.length == 2)
                    {
                        String type = idArr[0];
                        String realDocId = idArr[1];

                        if("primary".equalsIgnoreCase(type))
                        {
                            ret = Environment.getExternalStorageDirectory() + "/" + realDocId;
                        }
                    }
                }
            }
        }

        return ret;
    }

    /* Check whether current android os version is bigger than kitkat or not. */
    private static boolean isAboveKitKat()
    {
        boolean ret = false;
        ret = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT;
        return ret;
    }

    /* Check whether this uri represent a document or not. */
    @TargetApi(Build.VERSION_CODES.KITKAT)
    private static boolean isDocumentUri(Context ctx, Uri uri)
    {
        boolean ret = false;
        if(ctx != null && uri != null) {
            ret = DocumentsContract.isDocumentUri(ctx, uri);
        }
        return ret;
    }

    public static String formatStringArray(String[] array) {
        if (array==null) {
            return "<null>";
        } else {
            StringBuilder stringBuilder=new StringBuilder();
            stringBuilder.append("(Size:");
            stringBuilder.append(array.length);
            stringBuilder.append("items:");
            for (String s:array) {
                stringBuilder.append(s);
                stringBuilder.append(" ");
            }

            stringBuilder.append(")");
            return stringBuilder.toString();
        }

    }

    public static String formatIntArray(int[] array) {
        if (array==null) {
            return "<null>";
        } else {
            StringBuilder stringBuilder=new StringBuilder();
            stringBuilder.append("(Size:");
            stringBuilder.append(array.length);
            stringBuilder.append("items:");
            for (int i:array) {
                stringBuilder.append(i);
                stringBuilder.append(" ");
            }

            stringBuilder.append(")");
            return stringBuilder.toString();
        }

    }
    /* Check whether this uri is a content uri or not.
     *  content uri like content://media/external/images/media/1302716
     *  */
    private static boolean isContentUri(Uri uri)
    {
        boolean ret = false;
        if(uri != null) {
            String uriSchema = uri.getScheme();
            if("content".equalsIgnoreCase(uriSchema))
            {
                ret = true;
            }
        }
        return ret;
    }

    /* Check whether this uri is a file uri or not.
     *  file uri like file:///storage/41B7-12F1/DCIM/Camera/IMG_20180211_095139.jpg
     * */
    private static boolean isFileUri(Uri uri)
    {
        boolean ret = false;
        if(uri != null) {
            String uriSchema = uri.getScheme();
            if("file".equalsIgnoreCase(uriSchema))
            {
                ret = true;
            }
        }
        return ret;
    }


    /* Check whether this document is provided by ExternalStorageProvider. */
    private static boolean isExternalStoreDoc(String uriAuthority)
    {
        boolean ret = false;

        if("com.android.externalstorage.documents".equals(uriAuthority))
        {
            ret = true;
        }

        return ret;
    }

    /* Check whether this document is provided by DownloadsProvider. */
    private static boolean isDownloadDoc(String uriAuthority)
    {
        boolean ret = false;

        if("com.android.providers.downloads.documents".equals(uriAuthority))
        {
            ret = true;
        }

        return ret;
    }

    /* Check whether this document is provided by MediaProvider. */
    private static boolean isMediaDoc(String uriAuthority)
    {
        boolean ret = false;

        if("com.android.providers.media.documents".equals(uriAuthority))
        {
            ret = true;
        }

        return ret;
    }

    /* Check whether this document is provided by google photos. */
    private static boolean isGooglePhotoDoc(String uriAuthority)
    {
        boolean ret = false;

        if("com.google.android.apps.photos.content".equals(uriAuthority))
        {
            ret = true;
        }

        return ret;
    }

    /* Return uri represented document file real local path.*/
    private static String getImageRealPath(ContentResolver contentResolver, Uri uri, String whereClause)
    {
        String ret = "";

        // Query the uri with condition.
        Cursor cursor = contentResolver.query(uri, null, whereClause, null, null);

        if(cursor!=null)
        {
            boolean moveToFirst = cursor.moveToFirst();
            if(moveToFirst)
            {

                // Get columns name by uri type.
                String columnName = MediaStore.Images.Media.DATA;

                if( uri==MediaStore.Images.Media.EXTERNAL_CONTENT_URI )
                {
                    columnName = MediaStore.Images.Media.DATA;
                }else if( uri==MediaStore.Audio.Media.EXTERNAL_CONTENT_URI )
                {
                    columnName = MediaStore.Audio.Media.DATA;
                }else if( uri==MediaStore.Video.Media.EXTERNAL_CONTENT_URI )
                {
                    columnName = MediaStore.Video.Media.DATA;
                }

                // Get column index.
                int imageColumnIndex = cursor.getColumnIndex(columnName);

                // Get column value which is the uri related file local path.
                ret = cursor.getString(imageColumnIndex);
            }
        }

        return ret;
    }


}
