package com.qiwo.xkidswatcher_russia.thread;

import android.text.format.DateFormat;

import net.intari.CustomLogger.CustomLog;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class DateUtils {

    public static final String TAG = DateUtils.class.getSimpleName();

    /**
     * ����unixʱ��� (1970�����������)
     *
     * @return
     */
    public static long getUnixStamp() {
        return System.currentTimeMillis() / 1000;
    }

    /**
     * �õ����������
     *
     * @return
     */
    public static String getYestoryDate() {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DATE, -1);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        String yestoday = sdf.format(calendar.getTime());
        return yestoday;
    }

    /**
     * �õ����������
     *
     * @return
     */
    public static String getTodayDate() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        String date = sdf.format(new Date());
        return date;
    }

    /**
     * ʱ���ת��Ϊʱ���ʽ
     *
     * @param timeStamp
     * @return
     */
    public static String timeStampToStr(long timeStamp) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String date = sdf.format(timeStamp * 1000);
        return date;
    }

    /**
     * �õ����� yyyy-MM-dd
     *
     * @param timeStamp ʱ���
     * @return
     */
    public static String formatDate(long timeStamp) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        String date = sdf.format(timeStamp * 1000);
        return date;
    }

    /**
     * �õ�ʱ�� HH:mm:ss
     *
     * @param timeStamp ʱ���
     * @return
     */
    public static String getTime(long timeStamp) {
        String time = null;
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String date = sdf.format(timeStamp * 1000);
        String[] split = date.split("\\s");
        if (split.length > 1) {
            time = split[1];
        }
        return time;
    }

//	/**
//	 * ��һ��ʱ���ת������ʾ��ʱ���ַ�������ոգ�1��ǰ
//	 * 
//	 * @param timeStamp
//	 * @return
//	 */
//	public static String convertTimeToFormat(long timeStamp) {
//		long curTime = System.currentTimeMillis() / (long) 1000;
//		long time = curTime - timeStamp;
//
//		if (time < 60 && time >= 0) {
//			return "just now";
//		} else if (time >= 60 && time < 3600) {
//			return time / 60 + "mins ago";
//		} else if (time >= 3600 && time < 3600 * 24) {
//			return time / 3600 + "hours ago";
//		} else if (time >= 3600 * 24 && time < 3600 * 24 * 30) {
//			return time / 3600 / 24 + "days ago";
//		} else if (time >= 3600 * 24 * 30 && time < 3600 * 24 * 30 * 12) {
//			return time / 3600 / 24 / 30 + "months ago";
//		} else if (time >= 3600 * 24 * 30 * 12) {
//			return time / 3600 / 24 / 30 / 12 + "years ago";
//		} else {
//			return "just now";
//		}
//	}

    /**
     * 俄罗斯版
     *
     * @param timeStamp
     * @return
     */
    public static String convertTimeToFormat(long timeStamp) {
        long curTime = System.currentTimeMillis() / (long) 1000;
        long time = curTime - timeStamp;

        if (time < 60 && time >= 0) {
            return "прямо сейчас";
        } else if (time >= 60 && time < 3600) {
            return time / 60 + "минут Перед";
        } else if (time >= 3600 && time < 3600 * 24) {
            return time / 3600 + "час Перед";
        } else if (time >= 3600 * 24 && time < 3600 * 24 * 30) {
            return time / 3600 / 24 + "день Перед";
        } else if (time >= 3600 * 24 * 30 && time < 3600 * 24 * 30 * 12) {
            return time / 3600 / 24 / 30 + "месяц Перед";
        } else if (time >= 3600 * 24 * 30 * 12) {
            return time / 3600 / 24 / 30 / 12 + "год Перед";
        } else {
            return "прямо сейчас";
        }
    }

    /**
     * ��һ��ʱ���ת������ʾ��ʱ���ַ�����(���ٷ���)
     *
     * @param timeStamp
     * @return
     */
    public static String timeStampToFormat(long timeStamp) {
        long curTime = System.currentTimeMillis() / (long) 1000;
        long time = curTime - timeStamp;
        return time / 60 + "";
    }

    public static String getTimes(int time) {

        String str = "";
        int h = time / 60 / 60;
        int m = time / 60;
        int s = time % 60;
        if (h == 0)
            str = String.format("%02d", m) + ":" + String.format("%02d", s);
        else
            str = String.format("%02d", h) + ":" + String.format("%02d", m)
                    + ":" + String.format("%02d", s);
        return str;
    }

    /**
     * 返回美国时间格式 	v2
     * <p>
     * 1. 今天	08:32 AM;
     * 2. 昨天	Yestoday 08:32 AM;
     * 3. 七天内 	Monday 08:32 AM;
     * 4. 七天外	2015/2/3 08:32 AM;
     *
     * @param str
     * @return
     */
    public static String getDate(long time) {

        long curTime = System.currentTimeMillis() / (long) 1000;

        //今天0点
        Calendar today = new GregorianCalendar();
        today.set(Calendar.HOUR_OF_DAY, 0);
        today.set(Calendar.MINUTE, 0);
        today.set(Calendar.SECOND, 0);
        long todaystart = today.getTimeInMillis();
        //昨天0点
        long yestodaystart = todaystart - 24 * 60 * 60 * 1000;
        //本周开始
        Calendar monday = new GregorianCalendar();
        monday.setFirstDayOfWeek(Calendar.MONDAY);
        monday.set(Calendar.HOUR_OF_DAY, 0);
        monday.set(Calendar.MINUTE, 0);
        monday.set(Calendar.SECOND, 0);
        monday.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
        long firstDayOfWeek = monday.getTimeInMillis();
        //七日内开始
        long weekStart = todaystart - 24 * 60 * 60 * 1000 * 6;
        if (time > todaystart) {
            //今天
			/*SimpleDateFormat formatter = new SimpleDateFormat("HH:mm");
			String datestr = formatter.format(time);
			return datestr;*/
            String ctime = android.text.format.DateFormat.format("hh:mm", new Date(time)).toString();
//			String period = Calendar.getInstance().getTime().getHours() >= 12 ? "PM" : "AM";
            String period = new Date(time).getHours() >= 12 ? "PM" : "AM";
            return ctime + " " + period;
        } else if (time > yestodaystart && time < todaystart) {
            //昨天
//			SimpleDateFormat formatter = new SimpleDateFormat("HH:mm");
//			String datestr = formatter.format(time);
//			return "Yesterday " + datestr;
            String ctime = android.text.format.DateFormat.format("hh:mm", new Date(time)).toString();
//			String period = Calendar.getInstance().getTime().getHours() >= 12 ? "PM" : "AM";
            String period = new Date(time).getHours() >= 12 ? "PM" : "AM";
            return "Yesterday " + ctime + " " + period;
        } else if (time > weekStart && time < yestodaystart) {
            //七日内
//			SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd, HH:mm");
//			String str = formatter.format(new Date(time));
//			ParsePosition pos = new ParsePosition(0);
//			Date strtodate = formatter.parse(str, pos);
//			String j = strtodate.toString();
//			String[] k = j.split(" ");
//			String period = new Date(time).getHours() >= 12 ? "PM" : "AM";
//			//return k[2] + k[1].toUpperCase() + k[5].substring(2, 4);
//			//return k[2] + " " + k[1] + " " + k[5] + "  " + k[3];
//			return getDayOfWeek(k[0]) + " " + k[3].substring(0,5) + " " + period;
            String ctime = android.text.format.DateFormat.format("hh:mm", new Date(time)).toString();
            String period = new Date(time).getHours() >= 12 ? "PM" : "AM";
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(new Date(time));
            return getDayOfWeek(calendar.get(Calendar.DAY_OF_WEEK)) + " " + ctime + " " + period;
        } else {
            //七日外
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy/MM/dd hh:mm");
            String str = formatter.format(new Date(time));
            String period = new Date(time).getHours() >= 12 ? "PM" : "AM";
            return str + " " + period;
        }
    }

    public static String getDayOfWeek(String str) {
        String dayOfWeek = null;
        switch (str) {
            case "Mon":
                dayOfWeek = "Monday";
                break;
            case "Tue":
                dayOfWeek = "Tuesday";
                break;
            case "Wed":
                dayOfWeek = "Wednesday";
                break;
            case "Thu":
                dayOfWeek = "Thursday";
                break;
            case "Fri":
                dayOfWeek = "Friday";
                break;
            case "Sat":
                dayOfWeek = "Saturday";
                break;
            case "Sun":
                dayOfWeek = "Sunday";
                break;
        }
        return dayOfWeek;
    }

    public static String getDayOfWeek(int i) {
        String dayOfWeek = null;
        switch (i) {
            case 1:
                dayOfWeek = "Sunday";
                break;
            case 2:
                dayOfWeek = "Monday";
                break;
            case 3:
                dayOfWeek = "Tuesday";
                break;
            case 4:
                dayOfWeek = "Wednesday";
                break;
            case 5:
                dayOfWeek = "Thursday";
                break;
            case 6:
                dayOfWeek = "Friday";
                break;
            case 7:
                dayOfWeek = "Saturday";
                break;
        }
        return dayOfWeek;
    }

    public static String getMonthString(int i) {
        switch (i) {
            case 1:
                return "Январь";

            case 2:
                return "Февраль";

            case 3:
                return "Март";

            case 4:
                return "Апрель";

            case 5:
                return "Май";

            case 6:
                return "Июнь";

            case 7:
                return "Июль";

            case 8:
                return "Август";

            case 9:
                return "Сентябрь";

            case 10:
                return "Октябрь";

            case 11:
                return "Ноябрь";

            case 12:
                return "Декабрь";

            default:
                return "Январь";
        }
    }

    /**
     * 将时间格式转换成dd/MM/yyyy格式;
     */
    public static String getNewDateFomate(long birthday) {
        return DateFormat.format("dd/MM/yyyy", birthday * 1000).toString();
    }

    /**
     * 将时间格式从yyyy-MM-dd转换成dd/MM/yyyy格式;
     */
    public static String change2NewDateFomate(String birthday) {
        SimpleDateFormat sdf_old = new SimpleDateFormat("yyyy-MM-dd");
        try {
            Date parse_birthday = sdf_old.parse(birthday);

            SimpleDateFormat sdf_new = new SimpleDateFormat("dd/MM/yyyy");
            return sdf_new.format(parse_birthday);
        } catch (ParseException e) {
            CustomLog.logException(TAG,e);
        }
        return null;
    }
}
