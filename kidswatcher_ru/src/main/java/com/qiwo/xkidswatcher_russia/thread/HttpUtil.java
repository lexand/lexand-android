package com.qiwo.xkidswatcher_russia.thread;

import android.util.Log;

import net.intari.CustomLogger.CustomLog;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.CookieStore;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.KeyStore;

import cz.msebera.android.httpclient.HttpEntity;
import cz.msebera.android.httpclient.HttpResponse;
import cz.msebera.android.httpclient.HttpVersion;
import cz.msebera.android.httpclient.client.HttpClient;
import cz.msebera.android.httpclient.client.methods.HttpPost;
import cz.msebera.android.httpclient.conn.ClientConnectionManager;
import cz.msebera.android.httpclient.conn.scheme.PlainSocketFactory;
import cz.msebera.android.httpclient.conn.scheme.Scheme;
import cz.msebera.android.httpclient.conn.scheme.SchemeRegistry;
import cz.msebera.android.httpclient.conn.scheme.SocketFactory;
import cz.msebera.android.httpclient.entity.StringEntity;
import cz.msebera.android.httpclient.impl.client.DefaultHttpClient;
import cz.msebera.android.httpclient.impl.conn.tsccm.ThreadSafeClientConnManager;
import cz.msebera.android.httpclient.params.BasicHttpParams;
import cz.msebera.android.httpclient.params.HttpConnectionParams;
import cz.msebera.android.httpclient.params.HttpParams;
import cz.msebera.android.httpclient.params.HttpProtocolParams;
import cz.msebera.android.httpclient.protocol.HTTP;

import static com.qiwo.xkidswatcher_russia.Constants.NETWORK_NUM_RETRIES;
import static com.qiwo.xkidswatcher_russia.Constants.OLD4_HTTP_TIMEOUT_DEFAULT;
import static com.qiwo.xkidswatcher_russia.Constants.OLD_HTTP_SO_TIMEOUT_DEFAULT;
import static com.qiwo.xkidswatcher_russia.Constants.OLD_HTTP_TIMEOUT_DEFAULT;
import static com.qiwo.xkidswatcher_russia.Constants.TIMEOUT_MULTIPLIER;

public class HttpUtil {
    public static final String TAG = HttpUtil.class.getSimpleName();

    /**
     *
     */
    public static CookieStore cookieStore;
    private static DefaultHttpClient httpClient;

    static {
        if (httpClient == null) {
            httpClient = createHttpClient();
        }
    }

    private static DefaultHttpClient createHttpClient() {
        CustomLog.d(TAG, "createHttpClient");
        HttpParams params = new BasicHttpParams();

        return new DefaultHttpClient(params);
    }

    /*
     *
     */

    public static String SendRequest(String adress_Http, String strJson) {

        String returnLine = "";
        try {

            CustomLog.d(TAG,"**************锟斤拷始http通讯**************");
            CustomLog.d(TAG,"**************锟斤拷锟矫的接口碉拷址为**************"
                    + adress_Http);
            CustomLog.d(TAG,"**************锟斤拷锟斤拷锟酵碉拷锟斤拷锟斤拷为**************"
                    + strJson);
            URL my_url = new URL(adress_Http);
            HttpURLConnection connection = (HttpURLConnection) my_url
                    .openConnection();
            connection.setDoOutput(true);
            connection.setDoInput(true);
            connection.setRequestMethod("POST");
            connection.setConnectTimeout(5000);
            connection.setReadTimeout(5000);
            connection.setUseCaches(false);
            connection.setInstanceFollowRedirects(true);
            connection.setRequestProperty("Content-Type", "application/json");
            connection.connect();
            DataOutputStream out = new DataOutputStream(
                    connection.getOutputStream());

            byte[] content = strJson.getBytes("utf-8");

            out.write(content, 0, content.length);
            out.flush();
            out.close(); // flush and close

            BufferedReader reader = new BufferedReader(new InputStreamReader(
                    connection.getInputStream(), "utf-8"));

            // StringBuilder builder = new StringBuilder();

            String line = "";

            CustomLog.d(TAG,"Contents of post request start");

            while ((line = reader.readLine()) != null) {
                // line = new String(line.getBytes(), "utf-8");
                returnLine += line;
                CustomLog.d(TAG,line);
            }
            CustomLog.d(TAG,"Contents of post request ends");

            reader.close();
            connection.disconnect();
            CustomLog.d(TAG,"========锟斤拷锟截的斤拷锟斤拷锟轿�=======" + returnLine);

        } catch (Exception e) {
            CustomLog.logException(TAG,e);
        }
        return returnLine;
    }

    /**
     * 正常HTTP交互
     *
     * @param url
     * @param StrJson
     * @return
     */

    public static String getObjectResult(String url, String StrJson) {
        CustomLog.i(TAG+":getObjectResult()","Will be POST'ing to url:"+url+" with content:"+StrJson);
        int retryCount=0;
        float timeoutMultiplier=1.0f;
        while (retryCount<NETWORK_NUM_RETRIES) {
            String result=getObjectResultInternal(url,StrJson,timeoutMultiplier);
            if (result==null) {
                retryCount++;
                timeoutMultiplier=timeoutMultiplier*TIMEOUT_MULTIPLIER;
                CustomLog.e(TAG+":getObjectResult()","Will retry operation. retry cout is "+retryCount+", timeout multiplier is "+timeoutMultiplier);
            } else {
                CustomLog.v(TAG+":getObjectResult()","will return result "+result);
                return result;
            }
        }
        CustomLog.w(TAG+":getObjectResult()","Will return empty result -:(, no more retries allowed");
        return "";
    }

    public static String getObjectResultInternal(String url, String StrJson,float timeoutMultiplier) {
        String result = null;
        InputStream is = null;
        CustomLog.i(TAG+":getObjectResultInternal()","POST'ing to url:"+url+" with content:"+StrJson);
        try {

            HttpPost httppost = new HttpPost(url);
            httppost.setHeader("content-type", "application/json");
            if (StrJson != null) {
                httppost.setEntity(new StringEntity(StrJson, "utf-8"));
            }

            HttpResponse response = (HttpResponse) getNewHttpClient(timeoutMultiplier).execute(
                    httppost);

            HttpEntity entity = response.getEntity();
            is = entity.getContent();

            BufferedReader reader = new BufferedReader(new InputStreamReader(
                    is, "utf-8"));
            StringBuilder sb = new StringBuilder();
            String line = null;
            while ((line = reader.readLine()) != null) {
                sb.append(line);
            }
            is.close();
            result = sb.toString();
            CustomLog.d(TAG+":getObjectResult()",result);

        } catch (Exception e) {
            CustomLog.logException(TAG+":getObjectResult()",e);
        }
        return result;
    }

    /**
     * 绕过HTTPS验证
     *
     * @param url
     * @param StrJson
     * @return
     */
    public static String getObjectResult4(String url, String StrJson) {
        String result = "";
        InputStream is = null;
        CustomLog.d(TAG,"URL:"+url+".json:"+StrJson);
        try {

            HttpPost httppost = new HttpPost(url);
            httppost.setHeader("content-type", "application/json");
            if (StrJson != null) {
                httppost.setEntity(new StringEntity(StrJson, "utf-8"));
            }

            HttpResponse response = (HttpResponse) getNewHttpClient4().execute(
                    httppost);

            HttpEntity entity = response.getEntity();
            is = entity.getContent();

            BufferedReader reader = new BufferedReader(new InputStreamReader(
                    is, "utf-8"));
            StringBuilder sb = new StringBuilder();
            String line = null;
            while ((line = reader.readLine()) != null) {
                sb.append(line);
            }
            is.close();
            result = sb.toString();
            CustomLog.d(TAG+":getResult",result);

        } catch (Exception e) {
            CustomLog.logException(TAG,e);
        }
        return result;
    }

    public static HttpClient getNewHttpClient() {
        try {
            KeyStore trustStore = KeyStore.getInstance(KeyStore
                    .getDefaultType());
            trustStore.load(null, null);

            SSLSocketFactory sf = new SSLSocketFactory(trustStore);
            sf.setHostnameVerifier(SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);

            HttpParams params = new BasicHttpParams();
            HttpProtocolParams.setVersion(params, HttpVersion.HTTP_1_1);
            HttpProtocolParams.setContentCharset(params, HTTP.UTF_8);
            HttpConnectionParams.setConnectionTimeout(params, OLD_HTTP_TIMEOUT_DEFAULT);
            HttpConnectionParams.setSoTimeout(params, OLD_HTTP_SO_TIMEOUT_DEFAULT);

            SchemeRegistry registry = new SchemeRegistry();
            registry.register(new Scheme("http", PlainSocketFactory
                    .getSocketFactory(), 80));
            registry.register(new Scheme("https", (SocketFactory) sf, 443));

            ClientConnectionManager ccm = new ThreadSafeClientConnManager(
                    params, registry);

            return new DefaultHttpClient(ccm, params);
        } catch (Exception e) {
            CustomLog.logException(TAG,e);
            return new DefaultHttpClient();
        }
    }
    public static HttpClient getNewHttpClient(float timeoutMultiplier) {
        try {
            KeyStore trustStore = KeyStore.getInstance(KeyStore
                    .getDefaultType());
            trustStore.load(null, null);

            SSLSocketFactory sf = new SSLSocketFactory(trustStore);
            sf.setHostnameVerifier(SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);

            HttpParams params = new BasicHttpParams();
            HttpProtocolParams.setVersion(params, HttpVersion.HTTP_1_1);
            HttpProtocolParams.setContentCharset(params, HTTP.UTF_8);
            float connectionTimeout=timeoutMultiplier*(float)OLD_HTTP_TIMEOUT_DEFAULT;
            float soTimeout=timeoutMultiplier*(float)OLD_HTTP_SO_TIMEOUT_DEFAULT;
            HttpConnectionParams.setConnectionTimeout(params, Math.round(connectionTimeout));
            HttpConnectionParams.setSoTimeout(params, Math.round(soTimeout));

            SchemeRegistry registry = new SchemeRegistry();
            registry.register(new Scheme("http", PlainSocketFactory
                    .getSocketFactory(), 80));
            registry.register(new Scheme("https", (SocketFactory) sf, 443));

            ClientConnectionManager ccm = new ThreadSafeClientConnManager(
                    params, registry);

            return new DefaultHttpClient(ccm, params);
        } catch (Exception e) {
            CustomLog.logException(TAG,e);
            return new DefaultHttpClient();
        }
    }

    public static HttpClient getNewHttpClient4() {
        try {
            KeyStore trustStore = KeyStore.getInstance(KeyStore
                    .getDefaultType());
            trustStore.load(null, null);

            SSLSocketFactory sf = new SSLSocketFactory(trustStore);
            sf.setHostnameVerifier(SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);

            HttpParams params = new BasicHttpParams();
            HttpProtocolParams.setVersion(params, HttpVersion.HTTP_1_1);
            HttpProtocolParams.setContentCharset(params, HTTP.UTF_8);
            HttpConnectionParams.setConnectionTimeout(params, OLD4_HTTP_TIMEOUT_DEFAULT);
            HttpConnectionParams.setSoTimeout(params, OLD4_HTTP_TIMEOUT_DEFAULT);

            SchemeRegistry registry = new SchemeRegistry();
            registry.register(new Scheme("http", PlainSocketFactory
                    .getSocketFactory(), 80));
            registry.register(new Scheme("https", (SocketFactory) sf, 443));

            ClientConnectionManager ccm = new ThreadSafeClientConnManager(
                    params, registry);

            return new DefaultHttpClient(ccm, params);
        } catch (Exception e) {
            CustomLog.logException(TAG,e);
            return new DefaultHttpClient();
        }
    }

    /**
     * get
     */

    public static DefaultHttpClient getHttpClient() {
        if (cookieStore != null) {
            httpClient
                    .setCookieStore((cz.msebera.android.httpclient.client.CookieStore) cookieStore);
        }
        return httpClient;
    }

    public static String getStrResult(String url) {
        String result = "";
        InputStream is = null;

        CustomLog.e(TAG,"URL:" + url);
        try {
            DefaultHttpClient httpclient = getHttpClient();
            HttpPost httppost = new HttpPost(url);
            // httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs,
            // "UTF-8"));
            HttpResponse response = httpclient.execute(httppost);

            // if (httpclient.getCookieStore() != null) {
            // cookieStore = (CookieStore) httpclient.getCookieStore();
            // }
            HttpEntity entity = response.getEntity();
            is = entity.getContent();
            BufferedReader reader = new BufferedReader(new InputStreamReader(
                    is, "utf-8"));
            StringBuilder sb = new StringBuilder();
            String line = null;
            while ((line = reader.readLine()) != null) {
                sb.append(line);
            }
            is.close();
            result = sb.toString();
            CustomLog.d(TAG+":getResult",result);
        } catch (Exception e) {
            CustomLog.logException(TAG,e);
        }
        return result;
    }

}
