package com.qiwo.xkidswatcher_russia.thread;

import net.intari.CustomLogger.CustomLog;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Map;
import java.util.Set;

public class CommonThreadMethod {

    public static final String TAG = CommonThreadMethod.class.getSimpleName();

    /**
     * 格式化StrJSON
     */

    public static String JsonFormat(Map<String, String> Str) {

        if (Str == null || Str.size() == 0)
            return null;

        Set<Map.Entry<String, String>> entrySet = Str.entrySet();

        JSONObject StrJson = new JSONObject();
        try {
            for (Map.Entry<String, String> entry : entrySet) {
                StrJson.put(entry.getKey(), entry.getValue());
            }
        } catch (JSONException e) {
            CustomLog.logException(TAG,e);
        }

        return StrJson.toString();
    }
}
