package com.qiwo.xkidswatcher_russia.thread;

import android.util.Log;

import net.intari.CustomLogger.CustomLog;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

import cz.msebera.android.httpclient.HttpEntity;
import cz.msebera.android.httpclient.HttpResponse;
import cz.msebera.android.httpclient.NameValuePair;
import cz.msebera.android.httpclient.client.CookieStore;
import cz.msebera.android.httpclient.client.entity.UrlEncodedFormEntity;
import cz.msebera.android.httpclient.client.methods.HttpPost;
import cz.msebera.android.httpclient.impl.client.DefaultHttpClient;
import cz.msebera.android.httpclient.params.BasicHttpParams;
import cz.msebera.android.httpclient.params.HttpParams;

/**
 * Is this class used AT ALL?!
 * It appears it doesnt't
 * and it needs to be rewritten for modern api
 */

public class WebUtil {

    private static final String TAG = WebUtil.class.getSimpleName();

    public static int timeoutConnection = 8000;
    public static int timeoutSocket = 10000;
    public static CookieStore cookieStore;
    private static DefaultHttpClient httpClient;

    static {
        if (httpClient == null) {
            httpClient = createHttpClient();
        }
    }

    public static DefaultHttpClient getHttpClient() {
        if (cookieStore != null) {
            httpClient.setCookieStore(cookieStore);
        }
        return httpClient;
    }

    public static JSONArray getResult(String url,
                                      ArrayList<NameValuePair> nameValuePairs) {
        JSONArray json = null;
        String result = "";
        InputStream is = null;
        Log.e(url, String.valueOf(nameValuePairs));
        try {
            DefaultHttpClient httpclient = getHttpClient();
            HttpPost httppost = new HttpPost(url);
            if (nameValuePairs != null) {
                httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs,
                        "UTF-8"));
            }
            HttpResponse response = httpclient.execute(httppost);

            if (httpclient.getCookieStore() != null) {
                cookieStore = httpclient.getCookieStore();
            }
            HttpEntity entity = response.getEntity();
            is = entity.getContent();

            BufferedReader reader = new BufferedReader(new InputStreamReader(
                    is, "utf-8"));
            StringBuilder sb = new StringBuilder();
            String line = null;
            while ((line = reader.readLine()) != null) {
                sb.append(line);
            }
            is.close();
            result = sb.toString();
            json = new JSONArray(result);
            CustomLog.d(TAG+":getResult",result);
        } catch (Exception e) {
            CustomLog.logException(TAG,e);

        }
        return json;
    }

    public static String getResultReuturnStr(String url,
                                             ArrayList<NameValuePair> nameValuePairs) {
        String result = "";
        InputStream is = null;
        Log.e(url, String.valueOf(nameValuePairs));
        try {
            DefaultHttpClient httpclient = getHttpClient();
            HttpPost httppost = new HttpPost(url);
            if (nameValuePairs != null) {
                httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs,
                        "UTF-8"));
            }
            HttpResponse response = httpclient.execute(httppost);

            if (httpclient.getCookieStore() != null) {
                cookieStore = httpclient.getCookieStore();
            }
            HttpEntity entity = response.getEntity();
            is = entity.getContent();

            BufferedReader reader = new BufferedReader(new InputStreamReader(
                    is, "utf-8"));
            StringBuilder sb = new StringBuilder();
            String line = null;
            while ((line = reader.readLine()) != null) {
                sb.append(line);
            }
            is.close();
            result = sb.toString();
            // json = new JSONArray(result);
            CustomLog.d(TAG+":getResult",result);
        } catch (Exception e) {
            CustomLog.logException(TAG,e);

        }
        return result;
    }

    public static JSONObject getObjectResult(String url,
                                             ArrayList<NameValuePair> nameValuePairs) {
        JSONObject json = null;
        String result = "";
        InputStream is = null;
        Log.e(url, String.valueOf(nameValuePairs));
        try {
            DefaultHttpClient httpclient = getHttpClient();
            HttpPost httppost = new HttpPost(url);
            if (nameValuePairs != null) {
                httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs,
                        "UTF-8"));
            }

            HttpResponse response = httpclient.execute(httppost);

            if (httpclient.getCookieStore() != null) {
                cookieStore = httpclient.getCookieStore();
            }
            HttpEntity entity = response.getEntity();
            is = entity.getContent();

            BufferedReader reader = new BufferedReader(new InputStreamReader(
                    is, "utf-8"));
            StringBuilder sb = new StringBuilder();
            String line = null;
            while ((line = reader.readLine()) != null) {
                sb.append(line);
            }
            is.close();
            result = sb.toString();
            json = new JSONObject(result);
            CustomLog.d(TAG+":getResult",result);

        } catch (Exception e) {
            CustomLog.logException(TAG,e);

        }
        return json;
    }

    public static JSONObject getResult(String url) {
        JSONObject json = null;
        String result = "";
        InputStream is = null;
        try {
            DefaultHttpClient httpclient = getHttpClient();
            HttpPost httppost = new HttpPost(url);
            // httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs,
            // "UTF-8"));
            HttpResponse response = httpclient.execute(httppost);

            if (httpclient.getCookieStore() != null) {
                cookieStore = httpclient.getCookieStore();
            }
            HttpEntity entity = response.getEntity();
            is = entity.getContent();
            BufferedReader reader = new BufferedReader(new InputStreamReader(
                    is, "utf-8"));
            StringBuilder sb = new StringBuilder();
            String line = null;
            while ((line = reader.readLine()) != null) {
                sb.append(line);
            }
            is.close();
            result = sb.toString();
            json = new JSONObject(result);
            CustomLog.d(TAG+":getResult",result);
        } catch (Exception e) {
            CustomLog.logException(TAG,e);

        }
        return json;
    }

    public static String getStrResult(String url) {
        String result = "";
        InputStream is = null;

        CustomLog.e(TAG, "URL:" + url);
        try {
            DefaultHttpClient httpclient = getHttpClient();
            HttpPost httppost = new HttpPost(url);
            // httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs,
            // "UTF-8"));
            HttpResponse response = httpclient.execute(httppost);

            if (httpclient.getCookieStore() != null) {
                cookieStore = httpclient.getCookieStore();
            }
            HttpEntity entity = response.getEntity();
            is = entity.getContent();
            BufferedReader reader = new BufferedReader(new InputStreamReader(
                    is, "utf-8"));
            StringBuilder sb = new StringBuilder();
            String line = null;
            while ((line = reader.readLine()) != null) {
                sb.append(line);
            }
            is.close();
            result = sb.toString();
            CustomLog.d(TAG+":getResult",result);
        } catch (Exception e) {
            CustomLog.logException(TAG,e);
        }
        return result;
    }

    private static DefaultHttpClient createHttpClient() {
        CustomLog.d(TAG, "createHttpClient");
        HttpParams params = new BasicHttpParams();
        return new DefaultHttpClient(params);
    }

}
