package com.qiwo.xkidswatcher_russia.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.qiwo.xkidswatcher_russia.R;
import com.qiwo.xkidswatcher_russia.bean.Baby;
import com.qiwo.xkidswatcher_russia.event.BaseEvent;
import com.qiwo.xkidswatcher_russia.util.KidsWatUtils;
import net.intari.CustomLogger.CustomLog;
import com.qiwo.xkidswatcher_russia.widget.CircleImageView;

import org.greenrobot.eventbus.EventBus;

import java.util.List;

public class PhotoAdapter extends BaseAdapter {
    private final static String TAG = PhotoAdapter.class.getSimpleName();

    private List<Baby> list;
    private Context context;
    private String currentFamailyId = "";

    // private int currentIndex = 0;

    public PhotoAdapter(Context context, List<Baby> list) {
        this.context = context;
        this.list = list;
    }

    public void setList(List<Baby> list) {
        this.list = list;
    }

    // public void setCurrentIndex(int index) {
    // currentIndex = index;
    // }

    public void setCurrentFamailyId(String family_id) {
        currentFamailyId = family_id;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater inflater = LayoutInflater.from(context);
            convertView = inflater.inflate(R.layout.view_baby_photo_item, null);
        }

        Baby bb = list.get(position);
        final LinearLayout linearLayout_baby = (LinearLayout) convertView
                .findViewById(R.id.linearLayout_baby);
        final CircleImageView iv = (CircleImageView) convertView
                .findViewById(R.id.img);
        final ImageView ivPoint = (ImageView) convertView
                .findViewById(R.id.imageView_checked);

        ivPoint.setVisibility(bb.getFamily_id().equalsIgnoreCase(
                currentFamailyId) ? View.GONE : View.GONE);

        linearLayout_baby.setTag(bb.getFamily_id());
        linearLayout_baby.setBackgroundColor(bb.getFamily_id()
                .equalsIgnoreCase(currentFamailyId) ? Color.argb(255, 0xCA, 0xF1, 0xFF) : Color.argb(255, 0xE6,
                0xF9, 0xFF));
        // Color.WHITE : Color.GRAY);
        // #CAF1FF,#E6F9FF

        // -------------
        iv.setImageBitmap(KidsWatUtils.getBabyImg_v3(context, bb.getFamily_id(), bb.getSex(), bb));
        // -------------

        TextView tv = (TextView) convertView.findViewById(R.id.txt);
        // tv.setText(bb.getNickname());
        tv.setText(bb.getNickname());

        linearLayout_baby.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                CustomLog.d(TAG,"click_" + v.getTag());
                // String msg = String.format("{\"position\":\"%s\"}",
                // v.getTag());
                BaseEvent event = new BaseEvent(
                        BaseEvent.MSGTYPE_5___USERLIST_INDEX_CHANGE, v.getTag()
                        .toString());
                EventBus.getDefault().post(event);
            }
        });
        return convertView;

    }
}
