package com.qiwo.xkidswatcher_russia.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.qiwo.xkidswatcher_russia.AppContext;
import com.qiwo.xkidswatcher_russia.R;
import com.qiwo.xkidswatcher_russia.bean.Record461;
import com.qiwo.xkidswatcher_russia.fragment.RecentFragment;
import com.qiwo.xkidswatcher_russia.thread.DateUtils;
import com.qiwo.xkidswatcher_russia.util.KidsWatUtils;

import java.util.List;

public class Record461Adapter extends BaseAdapter {

    LinearLayout view_a, view_b;
    ImageView view_b_image, view_c_image, recordchoice_left,
            recordchoice_right;
    FrameLayout view_c;
    TextView view_b_text, view_c_text, view_a_text;
    private List<Record461> recordlist;
    private Context context;
    private String KidName;
    private long timePre;

    public Record461Adapter(Context context, List<Record461> recordlist,
                            String KidName) {
        this.context = context;
        this.recordlist = recordlist;
        this.KidName = KidName;
    }

    public void setV(int v) {

    }

    @Override
    public int getCount() {
        return recordlist.size();
    }

    @Override
    public Object getItem(int position) {
        return recordlist.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @SuppressLint({"ViewHolder", "DefaultLocale"})
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        // if (convertView == null) {
        convertView = LayoutInflater.from(context).inflate(
                R.layout.record_461_item, null);

        // }

        view_a = (LinearLayout) convertView.findViewById(R.id.view_a);
        view_b = (LinearLayout) convertView.findViewById(R.id.view_b);
        view_c = (FrameLayout) convertView.findViewById(R.id.view_c);

        view_b_image = (ImageView) convertView.findViewById(R.id.view_b_image);
        view_c_image = (ImageView) convertView.findViewById(R.id.view_c_image);
        recordchoice_left = (ImageView) convertView
                .findViewById(R.id.recordchoice_left);
        recordchoice_right = (ImageView) convertView
                .findViewById(R.id.recordchoice_right);

        view_a_text = (TextView) convertView.findViewById(R.id.view_a_text);
        view_b_text = (TextView) convertView.findViewById(R.id.view_b_text);
        view_c_text = (TextView) convertView.findViewById(R.id.view_c_text);

        Record461 record = recordlist.get(position);
        String relation = record.getRelation();
        switch (record.getCall_type()) {
            case 1:
//			CustomLog.d(TAG,"----------------- call -----------------");
//			Called Child 00:12 
                view_c_text
                        .setText("Called "
                                + KidName
                                + " "
                                + DateUtils.getTimes(Integer.parseInt(record
                                .getLast_time())));
                view_b.setVisibility(8);
                break;
            case 2:
                //手机打给手表
//			CustomLog.d(TAG,"----------------- miss call -----------------");
//			Called Child (Rejected)
                view_c_text.setText("Called " + KidName + " (Missed)");
                view_c_text.setTextColor(Color.RED);
                view_b.setVisibility(8);
                break;
            case 3:
//			CustomLog.d(TAG,"----------------- xx call -----------------");
                //		Called Dad 00:12

                if ("other".equals(relation)) {
                    relation = "Other";
                }
                view_b_text
                        .setText("Called " + relation + " "
                                + DateUtils.getTimes(Integer.parseInt(record
                                .getLast_time())));
                view_c_text.setTextColor(Color.RED);
                view_c.setVisibility(8);
                break;
            case 4:
                //手表打给手机
//			CustomLog.d(TAG,"----------------- xx miss call -----------------");
//			Called Dad (Rejected)
                if ("other".equals(relation)) {
                    relation = "Other";
                }
                view_b_text
                        .setText("Called " + relation + " (Missed)");
                view_b_text.setTextColor(Color.RED);
                view_c.setVisibility(8);
                break;
            case 5:
                //监听电话接通
//			CustomLog.d(TAG,"----------------- monitor -----------------");
//			Monitored Child 00:12
                view_c_text.setText("Monitored " + KidName + " " + DateUtils.getTimes(Integer.parseInt(record
                        .getLast_time())));
                view_b.setVisibility(8);
                break;
            case 6:
                //监听电话未接通
//			CustomLog.d(TAG,"----------------- monitor missed -----------------");
//			Monitored Child (Rejected)
                view_c_text
                        .setText("Monitored " + KidName + " (Failed)");
                view_c_text.setTextColor(Color.RED);
                view_b.setVisibility(8);
                break;
            case 119:
                view_a.setVisibility(4);
                view_b.setVisibility(4);
                view_c.setVisibility(4);
                return convertView;

            default:
                break;
        }

//		view_a_text.setText(DateUtils.convertTimeToFormat(Long.parseLong(record
//				.getBegin_time())));
        //美国时间格式
//		CustomLog.d(TAG,"-------------begin---------------");
        //CustomLog.d(record.getBegin_time()+"");
        long time = Long.parseLong(record.getBegin_time()) * 1000;
		/*CustomLog.d(TAG,"this time = "+time);
		if(position > 0){
			Record461 recordPre = recordlist.get(position - 1);
			timePre = Long.parseLong(recordPre.getBegin_time()) * 1000;
			if(time - timePre < 10 * 60 *1000){
				CustomLog.d(TAG," pre time = "+timePre);
				view_a.setVisibility(8);
				record.setBegin_time(time+"");
			}
		}else{
			timePre = Long.parseLong(recordlist.get(0).getBegin_time()) * 1000;
		}
		CustomLog.d(TAG,"-------------end---------------");*/
        if (RecentFragment.subTime.contains(position)) {
            view_a.setVisibility(8);
        }
//		view_a_text.setText(DateUtils.getEDate(time));
        view_a_text.setText(DateUtils.getDate(time));
		/*long time = Long.parseLong(record.getBegin_time()) * 1000;
		if(time - lastRecordTime > 10 * 60 * 1000){
			lastRecordTime = time;
			view_a_text.setText(DateUtils.getDate(time));
			view_a.setVisibility(0);
		}else{
			view_a.setVisibility(8);
		}*/
        //		System.out.println(record.getType()+"-----type----");
//		record.setType(2);

        if (!RecentFragment.isEditMode()) {
            record.setType(1);
        } else {
            record.setType(0);
        }

        if (record.getType() == 0) {
            recordchoice_left.setVisibility(8);
            recordchoice_right.setVisibility(8);
        } else {
            recordchoice_left.setVisibility(0);
            recordchoice_right.setVisibility(0);
        }

		/*view_b_text.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
			}
		});*/
		/*view_c_text.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
			}
		});*/
        //CustomLog.d(TAG,"family_id:"+AppContext.getInstance().getCurrentFamily().family_id+",AppContext.getInstance().getCurrentFamily().base64_img:"+",sex:"+AppContext.getInstance().getCurrentFamily().sex);
		/*KidsWatUtils.setKidsImage(context, view_b_image,
				AppContext.getInstance().getCurrentFamily().family_id,
				AppContext.getInstance().getCurrentFamily().base64_img,
				AppContext.getInstance().getCurrentFamily().sex);*/
//		CustomLog.d(TAG,"img_path="+AppContext.getInstance().getCurrentFamily().img_path);

//		if(!TextUtils.isEmpty(AppContext.getInstance().getCurrentFamily().img_path)){
//			AppContext.getInstance().getImageLoader().displayImage("http://"+AppContext.getInstance().getCurrentFamily().img_path, view_b_image,AppContext.getInstance().getCommenOptions());
//		}else{
//			int imgSrcId = AppContext.getInstance().getCurrentFamily().sex == 1 ? R.drawable.icon_boy
//					: R.drawable.icon_girl;
//			view_b_image.setImageResource(imgSrcId);
//		}

        Bitmap img = KidsWatUtils.getBabyImg_v3(context, AppContext.getInstance().getCurrentFamily().family_id, AppContext.getInstance().getCurrentFamily().sex,
                AppContext.getInstance().getCurrentFamily());
        view_b_image.setImageBitmap(img);

        if (record.getRelation().toLowerCase()
                .equals("dad"))
            view_c_image.setBackgroundResource(R.drawable.icon_dad);
        else if (record.getRelation().toLowerCase()
                .equals("mom"))
            view_c_image.setBackgroundResource(R.drawable.icon_mom);
        else if (record.getRelation().toLowerCase()
                .equals("grandpa"))
            view_c_image.setBackgroundResource(R.drawable.icon_grandpa);
        else if (record.getRelation().toLowerCase()
                .equals("grandma"))
            view_c_image.setBackgroundResource(R.drawable.icon_grandma);
        else
            view_c_image.setBackgroundResource(R.drawable.icon_other);

        if (RecentFragment.selections.contains(position)) {
            recordchoice_left.setImageResource(R.drawable.recordchoice_s);
        }
        return convertView;
    }
}
