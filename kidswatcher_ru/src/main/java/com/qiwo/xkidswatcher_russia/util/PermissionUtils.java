package com.qiwo.xkidswatcher_russia.util;

import android.content.ClipData;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;

import com.qiwo.xkidswatcher_russia.Utils;

import net.intari.CustomLogger.CustomLog;

import java.util.List;

/**
 * Created by Dmitriy Kazimirov on 29/01/2019.
 */
public class PermissionUtils {

    public static final String TAG = PermissionUtils.class.getSimpleName();

    public static void grantPermissionsForUri(Context context, Uri uri, Intent intent) {

        CustomLog.i(TAG,"Granting permissions for "+uri.toString()+" for intent:"+ Utils.toUri(intent));
        intent.setClipData(ClipData.newRawUri("", uri));
        intent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION|Intent.FLAG_GRANT_READ_URI_PERMISSION|Intent.FLAG_DEBUG_LOG_RESOLUTION);


        /*
        List<ResolveInfo> resolvedIntentActivities = context.getPackageManager().queryIntentActivities(intent, PackageManager.MATCH_DEFAULT_ONLY);
        for (ResolveInfo resolvedIntentInfo : resolvedIntentActivities) {
            String packageName = resolvedIntentInfo.activityInfo.packageName;
            context.grantUriPermission(packageName, uri, Intent.FLAG_GRANT_WRITE_URI_PERMISSION | Intent.FLAG_GRANT_READ_URI_PERMISSION);
        }
        */
    }

}
