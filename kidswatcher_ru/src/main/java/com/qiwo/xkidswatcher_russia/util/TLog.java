package com.qiwo.xkidswatcher_russia.util;

import android.util.Log;

import com.google.code.microlog4android.Logger;
import com.google.code.microlog4android.LoggerFactory;

import net.intari.CustomLogger.CustomLog;

/**
 * TODO:just replace those calls with CustomLog.xxx
 * dkzm
 */
public class TLog {

    private static final Logger logger = LoggerFactory.getLogger();
    public static boolean DEBUG = true;

    public TLog() {
    }

    public static final void analytics(String tag,String log) {
        if (DEBUG) {
            Log.d(tag, log);
        }
        CustomLog.d(tag,log);

    }
    public static final void logException(String tag,Exception ex) {
        CustomLog.logException(tag,ex);
    }
    public static final void logThrowable(String tag,Throwable thr) {
        CustomLog.logException(tag,thr);
    }


    public static final void error(String tag, String log) {
        CustomLog.e(tag,log);
        if (DEBUG) {
            logger.error(log);
			Log.e(tag, log);
        }
    }

    public static final void log(String tag, String log) {
        CustomLog.i(tag,log);
        if (DEBUG) {
            logger.debug(log);
//			Log.i(tag, log);
        }
    }

    public static final void logv(String tag,String log) {
        CustomLog.v(tag,log);
        if (DEBUG)
            Log.v(tag, log);
    }

    public static final void warn(String tag,String log) {
        CustomLog.w(tag,log);

        if (DEBUG)
            Log.w(tag, log);
    }
}
