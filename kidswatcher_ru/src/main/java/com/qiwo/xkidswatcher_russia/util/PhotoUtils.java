package com.qiwo.xkidswatcher_russia.util;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.database.Cursor;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import androidx.core.content.FileProvider;
import android.widget.Toast;

import com.qiwo.xkidswatcher_russia.AppContext;
import com.qiwo.xkidswatcher_russia.KidsWatConfig;
import com.qiwo.xkidswatcher_russia.R;
import com.qiwo.xkidswatcher_russia.ui.KidsProfileActivity;

import net.intari.CustomLogger.CustomLog;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import static com.qiwo.xkidswatcher_russia.Constants.CAMERA_PREFIX;
import static com.qiwo.xkidswatcher_russia.Constants.CAMERA_SUFFIX;
import static com.qiwo.xkidswatcher_russia.Constants.CROP_PREFIX;
import static com.qiwo.xkidswatcher_russia.Constants.DEFAULT_IMAGE_FORMAT;
import static com.qiwo.xkidswatcher_russia.Constants.PHOTO_PROVIDER;
import static com.qiwo.xkidswatcher_russia.util.PermissionUtils.grantPermissionsForUri;

/**
 * Created by Dmitriy Kazimirov on 29/01/2019.
 * Helper tools for photo taking / picking. Android 7+ is supported
 * Common code from ShowDialogActivity  (and KidsProfileActivity, which contains older versions) should be moved here.
 * Also see SetPhotoUtil / ImageUtils
 */
public class PhotoUtils {
    public static final String TAG = PhotoUtils.class.getSimpleName();
    public final static String FILE_SAVEPATH = KidsWatConfig.getTempFilePath();

    public static final String GOOGLE_PHOTOS_PACKAGE_NAME = "com.google.android.apps.photos";

    private static final String TEMP_PREFIX="temp$$_";
    private final static int PHOTO_FOR_CROP = 3019;
    public static String imagePath = null;
    public final static int CROP_SIZE = 120;

    public static boolean isGooglePhotosInstalled(Context context) {
        PackageManager packageManager = context.getPackageManager();
        try {
            return packageManager.getPackageInfo(GOOGLE_PHOTOS_PACKAGE_NAME, PackageManager.GET_ACTIVITIES) != null;
        } catch (PackageManager.NameNotFoundException e) {
            return false;
        }
    }
    public static void copyFromStream(InputStream in, File dst) throws IOException {
        try {
            OutputStream out = new FileOutputStream(dst);
            try {
                // Transfer bytes from in to out
                byte[] buf = new byte[1024];
                int len;
                while ((len = in.read(buf)) > 0) {
                    out.write(buf, 0, len);
                }
            } finally {
                out.close();
            }
        } finally {
            in.close();
        }
    }

    public static Uri makeUriFromUri(Context context, Uri originalUri, String format) {
        String storageState = Environment.getExternalStorageState();
        if (storageState.equals(Environment.MEDIA_MOUNTED)) {
            File savedir = new File(KidsWatConfig.getTempFilePath());
            if (!savedir.exists()) {
                savedir.mkdirs();
            }
        } else {

            Toast.makeText(context, R.string.tip_no_access_to_sdcard,
                    Toast.LENGTH_SHORT).show();

            return null;
        }

        String displayName=getURIDisplayName(context,originalUri);
        File f=new File(displayName);
        final String[] split = f.getPath().split(":");//split the path.
        String ext=null;
        if (split.length>1) {
            ext=split[split.length-1];
        }
        format = StringUtils.isEmpty(ext) ? format : ext;
        CustomLog.d(TAG,"Uri:"+originalUri.toString()+".DisplayName:"+displayName+", # splits:"+split.length+", ext:"+ext+",format:"+format);



        String timeStamp = new SimpleDateFormat("yyyyMMddHHmmss")
                .format(new Date());
        String newFileName = CROP_PREFIX + KidsWatConfig.getDefaultFamilyId() + "."
                + format;
        String resultPath = KidsWatConfig.getTempFilePath() + newFileName;

        if ((AppContext.getInstance().getCurrentFamily()!=null) && (AppContext.getInstance().getCurrentFamily().family_id!=null)) {
            resultPath=String.format("%sbb_%s.jpg",
                    KidsWatConfig.getTempFilePath(), AppContext.getInstance().getCurrentFamily().family_id);
        } else {
            CustomLog.w(TAG,"Failed to get current family. using base name version of file instead");
        }

        CustomLog.d(TAG,"resultPath:"+resultPath);

        File resultFile = new File(resultPath);
        try {
            InputStream inputStream=context.getContentResolver().openInputStream(originalUri);
            copyFromStream(inputStream,resultFile);
            //update 'full'(?) image
            //saveBbImgPath=resultPath;

        } catch (IOException ex) {
            CustomLog.logException(TAG,ex);
        }

        Uri uri = FileProvider.getUriForFile(context,
                PHOTO_PROVIDER,
                resultFile);

        CustomLog.d(TAG,"Made URI:originalUri:"+originalUri+", resultPath:"+resultPath+", resultUri:"+uri);
        return uri;
    }

    public static String getURIDisplayName(Context context, Uri uri){
        String[] projection = {MediaStore.MediaColumns.DISPLAY_NAME};
        ContentResolver cr = context.getApplicationContext().getContentResolver();

        String fileName=null;
        Cursor metaCursor = cr.query(uri, projection, null, null, null);
        if (metaCursor != null) {
            try {
                if (metaCursor.moveToFirst()) {
                    fileName = metaCursor.getString(0);
                }
            } finally {
                metaCursor.close();
            }
        }
        return fileName;
    }


    /**
     * Uses ExifSupportLibrary
     * see https://android-developers.googleblog.com/2016/12/introducing-the-exifinterface-support-library.html
     * https://stackoverflow.com/questions/39540646/how-to-get-exif-data-from-an-inputstream-rather-than-a-file
     *
     * Note: ExifInterface will not work with remote InputStreams, such as those returned from a HttpURLConnection.
     * It is strongly recommended to only use them with content:// or file:// URIs.
     *
     * @param uri
     * @return
     */

    public static int getBitmapDegree(String tag,Context context,Uri uri) {
        //if (Build.VERSION.SDK_INT>=Build.VERSION_CODES.N)
        if (Build.VERSION.SDK_INT>=Build.VERSION_CODES.N) {
            CustomLog.v(TAG,"Android 7.0 or later - using support library");
            return getBitmapDegreeAPI24(tag,context,uri);
            //return getBitmapDegreePrePI24(tag,context,uri);

        } else {
            CustomLog.v(TAG,"NOT Android 7.0 or later - using hacks");
            return getBitmapDegreePrePI24(tag,context,uri);
        }
    }

    public static int getBitmapDegreePrePI24(String tag,Context context,Uri uri) {
        int degree = 0;

        InputStream in=null;
        try {
            in = context.getContentResolver().openInputStream(uri);
            //generate temp file
            File cacheDir=context.getCacheDir();

            String timeStamp = new SimpleDateFormat("yyyyMMddHHmmss")
                    .format(new Date());
            String tempFileName =  TEMP_PREFIX+timeStamp;
            String tempFileNameAndPath = cacheDir +"/"+ tempFileName;
            File tempFile = new File(tempFileNameAndPath);

            CustomLog.d(tag,"Will use temp file "+tempFileName);
            copyFromStream(in,tempFile);
            CustomLog.d(tag,"copied. asking for result");
            int res=getBitmapDegree(tempFileNameAndPath);
            CustomLog.d(tag,"got result "+res+". deleting "+tempFileNameAndPath);
            tempFile.delete();
            CustomLog.d(tag,"Just returning "+res+", "+tempFileNameAndPath+" should be deleted");
            return res;



        } catch (IOException e) {
            // Handle any errors
            CustomLog.logException(tag,e);
        } finally {
            if (in != null) {
                try {
                    in.close();
                } catch (IOException ignored) {

                }
            }
        }


        return degree;
    }




    /**
     * Uses ExifSupportLibrary
     * see https://android-developers.googleblog.com/2016/12/introducing-the-exifinterface-support-library.html
     * https://stackoverflow.com/questions/39540646/how-to-get-exif-data-from-an-inputstream-rather-than-a-file
     *
     * Note: ExifInterface will not work with remote InputStreams, such as those returned from a HttpURLConnection.
     * It is strongly recommended to only use them with content:// or file:// URIs.
     *
     * @param uri
     * @return
     */
    @TargetApi(Build.VERSION_CODES.N)
    public static int getBitmapDegreeAPI24(String tag,Context context,Uri uri) {
        int degree = 0;

        InputStream in=null;
        try {
            in = context.getContentResolver().openInputStream(uri);
            ExifInterface exifInterface = new ExifInterface(in);
            // Now you can extract any Exif tag you want
            // Assuming the image is a JPEG or supported raw format
            int orientation = exifInterface.getAttributeInt(
                    ExifInterface.TAG_ORIENTATION,
                    ExifInterface.ORIENTATION_NORMAL);

            switch (orientation) {
                case ExifInterface.ORIENTATION_ROTATE_90:
                    degree = 90;
                    break;
                case ExifInterface.ORIENTATION_ROTATE_180:
                    degree = 180;
                    break;
                case ExifInterface.ORIENTATION_ROTATE_270:
                    degree = 270;
                    break;
            }

        } catch (IOException e) {
            // Handle any errors
            CustomLog.logException(tag,e);
        } finally {
            if (in != null) {
                try {
                    in.close();
                } catch (IOException ignored) {

                }
            }
        }


        return degree;
    }

    public static int getBitmapDegree(String path) {
        int degree = 0;
        try {

            ExifInterface exifInterface = new ExifInterface(path);

            int orientation = exifInterface.getAttributeInt(
                    ExifInterface.TAG_ORIENTATION,
                    ExifInterface.ORIENTATION_NORMAL);

            switch (orientation) {
                case ExifInterface.ORIENTATION_ROTATE_90:
                    degree = 90;
                    break;
                case ExifInterface.ORIENTATION_ROTATE_180:
                    degree = 180;
                    break;
                case ExifInterface.ORIENTATION_ROTATE_270:
                    degree = 270;
                    break;
            }
        } catch (IOException e) {
            CustomLog.logException(TAG,e);
        }

        return degree;
    }

    /* WHY KidsProfileActivity does not use THIS!! */
    /*
    public static void doCrop(Activity context, Uri uri, Uri imageUri) {
        Uri newUri=makeUriFromUri(context,uri, DEFAULT_IMAGE_FORMAT);

        CustomLog.v(TAG,"doCrop. inputUri:"+uri+",outputUri:"+imageUri);
        Intent intent = new Intent("com.android.camera.action.CROP");
        intent.setDataAndType(uri, "image/*");
        intent.putExtra("crop", "true");
        intent.putExtra("aspectX", 1);
        intent.putExtra("aspectY", 1);
        intent.putExtra("outputX", 120);
        intent.putExtra("outputY", 120);
        intent.putExtra("scale", true);
//		intent.putExtra("return-data", true);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
//		intent.putExtra("outputFormat", Bitmap.CompressFormat.JPEG.toString());
//		intent.putExtra("noFaceDetection", false);
        grantPermissionsForUri(context,imageUri,intent);

        context.startActivityForResult(intent, PHOTO_FOR_CROP);
    }
    */

    /* Or this ?! */
    public static void doCrop(Activity context, Uri uri) {
        //Uri copyOfSourceURi=makeUriFromUri(context,uri, DEFAULT_IMAGE_FORMAT);

        Uri newUri=makeUriFromUri(context,uri, DEFAULT_IMAGE_FORMAT);
        CustomLog.v(TAG,"doCrop. inputUri(and outputUri) :"+uri+", newUri:"+newUri);

        //I'm AWARE of https://commonsware.com/blog/2013/01/23/no-android-does-not-have-crop-intent.html
        //Cropper needs to be replaced by in-app code
        boolean googlePhotosInstalled=isGooglePhotosInstalled(context);
        CustomLog.d(TAG,"GooglePhotos is installed:"+googlePhotosInstalled);

        Intent intent = new Intent("com.android.camera.action.CROP");
        intent.setDataAndType(newUri, "image/*");//uri
        intent.putExtra("crop", "true");
        intent.putExtra("aspectX", 1);
        intent.putExtra("aspectY", 1);
        //intent.putExtra("outputX", 120);
        //intent.putExtra("outputY", 120);
        intent.putExtra("scale", true);


        intent.putExtra("outputX", CROP_SIZE);
        intent.putExtra("outputY", CROP_SIZE);
        intent.putExtra("scale", true);
        intent.putExtra("scaleUpIfNeeded", true);

//		intent.putExtra("return-data", true);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, newUri);
//		intent.putExtra("outputFormat", Bitmap.CompressFormat.JPEG.toString());
//		intent.putExtra("noFaceDetection", false);

        //grant permissions
        grantPermissionsForUri(context,newUri,intent);

        if (googlePhotosInstalled) {
            intent.setPackage(GOOGLE_PHOTOS_PACKAGE_NAME);
            try {
                CustomLog.d(TAG,"Trying to handle crop using Google Photos. Intent:"+intent.toURI());
                context.startActivityForResult(intent, PHOTO_FOR_CROP);
            } catch (ActivityNotFoundException ex) {
                //this makes NO sense. This should NEVER EVER happen
                CustomLog.logException(TAG,ex);
                Toast.makeText(context, R.string.error_no_cropper,
                        Toast.LENGTH_LONG).show();
            } catch (Exception ex) {
                CustomLog.logException(TAG, ex);
                Toast.makeText(context, R.string.error_cropper_google_photos,
                        Toast.LENGTH_LONG).show();
            }


        } else {
            List<ResolveInfo> list = context.getPackageManager().queryIntentActivities(intent,0);
            int size = list.size();
            if (size==0) {
                CustomLog.e(TAG,"No crop app found");
                Toast.makeText(context, R.string.error_no_cropper,
                        Toast.LENGTH_LONG).show();

            } else {
                CustomLog.d(TAG,size+" possible crop apps");
                try {
                    CustomLog.d(TAG,"Trying to handle crop using regular means because Google Photos was not found. Intent:"+intent.toURI());
                    context.startActivityForResult(intent, PHOTO_FOR_CROP);
                } catch (ActivityNotFoundException ex) {
                    CustomLog.logException(TAG,ex);
                    Toast.makeText(context, R.string.error_no_cropper,
                            Toast.LENGTH_LONG).show();
                } catch (Exception ex) {
                    CustomLog.logException(TAG, ex);
                    Toast.makeText(context, R.string.error_cropper,
                            Toast.LENGTH_LONG).show();
                }
            }

        }


    }



    public static Uri getTempImageFile(String tag,Context context) {
        /*
        File cacheDir=context.getExternalCacheDir();
        if (cacheDir==null) {
            CustomLog.w(TAG,"No external cache directory available. Will use internal cache directory");
            cacheDir=context.getCacheDir();
        }

               CustomLog.i(TAG,"Will use cache directory:"+cacheDir.getAbsolutePath());

        */



        String storageState = Environment.getExternalStorageState();
        if (storageState.equals(Environment.MEDIA_MOUNTED)) {
            File savedir = new File(FILE_SAVEPATH);
            if (!savedir.exists()) {
                savedir.mkdirs();
            }
        } else {

            Toast.makeText(context, R.string.tip_no_access_to_sdcard,
                    Toast.LENGTH_SHORT).show();

            return null;
        }

       /*
        String storageState = Environment.getExternalStorageState();
        if (storageState.equals(Environment.MEDIA_MOUNTED)) {
            File cacheDir= context.getExternalCacheDir();
            if (!cacheDir.exists()) {
                cacheDir.mkdirs();
            }
        } else {
            Toast.makeText(ShowDialogActivity.this, "无法保存上传的头像，请检查SD卡是否挂载", // dkzm: aka Unable to save uploaded avatar, please check if SD card is mounted"
                    Toast.LENGTH_SHORT).show();
            return null;
        }*/

       /*
        File cacheDir= context.getExternalCacheDir();
        if (!cacheDir.exists()) {
            cacheDir.mkdirs();
        }
        */


        String timeStamp = new SimpleDateFormat("yyyyMMddHHmmss")
                .format(new Date());
        String tempFileName = CAMERA_PREFIX + timeStamp + CAMERA_SUFFIX;
        String tempFileNameAndPath = FILE_SAVEPATH + tempFileName;
        File tempFile = new File(tempFileNameAndPath);

        Uri tempImageUri = FileProvider.getUriForFile(context,
                PHOTO_PROVIDER,
                tempFile);

        //tempImageUriLocal=Uri.fromFile(tempFile);
        CustomLog.d(tag,"getTempImageFile: tempFileNameAndPath:"+tempFileNameAndPath);//", tempImageUriLocal:"+ tempImageUriLocal+",tempImageUri:"+tempImageUri);
        return tempImageUri;
    }

}
