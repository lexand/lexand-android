package com.qiwo.xkidswatcher_russia.util;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Base64;

import net.intari.CustomLogger.CustomLog;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class Base64Encode {
    public static final String TAG = Base64Encode.class.getSimpleName();

    /*
     * @param imgPath
     * @param bitmap
     * @param imgFormat 图片格式
     * @return
     */
    public static String imgToBase64(String imgPath) {

        // if (imgPath != null && imgPath.length() > 0) {
        // bitmap = readBitmap(imgPath);
        // }

        Bitmap bitmap = readBitmap(imgPath);
        if (bitmap == null) {
            // bitmap not found!!
        }
        ByteArrayOutputStream out = null;
        try {
            out = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, out);

            out.flush();
            out.close();

            byte[] imgBytes = out.toByteArray();
            return Base64.encodeToString(imgBytes, Base64.DEFAULT);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            return null;
        } finally {
            try {
                out.flush();
                out.close();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                CustomLog.logException(TAG,e);
            }
        }
    }

    private static Bitmap readBitmap(String imgPath) {
        try {
            return BitmapFactory.decodeFile(imgPath);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            return null;
        }

    }

    /**
     * encodeBase64File:(将文件转成base64 字符串). <br/>
     *
     * @param path 文件路径
     * @return
     * @throws Exception
     * @author guhaizhou@126.com
     * @since JDK 1.6
     */
    public static String encodeBase64File(String path) throws Exception {
        File file = new File(path);
        FileInputStream inputFile = new FileInputStream(file);
        byte[] buffer = new byte[(int) file.length()];
        inputFile.read(buffer);
        inputFile.close();
        return Base64.encodeToString(buffer, Base64.DEFAULT);
    }

    public static String encodeBase64FileFromContext(Context c, String filename)
            throws IOException {

        FileInputStream inputFile = c.openFileInput(filename);
        byte[] buffer = new byte[inputFile.available()];
        inputFile.read(buffer);
        return Base64.encodeToString(buffer, Base64.URL_SAFE);

        // FileOutputStream fos = context.openFileOutput(fileName,
        // Context.MODE_PRIVATE);
        // ByteArrayOutputStream stream = new ByteArrayOutputStream();

        // File file = new File(path);
        /*
         * FileInputStream inputFile; try { inputFile =
         * c.openFileInput(filename); if (inputFile == null) { return null; }
         * ByteArrayOutputStream out = new ByteArrayOutputStream(); byte[] bys =
         * new byte[4096]; for (int p = -1; (p = inputFile.read(bys)) != -1;) {
         * out.write(bys, 0, p); } byte[] buffer = out.toByteArray(); return
         * Base64.encodeToString(buffer, Base64.DEFAULT); } catch IOException e)
         * { // TODO Auto-generated catch block CustomLog.logException(TAG,e); }// new
         * FileInputStream(file); catch (FileNotFoundException e) { // TODO
         * Auto-generated catch block CustomLog.logException(TAG,e); }// new
         * FileInputStream(file);
         */
        // return "";

        // byte[] buffer = new byte[(int) inputFile..length()];
        // inputFile.read(buffer);
        // inputFile.close();

    }

    /**
     * decoderBase64File:(将base64字符解码保存文件). <br/>
     *
     * @param base64Code 编码后的字串
     * @param savePath   文件保存路径
     * @throws Exception
     * @author guhaizhou@126.com
     * @since JDK 1.6
     */
    public static void decoderBase64File(String base64Code, String savePath)
            throws Exception {
        // byte[] buffer = new BASE64Decoder().decodeBuffer(base64Code);
        byte[] buffer = Base64.decode(base64Code, Base64.URL_SAFE);
        FileOutputStream out = new FileOutputStream(savePath);
        out.write(buffer);
        out.close();
        CustomLog.d(TAG,"save file ok." + savePath);
    }

    public static void decoderBase64FileForAudio(String base64Code,
                                                 String savePath) throws Exception {
        // byte[] buffer = new BASE64Decoder().decodeBuffer(base64Code);
        byte[] buffer = Base64.decode(base64Code, Base64.DEFAULT);
        FileOutputStream out = new FileOutputStream(savePath);
        out.write(buffer);
        out.close();
        CustomLog.d(TAG,"save file ok." + savePath);
    }

    /**
     * @param base64Data
     * @param imgName    图片格式
     */
    public static void base64ToBitmap(String base64Data, String imgName) {
        byte[] bytes = Base64.decode(base64Data, Base64.DEFAULT);
        Bitmap bitmap = BitmapFactory.decodeByteArray(bytes, 0, bytes.length);

        File myCaptureFile = new File("/sdcard/", imgName);
        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(myCaptureFile);
        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            CustomLog.logException(TAG,e);
        }
        boolean isTu = bitmap.compress(Bitmap.CompressFormat.JPEG, 100, fos);
        if (isTu) {
            // fos.notifyAll();
            try {
                fos.flush();
                fos.close();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                CustomLog.logException(TAG,e);
            }
        } else {
            try {
                fos.close();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                CustomLog.logException(TAG,e);
            }
        }
    }
}
