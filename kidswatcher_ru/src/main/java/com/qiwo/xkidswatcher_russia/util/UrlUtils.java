package com.qiwo.xkidswatcher_russia.util;

import net.intari.CustomLogger.CustomLog;

import org.kymjs.kjframe.http.HttpParams;

public class UrlUtils {
	public static final String TAG = UrlUtils.class.getSimpleName();

	public static String makeURL(String p_url, HttpParams params) {
		StringBuilder url = new StringBuilder(p_url);
		if (params == null)
			return p_url;
		if (url.indexOf("?") < 0)
			url.append('?');

		// for (String name : params.keySet()) {
		// String value = params.get(name);
		// System.out.println(name + "=" + value);
		// if (value != null && !StringUtils.isEmpty(value)
		// && !value.equalsIgnoreCase("null")) {
		// url.append('&');
		// url.append(name);
		// url.append('=');
		// // 对参数进行编码
		// try {
		// url.append(URLEncoder.encode(
		// String.valueOf(params.get(name)), "UTF-8"));
		// } catch (UnsupportedEncodingException e) {
		// CustomLog.logException(TAG,e);
		// }
		// }
		// }
		CustomLog.d(TAG,params.getUrlParams().toString());
		url.append(params.getUrlParams());
		String __xurl = url.toString().replace("?&", "?");
		CustomLog.d(TAG,__xurl);
		return __xurl;
	}
}
