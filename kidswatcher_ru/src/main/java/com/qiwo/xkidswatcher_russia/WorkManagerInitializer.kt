package com.qiwo.xkidswatcher_russia

import android.app.ActivityManager
import android.content.ContentProvider
import android.content.ContentValues
import android.content.Context
import android.content.SharedPreferences
import android.content.pm.PackageInfo
import android.content.pm.PackageManager
import android.database.Cursor
import android.net.Uri
import android.os.Build
import android.os.Environment
import android.util.Log
import androidx.annotation.RestrictTo
import androidx.work.*
import androidx.work.impl.WorkManagerImpl
import butterknife.ButterKnife
import com.crashlytics.android.Crashlytics
import com.google.common.util.concurrent.ListenableFuture
import com.google.firebase.FirebaseApp
import com.google.firebase.remoteconfig.FirebaseRemoteConfig
import com.google.firebase.remoteconfig.FirebaseRemoteConfigSettings
import com.google.gson.Gson
import com.nostra13.universalimageloader.cache.disc.impl.UnlimitedDiscCache
import com.nostra13.universalimageloader.cache.disc.naming.Md5FileNameGenerator
import com.nostra13.universalimageloader.cache.memory.impl.UsingFreqLimitedMemoryCache
import com.nostra13.universalimageloader.core.DisplayImageOptions
import com.nostra13.universalimageloader.core.ImageLoader
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration
import com.nostra13.universalimageloader.core.assist.QueueProcessingType
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer
import com.nostra13.universalimageloader.utils.StorageUtils
import com.qiwo.xkidswatcher_russia.AnalyticsUtils.AnalyticsUtils
import com.qiwo.xkidswatcher_russia.BuildConfig.BUILD_GIT_SHA1
import com.qiwo.xkidswatcher_russia.Constants.*
import com.qiwo.xkidswatcher_russia.api.ApiHttpClient
import com.qiwo.xkidswatcher_russia.api.remote.KidsWatApiUrl
import com.qiwo.xkidswatcher_russia.bean.*
import com.qiwo.xkidswatcher_russia.util.KidsWatUtils
import com.qiwo.xkidswatcher_russia.util.StringUtils
import com.qiwo.xkidswatcher_russia.util.TLog
import com.yandex.metrica.YandexMetrica
import com.yandex.metrica.YandexMetricaConfig
import io.fabric.sdk.android.Fabric
import net.intari.AndroidToolboxNanoCore.CoreUtils
import net.intari.AndroidToolboxNanoCore.Extensions.logger
import net.intari.CustomLogger.CustomLog
import org.json.JSONObject
import org.kymjs.kjframe.KJHttp
import org.kymjs.kjframe.bitmap.BitmapConfig
import org.kymjs.kjframe.http.HttpCallBack
import org.kymjs.kjframe.http.HttpConfig
import org.kymjs.kjframe.utils.KJLoger
import ru.yandex.yandexmapkit.utils.GeoPoint
import java.util.*
import java.util.concurrent.TimeUnit

/**
 * Created by Dmitriy Kazimirov on 22/02/2019.
 * Based off https://medium.com/@programmerr47/custom-work-manager-initialization-efdd2afa6459
 * Our big thanks to Yandex.Metrica
 * This class now handles many things which were part of AppContext's
 *
 */

class MyWorkManagerInitializer : DummyContentProvider() {
    companion object {
        val logger = logger<MyWorkManagerInitializer>()
        val TAG = MyWorkManagerInitializer::class.java.simpleName
        internal var internal_bean___watch_data: beanFor___get_watch_data? = null
        internal var internal_locationResponse: JSONObject? = null

        private lateinit var  firebaseRemoteConfig: FirebaseRemoteConfig

        private val UPDATE_LOCATION_ON_RESUME_IN_BACKGROUND_KEY = "updateLocationOnResumeInBackground"

        // ----------------
        // public static final int PAGE_SIZE = 20;
        // public static String loginUid;
        var google_gcm_appid = ""
        var isLogin: Boolean = false
        private lateinit var  imageLoader: ImageLoader // 处理图片
        lateinit var options: DisplayImageOptions// 处理图片的参数
        lateinit var commenOptions: DisplayImageOptions// 处理图片的参数
        var codess = 361
        //保存设备的最新定位点
        var lng: GeoPoint? = null
        // public List<beanForDb___Family> list_familyinfo = null;
        var family_list: beanFor___get_family_list? = null
        //获取家庭圈信息，保存家庭圈信息的，全局
        var currentFamily: beanForDb___Family? = null
        //没用
        var currentBeanFamily: beanFor___get_family_info? = null
        //家庭圈和手表的映射关系
        var map_familyinfo: Map<String, beanForDb___Family>? = null
        var loginUser_kid: beanForDb___LoginMember? = null
        private lateinit var mEditor: SharedPreferences.Editor
        private lateinit var mSpSettingInfo: SharedPreferences
        private var mGson: Gson = Gson()


        fun setInternalLocationResponse(data: JSONObject?) {
            if (data != null) {
                internal_locationResponse = data
            } else {
                logger.w { "Attempt to set internal location data with null" }
            }
        }

        fun setInternal_bean___watch_data(data: beanFor___get_watch_data?) {
            if (data != null) {
                internal_bean___watch_data = data
            } else {
                logger.w {  "Attempt to set internal watch data with null" }
            }
        }

        fun getInternal_bean___watch_data(): beanFor___get_watch_data? {
            return internal_bean___watch_data
        }


        fun getSharedPref(): SharedPreferences {
            return mSpSettingInfo
        }

        fun getSharedPrefEditor(): SharedPreferences.Editor {
            return mEditor
        }


        fun getImgLoader(): ImageLoader {
            return imageLoader
         }
        fun getGson(): Gson {
            return mGson
            /*
            if (mGson == null) {
                val gson=Gson()
                mGson = gson
                return gson
            } else {
                return mGson
            }
            */
        }


        fun getFamilyListCount(): Int {
            val familiyList= map_familyinfo
            return if (familiyList != null) {
                familiyList.size
            } else 0
        }

        fun initRemoteConfig() {
            logger.i { "Initing remote config..." }
            // Get Remote Config instance.
            // [START get_remote_config_instance]
            firebaseRemoteConfig= FirebaseRemoteConfig.getInstance();
            // [END get_remote_config_instance]

            // Create a Remote Config Setting to enable developer mode, which you can use to increase
            // the number of fetches available per hour during development. See Best Practices in the
            // README for more information.
            // [START enable_dev_mode]
            logger.d { "Setting remote config configuration..." }
            val configSettings = FirebaseRemoteConfigSettings.Builder()
                    .setDeveloperModeEnabled(BuildConfig.DEBUG)
                    .build()
            firebaseRemoteConfig.setConfigSettings(configSettings)
            // [END enable_dev_mode]

            // Set default Remote Config parameter values. An app uses the in-app default values, and
            // when you need to adjust those defaults, you set an updated value for only the values you
            // want to change in the Firebase console. See Best Practices in the README for more
            // information.
            // [START set_default_values]
            logger.d { "Setting remote config defaults..." }
            firebaseRemoteConfig.setDefaults(R.xml.remote_config_defaults);
            // [END set_default_values]
            //now fetch
            var cacheExpiration: Long = 3600 // 1 hour in seconds.
            // If your app is using developer mode, cacheExpiration is set to 0, so each fetch will
            // retrieve values from the service.
            if (firebaseRemoteConfig.getInfo().getConfigSettings().isDeveloperModeEnabled()) {
                cacheExpiration = 0
            }
            logger.d { "Cache expiration:${cacheExpiration} seconds" }
            firebaseRemoteConfig.fetch()
                    .addOnCompleteListener {  }


        }

        fun getRemoteConfig():FirebaseRemoteConfig= firebaseRemoteConfig
    }

    
    private var loggingInited = false
    private val loggingSyncObject = Any()

    private fun initLogging(context:Context) {

        synchronized(loggingSyncObject) {
            if (loggingInited) {
                logger.w { "Logging was arleady initialized" }
                return
            }
            loggingInited = true
        }

        //init logging subsystem
        //I  use NSLogger for development purposes
        //if we are under Robolectric - just do minimal setup - it makes no sense to send data to crashlytics from Roboletric
        if ("robolectric" != Build.FINGERPRINT) {

            CustomLog.setIsDebug(BuildConfig.DEBUG)
            CustomLog.setLogExceptions(Constants.LOG_NON_FATALS)
            CustomLog.setAutomaticUnescape(true);
            CustomLog.setLogCrashlytics(true)//!BuildConfig.DEBUG)
            initLogHostInternal()



            CustomLog.setContext(context)
            logger.v { "Version Name:" + BuildConfig.VERSION_NAME }
            logger.v { "Version Code:" + Integer.valueOf(BuildConfig.VERSION_CODE) }
            logger.v { "\u0414\u043E\u0440\u0430\u0431\u043E\u0442\u043A\u0438 \u043F\u043E\u0441\u043B\u0435 \u0441\u0435\u0440\u0435\u0434\u0438\u043D\u044B \u044F\u043D\u0432\u0430\u0440\u044F \u0434\u0432\u0435 \u0442\u044B\u0441\u044F\u0447\u0438 \u0434\u0435\u0432\u044F\u0442\u043D\u0430\u0434\u0446\u0430\u0442\u043E\u0433\u043E \u0433\u043E\u0434\u0430 \u0432\u044B\u043F\u043E\u043B\u043D\u0435\u043D\u044B \u041A\u0430\u0437\u0438\u043C\u0438\u0440\u043E\u0432\u044B\u043C \u0414\u043C\u0438\u0442\u0440\u0438\u0435\u043C \u0412\u043B\u0430\u0434\u0438\u043C\u0438\u0440\u043E\u0432\u0438\u0447\u0435\u043C" }
            logger.v { "Build type:" + BuildConfig.BUILD_TYPE }
            logger.v { "Device's board:" + Build.BOARD }
            logger.v { "Device's bootloader:" + Build.BOOTLOADER }
            logger.v { "Device's brand:" + Build.BRAND }
            logger.v { "Device's display:" + Build.DISPLAY }
            logger.v { "Device's fingerprint:+" + Build.FINGERPRINT }
            logger.v { "Device's industrial design:" + Build.DEVICE }
            //logger.v { "Device ABI:" + Build.CPU_ABI + " / " + Build.CPU_ABI2 }
            logger.v { "Device's hardware:" + Build.HARDWARE }
            logger.v { "Heap size:" + Runtime.getRuntime().maxMemory() / Constants.BYTES_PER_MB + " Mb" }
            val locale = Locale.getDefault()
            logger.v { "Locale:$locale" }
            val version = context.applicationContext.getResources().getString(R.string.internalAppVersionLong,
                    BuildConfig.VERSION_NAME,
                    BuildConfig.VERSION_CODE,
                    BuildConfig.BUILD_DATE_TIME,
                    BUILD_GIT_SHA1,
                    BuildConfig.BUILD_USER,
                    BuildConfig.BUILD_HOST
            )
            logger.v { version }

            // Store base info to crashlytics
            CustomLog.setString("BuildType", BuildConfig.BUILD_TYPE)
            CustomLog.setString("BuildFlavour", BuildConfig.FLAVOR)
            CustomLog.setString("InternalVersion", version)
            CustomLog.setString("Locale", locale.toString())
            //This dublicates some of information arleady collected by Crashlytics
            CustomLog.setString("DeviceBoard", Build.BOARD)
            CustomLog.setString("DeviceBootloader", Build.BOOTLOADER)
            CustomLog.setString("DeviceBrand", Build.BRAND)
            CustomLog.setString("DeviceDevice", Build.DEVICE)
            CustomLog.setString("DeviceFingerprint", Build.FINGERPRINT)
            CustomLog.setString("DeviceABIs", Build.CPU_ABI + " / " + Build.CPU_ABI2)
            CustomLog.setString("DeviceHardware", Build.HARDWARE)
            CustomLog.setString("HeapSize", (Runtime.getRuntime().maxMemory() / Constants.BYTES_PER_MB).toString() + " Mb")


            val am = context.applicationContext.getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager

            if (Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT) {
                CustomLog.setBool("isLowRamDevice", am.isLowRamDevice)
                logger.v { "isLowRamDevice:" + am.isLowRamDevice }
            } else {
                logger.v { "Can't check low ram status - device is pre-4.4" }
            }
        }
    }

        private fun initCrashHandler(context: Context) {
            if ("robolectric" != Build.FINGERPRINT) {
                if (BuildConfig.DEBUG) {
                    logger.w { "Debug build - not activating Crashlytis" }
                } else {
                    logger.i { "Non-debug build - activating Crashlytics" }
                    Fabric.with(context.applicationContext, Crashlytics())
                }
            }
        }


        private fun initLogHostInternal() {
            CustomLog.setLogDestination(LOG_HOST, LOG_PORT)

        }

        private fun initLogHostXtraTrace() {
            CustomLog.setLogDestination(LOG_HOST_XTRA_TRACE, LOG_PORT)
        }


    /*
        fun initFirebase(context: Context) {
            //This is necessary to prevent issues like https://github.com/yandexmobile/metrica-sdk-android/issues/77
            //This call is NOT under release-only config because it's not decided yet if Firebase will be used in release
            FirebaseApp.initializeApp(context.applicationContext)
            //initRemoteConfig()
        }
    */
    

    private fun initDebugOnlySettings() {
        ButterKnife.setDebug(BuildConfig.DEBUG)
    }

    var firstStageInitDone: Boolean? = false
    var secondStageInitDone: Boolean? = false

    fun performFirstStageInit(context: Context) {
        if (firstStageInitDone!!) {
            logger.w { "First-stage init was arleady done. do not performing it again" }
            return
        }
        //Order of operations is important
        initCrashHandler(context)//MUST be 1st
        initLogging(context)//MUST be 2nd
        //initFirebase(context)//better do this after 2nd - if we could use metrica
        //initLifecycleListeners()//SHOULD be 3rd (or 4th) if this functionality is used

        initDebugOnlySettings()
        firstStageInitDone = true
    }

    fun performSecondStageInit(context: Context) {
        if (secondStageInitDone!!) {
            logger.w {  "Second-stage init was arleady done. do not performing it again" }
            return
        }

        //initSpecialAnalytics();
        //initYandexMetrica()//initFirebase() must be called
        //initLogInitialState()
        init(context)
        kidsInit()


        // 图片加载
        initImageLoader(context)

        //initLocationUpdatesInBackground();
        secondStageInitDone = true
    }

    fun performInit(context: Context) {
        performFirstStageInit(context)
        performSecondStageInit(context)
    }

    /**
     * 初始化加载图片的工具
     * TODO:Rewrite using Picasso?
     */
    fun initImageLoader(context: Context) {
        logger.v { "initing ImageLoader" }

        val cacheDir = StorageUtils.getOwnCacheDirectory(context, "imageloader/Cache")
        // 加载图片的工具以及参数
        val imgLoader = ImageLoader.getInstance()
        val config = ImageLoaderConfiguration.Builder(context.applicationContext)
                .threadPriority(Thread.NORM_PRIORITY - 2)
                .denyCacheImageMultipleSizesInMemory()
                .memoryCache(UsingFreqLimitedMemoryCache(2 * 1024 * 1024))
                .memoryCacheSize(2 * 1024 * 1024)
                .discCacheSize(50 * 1024 * 1024)
                .discCacheFileCount(100) //缓存的文件数量
                .discCache(UnlimitedDiscCache(cacheDir))//自定义缓存路径
                .defaultDisplayImageOptions(DisplayImageOptions.createSimple())
                .discCacheFileNameGenerator(Md5FileNameGenerator())
                .tasksProcessingOrder(QueueProcessingType.LIFO)
                .build()
        imgLoader.init(config)

        options = DisplayImageOptions.Builder()
                .showStubImage(R.drawable.my_user_photo) // 加载开始默认的图片
                .showImageForEmptyUri(R.drawable.my_user_photo) // url爲空会显示该图片，自己放在drawable里面的
                .showImageOnFail(R.drawable.my_user_photo) // 加载图片出现问题，会显示该图片
                .cacheInMemory(true) // 缓存用
                .cacheOnDisc(true) // 缓存用
                .displayer(RoundedBitmapDisplayer(180)) // 图片圆角显示，值为整数
                .build()

        commenOptions = DisplayImageOptions.Builder()
                .cacheInMemory(true) // 缓存用
                .cacheOnDisc(true) // 缓存用
                //				.displayer(new RoundedBitmapDisplayer(180)) // 图片圆角显示，值为整数
                .build()

        imageLoader = imgLoader
        logger.v { "initing ImageLoader..done" }

    }

    private fun init(context: Context) {

        logger.v {"App init in progress. Context is ${context}" }


        logger.d { "ExternalStorageDirectory: ${Environment.getExternalStorageDirectory()}" }
        logger.d { "ExternalCacheDir:${context.getExternalCacheDir()} " }


        mSpSettingInfo = context.getSharedPreferences("SettingInfo",
                Context.MODE_PRIVATE)
        mEditor = mSpSettingInfo.edit()

        logger.v { "Settings init done" }

        //TODO:rewrite at least basics of network layer to OkHttp3, or, better, to OkHttp3/Retrofit2/Rx2
        //init static version of KJHtttp
        HttpConfig.DEBUG = true
        HttpConfig.CACHEPATH = "kidswatcher/cache"
        HttpConfig.TIMEOUT = OLD_HTTP_TIMEOUT_DEFAULT //20000;
        // 初始化网络请求
        val client = KJHttp()
        ApiHttpClient.setHttpClient(client)
        // Log控制器
        KJLoger.openDebutLog(false)
        TLog.DEBUG = BuildConfig.DEBUG
        // Bitmap缓存地址
        //newer KJS doesnt support CACHEPATH
        BitmapConfig.CACHEPATH = "kidswatcher/imagecache"
        logger.d { "KJ init done" }

    }

    private fun kidsInit() {
        // list_familyinfo = new ArrayList<beanForDb___Family>();
        map_familyinfo = HashMap()
        logger.v { "Preparing to make kids" }

    }

    fun clearUserInfo() {
        //
        logger.v { "Clearing using info..." }

    }


    // ------------------------------------




    override fun onCreate(): Boolean {
        logger.d { "onCreate() for provider. calling initialize" }
        WorkManager.initialize(context!!, Configuration.Builder().build())
        //run your tasks here
        logger.d { "onCreate() for provider. calling performInit()" }
        //looks like I have to trick with static vars or move ALL init here
        performInit(context!!)
        //AppContext.getInstance().performInit()
        logger.d { "onCreate() for provider. initing background updater" }
        initLocationUpdatesInBackground()
        return true
    }


    fun initLocationUpdatesInBackground() {
        logger.d {"Init background location updates" }
        internal_get_watch_data()
    }

    private fun internal_get_watch_data() {
        val uid = KidsWatConfig.getUserUid()
        val access_token = KidsWatConfig.getUserToken()
        val family_id = KidsWatConfig.getDefaultFamilyId()

        if (StringUtils.isEmpty(uid))
            return

        val a = Calendar.getInstance()
        val year = a.get(Calendar.YEAR)
        val month = a.get(Calendar.MONTH)
        val dayOfMonth = a.get(Calendar.DAY_OF_MONTH)

        val utcToday = KidsWatUtils.getUtcTimeAt0Time(year, month, dayOfMonth)

        val request_url = KidsWatApiUrl.getUrlFor___get_watch_data(
                uid, access_token, family_id, utcToday)

        ApiHttpClient.get(request_url, object : HttpCallBack() {

            override fun onPreStart() {
                logger.d {"internal_get_watch_data:Pre-starting..." }
            }

            override fun onSuccess(t: String?) {
                super.onSuccess(t)
                logger.d { String.format("internal_get_watch_data:onSuccess:url:%s\nt:%s", request_url, t) }

                try {

                    val response = getGson().fromJson(t, beanFor___get_watch_data::class.java)

                    if (response.error == 0) {
                        logger.d {"internal_get_watch_data:got non-error response" }
                        setInternal_bean___watch_data(response)


                        if (response.info.ddata.total_rows > 0) {
                            // -------add marker to map--------
                            logger.d { "Got ${response.info.ddata.rows.size} marker(s) - nothing to do with them at this time" }
                            //init periodic updates
                            initPeriodicLocationUpdates()
                        } else {
                            //mapController.setPositionAnimationTo(new GeoPoint(0f, 0f), 2f);
                            //showNoLocationResult();
                            logger.d {"didn't get markers" }
                        }

                    } else {
                        logger.d {"No track" }
                        //showLongToast(getActivity().getApplicationContext().getResources().getString(R.string.tip_loadtrack_faild));
                    }
                } catch (ex:Exception) {
                    //LEX-120, possible reason:capture portals
                    logger.e { "parse error. ${t}"}
                    CustomLog.logException(ex)

                }
            }

            override fun onFailure(errorNo: Int, strMsg: String?) {
                super.onFailure(errorNo, strMsg)
                val msg = String.format("errorNo:%s\n%s", errorNo, strMsg)
                logger.d {"internal_get_watch_data:onFailure:error=$msg" }
            }

            override fun onFinish() {
                logger.d { "internal_get_watch_data:onFinish" }
            }

        }, context.applicationContext)

    }

    private val WORK_TAG = "LocationUpdateTag"
    private val workFutureResult: ListenableFuture<ListenableWorker.Result>? = null
    private fun initPeriodicLocationUpdates() {
        logger.d {"Initing periodic location updates" }
        // Create a Constraints object that defines when the task should run
        val builder = Constraints.Builder()
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            logger.d {"NOT requiring 'when idle'" }
            builder.setRequiresDeviceIdle(false)
        }

        // builder.setRequiresBatteryNotLow(true);//don't eat battery
        builder.setRequiresCharging(false)//NOT require charging
        //Not setting 'CONNECTED' because system it doesn't consider Note 9's Wired Ethernet (it has one in DeX Station) as 'Connected'
        //builder.setRequiredNetworkType(NetworkType.CONNECTED);
        // Many other constraints are available, see the
        // Constraints.Builder reference
        val constraints = builder.build()

        val locationUpdateBuilder = PeriodicWorkRequest.Builder(LocationUpdateWorker::class.java,
                Constants.PERIODIC_COORD_REQUESTS_MINUTES.toLong(), TimeUnit.MINUTES)
        //1, TimeUnit.HOURS);
        locationUpdateBuilder.addTag(WORK_TAG)
        locationUpdateBuilder.setConstraints(constraints)
        // ...if you want, you can apply constraints to the builder here...

        // Create the actual work object:
        val locationUpdateWork = locationUpdateBuilder.build()
        // Then enqueue the recurring task:

        //workFutureResult=WorkManager.getInstance().beginUniqueWork(WORK_TAG,
        //        ExistingWorkPolicy.KEEP, locationUpdateWork).enqueue();
        val operation = WorkManager.getInstance().enqueue(locationUpdateWork)
        logger.d { "Asked for periodic updates of location. Operation:$operation" }
        //perform it NOW too
        //internal_get_location();

    }
}
//where
abstract class DummyContentProvider : ContentProvider() {
    override fun onCreate() = true

    override fun insert(uri: Uri, values: ContentValues?) = null
    override fun query(uri: Uri, projection: Array<String>?, selection: String?, selectionArgs: Array<String>?, sortOrder: String?) = null
    override fun update(uri: Uri, values: ContentValues?, selection: String?, selectionArgs: Array<String>?) = 0
    override fun delete(uri: Uri, selection: String?, selectionArgs: Array<String>?) = 0

    override fun getType(uri: Uri) = null
}

