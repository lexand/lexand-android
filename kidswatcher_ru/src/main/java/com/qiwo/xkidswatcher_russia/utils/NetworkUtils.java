package com.qiwo.xkidswatcher_russia.utils;

import com.qiwo.xkidswatcher_russia.Constants;

import net.intari.CustomLogger.CustomLog;

import java.util.concurrent.TimeUnit;

import okhttp3.CertificatePinner;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;

/**
 * Created by Dmitriy Kazimirov on 27/02/2019.
 */
public class NetworkUtils {
    public static final String TAG = NetworkUtils.class.getSimpleName();

    public static OkHttpClient getOkHttpClient() {

        HttpLoggingInterceptor logging = new HttpLoggingInterceptor(message -> CustomLog.i(TAG,message));

        logging.setLevel(HttpLoggingInterceptor.Level.BODY);

        OkHttpClient client = new OkHttpClient.Builder()
                .addNetworkInterceptor(logging)
                //.addInterceptor(new OkHttpLoggingInterceptor())
                .writeTimeout(Constants.NETWORK_TIMEOUT_IN_SECONDS, TimeUnit.SECONDS)
                .readTimeout(Constants.NETWORK_TIMEOUT_IN_SECONDS, TimeUnit.SECONDS)
                .connectTimeout(Constants.NETWORK_TIMEOUT_IN_SECONDS, TimeUnit.SECONDS)
                .build();
        return client;
    }
}
