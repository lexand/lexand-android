package com.qiwo.xkidswatcher_russia.utils;

import net.intari.CustomLogger.CustomLog;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by Dmitriy Kazimirov on 27/02/2019.
 * Yes,I'm aware of https://github.com/square/okhttp/tree/master/okhttp-logging-interceptor
 */
public class OkHttpLoggingInterceptor implements Interceptor {
    public static final String TAG = OkHttpLoggingInterceptor.class.getSimpleName();

    @Override public Response intercept(Interceptor.Chain chain) throws IOException {
        Request request = chain.request();

        long t1 = System.nanoTime();
        CustomLog.i(TAG,String.format("Sending request %s on %s\nHeaders:\n%s",
                request.url(), chain.connection(), request.headers()));

        Response response = chain.proceed(request);

        long t2 = System.nanoTime();
        CustomLog.i(TAG,String.format("Received response for %s in %.1fms\nHeaders:\n%s\nBody:\n%s",
                response.request().url(), (t2 - t1) / 1e6d,
                response.headers(),
                response.body().string()
                ));

        return response;
    }
}