package com.qiwo.xkidswatcher_russia.dialog;

import android.app.Activity;
import android.content.Context;

import com.qiwo.xkidswatcher_russia.R;
import net.intari.CustomLogger.CustomLog;

public class DialogHelper {

    public static final String TAG = DialogHelper.class.getSimpleName();

    public static CommonDialog getPinterestDialog(Context context) {
        return new CommonDialog(context, R.style.dialog_common);
    }

    public static CommonDialog getPinterestDialogCancelable(Context context) {
        CommonDialog dialog = new CommonDialog(context, R.style.dialog_common);
        dialog.setCanceledOnTouchOutside(true);
        return dialog;
    }

    public static WaitDialog getWaitDialog(Activity activity, int message) {
        WaitDialog dialog = null;
        try {
            dialog = new WaitDialog(activity, R.style.dialog_waiting);
            dialog.setMessage(message);
        } catch (Exception e) {
            CustomLog.logException(TAG,e);
        }
        return dialog;
    }

    public static WaitDialog getWaitDialog(Activity activity, String message) {
        WaitDialog dialog = null;
        try {
            dialog = new WaitDialog(activity, R.style.dialog_waiting);
            dialog.setMessage(message);
        } catch (Exception ex) {
            CustomLog.logException(TAG,ex);
        }
        return dialog;
    }

    public static WaitDialog getCancelableWaitDialog(Activity activity,
                                                     String message) {
        WaitDialog dialog = null;
        try {
            dialog = new WaitDialog(activity, R.style.dialog_waiting);
            dialog.setMessage(message);
            dialog.setCancelable(true);
        } catch (Exception ex) {
            CustomLog.logException(TAG,ex);
        }
        return dialog;
    }

}
