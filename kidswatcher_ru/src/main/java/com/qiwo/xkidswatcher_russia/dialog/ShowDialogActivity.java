package com.qiwo.xkidswatcher_russia.dialog;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import androidx.annotation.NonNull;
import com.google.android.material.snackbar.Snackbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.Toast;

import com.qiwo.xkidswatcher_russia.AppContext;
import com.qiwo.xkidswatcher_russia.KidsWatConfig;
import com.qiwo.xkidswatcher_russia.R;
import com.qiwo.xkidswatcher_russia.Utils;
import com.qiwo.xkidswatcher_russia.util.Base64Encode;
import com.qiwo.xkidswatcher_russia.util.Contanst;
import com.qiwo.xkidswatcher_russia.util.ImageUtils;
import com.qiwo.xkidswatcher_russia.util.PhotoUtils;

import net.intari.CustomLogger.CustomLog;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import static com.qiwo.xkidswatcher_russia.Constants.IMAGE_NAME_FOR_AVATAR;
import static com.qiwo.xkidswatcher_russia.Constants.PHOTO_PROVIDER;
import static com.qiwo.xkidswatcher_russia.util.ImageUtils.REQUEST_CODE_IMAGE_PICK;
import static com.qiwo.xkidswatcher_russia.util.PermissionUtils.grantPermissionsForUri;
import static com.qiwo.xkidswatcher_russia.util.PhotoUtils.CROP_SIZE;
import static com.qiwo.xkidswatcher_russia.util.PhotoUtils.copyFromStream;
import static com.qiwo.xkidswatcher_russia.util.PhotoUtils.getBitmapDegree;
import static com.qiwo.xkidswatcher_russia.util.PhotoUtils.getTempImageFile;

@SuppressLint("SdCardPath")
public class ShowDialogActivity extends AppCompatActivity implements OnClickListener {
    public static final String TAG = ShowDialogActivity.class.getSimpleName();


    public static Bitmap photoBitmap = null;
    public static int photoMatrix = 0;
    //private final int CAMERA_FOR_PHOTO = 3017;
    //private final int PHOTO_FOR_PHOTO = 3018;
    private final int PHOTO_FOR_CROP = 3019;
    private Button camera, photo, cancle;
    private Uri imageUri;


    private Uri tempImageUri;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AppContext.getInstance().getImageLoader().clearDiscCache();
        AppContext.getInstance().getImageLoader().clearMemoryCache();

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        Window window = getWindow();
        WindowManager.LayoutParams wl = window.getAttributes();
        wl.flags = WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON;
        wl.alpha = 0.9f;
        window.setAttributes(wl);

        setContentView(R.layout.dialog_activity_photo);

        File baseImageFile=new File(KidsWatConfig.getTempFilePath()+"/photo.png");

        imageUri = FileProvider.getUriForFile(this,
                PHOTO_PROVIDER,
                baseImageFile);

        initView();
        addListener();
    }

    public void initView() {
        camera = (Button) this.findViewById(R.id.user_dialog_creama);
        photo = (Button) this.findViewById(R.id.user_dialog_photo);
        cancle = (Button) this.findViewById(R.id.user_dialog_cancle);
    }


    private String getPath() {
        //return PhotoUtils.getTempImageFile(TAG,AppContext.getInstance());

        String path = null;
        boolean isExit = Environment.getExternalStorageState().equals(
                Environment.MEDIA_MOUNTED);
        if (isExit) {
            path = Environment.getExternalStorageDirectory().getPath();
        } else {
            path = Environment.getDataDirectory().getPath();
        }
        return path;
    }

    private void addListener() {
        camera.setOnClickListener(this);
        photo.setOnClickListener(this);
        cancle.setOnClickListener(this);
    }

    @Override
    public void finish() {
        super.finish();
        this.overridePendingTransition(0, R.anim.activity_out);
    }

    // private File file;



    private void startActionCameraInternal() {

        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        Uri photoURI;
        try {
            tempImageUri=getTempImageFile(TAG,ShowDialogActivity.this);
            photoURI=tempImageUri;
            CustomLog.d(TAG,"Photo file is "+photoURI.toString());
        } catch (Exception ex) {
            CustomLog.logException(TAG,ex);
            return;
        }
        grantPermissionsForUri(this,photoURI,takePictureIntent);
        takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);


        // If you call startActivityForResult() using an intent that no app can handle, your app will crash.
        // So as long as the result is not null, it's safe to use the intent.
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            // Start the image capture intent to take photo
            CustomLog.d(TAG,"taking photo using "+Utils.toUri(takePictureIntent));
            startActivityForResult(takePictureIntent, ImageUtils.REQUEST_CODE_IMAGE_FROM_CAMERA);
        } else {
            CustomLog.e(TAG,"Can't take photo using "+Utils.toUri(takePictureIntent)+" - nobody can handle this one");
            Toast.makeText(ShowDialogActivity.this, R.string.no_camera,
                    Toast.LENGTH_SHORT).show();
        }

    }

    private void startImagePick() {
        Intent intent;
        if (Build.VERSION.SDK_INT < 19) {
            intent = new Intent();
            intent.setAction(Intent.ACTION_GET_CONTENT);
            intent.setType("image/*");
            startActivityForResult(intent,
                    REQUEST_CODE_IMAGE_PICK);
        } else {
            intent = new Intent(Intent.ACTION_PICK,
                    MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            intent.setType("image/*");
            startActivityForResult(intent,
                    REQUEST_CODE_IMAGE_PICK);
        }

    }


    private void startActionCamera() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
                CustomLog.d(TAG,"Arleady  have camera permission");
                //now check storage permission
                if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                    CustomLog.d(TAG,"Arleady have storage permission");
                    startActionCameraInternal();

                } else {
                    //no permission, ask!

                    if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                            Manifest.permission.CAMERA)) {

                        Snackbar.make(camera, R.string.location_permission_storage_rationale, Snackbar.LENGTH_LONG)
                                .setAction(R.string.grant_perm, new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        CustomLog.d(TAG,"User agreed with rationale");
                                        doAskForCameraPermission();
                                    }
                                })
                                .show();
                    } else {
                        CustomLog.d(TAG,"No need to provide rationale for storage permission");
                        doAskForStoragePermission();
                    }
                }
            } else {

                if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                        Manifest.permission.CAMERA)) {

                    Snackbar.make(camera, R.string.location_permission_photos_rationale, Snackbar.LENGTH_LONG)
                            .setAction(R.string.grant_perm, new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    CustomLog.d(TAG,"User agreed with rationale");
                                    doAskForCameraPermission();
                                }
                            })
                            .show();
                } else {
                    CustomLog.d(TAG,"No need to provide rationale for camera permission");
                    doAskForCameraPermission();
                }


            }
        } else {
            CustomLog.d(TAG,"No need to ask for  camera permission");
            startActionCameraInternal();
        }
    }

    private void doAskForStoragePermission(){
        ActivityCompat.requestPermissions(ShowDialogActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, Contanst.REQUEST_CODE_REQUEST_PERMISSION_STORAGE);
        CustomLog.d(TAG,"Asking for storage permission");
    }

    private void doAskForCameraPermission(){
        ActivityCompat.requestPermissions(ShowDialogActivity.this, new String[]{Manifest.permission.CAMERA}, Contanst.REQUEST_CODE_REQUEST_PERMISSION_CAMERA);
        CustomLog.d(TAG,"Asking for camera permission");
    }



    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.user_dialog_creama:
                startActionCamera();
                break;
            case R.id.user_dialog_photo:
                startImagePick();
                break;
            case R.id.user_dialog_cancle:
                this.finish();
                break;
        }
    }



    @SuppressLint("Override")
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == Contanst.REQUEST_CODE_REQUEST_PERMISSION_STORAGE) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                //permission granted
                CustomLog.d(TAG,"Now have storage permission");
                startActionCamera();//go and ask storage permission if needed
                //startActionCameraInternal();
            } else {
                //permission denied
                //now tell user he was baad
                Toast.makeText(ShowDialogActivity.this, getApplicationContext().getResources().getString(R.string.location_permission_storage), Toast.LENGTH_LONG).show();
            }
        } else if (requestCode == Contanst.REQUEST_CODE_REQUEST_PERMISSION_CAMERA) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                //permission granted
                CustomLog.d(TAG,"Now have camera permission");
                startActionCamera();//go and ask storage permission if needed

                //startActionCameraInternal();
            } else {
                //permission denied
                //now tell user he was baad

                Toast.makeText(ShowDialogActivity.this, getApplicationContext().getResources().getString(R.string.location_permission_photos), Toast.LENGTH_LONG).show();
            }
        }
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {



        //wtf?!
        if (resultCode != Activity.RESULT_OK) {
            CustomLog.w(TAG,"Got OnActivityResult(requestCode="+requestCode+",resultCode="+resultCode+", but resultCode is NOT RESULT_OK, it's:"+resultCode);
            Toast.makeText(this, getApplicationContext().getResources().getString(R.string.error_cropper_not_ok), Toast.LENGTH_LONG).show();

            //use old result data as is
            if (resultCode == Activity.RESULT_CANCELED) { //0) {
                if (null != data && !"".equals(data)) {
                    if (data.getExtras() != null) {
                        photoBitmap = (Bitmap) data.getExtras().get("data");
                    } else {
                        CustomLog.e(TAG,"null extras-data");
                    }
                } else {
                    CustomLog.e(TAG,"null data");
                    return;
                }
            } else {
                CustomLog.w(TAG,"Non-zero resultCode, it's "+resultCode);
                return;
            }
        }

        switch (requestCode) {
            case ImageUtils.REQUEST_CODE_IMAGE_FROM_CAMERA: //CAMERA_FOR_PHOTO
                CustomLog.d(TAG,"onActivityResult.RequestCode:ImageUtils.REQUEST_CODE_IMAGE_FROM_CAMERA (replacement for CAMERA_FOR_PHOTO).data.getData():"+data+". tempImageUri:"+tempImageUri);
                doCameraForPhoto(tempImageUri);
                break;

            case PHOTO_FOR_CROP:
                //result from image cropping by SetPhotoUtil.doCrop(...)

                /*
                Bundle extras = data.getExtras();
                if (extras != null){
                    bmp = extras.getParcelable("data");
                }
                File f = new File(selectImageUri.getPath());
                if (f.exists()) f.delete();
                Intent inten3 = new Intent(this, tabActivity.class);
                startActivity(inten3);
                */

                CustomLog.d(TAG,"onActivityResult.RequestCode:PHOTO_FOR_CROP:got cropped image, old path is "+getPath());

                if (data != null && data.getData() != null) {
                    //make it usable as regular photo image
                    updateAvatar(this,data.getData(),IMAGE_NAME_FOR_AVATAR,resultCode);
                } else {
                    CustomLog.w(TAG,"Empty data...");
                    Toast.makeText(ShowDialogActivity.this, getApplicationContext().getResources().getString(R.string.error_no_crop_data), Toast.LENGTH_LONG).show();

                }
                finish();
                break;

            case REQUEST_CODE_IMAGE_PICK:
                CustomLog.d(TAG,"onActivityResult.RequestCode:REQUEST_CODE_IMAGE_PICK:got cropped image, old path is "+getPath()+".data:"+ Utils.toUri(data));

                CustomLog.d(TAG,"data.getdata:" + data.getData() + ",imgurl:" + imageUri);

                if (data != null && data.getData() != null)
                    PhotoUtils.doCrop(this,data.getData());


                /*
                Uri newUri=makeUriFromUri(ShowDialogActivity.this,imageUri, Constants.DEFAULT_IMAGE_FORMAT);
                CustomLog.d(TAG,"Original imageURI:"+imageUri+", newUri:"+newUri+", data:"+data.getData());
                PhotoUtils.doCrop(this, data.getData(), newUri);
                */

                break;
        }

        super.onActivityResult(requestCode, resultCode, data);
    }

    private void updateAvatar(Context context, Uri uri, String imageName,int resultCode) {
        CustomLog.i(TAG,"Will (try to) update avatar from "+uri.toString());


        //Uri newUri=makeUriFromUri(this,data.getData(),DEFAULT_IMAGE_FORMAT);
        String destName= KidsWatConfig.getTempFilePath()+"photo.jpg";//getPath() + "/photo.png";
        File destFile=new File(destName);
        CustomLog.d(TAG,"Try to process cropped image to "+destName+" from "+uri);
        try {
            InputStream inputStream=getContentResolver().openInputStream(uri);
            copyFromStream(inputStream,destFile);
            PhotoUtils.imagePath = destName;
            setResult(resultCode);
        } catch (IOException ex) {
            CustomLog.logException(TAG,ex);
            setResult(Activity.RESULT_CANCELED);
        }


    }

    private void doCameraForPhoto(Uri uri) {
        photoMatrix=getBitmapDegree(TAG,this,uri);
        //photoMatrix = getBitmapDegree(getPath() + "/photo.png");
        CustomLog.d(TAG,"imgurl:" + uri + ",(old) path:" + KidsWatConfig.getTempFilePath() + "/photo.png, photoMatrix:"+photoMatrix);
        //PhotoUtils.doCrop(this, uri, uri);
        PhotoUtils.doCrop(this, uri);

    }



}
