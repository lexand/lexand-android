package com.qiwo.xkidswatcher_russia;

/**
 * Created by Dmitriy Kazimirov on 24/01/2019.
 */

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;
import com.google.firebase.messaging.FirebaseMessaging;

import net.intari.CustomLogger.CustomLog;

import java.io.IOException;

public class MyInstanceIDListenerService extends FirebaseInstanceIdService {
    public static final String TAG = MyInstanceIDListenerService.class.getSimpleName();

    private static final String[] TOPICS = {"global"};

    /**
     * Called if InstanceID token is updated. This may occur if the security of
     * the previous token had been compromised. This call is initiated by the
     * InstanceID provider.
     */
    // [START refresh_token]
    @Override
    public void onTokenRefresh() {

        // Get updated InstanceID token.
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        CustomLog.d(TAG, "FCM:Refreshed token: " + refreshedToken);
        // TODO: Implement this method to send any registration to your app's servers.
        sendRegistrationToServer(this,refreshedToken);

        // Subscribe to topic channels
        try {
            subscribeTopics(refreshedToken);
        } catch (IOException ex) {
            CustomLog.logException(ex);
        }
        // Fetch updated Instance ID token and notify our app's server of any changes (if applicable).
        //Intent intent = new Intent(this, RegistrationIntentService.class);
        //startService(intent);
    }
    // [END refresh_token]


    private static void sendRegistrationToServer(Context context, String token) {
        // Add custom implementation, as needed.

        CustomLog.d(TAG, "FCM:Device registered: regId = " + token);
        if (token!=null) {
            AppContext.getInstance().setGCMRegID(token);
            KidsWatConfig.setGcmAppid(token);
            ServerUtilities.register(context, token);
        }

        if (!KidsWatConfig.getFCMMigrationStarted()) {
            CustomLog.d(TAG, "FCM:Migration was not marked as started. marking");
            KidsWatConfig.setFCMMigrationStarted(true);
        }
    }
    /**
     * Subscribe to any GCM topics of interest, as defined by the TOPICS constant.
     * https://developers.google.com/cloud-messaging/android/android-migrate-gcmpubsub
     *
     * @param token GCM token
     * @throws IOException if unable to reach the GCM PubSub service
     */
    // [START subscribe_topics]
    private void subscribeTopics(String token) throws IOException {
        FirebaseMessaging pubSub = FirebaseMessaging.getInstance();
        for (String topic : TOPICS) {
            pubSub.subscribeToTopic(topic);
        }
    }
    // [END subscribe_topics]
}