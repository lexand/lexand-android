package com.qiwo.xkidswatcher_russia.interf;

/**
 *
 */
public interface BaseViewInterface {

    public void initView();

    public void initData();

    public void preInitView();
}
