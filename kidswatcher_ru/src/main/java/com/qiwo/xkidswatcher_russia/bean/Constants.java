package com.qiwo.xkidswatcher_russia.bean;

public class Constants {

    public static final String OK = "确定";
    public static final String APPNAME = "儿童手环";
    public static final String YES = "确定";
    public static final String EDIT = "编辑";
    public static final String DELETE = "删除";
    public static final String DELETE_OK = "删除成功!";
    public static final String MONITOR = "监听";
    public static final String CANCEL = "取消";
    public static final String CALL = "通话";
    public static final String CLEAR = "清空";
    public static final String BOY = "男孩";
    public static final String GIRL = "女孩";
    public static final String NICKNAME = "宝贝昵称";
    public static final String TAKE_PHOTO = "拍照";
    public static final String SELECT_FROM_ALBUM = "从手机相册选取";
    public static final String SAFEZONES = "安全区域";
    public static final String RADIUS = "半径范围";
    public static final String SAVE = "保存";
    public static final String REMINDER = "提示";
    public static final String TIP_TURN_ON_BLUE_TOOTH = "请打开手机蓝牙功能.";

    public static final String ACTION_ADD = "ADD";
    public static final String ACTION_EDIT = "EDIT";
    //	public static final String OK = "OK";
//	public static final String APPNAME = "Kid\'s Watcher";
//	public static final String YES = "Yes";
//	public static final String EDIT = "Edit";
//	public static final String DELETE = "Delete";
//	public static final String DELETE_OK = "Delete OK!";
//	public static final String MONITOR = "Monitor";
//	public static final String CANCEL = "Cancel";
//	public static final String CALL = "Call";
//	public static final String CLEAR = "Clear";
//	public static final String BOY = "Boy";
//	public static final String GIRL = "Girl";
//	public static final String NICKNAME = "NickName";
//	public static final String TAKE_PHOTO = "Take Photo";
//	public static final String SELECT_FROM_ALBUM = "Select From Album";
//	public static final String SAFEZONES = "Safe Zones";
//	public static final String RADIUS = "Radius";
//	public static final String SAVE = "Save";
//	public static final String REMINDER = "Reminder";
//	public static final String TIP_TURN_ON_BLUE_TOOTH = "Please Turn on the BlueTooth.";
//	
    public static final int REQUEST_STORAGE_REQUEST_PERMISSION = 130;
    public static final int REQUEST_PHONE_REQUEST_PERMISSION = 131;
    public static final int REQUEST_CODE_REQUEST_PERMISSION = 132;
    public static final int REQUEST_READ_CONTACT_REQUEST_PERMISSION = 133;
    public static final int REQUEST_READ_SMS_REQUEST_PERMISSION = 134;
    public static final int REQUEST_LOCATION_REQUEST_PERMISSION = 134;
    public static final int REQUEST_CONTACT_REQUEST_PERMISSION = 135;

    public static final int REQUEST_CODE_RELATION = 300;
    public static final int REQUEST_CODE_COUNTRY_CODE = 301;
    public static final int REQUEST_CODE_FAMILY_MEMBER = 302;

    public static final int RESULT_CODE_RELATION = 400;
    public static final int RESULT_CODE_COUNTRY = 401;

}
