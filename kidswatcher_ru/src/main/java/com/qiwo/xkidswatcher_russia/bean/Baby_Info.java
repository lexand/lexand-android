package com.qiwo.xkidswatcher_russia.bean;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.qiwo.xkidswatcher_russia.util.SimpleDateUtil;

public class Baby_Info {
//	"baby_info": {
//    "uid": 10000057,
//    "group": 10000521,
//    "last_modify_time": 1452247339,
//    "family_id": "10000521",
//    "nickname": "10086",
//    "birthday": 1452247295,
//    "sex": 0,
//    "height": 1,
//    "weight": 20,
//    "grade": "0",
//    "doc_type": "entity_kid_info",
//    "img": ""
//	}

    @Expose
    @SerializedName("uid")
    private String uid;

    @Expose
    @SerializedName("group")
    private String group;

    @Expose
    @SerializedName("last_modify_time")
    private long last_modify_time;

    @Expose
    @SerializedName("family_id")
    private String family_id;

    @Expose
    @SerializedName("nickname")
    private String nickname;

    @Expose
    @SerializedName("birthday")
    private long birthday;

    @Expose
    @SerializedName("sex")
    private int sex;

    @Expose
    @SerializedName("height")
    private float height;
    @Expose
    @SerializedName("weight")
    private float weight;

    @Expose
    @SerializedName("grade")
    private String grade;

    @Expose
    @SerializedName("entity_kid_info")
    private String entity_kid_info;

    @Expose
    @SerializedName("img")
    private String img;

    @Expose
    @SerializedName("img_path")
    private String img_path;

    public String getImg_path() {
        return img_path;
    }

    public void setImg_path(String img_path) {
        this.img_path = img_path;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    public long getLast_modify_time() {
        return last_modify_time;
    }

    public void setLast_modify_time(long last_modify_time) {
        this.last_modify_time = last_modify_time;
    }

    public String getFamily_id() {
        return family_id;
    }

    public void setFamily_id(String family_id) {
        this.family_id = family_id;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public long getBirthday() {
        return birthday;
    }

    public void setBirthday(long birthday) {
        this.birthday = birthday;
    }

    public String getBirthdayText() {
        return SimpleDateUtil.convert2String(birthday * 1000,
                SimpleDateUtil.DATE_FORMAT);
    }

    public int getSex() {
        return sex;
    }

    public void setSex(int sex) {
        this.sex = sex;
    }

    public String getSexText() {
        return sex == 1 ? "boy" : "girl";
    }

    public float getHeight() {
        return height;
    }

    public void setHeight(float height) {
        this.height = height;
    }

    public float getWeight() {
        return weight;
    }

    public void setWeight(float weight) {
        this.weight = weight;
    }

    public String getGrade() {
        return grade;
    }

    public void setGrade(String grade) {
        this.grade = grade;
    }

    public String getEntity_kid_info() {
        return entity_kid_info;
    }

    public void setEntity_kid_info(String entity_kid_info) {
        this.entity_kid_info = entity_kid_info;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }
}
