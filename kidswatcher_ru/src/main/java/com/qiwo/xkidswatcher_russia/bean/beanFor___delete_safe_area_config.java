package com.qiwo.xkidswatcher_russia.bean;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


//{
//    "error": 0, 
//    "info": {
//        "message": "Successful operation!", 
//        "data": {
//            "config": [
//                {
//                    "latitude": "0.0", 
//                    "longitude": "0.0", 
//                    "radius": 500, 
//                    "name": "schoolll", 
//                    "address": "", 
//                    "create_user": "13428795901"
//                }
//            ]
//        }
//    }
//}

//{"error":0,"info":{"message":"no data","data":{"config":[]}}}


public class beanFor___delete_safe_area_config {

    @Expose
    @SerializedName("error")
    public int error;

    @Expose
    @SerializedName("info")
    public CInfo info;

    public static class CInfo {
        @Expose
        @SerializedName("message")
        public String message;

        @Expose
        @SerializedName("total")
        public int total;

        @Expose
        @SerializedName("data")
        public CData ddata;

    }

    public static class CData {

        @Expose
        @SerializedName("config")
        public String config;

    }

}
