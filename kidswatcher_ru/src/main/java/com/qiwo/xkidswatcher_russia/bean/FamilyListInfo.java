package com.qiwo.xkidswatcher_russia.bean;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

//"info": {
//    "data": [
//        {
//            "uid": 10000001, 
//            "status": 1, 
//            "admin": 1, 
//            "msg_type": 0, 
//            "relation": "sister", 
//            "family_id": 10000008, 
//            "datatype": "group", 
//            "mobile": "13428795901"
//        }
//    ], 
//    "total": 1, 
//    "message": "Successful operation!"
//}


public class FamilyListInfo {

    @Expose
    @SerializedName("data")
    private List<FamilyMember> ddata;
    @Expose
    @SerializedName("total")
    private int total;
    @Expose
    @SerializedName("message")
    private String message;

    public List<FamilyMember> getDdata() {
        return ddata;
    }

    public void setDdata(List<FamilyMember> ddata) {
        this.ddata = ddata;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }


}
