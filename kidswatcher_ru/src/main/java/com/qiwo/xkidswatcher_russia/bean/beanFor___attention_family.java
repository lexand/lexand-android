package com.qiwo.xkidswatcher_russia.bean;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

//{
//    "data": {
//        "device_id": [
//            {
//                "value": {
//                    "uid": 10000001, 
//                    "device_key": "7afc3a1e789673fe", 
//                    "status": 0, 
//                    "hard_code": "a1300456", 
//                    "verify": 5729, 
//                    "qrcode": "85e9f1877fae7ab1", 
//                    "sim": "13049489420", 
//                    "add_time": 1421057333
//                }, 
//                "doc_id": "device::0000000000989686"
//            }
//        ]
//    }, 
//    "message": "device exist", 
//    "error": 1043
//}

public class beanFor___attention_family {

	@Expose
	@SerializedName("error")
	public int error;

	@Expose
	@SerializedName("info")
	public CInfo info;

	public static class CInfo {
		@Expose
		@SerializedName("message")
		public String message;
	}
}
