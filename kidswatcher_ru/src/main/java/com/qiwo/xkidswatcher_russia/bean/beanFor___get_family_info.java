package com.qiwo.xkidswatcher_russia.bean;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

//{
//    "error": 0, 
//    "info": {
//        "message": "Successful operation!", 
//        "data": {
//            "device_id": "0000000000989686"
//        }, 
//        "baby": {
//            "uid": 10000001, 
//            "group": 10000008, 
//            "family_id": "10000008", 
//            "img": "", 
//            "nickname": "czzzz", 
//            "birthday": "1421057310517", 
//            "sex": "0", 
//            "height": "1.07", 
//            "weight": 20.5, 
//            "grade": "0"
//        }, 
//        "relation": "sister"
//    }
//}

public class beanFor___get_family_info {

    @Expose
    @SerializedName("error")
    private int error;

    @Expose
    @SerializedName("info")
    private familyInfo info;

    public familyInfo getInfo() {
        return info;
    }

    public void setInfo(familyInfo info) {
        this.info = info;
    }

    public int getError() {
        return error;
    }

    public void setError(int error) {
        this.error = error;
    }

}
