package com.qiwo.xkidswatcher_russia.bean;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class beanFor___get_family_info_v2 {
    /*{
       "version": 5612,
       "relation": "Mom",
       "device_id": "00000000568f8905",
       "baby_info": {
           "uid": 10000057,
           "group": 10000521,
           "last_modify_time": 1452247339,
           "family_id": "10000521",
           "nickname": "10086",
           "birthday": 1452247295,
           "sex": 0,
           "height": 1,
           "weight": 20,
           "grade": "0",
           "doc_type": "entity_kid_info",
           "img": ""
       },
       "w461_device_country_code": "86",
       "device_country_code": "86",
       "w461_device_sim_no": "10086",
       "device_sim_no": "10086",
       "admin": 1,
       "country_code": "86",
       "mobile": "18236886950"
}*/
    @Expose
    @SerializedName("version")
    private int version;

    @Expose
    @SerializedName("relation")
    private String relation;

    @Expose
    @SerializedName("device_id")
    private String device_id;

    @Expose
    @SerializedName("baby_info")
    private Baby_Info baby_info;

    @Expose
    @SerializedName("w461_device_country_code")
    private String w461_device_country_code;

    @Expose
    @SerializedName("device_country_code")
    private String device_country_code;

    @Expose
    @SerializedName("w461_device_sim_no")
    private String w461_device_sim_no;

    @Expose
    @SerializedName("device_sim_no")
    private String device_sim_no;

    @Expose
    @SerializedName("admin")
    private int admin;

    @Expose
    @SerializedName("country_code")
    private int country_code;
    @Expose

    @SerializedName("mobile")
    private long mobile;

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public String getRelation() {
        return relation;
    }

    public void setRelation(String relation) {
        this.relation = relation;
    }

    public String getDevice_id() {
        return device_id;
    }

    public void setDevice_id(String device_id) {
        this.device_id = device_id;
    }

    public Baby_Info getBaby_info() {
        return baby_info;
    }

    public void setBaby_info(Baby_Info baby_info) {
        this.baby_info = baby_info;
    }

    public String getW461_device_country_code() {
        return w461_device_country_code;
    }

    public void setW461_device_country_code(String w461_device_country_code) {
        this.w461_device_country_code = w461_device_country_code;
    }

    public String getDevice_country_code() {
        return device_country_code;
    }

    public void setDevice_country_code(String device_country_code) {
        this.device_country_code = device_country_code;
    }

    public String getW461_device_sim_no() {
        return w461_device_sim_no;
    }

    public void setW461_device_sim_no(String w461_device_sim_no) {
        this.w461_device_sim_no = w461_device_sim_no;
    }

    public String getDevice_sim_no() {
        return device_sim_no;
    }

    public void setDevice_sim_no(String device_sim_no) {
        this.device_sim_no = device_sim_no;
    }

    public int getAdmin() {
        return admin;
    }

    public void setAdmin(int admin) {
        this.admin = admin;
    }

    public int getCountry_code() {
        return country_code;
    }

    public void setCountry_code(int country_code) {
        this.country_code = country_code;
    }

    public long getMobile() {
        return mobile;
    }

    public void setMobile(long mobile) {
        this.mobile = mobile;
    }


}
