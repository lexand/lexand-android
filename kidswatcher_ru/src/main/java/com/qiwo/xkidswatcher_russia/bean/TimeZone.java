package com.qiwo.xkidswatcher_russia.bean;


import java.io.Serializable;

public class TimeZone implements Serializable {

    private String timeZone;
    private boolean isOK;

    public String getTimeZone() {
        return timeZone;
    }

    public void setTimeZone(String timeZone) {
        this.timeZone = timeZone;
    }

    public boolean isOK() {
        return isOK;
    }

    public void setOK(boolean OK) {
        isOK = OK;
    }
}
