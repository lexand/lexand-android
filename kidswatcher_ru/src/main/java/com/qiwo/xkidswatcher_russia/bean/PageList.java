package com.qiwo.xkidswatcher_russia.bean;

import java.util.List;

/**
 * 类名 PageList.java</br>
 * <p>
 * 说明 类的描述
 */
public interface PageList<T> {

    public int getPageSize();

    public int getCount();

    public List<T> getList();
}
