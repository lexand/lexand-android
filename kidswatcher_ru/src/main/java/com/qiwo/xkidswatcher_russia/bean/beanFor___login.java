package com.qiwo.xkidswatcher_russia.bean;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

//{
//    "error": 0, 
//    "info": {
//        "message": "Request successful!", 
//        "user": {
//            "name": "", 
//            "phone": "13428795901", 
//            "reg_time": 1432706167, 
//            "uid": 10000017, 
//            "push_token": "xxxxx", 
//            "plat": "1", 
//            "country_code": "86", 
//            "global_sesison": {
//                "token": "sess::f59732e270060a0c1dc46be4128db388", 
//                "expiration": "2015-06-15 15:52:28"
//            }
//        }
//    }
//}

public class beanFor___login {

    @Expose
    @SerializedName("error")
    public int error;

    @Expose
    @SerializedName("info")
    public CInfo info;

    public static class CInfo {
        @Expose
        @SerializedName("message")
        public String message;

        @Expose
        @SerializedName("user")
        public CUser user;
    }

    public static class CGlobal_sesison {

        @Expose
        @SerializedName("expiration")
        public String expiration;

        @Expose
        @SerializedName("token")
        public String token;
    }

    public static class CUser {

        @Expose
        @SerializedName("uid")
        public String uid;

        @Expose
        @SerializedName("reg_time")
        public long reg_time;

        @Expose
        @SerializedName("start_time")
        public int start_time;

        @Expose
        @SerializedName("end_time")
        public int end_time;

        @Expose
        @SerializedName("global_sesison")
        public CGlobal_sesison global_sesison;

        @Expose
        @SerializedName("country_code")
        public String country_code;

        @Expose
        @SerializedName("phone")
        public String phone;

        @Expose
        @SerializedName("push_token")
        public String push_token;

        @Expose
        @SerializedName("plat")
        public int plat;

        @Expose
        @SerializedName("name")
        public String name;

        @Expose
        @SerializedName("img_path")
        public String img_path;

    }
}
