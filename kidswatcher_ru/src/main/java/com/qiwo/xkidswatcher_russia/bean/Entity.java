package com.qiwo.xkidswatcher_russia.bean;

import java.io.Serializable;

/**
 * 实体类
 */
@SuppressWarnings("serial")
public abstract class Entity implements Serializable {

    protected String _id;
    // 缓存的key
    protected String cacheKey;

    public String getId() {
        return _id;
    }

    public void setId(String id) {
        _id = id;
    }

    public String getCacheKey() {
        return cacheKey;
    }

    public void setCacheKey(String cacheKey) {
        this.cacheKey = cacheKey;
    }
}
