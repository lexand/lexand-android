package com.qiwo.xkidswatcher_russia.bean;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

//{
//    "error": 0, 
//    "info": {
//        "message": "success option"
//    }
//}

//{"error":0,"info":{"message":"no data","data":{"config":[]}}}


public class beanFor___add_safe_area_config {

    @Expose
    @SerializedName("error")
    public int error;

    @Expose
    @SerializedName("info")
    public CInfo info;

    public static class CInfo {
        @Expose
        @SerializedName("message")
        public String message;

        @Expose
        @SerializedName("total")
        public int total;

        @Expose
        @SerializedName("data")
        public List<CData> ddata;

    }

    public static class CData {

        @Expose
        @SerializedName("config")
        public String config;

    }

}
