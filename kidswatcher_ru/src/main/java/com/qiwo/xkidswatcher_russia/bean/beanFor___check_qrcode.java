package com.qiwo.xkidswatcher_russia.bean;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

//{
//    "data": {
//        "device_id": [
//            {
//                "value": {
//                    "uid": 10000001, 
//                    "device_key": "7afc3a1e789673fe", 
//                    "status": 0, 
//                    "hard_code": "a1300456", 
//                    "verify": 5729, 
//                    "qrcode": "85e9f1877fae7ab1", 
//                    "sim": "13049489420", 
//                    "add_time": 1421057333
//                }, 
//                "doc_id": "device::0000000000989686"
//            }
//        ]
//    }, 
//    "message": "device exist", 
//    "error": 1043
//}

public class beanFor___check_qrcode {

    @Expose
    @SerializedName("error")
    public int error;

    @Expose
    @SerializedName("message")
    public String message;

    @Expose
    @SerializedName("data")
    public Cdata ddata;

    public static class CValue {
        @Expose
        @SerializedName("uid")
        public String uid;

        @Expose
        @SerializedName("device_key")
        public String device_key;

        @Expose
        @SerializedName("status")
        public int status;

        @Expose
        @SerializedName("hard_code")
        public String hard_code;

        @Expose
        @SerializedName("verify")
        public String verify;

        @Expose
        @SerializedName("qrcode")
        public String qrcode;

        @Expose
        @SerializedName("sim")
        public String sim;

        @Expose
        @SerializedName("add_time")
        public long add_time;

    }

    public static class Cdevice_id {

        @Expose
        @SerializedName("value")
        public CValue value;

        @Expose
        @SerializedName("doc_id")
        public String doc_id;
    }

    public static class Cdata {

        @Expose
        @SerializedName("device_id")
        public List<Cdevice_id> device_id;

    }
}
