package com.qiwo.xkidswatcher_russia.bean;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

//{
//    "uid": 10000001, 
//    "status": 1, 
//    "admin": 1, 
//    "msg_type": 0, 
//    "relation": "sister", 
//    "family_id": 10000008, 
//    "datatype": "group", 
//    "mobile": "13428795901"
//}

public class FamilyMember {

    @Expose
    @SerializedName("uid")
    private String uid;

    @Expose
    @SerializedName("status")
    private int status;

    @Expose
    @SerializedName("admin")
    private int admin;

    @Expose
    @SerializedName("msg_type")
    private int msg_type;

    @Expose
    @SerializedName("relation")
    private String relation;

    @Expose
    @SerializedName("family_id")
    private String family_id;

    @Expose
    @SerializedName("datatype")
    private String datatype;

    @Expose
    @SerializedName("mobile")
    private String mobile;

    @Expose
    @SerializedName("country_code")
    private String country_code;

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getDatatype() {
        return datatype;
    }

    public void setDatatype(String datatype) {
        this.datatype = datatype;
    }

    public String getFamily_id() {
        return family_id;
    }

    public void setFamily_id(String family_id) {
        this.family_id = family_id;
    }

    public String getRelation() {
        return relation;
    }

    public void setRelation(String relation) {
        this.relation = relation;
    }

    public int getMsg_type() {
        return msg_type;
    }

    public void setMsg_type(int msg_type) {
        this.msg_type = msg_type;
    }

    public int getAdmin() {
        return admin;
    }

    public void setAdmin(int admin) {
        this.admin = admin;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    /**
     * @return the country_code
     */
    public String getCountry_code() {
        return country_code;
    }

    /**
     * @param country_code the country_code to set
     */
    public void setCountry_code(String country_code) {
        this.country_code = country_code;
    }

}
