package com.qiwo.xkidswatcher_russia.bean;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;


//{
//    "error": 0, 
//    "info": {
//        "message": "Successful operation!", 
//        "data": {
//            "config": [
//                {
//                    "latitude": "0.0", 
//                    "longitude": "0.0", 
//                    "radius": 500, 
//                    "name": "schoolll", 
//                    "address": "", 
//                    "create_user": "13428795901"
//                }
//            ]
//        }
//    }
//}

//{"error":0,"info":{"message":"no data","data":{"config":[]}}}


//{
//    "error": 0,
//    "info": {
//        "message": "Successful operation!",
//        "data": {
//            "config": [
//                {
//                    "latitude": "22.63068",
//                    "longitude": "114.024704",
//                    "radius": 500,
//                    "name": "home",
//                    "address": "",
//                    "create_user": "10000000"
//                }
//            ]
//        }
//    }
//}

public class beanFor___get_safe_area_config {

    @Expose
    @SerializedName("error")
    public int error;

    @Expose
    @SerializedName("info")
    public CInfo info;

    public static class CInfo {
        @Expose
        @SerializedName("message")
        public String message;

        // @Expose
        // @SerializedName("total")
        // public int total;

        @Expose
        @SerializedName("data")
        public CData ddata;

    }

    public static class CData {

        @Expose
        @SerializedName("config")
        public List<CConfig> config;

    }

    public static class CConfig implements Serializable {

        /**
         *
         */
        private static final long serialVersionUID = 6201781534953153204L;

        @Expose
        @SerializedName("latitude")
        public double latitude;

        @Expose
        @SerializedName("longitude")
        public double longitude;

        @Expose
        @SerializedName("radius")
        public int radius;

        @Expose
        @SerializedName("name")
        public String name;

        @Expose
        @SerializedName("address")
        public String address;

        @Expose
        @SerializedName("create_user")
        public String create_user;

    }

}
