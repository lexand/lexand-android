package com.qiwo.xkidswatcher_russia.bean;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

//{
//    "device_id": "0000000000989686"
//}
public class Device {

    @Expose
    @SerializedName("device_id")
    private String device_id;
    @Expose
    @SerializedName("device_iccid")
    private String device_iccid;

    public String getDevice_id() {
        return device_id;
    }

    public void setDevice_id(String device_id) {
        this.device_id = device_id;
    }

    public String getDevice_iccid() {
        return device_iccid;
    }

    public void setDevice_iccid(String device_iccid) {
        this.device_iccid = device_iccid;
    }
}
