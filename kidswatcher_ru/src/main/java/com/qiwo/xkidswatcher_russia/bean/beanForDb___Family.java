package com.qiwo.xkidswatcher_russia.bean;

public class beanForDb___Family {
    public String family_id;
    public String device_id;
    public String create_uid;
    public String base64_img;
    public String img_path;
    public String nickname;

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("beanForDb___Family{");
        sb.append("family_id='").append(family_id).append('\'');
        sb.append(", device_id='").append(device_id).append('\'');
        sb.append(", create_uid='").append(create_uid).append('\'');
        sb.append(", base64_img='").append(base64_img).append('\'');
        sb.append(", img_path='").append(img_path).append('\'');
        sb.append(", nickname='").append(nickname).append('\'');
        sb.append(", birthday=").append(birthday);
        sb.append(", birthdayText='").append(birthdayText).append('\'');
        sb.append(", sex=").append(sex);
        sb.append(", sexText='").append(sexText).append('\'');
        sb.append(", height=").append(height);
        sb.append(", weight=").append(weight);
        sb.append(", grade=").append(grade);
        sb.append(", gradeText='").append(gradeText).append('\'');
        sb.append(", sim='").append(sim).append('\'');
        sb.append(", qrcode='").append(qrcode).append('\'');
        sb.append(", verify='").append(verify).append('\'');
        sb.append(", hard_code='").append(hard_code).append('\'');
        sb.append(", device_key='").append(device_key).append('\'');
        sb.append(", bt_addr='").append(bt_addr).append('\'');
        sb.append(", bt_addr_ex='").append(bt_addr_ex).append('\'');
        sb.append(", active_time=").append(active_time);
        sb.append(", bt_addr_pwd='").append(bt_addr_pwd).append('\'');
        sb.append(", valid_date='").append(valid_date).append('\'');
        sb.append(", last_record_no='").append(last_record_no).append('\'');
        sb.append(", d_mode=").append(d_mode);
        sb.append(", version='").append(version).append('\'');
        sb.append(", device_iccid='").append(device_iccid).append('\'');
        sb.append('}');
        return sb.toString();
    }

    public long birthday;
    public String birthdayText;
    public int sex;
    public String sexText;
    public float height;
    public float weight;
    public int grade;
    public String gradeText;

    public String sim;
    public String qrcode;
    public String verify;
    public String hard_code;
    public String device_key;
    public String bt_addr;
    public String bt_addr_ex;
    public long active_time;
    public String bt_addr_pwd;
    public String valid_date;
    public String last_record_no;
    public int d_mode = 0;
    public String version;
    public String device_iccid;
}
