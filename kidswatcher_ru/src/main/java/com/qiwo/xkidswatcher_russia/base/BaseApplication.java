package com.qiwo.xkidswatcher_russia.base;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.res.Resources;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import androidx.multidex.MultiDex;
import android.util.DisplayMetrics;
import android.widget.Toast;

import com.qiwo.xkidswatcher_russia.util.StringUtils;

import net.intari.CustomLogger.CustomLog;

@SuppressLint("InflateParams")
public class BaseApplication extends Application {

    public static final String TAG = BaseApplication.class.getSimpleName();

    static Context _context;
    static Resources _resource;
    private static String PREF_NAME = "creativelocker.pref";
    private static String LAST_REFRESH_TIME = "last_refresh_time.pref";
    private static String lastToast = "";
    private static long lastToastTime;

    private static boolean sIsAtLeastGB;

    static {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.GINGERBREAD) {
            sIsAtLeastGB = true;
        }
    }

    public static synchronized BaseApplication context() {
        return (BaseApplication) _context;
    }

    public static Resources resources() {
        return _resource;
    }

    /*
     * 放入已读文章列表中
     *
     * @param id
     */
    public static void putReadedPostList(String prefFileName, String key,
                                         String value) {
        SharedPreferences preferences = getPreferences(prefFileName);
        int size = preferences.getAll().size();
        Editor editor = preferences.edit();
        if (size >= 100) {
            editor.clear();
        }
        editor.putString(key, value);
        apply(editor);
    }

    /*
     * 读取是否是已读的文章列表
     *
     * @param id
     * @return
     */
    public static boolean isOnReadedPostList(String prefFileName, String key) {
        return getPreferences(prefFileName).contains(key);
    }

    /***
     * 记录列表上次刷新时间
     * @author 火蚁
     * 2015-2-9 下午2:21:37
     *
     * @return void
     * @param key
     * @param value
     */
    public static void putToLastRefreshTime(String key, String value) {
        SharedPreferences preferences = getPreferences(LAST_REFRESH_TIME);
        Editor editor = preferences.edit();
        editor.putString(key, value);
        apply(editor);
    }

    /***
     * 获取列表的上次刷新时间
     * @author 火蚁
     * 2015-2-9 下午2:22:04
     *
     * @return String
     * @param key
     * @return
     */
    public static String getLastRefreshTime(String key) {
        return getPreferences(LAST_REFRESH_TIME).getString(key, StringUtils.getCurTimeStr());
    }

    @TargetApi(Build.VERSION_CODES.GINGERBREAD)
    public static void apply(SharedPreferences.Editor editor) {
        if (sIsAtLeastGB) {
            editor.apply();
        } else {
            editor.commit();
        }
    }

    public static void set(String key, boolean value) {
        Editor editor = getPreferences().edit();
        editor.putBoolean(key, value);
        apply(editor);
    }

    public static void set(String key, String value) {
        Editor editor = getPreferences().edit();
        editor.putString(key, value);
        apply(editor);
    }

    public static boolean get(String key, boolean defValue) {
        return getPreferences().getBoolean(key, defValue);
    }

    public static String get(String key, String defValue) {
        return getPreferences().getString(key, defValue);
    }

    public static int get(String key, int defValue) {
        return getPreferences().getInt(key, defValue);
    }

    public static long get(String key, long defValue) {
        return getPreferences().getLong(key, defValue);
    }

    public static float get(String key, float defValue) {
        return getPreferences().getFloat(key, defValue);
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public static SharedPreferences getPreferences() {
        SharedPreferences pre = context().getSharedPreferences(PREF_NAME,
                Context.MODE_MULTI_PROCESS);
        return pre;
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public static SharedPreferences getPreferences(String prefName) {
        return context().getSharedPreferences(prefName,
                Context.MODE_MULTI_PROCESS);
    }

    public static int[] getDisplaySize() {
        return new int[]{getPreferences().getInt("screen_width", 480),
                getPreferences().getInt("screen_height", 854)};
    }

    public static void saveDisplaySize(Activity activity) {
        DisplayMetrics displaymetrics = new DisplayMetrics();
        activity.getWindowManager().getDefaultDisplay()
                .getMetrics(displaymetrics);
        SharedPreferences.Editor editor = getPreferences().edit();
        editor.putInt("screen_width", displaymetrics.widthPixels);
        editor.putInt("screen_height", displaymetrics.heightPixels);
        editor.putFloat("density", displaymetrics.density);
        editor.commit();
    }

    public static String string(int id) {
        return _resource.getString(id);
    }

    public static String string(int id, Object... args) {
        return _resource.getString(id, args);
    }

    public static void showToast(int message) {
        showToast(message, Toast.LENGTH_LONG);
    }


    public static void showToast(String message) {
        showToast(message, Toast.LENGTH_LONG);
    }


    public static void showToastShort(int message, Object... args) {
        showToast(message, Toast.LENGTH_SHORT, args);
    }

    public static void showToast(int message, int duration) {
        showToast(context().getString(message), duration);
    }

    public static void showToast(int message, int duration,
                                  Object... args) {
        showToast(context().getString(message, args), duration);
    }

    public static void showToast(final String message, final int duration) {

        CustomLog.w(TAG,"showToast(message="+message+") was called but no actual implementation was existing before I added one. Are you sure you wanted to call this method?.");

        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                CustomLog.d(TAG,"Displaying toast "+message+"|");
                Toast toast = Toast.makeText(context(), message, duration);
                toast.show();
            }
        });

    }

    @Override
    public void onCreate() {
        super.onCreate();
        MultiDex.install(this);
        _context = getApplicationContext();
        _resource = _context.getResources();
    }
}

