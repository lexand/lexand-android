package com.qiwo.xkidswatcher_russia.base;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.ActionBar.LayoutParams;
import androidx.appcompat.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.qiwo.xkidswatcher_russia.AnalyticsUtils.AnalyticsUtils;
import com.qiwo.xkidswatcher_russia.AppManager;
import com.qiwo.xkidswatcher_russia.AppStart;
import com.qiwo.xkidswatcher_russia.Constants;
import com.qiwo.xkidswatcher_russia.KidsWatConfig;
import com.qiwo.xkidswatcher_russia.R;
import com.qiwo.xkidswatcher_russia.Utils;
import com.qiwo.xkidswatcher_russia.event.BaseEvent;
import com.qiwo.xkidswatcher_russia.interf.BaseViewInterface;
import com.qiwo.xkidswatcher_russia.ui.SystemSettingActivity_v2;
import com.qiwo.xkidswatcher_russia.ui.UserLogin2Activity;
import com.qiwo.xkidswatcher_russia.ui.UserLoginActivity;
import com.qiwo.xkidswatcher_russia.util.TDevice;
import com.yandex.mapkit.MapKitFactory;

import net.intari.CustomLogger.CustomLog;

import org.greenrobot.eventbus.EventBus;
import org.kymjs.kjframe.utils.StringUtils;

import butterknife.ButterKnife;
import butterknife.Unbinder;


public abstract class BaseActivity extends AppCompatActivity implements
        View.OnClickListener, BaseViewInterface {

    public static final String INTENT_ACTION_EXIT_APP = "INTENT_ACTION_EXIT_APP";
    public static final String TAG = BaseActivity.class.getSimpleName();

    protected LayoutInflater mInflater;
    //protected ActionBar mActionBar;
    protected Handler mHandler;
    private TextView mTvActionTitle;
    private ProgressDialog progressDialog = null;

    Unbinder unbinder;

    @Override
    protected void onDestroy() {
        super.onDestroy();
        TDevice.hideSoftKeyboard(getCurrentFocus());
        unbinder.unbind();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //AnalyticsUtils.processActivityOnCreate(TAG,this);
        preInitView();
        AppManager.getAppManager().addActivity(this);
        onBeforeSetContentLayout();
        if (getLayoutId() != 0) {
            setContentView(getLayoutId());
        }
        mHandler = new Handler();
        //mActionBar = getSupportActionBar();
        mInflater = getLayoutInflater();
        //if (hasActionBar()) {
        //    initActionBar(mActionBar);
        //} else {
        //    //mActionBar.hide();
        //}

        // 通过注解绑定控件
        unbinder=ButterKnife.bind(this);

        init(savedInstanceState);
        initView();
        initData();
    }

    protected void onBeforeSetContentLayout() {
    }

    protected boolean hasActionBar() {
        return true;
    }

    protected int getLayoutId() {
        return 0;
    }

    protected View inflateView(int resId) {
        return mInflater.inflate(resId, null);
    }

    protected int getActionBarTitle() {
        return R.string.app_name;
    }

    public void setActionBarTitle(int resId) {
        if (resId != 0) {
            setActionBarTitle(getString(resId));
        }
    }

    public void setActionBarTitle(String title) {
        if (StringUtils.isEmpty(title)) {
            title = getString(R.string.app_name);
        }
        //if (hasActionBar() && mActionBar != null) {
        if (hasActionBar()) {
            if (mTvActionTitle != null) {
                mTvActionTitle.setText(title);
            }
            //mActionBar.setTitle(title);
        }
    }

    protected boolean hasBackButton() {
        return false;
    }

    protected int getActionBarCustomView() {
        return 0;
    }

    protected boolean haveSpinner() {
        return false;
    }

    protected void init(Bundle savedInstanceState) {
    }

    protected void initActionBar(ActionBar actionBar) {
        if (actionBar == null)
            return;
        if (hasBackButton()) {
            //mActionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
            int layoutRes = getActionBarCustomView();
            View view = inflateView(layoutRes == 0 ? R.layout.actionbar_custom_backtitle
                    : layoutRes);
            View back = view.findViewById(R.id.btn_back);
            if (back == null) {
                throw new IllegalArgumentException(
                        "can not find R.id.btn_back in customView");
            }
            back.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    TDevice.hideSoftKeyboard(getCurrentFocus());
                    onBackPressed();
                }
            });
            mTvActionTitle = (TextView) view
                    .findViewById(R.id.tv_actionbar_title);
            if (mTvActionTitle == null) {
                throw new IllegalArgumentException(
                        "can not find R.id.tv_actionbar_title in customView");
            }
            int titleRes = getActionBarTitle();
            if (titleRes != 0) {
                mTvActionTitle.setText(titleRes);
            }
            LayoutParams params = new LayoutParams(LayoutParams.MATCH_PARENT,
                    LayoutParams.MATCH_PARENT);
            actionBar.setCustomView(view, params);
            View spinner = actionBar.getCustomView().findViewById(R.id.spinner);
            if (haveSpinner()) {
                spinner.setVisibility(View.VISIBLE);
            } else {
                spinner.setVisibility(View.GONE);
            }
        } else {
            actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_TITLE);
            actionBar.setDisplayUseLogoEnabled(false);
            int titleRes = getActionBarTitle();
            if (titleRes != 0) {
                actionBar.setTitle(titleRes);
            }
        }
    }

    //protected Spinner getSpinner() {
    //    return (Spinner) mActionBar.getCustomView().findViewById(R.id.spinner);
    //}

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;

            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    public void showToast(int msgResid, int icon, int gravity) {
        showToast(getString(msgResid), icon, gravity);
    }

    public void showToast(String message, int icon, int gravity) {
        //
    }

    protected void showLongToast(String message) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }

    protected void showShortToast(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    public void showWaitDialog() {
        showWaitDialog(getResources().getString(R.string.loading));
    }

    public void showWaitDialog(String message) {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.setMessage(message);
        } else {
            progressDialog = ProgressDialog.show(this, null, message);
        }
    }

    public void hideWaitDialog() {
        //TODO:переписать это как то нормальным образом. При новом дизайне?
        // Потому что вот так - будет падать запросто. Потому что https://stackoverflow.com/questions/22924825/view-not-attached-to-window-manager-crash
        // А у нас еще и с таймерами игры!!
        // Поэтому такая вот пока заглушка
        try {
            if (progressDialog != null && progressDialog.isShowing()) {
                progressDialog.dismiss();
            }
        } catch (Exception ex) {
            CustomLog.i(TAG,"It's possible this is NOT an error!");
            CustomLog.logException(ex);
        }
    }

    protected void dismissDialog(String msg) {
        dismissDialog(msg, 800);
    }

    protected void dismissDialog(String msg, int delayTicks) {
        if (!StringUtils.isEmpty(msg)) {
            showWaitDialog(msg);
        }

        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                hideWaitDialog();
            }
        }, delayTicks);
    }

    protected void showConfirmDialog(String title, String message,
                                     String positiveText, String negativeText,
                                     DialogInterface.OnClickListener positiveListener,
                                     DialogInterface.OnClickListener negativeListener) {
        new AlertDialog.Builder(BaseActivity.this).setMessage(message)
                .setTitle(title)
                .setPositiveButton(positiveText, positiveListener)
                .setNegativeButton(negativeText, negativeListener).show();
    }

    protected void showConfirmDialog(String title, String message,
                                     String positiveText, String negativeText,
                                     DialogInterface.OnClickListener positiveListener) {

        DialogInterface.OnClickListener negativeListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        };
        showConfirmDialog(title, message, positiveText, negativeText,
                positiveListener, negativeListener);
    }

    protected void showConfirmInformation(String title, String message) {
        if (title == null)
            message = "\n" + message;
        new AlertDialog.Builder(this).setMessage(message).setTitle(title)
                .setPositiveButton("OK", null).show();
    }

    protected void showConfirmInformation(String title, String message, android.content.DialogInterface.OnClickListener listener) {
        if (title == null)
            message = "\n" + message;
        new AlertDialog.Builder(this).setMessage(message).setTitle(title)
                .setPositiveButton("OK", listener).show();
    }

    protected void showConfirmInformations(String title, String message) {
        if (title == null)
            message = "\n" + message;
        new AlertDialog.Builder(this).setMessage(message).setTitle(title)
                .setPositiveButton(getApplicationContext().getResources().getString(R.string.ok), null).show();
    }

    protected void showConfirmInformation(String message) {
        showConfirmInformation(null, message);
    }

    @Override
    public boolean onMenuOpened(int featureId, Menu menu) {

        // setOverflowIconVisible(featureId, menu);
        return super.onMenuOpened(featureId, menu);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        return super.onTouchEvent(event);
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        if (ev.getAction() == MotionEvent.ACTION_DOWN) {
            View v = getCurrentFocus();
            if (isShouldHideInput(v, ev)) {

                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                if (imm != null) {
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                }
            }
            return super.dispatchTouchEvent(ev);
        }
        // 必不可少，否则所有的组件都不会有TouchEvent了
        if (getWindow().superDispatchTouchEvent(ev)) {
            return true;
        }
        return onTouchEvent(ev);
    }

    public boolean isShouldHideInput(View v, MotionEvent event) {
        if (v != null && (v instanceof EditText)) {
            int[] leftTop = {0, 0};
            // 获取输入框当前的location位置
            v.getLocationInWindow(leftTop);
            int left = leftTop[0];
            int top = leftTop[1];
            int bottom = top + v.getHeight();
            int right = left + v.getWidth();
            if (event.getX() > left && event.getX() < right
                    && event.getY() > top && event.getY() < bottom) {
                // 点击的是输入框区域，保留点击EditText的事件
                return false;
            } else {
                return true;
            }
        }
        return false;
    }

    public static void doLogout(AppCompatActivity act) {
        CustomLog.d(TAG,"Logging out...sending event");
        BaseEvent event = new BaseEvent(BaseEvent.MSGTYPE_1___LOGOUT, "logout");
        EventBus.getDefault().post(event);
        CustomLog.d(TAG,"Logging out...clearing...");
        KidsWatConfig.cleanLoginInfo();

        // manually do that's needed - I'm not at all sure that message WILL reach main activity in time
        CustomLog.d(TAG,"Logging out...launching new intent...");

        Intent login_intent2 = new Intent(act, UserLogin2Activity.class);
        Intent login_intent = new Intent(act, UserLoginActivity.class);//use it!
        Intent intent = new Intent(act, AppStart.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TASK);
        act.startActivity(intent);

        CustomLog.d(TAG,"Logging out...finishing...");
        act.finish();
        CustomLog.d(TAG,"Logging out...done?");

    }
}
