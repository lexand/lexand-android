package com.qiwo.xkidswatcher_russia;

/**
 * Created by Dmitriy Kazimirov on 24/01/2019.
 */

/**
 * Copyright 2015 Google Inc. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import androidx.annotation.NonNull;
import androidx.core.app.JobIntentService;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import net.intari.CustomLogger.CustomLog;

import java.io.IOException;
/*
public class RegistrationIntentService extends JobIntentService {
    public static final String TAG = RegistrationIntentService.class.getSimpleName();
    public static final String SENDER_ID = "648991208617";
    public static final String SENT_TOKEN_TO_SERVER = "sentTokenToServer";
    public static final String REGISTRATION_COMPLETE = "registrationComplete";

    private static final String[] TOPICS = {"global"};



    public static void initiateGCMRegistration(Context context) {
        CustomLog.d(TAG,"Got  initial request for GCM, will not register just yet, context:"+context);

        new AsyncTask<Context, Void, Void>() {


            @Override
            protected Void doInBackground(final Context... contexts) {
                try {
                    CustomLog.d(TAG,"Will register using :"+contexts[0]);
                    doInternalRegister(contexts[0]);
                    //
                    return null;

                } catch (Exception e) {
                    CustomLog.logException(TAG,e);
                    return null;
                }
            }

            @Override
            protected void onPostExecute(final Void v) {
                try {

                } catch (Exception e) {
                    CustomLog.logException(TAG,e);

                }
            }

        }.execute(context);

    }
    public RegistrationIntentService() {
        CustomLog.d(TAG,"Starting GCM registration service");
        //super(TAG);
    }

    private static void doInternalRegister(final Context context) {
        try {
            CustomLog.d(TAG,"Getting initial token for GCM, context:"+context);
            InstanceID instanceID = InstanceID.getInstance(context);
            String token = instanceID.getToken(SENDER_ID,
                    GoogleCloudMessaging.INSTANCE_ID_SCOPE, null);
            CustomLog.d(TAG,"Got token "+token+"|");

            sendRegistrationToServer(context,token);

        } catch (Exception e) {
            CustomLog.logException(TAG,e);
        }
    }
    @Override
    protected void onHandleWork(@NonNull Intent intent) {
        CustomLog.d(TAG,"onHandleWork:"+Utils.toUri(intent));
        //doHandleIntentInternal(intent);
    }




    protected void doHandleIntentInternal(Intent intent) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);

        CustomLog.d(TAG,"intent:"+Utils.toUri(intent));
        try {
            // [START register_for_gcm]
            // Initially this call goes out to the network to retrieve the token, subsequent calls
            // are local.
            // R.string.gcm_defaultSenderId (the Sender ID) is typically derived from google-services.json.
            // See https://developers.google.com/cloud-messaging/android/start for details on this file.
            // [START get_token]
            InstanceID instanceID = InstanceID.getInstance(this);
            String token = instanceID.getToken(SENDER_ID,
                    GoogleCloudMessaging.INSTANCE_ID_SCOPE, null);
            // [END get_token]
            CustomLog.d(TAG, "GCM Registration Token: " + token);

            // TODO: Implement this method to send any registration to your app's servers.
            sendRegistrationToServer(this,token);

            // Subscribe to topic channels
            subscribeTopics(token);

            // You should store a boolean that indicates whether the generated token has been
            // sent to your server. If the boolean is false, send the token to your server,
            // otherwise your server should have already received the token.
            //sharedPreferences.edit().putBoolean(QuickstartPreferences.SENT_TOKEN_TO_SERVER, true).apply();
            // [END register_for_gcm]
        } catch (Exception e) {
            CustomLog.e(TAG, "Failed to complete token refresh."+ e.toString());
            CustomLog.logException(TAG,e);
            // If an exception happens while fetching the new token or updating our registration data
            // on a third-party server, this ensures that we'll attempt the update at a later time.
            sharedPreferences.edit().putBoolean(RegistrationIntentService.SENT_TOKEN_TO_SERVER, false).apply();
        }
        // Notify UI that registration has completed, so the progress indicator can be hidden.
        Intent registrationComplete = new Intent(RegistrationIntentService.REGISTRATION_COMPLETE);
        LocalBroadcastManager.getInstance(this).sendBroadcast(registrationComplete);
    }

    private static void sendRegistrationToServer(Context context,String token) {
        // Add custom implementation, as needed.

        CustomLog.d(TAG, "Device registered: regId = " + token);
        AppContext.getInstance().setGCMRegID(token);
        KidsWatConfig.setGcmAppid(token);

        ServerUtilities.register(context, token);
    }

    private void subscribeTopics(String token) throws IOException {
        GcmPubSub pubSub = GcmPubSub.getInstance(this);
        for (String topic : TOPICS) {
            pubSub.subscribe(token, "/topics/" + topic, null);
        }
    }
    // [END subscribe_topics]


} */