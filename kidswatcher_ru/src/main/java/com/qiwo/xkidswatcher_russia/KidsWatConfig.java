package com.qiwo.xkidswatcher_russia;

import android.content.SharedPreferences;
import android.os.Environment;

import com.qiwo.xkidswatcher_russia.bean.beanFor___login;
import net.intari.CustomLogger.CustomLog;

import java.io.File;

import static com.qiwo.xkidswatcher_russia.AppConfig.KEY_FAMILY_ID;
import static com.qiwo.xkidswatcher_russia.AppConfig.KEY_FRITST_START;
import static com.qiwo.xkidswatcher_russia.AppConfig.KEY_GCM_APP_ID;

/**
 * 应用程序配置类：用于保存用户相关信息及设置
 */
public class KidsWatConfig {

    public static final String TAG = KidsWatConfig.class.getSimpleName();

    private static final String BASE_DEV_URL = "http://dev.misafes1.qiwocloud1.com/";
    private static final String BASE_TEST_URL = "http://test.misafes1.qiwocloud1.com/";
    private static final String BASE_COMMON_URL = "http://misafes2.qiwocloud2.com/";

    private static final String FCM_MIGRATION_STARTED="fcm_migration_started_v2";

    private static final String API_URL = BASE_COMMON_URL;

    private KidsWatConfig() {
    }

    private static SharedPreferences getSharedPref() {
        return AppContext.getSharedPref();
    }

    private static SharedPreferences.Editor getSharedPrefEditor() {
        return AppContext.getSharedPrefEditor();
    }

    public static boolean getFCMMigrationStarted() {
        return getSharedPref().getBoolean(FCM_MIGRATION_STARTED, false);
    }


    public static void setFCMMigrationStarted(boolean started) {
        getSharedPrefEditor().putBoolean(FCM_MIGRATION_STARTED, started).commit();
    }


    // --------以下为公共方法设置获取值用--------
    public static boolean getSilenceMode() {
        return getSharedPref().getBoolean("silenceMode", false);
    }

    public static void setSilenceMode(boolean silenceMode) {
        getSharedPrefEditor().putBoolean("silenceMode", silenceMode).commit();
    }

    public static boolean getAlwaysOnMode() {
        return getSharedPref().getBoolean("alwaysOnMode", true);
    }

    public static void setAlwaysOnMode(boolean alwaysOnMode) {
        getSharedPrefEditor().putBoolean("alwaysOnMode", alwaysOnMode).commit();
    }

    public static int getSilenceTimeS() {
        return getSharedPref().getInt("silenceTimeS", 22);
    }

    public static void setSilenceTimeS(int silenceTimeS) {
        getSharedPrefEditor().putInt("silenceTimeS", silenceTimeS).commit();
    }

    public static int getSilenceTimeE() {
        return getSharedPref().getInt("silenceTimeE", 7);
    }

    public static void setSilenceTimeE(int silenceTimeE) {
        getSharedPrefEditor().putInt("silenceTimeE", silenceTimeE).commit();
    }

    // --------------------

    public static boolean isFristStart() {
        return getSharedPref().getBoolean(KEY_FRITST_START, true);
    }

    public static void setFristStart(boolean frist) {
        getSharedPrefEditor().putBoolean(KEY_FRITST_START, frist).commit();
    }

    public static String getDefaultFamilyId() {
        return getSharedPref().getString(KEY_FAMILY_ID, "");
    }

    public static void setDefaultFamilyId(String family_id) {
        getSharedPrefEditor().putString(KEY_FAMILY_ID, family_id).commit();
    }

    public static String getGcmAppid() {
        return getSharedPref().getString(KEY_GCM_APP_ID, "");
    }

    public static void setGcmAppid(String appid) {

        if (appid==null) {
            CustomLog.e(TAG,"Not storing GCM AppId, it's null. migration?");
            return;
        }
        CustomLog.d(TAG,"Storing GCM AppId:"+appid+"|");
        getSharedPrefEditor().putString(KEY_GCM_APP_ID, appid).commit();
    }

    // -------------以下为登陆用户相关信息.---------------
    public static String getApiUrl() {
        return getSharedPref().getString("service.api.url", API_URL);
    }

    public static void saveApiUrl(final String apiUrl) {
        getSharedPrefEditor().putString("service.api.url", apiUrl).commit();
    }

    public static String getUserDeviceId() {
        return getSharedPref().getString("user.device_id", "");
    }

    public static String getUserUid() {
        return getSharedPref().getString("user.uid", "");
    }

    public static String getUserToken() {
        return getSharedPref().getString("user.token", "");
    }

    public static String getUserPhone() {
        return getSharedPref().getString("user.phone", "");
    }

    public static void setUserPhone(final String phone) {
        getSharedPrefEditor().putString("user.phone", phone).commit();
    }

    public static String getUserPassword() {
        return getSharedPref().getString("user.password", "");
    }

    public static void setUserPassword(final String password) {
        getSharedPrefEditor().putString("user.password", password).commit();
    }

    public static String getUseCountryCode() {
        return getSharedPref().getString("user.country_code", "");
    }

    public static void saveUserInfo(final beanFor___login.CUser user,
                                    final String password) {

        // this.loginUid = user.uid;
        // this.isLogin = true;
        SharedPreferences.Editor spEditor = getSharedPrefEditor();

        // --------------------
        spEditor.putString("user.uid", user.uid);
        spEditor.putString("user.name", user.name);
        spEditor.putString("user.password", password);
        spEditor.putString("user.country_code", user.country_code);
        spEditor.putString("user.phone", user.phone);
        spEditor.putString("user.push_token", user.push_token);
        spEditor.putString("user.token", user.global_sesison.token);
        spEditor.putString("user.expiration", user.global_sesison.expiration);
        spEditor.commit();
    }

    public static void saveDeviceId(final String device_id) {
        getSharedPrefEditor().putString("user.device_id", device_id).commit();
    }

    public static void setUserCountryCode(final String country_code) {
        getSharedPrefEditor().putString("user.country_code", country_code)
                .commit();
    }

    public static void setUserName(final String name) {
        getSharedPrefEditor().putString("user.name", name).commit();
    }

    /**
     * 清除登录信息
     */
    public static void cleanLoginInfo() {
        // this.loginUid = "";
        // this.isLogin = false;
        SharedPreferences.Editor spEditor = getSharedPrefEditor();

        // --------------------
        spEditor.remove("user.uid");
        spEditor.remove("user.country_code");
        spEditor.remove("user.name");
        spEditor.remove("user.password");
        // spEditor.remove("user.country_code");
        spEditor.remove("user.phone");
        spEditor.remove("user.push_token");
        spEditor.remove("user.token");
        spEditor.remove("user.expiration");
        spEditor.commit();
    }

    //Why NOT to use app's cache folders?
    public static String getTempFilePath() {

        //String path = Environment.getExternalStorageDirectory()
        //String path = AppContext.getInstance().getExternalCacheDir()
        String path = AppContext.getInstance().getCacheDir()

                + "/KidsWatchRu/";///Pictures/abcp/";
        File file = new File(path);
        // 如果文件夹不存在则创建 - Create if the folder does not exist
        if (!file.exists() && !file.isDirectory()) {
            file.mkdirs();
        }
        return path;

    }

    public static String getUserImagepath() {
        return getSharedPref().getString("user.image_path", "");
    }

    public static void setUserImagepath(String image_path) {
        getSharedPrefEditor().putString("user.image_path", image_path).commit();
    }

    public static long getLastIgnoreUpdateTime() {
        return getSharedPref().getLong("user.LastIgnoreUpdateTime", 0l);
    }

    public static void setLastIgnoreUpdateTime(long time) {
        getSharedPrefEditor().putLong("user.LastIgnoreUpdateTime", time).commit();
    }

    public static long getLastShowRemianingTime() {
        return getSharedPref().getLong("user.LastShowRemianingTime", 0l);
    }

    public static void setLastShowRemianingTime(long time) {
        getSharedPrefEditor().putLong("user.LastShowRemianingTime", time).commit();
    }
}
