package com.qiwo.xkidswatcher_russia;

/**
 * Created by Dmitriy Kazimirov on 23/01/2019.
 */

public class Constants {
    /**
     * Configuration for NSLogger (development only)
     */
    /**
     * Лог - сервер (внутрисетевое имя)
     */
    public static final String LOG_HOST= "inari6.internal";//debug development host
    /**
     * Лог - сервер (внешнее имя)
     * НЕ должен используется в prod-сборках (для Play Store)
     * Даже наличие данного сервера - НЕ гарантируется
     */
    public static final String LOG_HOST_XTRA_TRACE="lexandlogs.viorsan.com";

    /**
     * Только для специальных целей ставить true!!!!
     * Включает принудительное использование внешнего лог - сервера!!!
     */
    public static final boolean FORCE_XTRA_LOG=false;

    public static final int LOG_PORT=50001;


    public static final boolean LOG_NON_FATALS=true;//don't bother Crashlytics (and DevOps team) with non-fatals
    public static final boolean USE_LOADING_PROGRESS_NOTIFICATIONS_DEBUG=true;//this is debug-only functionality for now
    public static final boolean USE_LOADING_PROGRESS_NOTIFICATIONS_NON_DEBUG=true;//this is debug-only functionality for now

    public static final long PLAYSERVCIES_ALERT_DELAY_MS=7000;

    public static final int BYTES_PER_KB=1024;
    public static final int BYTES_PER_MB=1024*BYTES_PER_KB;

    //public static final String YANDEX_API_KEY="2fb46cb0-a6a1-4dfb-bcef-c27ca6efcdc5";//Dkzm-for-lexand  "4bff9e75-a0d2-4c98-a39b-5b92486a5849";//  "5e144642-603c-4006-a495-05cffa116a37";
    public static final String YANDEX_API_KEY="4bff9e75-a0d2-4c98-a39b-5b92486a5849";//Lexand

    public static final String YANDEX_MAP_KIT_KEY="91050602-a0e4-4155-984e-907f7224f2ab";//Lexand

    public static final int MS_PER_SECOND=1000;

    public static final int NETWORK_TIMEOUT_IN_SECONDS=137;

    public static final int OLD_HTTP_TIMEOUT_PUT=NETWORK_TIMEOUT_IN_SECONDS * MS_PER_SECOND;    //was 20

    public static final int OLD_HTTP_TIMEOUT_GET=NETWORK_TIMEOUT_IN_SECONDS * MS_PER_SECOND; // was 10

    public static final int OLD_HTTP_TIMEOUT_DEFAULT=NETWORK_TIMEOUT_IN_SECONDS * MS_PER_SECOND;
    public static final int OLD_HTTP_SO_TIMEOUT_DEFAULT=OLD_HTTP_TIMEOUT_DEFAULT;


    //for getNewHttpClient4
    public static final int OLD4_HTTP_TIMEOUT_DEFAULT=NETWORK_TIMEOUT_IN_SECONDS * MS_PER_SECOND;//was 4
    public static final int OLD4_HTTP_SO_TIMEOUT_DEFAULT=OLD4_HTTP_TIMEOUT_DEFAULT;

    public static final String PHOTO_PROVIDER="com.qiwo.xkidswatcher_russia.photoprovider";//keep in sync with manifest

    public static final String CAMERA_PREFIX="lks_camera_";
    public static final String CROP_PREFIX="bb_";//"lks_processing_";



    public static final String CAMERA_SUFFIX=".jpg";
    public static final String DEFAULT_IMAGE_FORMAT="jpg";

    public static final String SOURCE_URI="lexand_original_uri";
    public static final String IMAGE_NAME_FOR_AVATAR="photo.jpg";

    public static final Boolean YANDEX_METRICA_GLOBAL_ENABLE=true;
    public static final Boolean APPSEE_GLOBAL_ENABLE=true;
    public static final String APPSEE_API_KEY="60b12d823be94628bf48248416d3d770";
    public static final Boolean APPSEE_ENABLE_DEBUG=false;
    public static final String APPSEE_APP_NAME="kidswatch-test";//Replace with Appsee's app name from dashboard
    public static final Boolean AMPLITUDE_GLOBAL_ENABLE=true;
    public static final String AMPLITUDE__API_KEY="6c3ea0ab9a31ea541d0c7380c80ec016";

    /*
    public static final Boolean UXCAM_GLOBAL_ENABLE=false;
    public static final String UXCAM_API_KEY="bgqveb3ybr2w98d";
    */

    public static final String EVENT_TYPE="event_type";
    public static final String EVENT_PAIRING_STARTED ="event_pairing_started";
    public static final String EVENT_PAIRING_UPDATING_SERVER_WITH_BABY ="event_pairing_updating_server_with_babyinfo";
    public static final String EVENT_PAIRING_SUCCESS ="event_pairing_success";
    public static final String EVENT_PAIRING_FAILED ="event_pairing_failed";
    public static final String EVENT_PAIRING_FAILED_NO_NETWORK ="event_pairing_failed_no_network";

    public static final String EVENT_PAIRING_FAILED_SERVER ="event_pairing_failed_due_to_server_error";

    public static final String EVENT_PAIRING_FAILED_NETWORK_ERROR ="event_pairing_network_erro";

    public static final String EVENT_PAIRING_POSSIBLE_FAIL ="event_pairing_possible_fail";
    public static final String EVENT_EXCEPTION="event_exception";

    public static final String EVENT_LOCATING_MODE_CHANGE_SUCCESS ="locat_mode_change_success";
    public static final String EVENT_LOCATING_MODE_CHANGE_ERROR ="loc_mode_change_error";
    public static final String EVENT_LOCATING_MODE_CHANGE_ERROR_DEVICE_SLEEPS =" locating_mode_change_error_device_sleeps";

    public static final boolean LOG_UPD_MODE_CONFIG_EVENTS=true;
    public static final String EVENT_UPD_MODE_CONFIG_START ="loc_upd_mode_config_start";
    public static final String EVENT_UPD_MODE_CONFIG_SUCCESS ="loc_upd_mode_config_success";
    public static final String EVENT_UPD_MODE_CONFIG_ERROR ="loc_upd_mode_config_error";
    public static final String EVENT_UPD_MODE_CONFIG_ERROR_DEVICE_SLEEPS ="loc_upd_mode_config_error_device_sleeps";
    public static final String EVENT_UPD_MODE_CONFIG_EXCEPTION ="loc_upd_mode_config_exception";
    public static final String EVENT_UPD_MODE_CONFIG_SERVER_FAILED ="loc_upd_mode_config_server_error";


    public static final boolean LOG_INTERNAL_GET_LOCATION=true;//TODO:make it remote-configurable option!!!, and make it configurable per-user
    public static final String EVENT_INTERNAL_GET_LOCATION_START ="loc_internal_get_start";
    public static final String EVENT_INTERNAL_GET_LOCATION_SUCCESS ="loc_internal_get_start";
    public static final String EVENT_INTERNAL_GET_LOCATION_ERROR ="loc_internal_get_error";
    public static final String EVENT_INTERNAL_GET_LOCATION_ERROR_DEVICE_SLEEPS ="loc_internal_get_error_device_sleeps";
    public static final String EVENT_INTERNAL_GET_LOCATION_EXCEPTION ="loc_internal_get_exception";
    public static final String EVENT_INTERNAL_GET_LOCATION_SERVER_ERROR ="loc_internal_get_server_errror";

    public static final boolean LOG_GET_LOCATION_EX=true;//TODO:make it remote-configurable option!!!, and make it configurable per-user
    public static final String EVENT_GET_LOCATION_EX_START ="loc_ex_get_start";
    public static final String EVENT_GET_LOCATION_EX_SUCCESS ="loc_ex_get_start";
    public static final String EVENT_GET_LOCATION_EX_ERROR ="loc_ex_get_error";
    public static final String EVENT_GET_LOCATION_EX_ERROR_DEVICE_SLEEPS ="loc_ex_get_error_device_sleeps";
    public static final String EVENT_GET_LOCATION_EX_EXCEPTION ="loc_ex_get_exception";
    public static final String EVENT_GET_LOCATION_EX_SERVER_ERROR ="loc_ex_get_server_errror";

    public static final boolean LOG_GET_LOCATION=true;//TODO:make it remote-configurable option!!!, and make it configurable per-user
    public static final String EVENT_GET_LOCATION_START ="loc_get_start";
    public static final String EVENT_GET_LOCATION_SUCCESS ="loc_get_start";
    public static final String EVENT_GET_LOCATION_ERROR ="loc_get_error";
    public static final String EVENT_GET_LOCATION_ERROR_DEVICE_SLEEPS ="loc_get_error_device_sleeps";
    public static final String EVENT_GET_LOCATION_EXCEPTION ="loc_get_exception";
    public static final String EVENT_GET_LOCATION_SERVER_ERROR ="loc_get_server_errror";



    public static final boolean LOG_GET_WATCH_DATA=true;//TODO:make it remote-configurable option!!!, and make it configurable per-user
    public static final String EVENT_GET_WATCH_DATA_START ="watch_data_get_start";
    public static final String EVENT_GET_WATCH_DATA_SUCCESS ="watch_data_get_start";
    public static final String EVENT_GET_WATCH_DATA_ERROR ="watch_data_get_error";
    public static final String EVENT_GET_WATCH_DATA_ERROR_DEVICE_SLEEPS ="watch_data_get_error_device_sleeps";
    public static final String EVENT_GET_WATCH_DATA_EXCEPTION ="watch_data_get_exception";
    public static final String EVENT_GET_WATCH_DATA_SERVER_ERROR ="watch_data_get_server_errror";
    public static final String EVENT_GET_WATCH_DATA_NO_RESULT ="watch_data_get_no_result";


    public static final boolean LOG_GOT_GCM_LOCATION=true;//TODO:make it remote-configurable option!!!, and make it configurable per-user

    public static final String EVENT_STRANGE_INTERNAL_ISSUE ="strange_thing_gui";//If THIS event is logged - this mean that I seriously considering crashing program at this point


    public static final String EVENT_APPSEE_SESSION_START="appsee_session_start";
    public static final String EVENT_APPSEE_SESSION_END ="appsee_session_end";
    public static final String APPSEE_SESSION_ID ="appsee_session_id";
    public static final String APPSEE_VIDEORECORDED ="appsee_videorecorded";
    public static final String APPSEE_SESSION_URL ="appsee_session_url";


    public static final String UID ="user_id";
    public static final String USERPHONE ="user_phone";
    public static final String DEVICESIM ="device_sim";
    public static final String FAMILIYID="family_id";
    public static final String NICKNAME="nickname";

    public static final String UTCTODAY ="utcToday";
    public static final String MESSAGE="message";
    public static final String ERRORCODE="errCode";
    public static final String ERREXCEPTION="exception";
    public static final String URL="url";

    public static final String JSON="json";
    public static final String LOCATION="location";
    public static final String TIME="time";
    public static final String LTIME="ltime";
    public static final String LTIME_FORMATTED="ltime_formatted";

    public static final String LOCATION_TYPE="locationType";
    public static final String CHARGE="CHARGE";
    public static final String ADDRESS="address";
    public static final String GEOPOINT="geopoint";

    public static final String DEVICE_ID="device_id";


    public static final int PAIRING_MAGIC1=123;

    public static final int NETWORK_MAX_IDLE_CONNECTIONS=1;
    public static final int KEEP_ALIVE_TIME_SECONDS=7;
    public static final int NETWORK_NUM_RETRIES=7;

    public static final float TIMEOUT_MULTIPLIER=1.618f;

    public static final int PERIODIC_COORD_REQUESTS_MINUTES=30;
}

