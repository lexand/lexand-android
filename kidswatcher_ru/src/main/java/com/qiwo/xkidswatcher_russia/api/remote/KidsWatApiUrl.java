package com.qiwo.xkidswatcher_russia.api.remote;

import android.content.Context;

import com.qiwo.xkidswatcher_russia.AppManager;
import com.qiwo.xkidswatcher_russia.AppStart;
import com.qiwo.xkidswatcher_russia.KidsWatConfig;
import com.qiwo.xkidswatcher_russia.event.BaseEvent;
import com.qiwo.xkidswatcher_russia.util.Encoder;
import com.qiwo.xkidswatcher_russia.util.StringUtils;
import net.intari.CustomLogger.CustomLog;

import org.greenrobot.eventbus.EventBus;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

public class KidsWatApiUrl {

    public static final String TAG = KidsWatApiUrl.class.getSimpleName();

    public static final String api_secret = "abcdefg";
    private static final String CUSTOMER_NO_RUSSIA = "1";
    public static int SESSIONTIMEOUT = 2015004;
    public static String UPDATE_URL;

    public static String getApiUrl() {
        return KidsWatConfig.getApiUrl();
    }


    public static void setTokenTimeOut(Context ctx) {

        KidsWatConfig.setFristStart(false);
        EventBus.getDefault().post(new BaseEvent(BaseEvent.MSGTYPE_1___RELOGIN, "relogin"));

        AppManager.getAppManager().finishActivity();
        AppManager.AppRestart(ctx);
    }


    public static String getUrlFor___login(String country_code,
                                           String username, String mmpassword, String push_token) {
        final String action = "login";
        Map<String, String> inputParams = new HashMap<String, String>();
        addExtraParaToMap(action, inputParams);

        inputParams.put("country_code", country_code);

        inputParams.put("username", username);
        inputParams.put("password", mmpassword);
        inputParams.put("plat", "1");
        inputParams.put("push_token", push_token);

        // --------------------
        return makeURL(getApiUrl(), inputParams);
    }

    public static String getUrlFor___check_identifier(String country_code,
                                                      String identifier) {
        final String action = "check_identifier";
        Map<String, String> inputParams = new HashMap<String, String>();
        addExtraParaToMap(action, inputParams);

        inputParams.put("country_code", country_code);
        inputParams.put("identifier", identifier);
        // --------------------
        return makeURL(getApiUrl(), inputParams);
    }

    public static String getUrlFor___reset_pwd(String country_code,
                                               String username, String new_password) {
        final String action = "reset_pwd";
        Map<String, String> inputParams = new HashMap<String, String>();
        addExtraParaToMap(action, inputParams);

        inputParams.put("country_code", country_code);
        inputParams.put("username", username);
        // inputParams.put("smscode", smscode);
        inputParams.put("new_password", new_password);
        // --------------------
        return makeURL(getApiUrl(), inputParams);
    }

    public static String getUrlFor___requre_sms_code(String country_code,
                                                     String phone) {
        final String action = "requre_sms_code";
        Map<String, String> inputParams = new HashMap<String, String>();
        addExtraParaToMap(action, inputParams);

        inputParams.put("country_code", country_code);
        inputParams.put("phone", phone);
        // --------------------
        return makeURL(getApiUrl(), inputParams);
    }

    public static String getUrlFor___validate_code(String country_code,
                                                   String phone, String code) {
        final String action = "validate_code";
        Map<String, String> inputParams = new HashMap<String, String>();
        addExtraParaToMap(action, inputParams);

        inputParams.put("country_code", country_code);
        inputParams.put("phone", phone);
        inputParams.put("code", code);
        // --------------------
        return makeURL(getApiUrl(), inputParams);
    }

    public static String getUrlFor___register(String country_code,
                                              String mobile, String password, String sms_code) {
        final String action = "register";
        Map<String, String> inputParams = new HashMap<String, String>();
        addExtraParaToMap(action, inputParams);

        inputParams.put("country_code", country_code);
        inputParams.put("mobile", mobile);
        inputParams.put("password", password);
        inputParams.put("sms_code", sms_code);
        // --------------------
        return makeURL(getApiUrl(), inputParams);
    }

    public static String getUrlFor___get_watch_info(String uid,
                                                    String access_token, String device_id) {
        final String action = "get_watch_info";
        Map<String, String> inputParams = new HashMap<String, String>();
        addExtraParaToMap(action, inputParams);
        addTokenToMap(inputParams, uid, access_token);

        inputParams.put("device_id", device_id);
        // --------------------
        return makeURL(getApiUrl(), inputParams);
    }

    public static String getUrlFor___get_family_list(String uid,
                                                     String access_token) {
        final String action = "get_family_list";
        Map<String, String> inputParams = new HashMap<String, String>();
        addExtraParaToMap(action, inputParams);
        addTokenToMap(inputParams, uid, access_token);
        // --------------------
        return makeURL(getApiUrl(), inputParams);
    }

    public static String getUrlFor___get_family_list_v2(String uid,
                                                        String access_token) {
        final String action = "get_family_list_v2";
        Map<String, String> inputParams = new HashMap<String, String>();
        addExtraParaToMap(action, inputParams);
        addTokenToMap(inputParams, uid, access_token);
        // --------------------
        return makeURL(getApiUrl(), inputParams);
    }

    public static String getUrlFor___get_family_info(String uid,
                                                     String access_token, String family_id) {
        final String action = "get_family_info";
        Map<String, String> inputParams = new HashMap<String, String>();
        addExtraParaToMap(action, inputParams);
        addTokenToMap(inputParams, uid, access_token);

        inputParams.put("family_id", family_id);
        // --------------------
        return makeURL(getApiUrl(), inputParams);
    }

    public static String getUrlFor___check_qrcode(String uid,
                                                  String access_token, String qrcode) {
        final String action = "check_qrcode";
        Map<String, String> inputParams = new HashMap<String, String>();
        addExtraParaToMap(action, inputParams);
        addTokenToMap(inputParams, uid, access_token);

        inputParams.put("qrcode", qrcode);
        // --------------------
        return makeURL(getApiUrl(), inputParams);
    }

    public static String getUrlFor___add_watch(String uid, String access_token,
                                               String qrcode, String country_code, String mobile, String relation,
                                               String version, String device_country_code, String device_sim_no) {
        final String action = "add_watch";
        Map<String, String> inputParams = new HashMap<String, String>();
        addExtraParaToMap(action, inputParams);
        addTokenToMap(inputParams, uid, access_token);

        inputParams.put("country_code", country_code);
        inputParams.put("qrcode", qrcode);
        inputParams.put("mobile", mobile);
        inputParams.put("relation", relation);
        inputParams.put("version", version);

        inputParams.put("device_country_code", device_country_code);
        inputParams.put("device_sim_no", device_sim_no);
        // --------------------
        return makeURL(getApiUrl(), inputParams);
    }

    public static String getUrlFor___add_baby_info(String uid,
                                                   String access_token, String family_id, String nickname,
                                                   long birthday, int sex, float height, float weight, int grade,
                                                   String img) {
        final String action = "add_baby_info";
        Map<String, String> inputParams = new HashMap<String, String>();
        addExtraParaToMap(action, inputParams);
        addTokenToMap(inputParams, uid, access_token);

        inputParams.put("family_id", family_id);
        inputParams.put("nickname", nickname);
        inputParams.put("birthday", String.valueOf(birthday));
        inputParams.put("sex", String.valueOf(sex));
        inputParams.put("height", String.valueOf(height));
        inputParams.put("weight", String.valueOf(weight));
        inputParams.put("grade", String.valueOf(grade));
        inputParams.put("img", img);

        // --------------------
        return makeURL(getApiUrl(), inputParams);
    }

    public static String getUrlFor___update_baby_info(String uid,
                                                      String access_token, String family_id, String nickname,
                                                      long birthday, int sex, float height, float weight, int grade) {
        final String action = "update_baby_info";
        Map<String, String> inputParams = new HashMap<String, String>();
        addExtraParaToMap(action, inputParams);
        addTokenToMap(inputParams, uid, access_token);

        inputParams.put("family_id", family_id);
        inputParams.put("nickname", nickname);
        inputParams.put("birthday", String.valueOf(birthday));
        inputParams.put("sex", String.valueOf(sex));
        inputParams.put("height", String.valueOf(height));
        inputParams.put("weight", String.valueOf(weight));
        inputParams.put("grade", String.valueOf(grade));
        // --------------------
        return makeURL(getApiUrl(), inputParams);
    }

    public static String getUrlFor___get_group_list(String uid,
                                                    String access_token, String family_id) {
        final String action = "get_group_list";
        Map<String, String> inputParams = new HashMap<String, String>();
        addExtraParaToMap(action, inputParams);
        addTokenToMap(inputParams, uid, access_token);

        inputParams.put("family_id", family_id);
        // --------------------
        return makeURL(getApiUrl(), inputParams);
    }

    public static String getUrlFor___get_group_msg_list(String uid,
                                                        String access_token) {
        final String action = "get_group_msg_list";
        Map<String, String> inputParams = new HashMap<String, String>();
        addExtraParaToMap(action, inputParams);
        addTokenToMap(inputParams, uid, access_token);
        // --------------------
        return makeURL(getApiUrl(), inputParams);
    }

    public static String getUrlFor___get_safe_area_config(String uid,
                                                          String access_token, String family_id) {
        final String action = "get_safe_area_config";
        Map<String, String> inputParams = new HashMap<String, String>();
        addExtraParaToMap(action, inputParams);
        addTokenToMap(inputParams, uid, access_token);
        // -------------------
        inputParams.put("family_id", family_id);
        // --------------------
        return makeURL(getApiUrl(), inputParams);
    }

    public static String getUrlFor___delete_safe_area_config(String uid,
                                                             String access_token, String family_id, double latitude,
                                                             double longitude) {
        final String action = "delete_safe_area_config";
        Map<String, String> inputParams = new HashMap<String, String>();
        addExtraParaToMap(action, inputParams);
        addTokenToMap(inputParams, uid, access_token);
        // -------------------
        inputParams.put("family_id", family_id);

        inputParams.put("latitude", String.valueOf(latitude));
        inputParams.put("longitude", String.valueOf(longitude));
        // --------------------
        return makeURL(getApiUrl(), inputParams);
    }

    public static String getUrlFor___add_safe_area_config(String uid,
                                                          String access_token, String family_id, double latitude,
                                                          double longitude, int radius, String name, String address,
                                                          String create_user) {
        final String action = "add_safe_area_config";
        Map<String, String> inputParams = new HashMap<String, String>();
        addExtraParaToMap(action, inputParams);
        addTokenToMap(inputParams, uid, access_token);
        // -------------------
        // uid int user id
        // access_token string The access token of login return
        // family_id string(required)
        // latitude string(required) latitude
        // longitude string(required) longitude
        // name string(required) 安全区的名字，比如‘学校’
        // address string(required) 地址
        // create_user string(required) 创建人
        // -----构造url地址及相关参数-----
        inputParams.put("family_id", family_id);

        inputParams.put("latitude", String.valueOf(latitude));
        inputParams.put("longitude", String.valueOf(longitude));
        inputParams.put("radius", String.valueOf(radius));

        inputParams.put("name", name);
        inputParams.put("address", address);
        inputParams.put("create_user", create_user);
        // --------------------
        return makeURL(getApiUrl(), inputParams);
    }

    public static String getUrlFor___update_safe_area_config(String uid,
                                                             String access_token, String family_id, double latitude,
                                                             double longitude, double old_latitude, double old_longitude,
                                                             int radius, String name, String address, String create_user) {
        final String action = "update_safe_area_config";
        Map<String, String> inputParams = new HashMap<String, String>();
        addExtraParaToMap(action, inputParams);
        addTokenToMap(inputParams, uid, access_token);
        // -------------------
        // uid int user id
        // access_token string The access token of login return
        // family_id string(required)
        // latitude string(required) latitude
        // longitude string(required) longitude
        // name string(required) 安全区的名字，比如‘学校’
        // address string(required) 地址
        // create_user string(required) 创建人
        // -----构造url地址及相关参数-----
        inputParams.put("family_id", family_id);

        inputParams.put("latitude", String.valueOf(latitude));
        inputParams.put("longitude", String.valueOf(longitude));

        inputParams.put("old_latitude", String.valueOf(old_latitude));
        inputParams.put("old_longitude", String.valueOf(old_longitude));

        inputParams.put("radius", String.valueOf(radius));
        inputParams.put("name", name);
        inputParams.put("address", address);
        inputParams.put("create_user", create_user);
        // --------------------
        return makeURL(getApiUrl(), inputParams);
    }

    public static String getUrlFor___get_location(String uid,
                                                  String access_token, String device_id, String family_id) {
        final String action = "get_location";
        Map<String, String> inputParams = new HashMap<String, String>();
        addExtraParaToMap(action, inputParams);
        addTokenToMap(inputParams, uid, access_token);
        // -------------------
        inputParams.put("device_id", device_id);
        inputParams.put("family_id", family_id);
        // --------------------
        return makeURL(getApiUrl(), inputParams);
    }

    public static String getUrlFor___get_location_ex(String uid,
                                                     String access_token, String device_id, String family_id) {
        final String action = "get_location_ex";
        Map<String, String> inputParams = new HashMap<String, String>();
        addExtraParaToMap(action, inputParams);
        addTokenToMap(inputParams, uid, access_token);
        // -------------------
        inputParams.put("device_id", device_id);
        inputParams.put("family_id", family_id);
        // --------------------
        return makeURL(getApiUrl(), inputParams);
    }

    public static String getUrlFor___get_watch_data(String uid,
                                                    String access_token, String family_id, long date) {
        final String action = "get_watch_data";
        Map<String, String> inputParams = new HashMap<String, String>();
        addExtraParaToMap(action, inputParams);
        addTokenToMap(inputParams, uid, access_token);
        // -------------------
        inputParams.put("family_id", family_id);
        inputParams.put("date", String.valueOf(date));
        // --------------------
        return makeURL(getApiUrl(), inputParams);
    }

    public static String getUrlFor___get_watch_data_latest(String uid,
                                                           String access_token, String family_id, long date) {
        final String action = "get_watch_data_lastest";
        Map<String, String> inputParams = new HashMap<String, String>();
        addExtraParaToMap(action, inputParams);
        addTokenToMap(inputParams, uid, access_token);
        inputParams.put("family_id", family_id);
        inputParams.put("date", String.valueOf(date));
        return makeURL(getApiUrl(), inputParams);
    }

    public static String getUrlFor___get_user_silent_time(String uid,
                                                          String access_token) {
        final String action = "get_user_silent_time";
        Map<String, String> inputParams = new HashMap<String, String>();
        addExtraParaToMap(action, inputParams);
        addTokenToMap(inputParams, uid, access_token);
        // -------------------
        return makeURL(getApiUrl(), inputParams);
    }

    public static String getUrlFor___exec_voice_record(String uid,
                                                       String access_token, String device_id) {
        final String action = "exec_voice_record";
        Map<String, String> inputParams = new HashMap<String, String>();
        addExtraParaToMap(action, inputParams);
        addTokenToMap(inputParams, uid, access_token);
        // -------------------
        inputParams.put("device_id", device_id);
        // --------------------
        return makeURL(getApiUrl(), inputParams);
    }

    public static String getUrlFor___attention_family(String uid,
                                                      String access_token, String qrcode, String mobile, String relation) {
        final String action = "attention_family";
        Map<String, String> inputParams = new HashMap<String, String>();
        addExtraParaToMap(action, inputParams);
        addTokenToMap(inputParams, uid, access_token);
        // -------------------
        inputParams.put("qrcode", qrcode);
        inputParams.put("mobile", mobile);
        inputParams.put("relation", relation);
        // --------------------
        return makeURL(getApiUrl(), inputParams);
    }

    public static String getUrlFor___get_device_runing_status(String uid,
                                                              String access_token, String device_id) {
        final String action = "get_device_runing_status";
        Map<String, String> inputParams = new HashMap<String, String>();
        addExtraParaToMap(action, inputParams);
        addTokenToMap(inputParams, uid, access_token);
        // -------------------
        inputParams.put("device_id", device_id);
        // --------------------
        return makeURL(getApiUrl(), inputParams);
    }

    public static String getUrlFor___validate_code(String phone, String code) {
        final String action = "validate_code";
        Map<String, String> inputParams = new HashMap<String, String>();
        addExtraParaToMap(action, inputParams);
        // -------------------
        inputParams.put("phone", phone);
        inputParams.put("code", code);
        // --------------------
        return makeURL(getApiUrl(), inputParams);
        // -------------------
    }

    public static String getUrlFor___rebind_watch(String uid,
                                                  String access_token, String family_id, String qrcode, String mobile) {
        final String action = "rebind_watch";
        Map<String, String> inputParams = new HashMap<String, String>();
        addExtraParaToMap(action, inputParams);
        addTokenToMap(inputParams, uid, access_token);
        // -------------------
        inputParams.put("family_id", family_id);
        inputParams.put("qrcode", qrcode);
        inputParams.put("mobile", mobile);
        // --------------------
        return makeURL(getApiUrl(), inputParams);
    }

    public static String getUrlFor___watch_shutdown(String uid,
                                                    String access_token, String device_id, String wtime) {
        final String action = "watch_shutdown";
        Map<String, String> inputParams = new HashMap<String, String>();
        addExtraParaToMap(action, inputParams);
        addTokenToMap(inputParams, uid, access_token);
        // -------------------
        inputParams.put("device_id", device_id);
        inputParams.put("wtime", wtime);
        // --------------------
        return makeURL(getApiUrl(), inputParams);
    }

    public static String getUrlFor___get_mode_config(String uid,
                                                     String access_token, String family_id) {
        final String action = "get_mode_config";
        Map<String, String> inputParams = new HashMap<String, String>();
        addExtraParaToMap(action, inputParams);
        addTokenToMap(inputParams, uid, access_token);
        // -------------------
        inputParams.put("family_id", family_id);
        // --------------------
        return makeURL(getApiUrl(), inputParams);
    }

    public static String getUrlFor___update_mode_config(String uid,
                                                        String access_token, String family_id, int mode) {
        final String action = "update_mode_config";
        Map<String, String> inputParams = new HashMap<String, String>();
        addExtraParaToMap(action, inputParams);
        addTokenToMap(inputParams, uid, access_token);
        // -------------------
        inputParams.put("family_id", family_id);
        inputParams.put("mode", String.valueOf(mode));
        // --------------------
        return makeURL(getApiUrl(), inputParams);
    }

    public static String getUrlFor___delete_watch(String uid,
                                                  String access_token, String device_id, String version) {
        final String action = "delete_watch";
        Map<String, String> inputParams = new HashMap<String, String>();
        addExtraParaToMap(action, inputParams);
        addTokenToMap(inputParams, uid, access_token);
        // -------------------
        inputParams.put("device_id", device_id);
        inputParams.put("version", version);

//		inputParams.put("customer_no","1");
        // --------------------
        return makeURL(getApiUrl(), inputParams);
    }

    public static String getUrlFor___fetch_voice_by_voice_id(String uid,
                                                             String access_token, String device_id, String command_no,
                                                             int total_request_number) {
        final String action = "fetch_voice_by_voice_id";
        Map<String, String> inputParams = new HashMap<String, String>();
        addExtraParaToMap(action, inputParams);
        addTokenToMap(inputParams, uid, access_token);
        // -------------------
        inputParams.put("device_id", device_id);
        inputParams.put("command_no", command_no);
        inputParams.put("total_request_number",
                String.valueOf(total_request_number));
        // --------------------
        return makeURL(getApiUrl(), inputParams);
    }

    public static String getUrlFor___agree_join_family(String uid,
                                                       String access_token, String family_id, int status, String mobile,
                                                       String relation, int admin, int msg_type, String group_uid) {
        final String action = "agree_join_family";
        Map<String, String> inputParams = new HashMap<String, String>();
        addExtraParaToMap(action, inputParams);
        addTokenToMap(inputParams, uid, access_token);
        // -------------------
        inputParams.put("family_id", family_id);
        inputParams.put("status", String.valueOf(status));
        inputParams.put("mobile", mobile);

        inputParams.put("relation", relation.length() == 0 ? "大爷" : relation);

        inputParams.put("admin", String.valueOf(admin));
        inputParams.put("datatype", String.valueOf(msg_type));
        inputParams.put("group_uid", group_uid);
        // --------------------
        return makeURL(getApiUrl(), inputParams);
    }

    public static String getUrlFor___refuse_join_family(String uid,
                                                        String access_token, String family_id, String group_uid) {
        final String action = "refuse_join_family";
        Map<String, String> inputParams = new HashMap<String, String>();
        addExtraParaToMap(action, inputParams);
        addTokenToMap(inputParams, uid, access_token);
        // -------------------
        inputParams.put("family_id", family_id);
        inputParams.put("group_uid", group_uid);
        // --------------------
        return makeURL(getApiUrl(), inputParams);
    }

    public static String getUrlFor___user_quit_from_family(String uid,
                                                           String access_token, String family_id, String version, String device) {
        final String action = "user_quit_from_family";
        Map<String, String> inputParams = new HashMap<String, String>();
        addExtraParaToMap(action, inputParams);
        addTokenToMap(inputParams, uid, access_token);
        // -------------------
        inputParams.put("family_id", family_id);
        inputParams.put("version", version);
        inputParams.put("device_id", device);
        // --------------------
        return makeURL(getApiUrl(), inputParams);
    }

    public static String getUrlFor___update_relation(String uid,
                                                     String access_token, String family_id, String relation,
                                                     String version, String device_id) {
        final String action = "update_relation";
        Map<String, String> inputParams = new HashMap<String, String>();
        addExtraParaToMap(action, inputParams);
        addTokenToMap(inputParams, uid, access_token);
        // -------------------
        inputParams.put("family_id", family_id);
        inputParams.put("relation", relation);
        inputParams.put("version", version);
        inputParams.put("device_id", device_id);
        // --------------------
        return makeURL(getApiUrl(), inputParams);
    }

    public static String getUrlFor___watch_buzzer(String uid,
                                                  String access_token, String device_id, int type, int wtime) {
        final String action = "watch_buzzer";
        Map<String, String> inputParams = new HashMap<String, String>();
        addExtraParaToMap(action, inputParams);
        addTokenToMap(inputParams, uid, access_token);
        // -------------------
        // inputParams.put("device_id", device_id);
        //
        // inputParams.put("type", String.valueOf(type));
        // inputParams.put("wtime", String.valueOf(mtime));

        inputParams.put("device_id", device_id);
        inputParams.put("type", String.valueOf(type));
        inputParams.put("wtime", String.valueOf(wtime));
        // --------------------
        return makeURL(getApiUrl(), inputParams);
    }

    public static String getUrlFor___logout(String uid) {
        final String action = "logout";
        Map<String, String> inputParams = new HashMap<String, String>();
        addExtraParaToMap(action, inputParams);
        // -------------------

        inputParams.put("uid", uid);
        inputParams.put("access_token", KidsWatConfig.getUserToken());
        // --------------------
        return makeURL(getApiUrl(), inputParams);
    }

    public static String getUrlFor___invite_join_family(String uid,
                                                        String access_token, String family_id, String from_country_code,
                                                        String from_mobile, String to_country_code, String to_mobile,
                                                        String version, String device_id, String relationship) {
        final String action = "invite_join_family";
        Map<String, String> inputParams = new HashMap<String, String>();
        addExtraParaToMap(action, inputParams);
        addTokenToMap(inputParams, uid, access_token);
        // -------------------
        inputParams.put("family_id", family_id);
        inputParams.put("from_country_code", from_country_code);
        inputParams.put("from_mobile", from_mobile);
        inputParams.put("to_country_code", to_country_code);
        inputParams.put("to_mobile", to_mobile);
        inputParams.put("version", version);
        inputParams.put("device_id", device_id);
        inputParams.put("relationship", relationship);
        // --------------------
        return makeURL(getApiUrl(), inputParams);
    }

    public static String getUrlFor___invite_join_family(String uid,
                                                        String access_token, String family_id, String from_country_code,
                                                        String from_mobile, String to_country_code, String to_mobile,
                                                        String version, String device_id) {
        final String action = "invite_join_family";
        Map<String, String> inputParams = new HashMap<String, String>();
        addExtraParaToMap(action, inputParams);
        addTokenToMap(inputParams, uid, access_token);
        // -------------------
        inputParams.put("family_id", family_id);
        inputParams.put("from_country_code", from_country_code);
        inputParams.put("from_mobile", from_mobile);
        inputParams.put("to_country_code", to_country_code);
        inputParams.put("to_mobile", to_mobile);
        inputParams.put("version", version);
        inputParams.put("device_id", device_id);
        // --------------------
        return makeURL(getApiUrl(), inputParams);
    }

    public static String getUrlFor___get_notice_list(String uid,
                                                     String access_token, int page, int page_size) {
        final String action = "get_notice_list";
        Map<String, String> inputParams = new HashMap<String, String>();
        addExtraParaToMap(action, inputParams);
        addTokenToMap(inputParams, uid, access_token);
        // -------------------
        inputParams.put("page", String.valueOf(page));
        inputParams.put("page_size", String.valueOf(page_size));
        // --------------------
        return makeURL(getApiUrl(), inputParams);
    }

    public static String getUrlFor___get_notice_list_ex(String uid,
                                                        String access_token, int page, int page_size) {
        final String action = "get_notice_list_ex";
        Map<String, String> inputParams = new HashMap<String, String>();
        addExtraParaToMap(action, inputParams);
        addTokenToMap(inputParams, uid, access_token);
        // -------------------
        inputParams.put("page", String.valueOf(page));
        inputParams.put("page_size", String.valueOf(page_size));
        // --------------------
        return makeURL(getApiUrl(), inputParams);
    }

    public static String getUrlFor___delete_notice_by_message_id(String uid,
                                                                 String access_token, String message_id_array) {
        final String action = "delete_notice_by_message_id";
        Map<String, String> inputParams = new HashMap<String, String>();
        addExtraParaToMap(action, inputParams);
        addTokenToMap(inputParams, uid, access_token);
        // -------------------
        inputParams.put("message_id_array", message_id_array);
        // --------------------
        return makeURL(getApiUrl(), inputParams);
    }

    public static String getUrlFor___clear_user_notice_list(String uid,
                                                            String access_token) {
        final String action = "clear_user_notice_list";
        Map<String, String> inputParams = new HashMap<String, String>();
        addExtraParaToMap(action, inputParams);
        addTokenToMap(inputParams, uid, access_token);
        // -------------------
        return makeURL(getApiUrl(), inputParams);
    }

    public static String getUrlFor___delete_family_member(String uid,
                                                          String access_token, String family_id, String country_code,
                                                          String mobile, String version, String device_id) {
        final String action = "delete_family_member";
        Map<String, String> inputParams = new HashMap<String, String>();
        addExtraParaToMap(action, inputParams);
        addTokenToMap(inputParams, uid, access_token);
        // -------------------
        inputParams.put("family_id", family_id);
        inputParams.put("country_code", country_code);
        inputParams.put("mobile", mobile);
        inputParams.put("version", version);
        inputParams.put("device_id", device_id);
        // --------------------
        return makeURL(getApiUrl(), inputParams);
    }

    public static String getUrlFor___update_user_silent_time(String uid,
                                                             String access_token, int start_time, int end_time,
                                                             int notice_status, int notice_alarm_status) {
        final String action = "update_user_silent_time";
        Map<String, String> inputParams = new HashMap<String, String>();
        addExtraParaToMap(action, inputParams);
        addTokenToMap(inputParams, uid, access_token);
        // -------------------
        // start_time int (required) 开始的时间
        // end_time int (required) 结束的时间
        // notice_status int (required) 是否免打扰[静音] 0 -- 关闭免打扰推送[响铃] 1--
        // 开启免打扰推送[设置的时间内静音]
        // notice_alarm_status int (required) 开启免打扰状态下 是否开启SOS响铃 //0 -- 关闭强制响铃 1
        // -- 开启强制响铃

        inputParams.put("start_time", String.valueOf(start_time));
        inputParams.put("end_time", String.valueOf(end_time));
        inputParams.put("notice_status", String.valueOf(notice_status));
        inputParams.put("notice_alarm_status",
                String.valueOf(notice_alarm_status));
        // --------------------
        return makeURL(getApiUrl(), inputParams);
    }

    public static String getUrlFor___charge_money_of_app_v2(String uid,
                                                            String access_token, String device_id, String policy_sequence,
                                                            String currency_country_code, String currency_symbol, String price,
                                                            String order_id) {
        final String action = "charge_money_of_app_v2";
        Map<String, String> inputParams = new HashMap<String, String>();
        addExtraParaToMap(action, inputParams);
        addTokenToMap(inputParams, uid, access_token);
        // -------------------
        // $currency_country_code = $this->post['currency_country_code'];
        // //充值时APP上显示对应其 货币国家码
        // $currency_symbol = $this->post['currency_symbol']; //`````````````
        // 货币符号
        // $price = $this->post['price'];

        inputParams.put("device_id", device_id);
        inputParams.put("policy_sequence", policy_sequence);

        inputParams.put("currency_country_code", currency_country_code);
        inputParams.put("currency_symbol", currency_symbol);
        inputParams.put("price", price);
        inputParams.put("order_id", order_id);
        inputParams.put("plat", "1");

        // --------------------
        return makeURL(getApiUrl(), inputParams);
    }

    public static String getUrlFor___get_charge_package_policy(String uid,
                                                               String access_token, String device_id, String qrcode) {
        final String action = "get_charge_package_policy_v2";
        Map<String, String> inputParams = new HashMap<String, String>();
        addExtraParaToMap(action, inputParams);
        addTokenToMap(inputParams, uid, access_token);
        // -------------------
        inputParams.put("device_id", device_id);
        inputParams.put("qrcode", qrcode);
        inputParams.put("plat", "1");
        // --------------------
        return makeURL(getApiUrl(), inputParams);
    }

    public static String getUrlFor___get_device_validate(String uid,
                                                         String access_token, String device_id) {
        final String action = "get_device_validate";
        Map<String, String> inputParams = new HashMap<String, String>();
        addExtraParaToMap(action, inputParams);
        addTokenToMap(inputParams, uid, access_token);
        // -------------------

        inputParams.put("device_id", device_id);

        // --------------------
        return makeURL(getApiUrl(), inputParams);
    }

    public static String getUrlFor___addappfeedback(String uid, String content) {
        return getUrlFor___addappfeedback(0, "2.0.0", uid, "", content, "");
    }

    public static String getUrlFor___addappfeedback(int productid,
                                                    String version, String uid, String name, String content, String img) {
        final String action = "addappfeedback";
        Map<String, String> inputParams = new HashMap<String, String>();
        addExtraParaToMap(action, inputParams);
        // -------------------
        // productid int(required)
        // version string The app version
        // uid string (required)
        // name string (required)
        // content string (required)
        // img string

        inputParams.put("productid", String.valueOf(productid));
        inputParams.put("version", version);
        inputParams.put("uid", uid);
        inputParams.put("name", name);
        inputParams.put("content", content);
        inputParams.put("img", img);

        // --------------------
        return makeURL(getApiUrl(), inputParams);
    }

    // -------++++++++++++++++++++++++++++++++++++++++++++--------

    private static void addTokenToMap(Map<String, String> inputParams,
                                      String uid, String access_token) {
        inputParams.put("uid", uid);
        inputParams.put("access_token", access_token);
    }

    private static void addExtraParaToMap(String action,
                                          Map<String, String> inputParams) {
        long cur_time = System.currentTimeMillis() / 1000;
        String apitoken = Encoder.encode("MD5", String.valueOf(cur_time)
                + api_secret);// SHA-1

        inputParams.put("action", action);// action参数
        inputParams.put("time", String.valueOf(cur_time));
        inputParams.put("api_token", apitoken);
        inputParams.put("customer_no", CUSTOMER_NO_RUSSIA);
    }

    public static Map<String, String> addExtraPara(String action,
                                                   Map<String, String> inputParams) {
        long cur_time = System.currentTimeMillis() / 1000;
        String apitoken = Encoder.encode("MD5", String.valueOf(cur_time)
                + api_secret);// SHA-1

        inputParams.put("action", action);// action参数
        inputParams.put("time", String.valueOf(cur_time));
        inputParams.put("api_token", apitoken);

        return inputParams;
    }

    /**
     * 给一个url拼接参数
     *
     * @param p_url
     * @param params
     * @return
     */
    public static String makeURL(String p_url, Map<String, String> params) {
        StringBuilder url = new StringBuilder(p_url);
        if (params.size() == 0)
            return p_url;
        if (url.indexOf("?") < 0)
            url.append('?');

        for (String name : params.keySet()) {
            String value = params.get(name);
            CustomLog.d(TAG,name + "=" + value);
            if (value != null && !StringUtils.isEmpty(value)
                    && !value.equalsIgnoreCase("null")) {
                url.append('&');
                url.append(name);
                url.append('=');
                // 对参数进行编码
                try {
                    url.append(URLEncoder.encode(
                            String.valueOf(params.get(name)), "UTF-8"));
                } catch (UnsupportedEncodingException e) {
                    CustomLog.logException(TAG,e);
                }
            }
        }
        String __xurl = url.toString().replace("?&", "?");
        // CustomLog.d(TAG,__xurl);
        return __xurl;
    }

    // --------------
    public static String getUrlFor___check_version(String customer_no,
                                                   String platform, String version) {
        final String action = "check_version";
        Map<String, String> inputParams = new HashMap<String, String>();
        addExtraParaToMap(action, inputParams);

        inputParams.put("customer_no", customer_no);
        inputParams.put("platform", platform);
        inputParams.put("version", version);
        // --------------------
        return makeURL(getApiUrl(), inputParams);
    }
}
