package com.qiwo.xkidswatcher_russia.api;

import java.util.Map;

import org.kymjs.kjframe.http.HttpCallBack;
import org.kymjs.kjframe.http.HttpParams;

import net.intari.CustomLogger.CustomLog;
import com.qiwo.xkidswatcher_russia.util.UrlUtils;

public class OperationResponseCallBack extends HttpCallBack {
	private final static String TAG = OperationResponseCallBack.class.getSimpleName();

	private String url;
	private HttpParams params;

	public OperationResponseCallBack(String _url, HttpParams _params) {
		this.url = _url;
		this.params = _params;
	}

	public OperationResponseCallBack(String _url) {
		this(_url, null);
	}

	@Override
	public void onSuccess(String t) {
		String full_url = UrlUtils.makeURL(url, params);
		String msg = String.format("url:%s\nparams:%s\n%s", full_url,
				params.getUrlParams(), t);
		CustomLog.d(TAG,"error=" + msg);
	}

	@Override
	public void onSuccess(Map<String, String> headers, byte[] t) {

	}

	@Override
	public void onFailure(int errorNo, String strMsg) {
		String full_url = UrlUtils.makeURL(url, params);
		String msg = String.format("url:%s\nparams:%s\nerrorNo:%s\n%s",
				full_url, params.getUrlParams(), errorNo, strMsg);
		CustomLog.d(TAG,"error=" + msg);
	}
}
