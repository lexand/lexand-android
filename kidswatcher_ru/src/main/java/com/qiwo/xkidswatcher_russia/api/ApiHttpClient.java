package com.qiwo.xkidswatcher_russia.api;

import android.content.Context;
import android.widget.Toast;

import com.qiwo.xkidswatcher_russia.AppContext;
import com.qiwo.xkidswatcher_russia.Constants;
import com.qiwo.xkidswatcher_russia.R;
import com.qiwo.xkidswatcher_russia.util.KidsWatUtils;
import net.intari.CustomLogger.CustomLog;

import net.intari.CustomLogger.CustomLog;

import org.kymjs.kjframe.KJHttp;
import org.kymjs.kjframe.http.HttpCallBack;
import org.kymjs.kjframe.http.HttpConfig;
import org.kymjs.kjframe.http.HttpParams;

public class ApiHttpClient {

    public static final String TAG = ApiHttpClient.class.getSimpleName();

    private static KJHttp client;

    public static KJHttp getHttpClient() {
        return client;
    }

    public static void setHttpClient(KJHttp kj) {
        client = kj;
    }

    public static void cancelAll() {
        client.cancelAll();
    }

    public static void get(String url, HttpCallBack handler, Context context) {
        boolean isNetworkAvailable = KidsWatUtils.isNetworkAvailable(context);
        if (isNetworkAvailable) {
            // client.get(url, null, false, handler);
            HttpConfig config = new HttpConfig();
            config.TIMEOUT = Constants.OLD_HTTP_TIMEOUT_GET;//1000 * 10;
            client.setConfig(config);
            client.get(url, null, false, handler);
        } else {
            CustomLog.e(TAG,"No internet connection available. can't get "+url);
            if (AppContext.getInstance()!=null) {
                //if we don't have App's class inited  - this mean we don't have GUI at all - don't even try to use it
                AppContext.showToast(R.string.tip_network_unavailable);
            } else {
                CustomLog.e(TAG,"NOT showing 'no network' toast - App's context is not YET ready");
            }
        }

    }
    public static void get(String url, HttpCallBack handler) {
        get(url,handler,AppContext.getInstance());
    }

    public static void log(String log) {
        CustomLog.d(TAG, log);
    }

    public static void post(String url, HttpParams params, HttpCallBack handler) {

        boolean isNetworkAvailable = KidsWatUtils.isNetworkAvailable(AppContext
                .getInstance());
        if (isNetworkAvailable) {
            log("POST " + url + "&" + params);

            HttpConfig config = new HttpConfig();
            config.TIMEOUT = Constants.OLD_HTTP_TIMEOUT_PUT;//1000 * 20;
            client.setConfig(config);
            client.post(url, params, false, handler);

        } else {
            CustomLog.e(TAG,"No internet connection available. can't post "+url);

            Toast.makeText(AppContext.getInstance(),
                    "пожалуйста, проверьте подключение и попробуйте снова.",
                    Toast.LENGTH_LONG).show();
        }

    }

}
