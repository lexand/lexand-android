package com.qiwo.xkidswatcher_russia;

/**
 * Created by Dmitriy Kazimirov on 24/01/2019.
 */


import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.qiwo.xkidswatcher_russia.db.SqlDb;
import com.qiwo.xkidswatcher_russia.event.BaseEvent;
import com.qiwo.xkidswatcher_russia.ui.EventAlertActivity;
import com.qiwo.xkidswatcher_russia.ui.MainActivity;
import com.qiwo.xkidswatcher_russia.util.KidsWatUtils;
import com.qiwo.xkidswatcher_russia.util.StringUtils;
import net.intari.CustomLogger.CustomLog;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Map;
import java.util.Set;

/**
 * Most of code migrated from GCMIntentService
 * Migrated to FCM
 * https://developers.google.com/cloud-messaging/android/android-migrate-gcmlistener
 */
public class MyGcmListenerService extends FirebaseMessagingService {
    public static final String TAG = MyGcmListenerService.class.getSimpleName();

    //private static final String TAG = "GCMIntentService";


    //开发/测试环境 apikey ： AIzaSyCxYwIr_zORIZsLBWfmmgkO_oo6p0Qpelg
    //开发/测试环境 sender_id ： 567665979722
    static int notify_id = 0;

    public static final String PUSH_CHANNEL_NAME="Пуш-сообщения";
    public static final String NOTIFICATION_CHANNEL_ID_PUSH = "com.qiwo.xkidswatcher_russia.push.v5";

    public static final String KEY_MESSAGE="message";

    public static final String KEY_CONTENT_TYPE="content_type";
    public static final String KEY_TITLE="title";
    public static final String KEY_CONTENT="content";
    public static final String KEY_DETAIL="detail";



    public static final String FCM_KEY_MESSAGE_ID="message_id";
    public static final String FCM_KEY_TIME="time";



    private static final String MAGIC_23_SOS="23";
    private static final String MAGIC_304_MANUAL="304";
    private static final String MAGIC_32_CHANGE_DEVICE_MODE_TO_FLIGHT_MODE ="32";
    private static final String MAGIC_33_CHANGE_DEVICE_MODE_TO_NORMAL_MODE ="33";
    private static final String MAGIC_20_LOCATION="20";
    private static final String MAGIC_3_DELETE_DEVICE="3";
    private static final String MAGIC_9_ADMIN_ADD_MEMBER="9";
    private static final String MAGIC_10_ADMIN_DELETE_MEMBER="10";
    private static final String MAGIC_1_WHITE_HOME_VOICE_PUSH="1";



    public static void initChannel(Context context){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CustomLog.d(TAG,"Oreo+: initing new channel with id "+NOTIFICATION_CHANNEL_ID_PUSH+" and name "+PUSH_CHANNEL_NAME);
            NotificationManager nm = (NotificationManager) context.getSystemService(NOTIFICATION_SERVICE);
            NotificationChannel channel=new NotificationChannel(NOTIFICATION_CHANNEL_ID_PUSH, PUSH_CHANNEL_NAME, NotificationManager.IMPORTANCE_HIGH);//_HIGH
            channel.canBypassDnd();
            channel.shouldShowLights();
            channel.shouldShowLights();
            channel.setLockscreenVisibility(Notification.VISIBILITY_PUBLIC);
            nm.createNotificationChannel(channel
            );

        } else {
            CustomLog.w(TAG,"Pre-Oreo - not initing notification channels");
        }

    }


    /**
     * Called when message is received.
     *
     * @param from SenderID of the sender.
     * @param data Data bundle containing message data as key/value pairs.
     *             For Set of keys use data.keySet().
     */
    // [START receive_message]


    @Override
    public void onMessageReceived(RemoteMessage remoteMessage){

        //public void onMessageReceived(String from, Bundle data) {

        String from = remoteMessage.getFrom();
        Map data = remoteMessage.getData();
        CustomLog.d(TAG, "From: " + from);
        CustomLog.d(TAG, "Data: " + data);



        CustomLog.i(TAG,"--------------------received FCM Message------------------" + data.toString());
        //[{content={"content_type":23,"message_id":"MSEESAGE_CENTER::14559378608069","time":1455937860,"detail":{"content_type":23,"detail":{"family_id":10000673,"address":"This location","latitude":22.63059425,"kid_name":"SOS","time":1455937858,"type":2,"location_type":2,"longitude":114.0248489}},"title":"SOS","content":"SOS."}, body=motion detection files!click here to view., collapse_key=do_not_collapse, from=567665979722, type=motion}]

        Set keys = data.keySet();
        for (Object s : keys) {
            Object value = data.get(s);
            CustomLog.d(TAG,"Data:"+s+"="+value);
        }




        /*
        if (!data.containsKey(KEY_MESSAGE)) {
            CustomLog.e(TAG,"Push notification DOES NOT contain 'message'. FCM?");
            return;
        }
        */



        if (!data.containsKey(KEY_CONTENT_TYPE)) {
            CustomLog.e(TAG,"Push notification DOES NOT contain 'content_type'");
            return;
        }
        if (!data.containsKey(KEY_TITLE)) {
            CustomLog.e(TAG,"Push notification DOES NOT contain 'title'");
            return;
        }
        if (!data.containsKey(KEY_CONTENT)) {
            CustomLog.e(TAG,"Push notification DOES NOT contain 'content'");
            return;
        }
        if (!data.containsKey(KEY_DETAIL)) {
            CustomLog.e(TAG,"Push notification DOES NOT contain 'detail'");
            return;
        }


        String message_id=(String)data.get(FCM_KEY_MESSAGE_ID);
        String detail=(String)data.get(KEY_DETAIL);
        String time=(String)data.get(FCM_KEY_TIME);
        String title=(String)data.get(KEY_TITLE);
        String content_type=(String)data.get(KEY_CONTENT_TYPE);
        String content=(String)data.get(KEY_CONTENT);

        CustomLog.i(TAG,"MessageID:"+message_id);
        CustomLog.i(TAG,"Timestamp:"+time);
        CustomLog.i(TAG,"Title:"+title);
        CustomLog.i(TAG,"ContentType:"+content_type);
        CustomLog.i(TAG,"Content:"+content);
        CustomLog.i(TAG,"DetailJSON(should be duplicate):"+detail);


        //String pushMessage = (String)data.get("message");

        //CustomLog.d(TAG, "Message: " + pushMessage);
        //never

        if (from.startsWith("/topics/")) {
            // message received from some topic.
        } else {
            // normal downstream message.
        }

        // [START_EXCLUDE]
        /**
         * Production applications would usually process the message here.
         * Eg: - Syncing with server.
         *     - Store message in local database.
         *     - Update UI.
         */

        /**
         * In some cases it may be useful to show a notification indicating to the user
         * that a message was received.
         */
        if (AppContext.getInstance().getIsLogin()) {
            CustomLog.d(TAG,"LoggedIn. asking for NoticeListEx");
            // ApplicationHelper.getInstance().get_notice_list_ex();
            KidsWatUtils.get_notice_list_ex();
        }


        /*

        String content_type = (String)data.get("content_type");
        CustomLog.d(TAG,"content type = " + content_type);
        String title = (String)data.get("title");
        String content = (String)data.get("content");
        String detail = (String)data.get("detail");
        */

        SqlDb db = SqlDb.get(this);
        db.deleteGcm_message();
        db.saveGcmMessage(title, content, data.toString());
        db.closeDb();

        if (!StringUtils.isEmpty(content_type) && AppContext.getInstance().getIsLogin()) {
            // define('PUSH_CONTENT_TYPE__DEVICE_CHANGE_TO_FLIGHT_MODE', 32);
            //	设备进入飞行模式
            // define('PUSH_CONTENT_TYPE__DEVICE_CHANGE_TO_NORMAL_MODE', 33);
            //	设备进行正常模式
            String family_id = null;

            if (AppContext.getInstance().getCurrentFamily() != null) {
                family_id = AppContext.getInstance().getCurrentFamily().family_id;
            }

            if (content_type.equalsIgnoreCase(MAGIC_23_SOS)) {
                // EventBus.getDefault().post(new Sos(""));
                JSONObject json;
                try {
                    //json = new JSONObject(data.getString("detail"));
                    json = new JSONObject(detail);
                    String nickname = json.getJSONObject("detail").getString(
                            "kid_name");
//					String message = String
//							.format("%s has sent out an SOS alert.\nPlease confirm the safety of your kid",
//									nickname);
                    String message = String.format("Ваш ребенок \"%s\" очень просит связаться с ним!", nickname);
                    CustomLog.i(TAG,"Got SOS:"+message);
                    Intent mintent = new Intent(this, EventAlertActivity.class);
                    mintent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    mintent.putExtra("message", message);
                    startActivity(mintent);
                } catch (Exception e) {
                    CustomLog.logException(TAG,e);
                }
            } else if (content_type.equalsIgnoreCase(MAGIC_32_CHANGE_DEVICE_MODE_TO_FLIGHT_MODE)) {
                try {
                    JSONObject js_detail = new JSONObject(detail);
                    String msg_family_id = js_detail.getString("family_id");
                    SqlDb dbx = SqlDb.get(this);
                    dbx.setDeviceModeBy_fid(msg_family_id, 2);
                    if (msg_family_id.equalsIgnoreCase(family_id)) {
                        dbx.setDeviceModeBy_fid(family_id, 2);
                        AppContext.getInstance().setCurrentFamily( dbx
                                .getFamilyBy_fid(family_id));
                        // EventBus.getDefault().post(
                        // new DeviceModeChange(family_id, 2));
                        BaseEvent event = new BaseEvent(
                                BaseEvent.MSGTYPE_3___CHANGE_DEVICE_MODE,
                                "DeviceModeChange");
                        EventBus.getDefault().post(event);
                    }
                    dbx.closeDb();
                } catch (JSONException e) {
                    CustomLog.logException(TAG,e);
                }

            } else if (content_type.equalsIgnoreCase(MAGIC_33_CHANGE_DEVICE_MODE_TO_NORMAL_MODE)) {

                try {
                    JSONObject js_detail = new JSONObject(detail);
                    String msg_family_id = js_detail.getString("family_id");
                    SqlDb dbx = SqlDb.get(this);
                    dbx.setDeviceModeBy_fid(msg_family_id, 1);
                    if (msg_family_id.equalsIgnoreCase(family_id)) {

                        dbx.setDeviceModeBy_fid(family_id, 1);
                        AppContext.getInstance().setCurrentFamily(dbx
                                .getFamilyBy_fid(family_id));
                        dbx.closeDb();
                        // EventBus.getDefault().post(
                        // new DeviceModeChange(family_id, 1));
                        BaseEvent event = new BaseEvent(
                                BaseEvent.MSGTYPE_3___CHANGE_DEVICE_MODE,
                                "DeviceModeChange");
                        EventBus.getDefault().post(event);
                    }
                    dbx.closeDb();
                } catch (JSONException e) {
                    CustomLog.logException(TAG,e);
                }
            } else if (content_type.equalsIgnoreCase(MAGIC_20_LOCATION)) {
                // location_msg
                CustomLog.d(TAG,"--------receive location msg:20 GCM_LOCATION--------");
                BaseEvent event = new BaseEvent(
                        BaseEvent.MSGTYPE_3___GCM_LOCATION, detail);
                EventBus.getDefault().post(event);
            } else if (content_type.equalsIgnoreCase(MAGIC_3_DELETE_DEVICE)) {
                // location_msg
                CustomLog.d(TAG,"--------receive location msg:3 DELETE_DEVICE --------");
                BaseEvent event = new BaseEvent(
                        BaseEvent.MSGTYPE_1___DELETE_DEVICE, content);
                EventBus.getDefault().post(event);
            } else if (content_type.equalsIgnoreCase(MAGIC_9_ADMIN_ADD_MEMBER)) {
                // location_msg
                CustomLog.d(TAG,"--------receive location msg:9 ADMIN_ADD_MEMBER --------");
                BaseEvent event = new BaseEvent(
                        BaseEvent.MSGTYPE_1___ADMIN_ADD_MEMBER, content);
                EventBus.getDefault().post(event);
            } else if (content_type.equalsIgnoreCase(MAGIC_10_ADMIN_DELETE_MEMBER)) {
                // location_msg
                CustomLog.d(TAG,"--------receive location msg:10 ADMIN_DELETE_MEMBER--------");
                BaseEvent event = new BaseEvent(
                        BaseEvent.MSGTYPE_1___ADMIN_DELETE_MEMBER, content);
                EventBus.getDefault().post(event);
            } else if (content_type.equalsIgnoreCase(MAGIC_1_WHITE_HOME_VOICE_PUSH)) {
                // location_msg

                try {
                    //JSONObject json = new JSONObject(
                    //        data.getString("detail"));
                    JSONObject json = new JSONObject(detail);

                    String command_no = json.getJSONObject("detail").getString(
                            "command_no");
                    String device_id = json.getJSONObject("detail").getString(
                            "device_id");

                    CustomLog.d(TAG,"--------receive location msg:1--------");
                    CustomLog.d(TAG,"CommandNo:"+command_no+", device_id:"+device_id);

                    String str = command_no + "," + device_id;

                    BaseEvent event = new BaseEvent(
                            BaseEvent.MSGTYPE_6___WHITE_HOME_VOICE_PUSH, str);
                    EventBus.getDefault().post(event);

                } catch (JSONException e) {
                    CustomLog.logException(TAG,e);
                }

            }
        }


        // if (content_type != null && content_type.equalsIgnoreCase("23")
        // && AppContext.getInstance().isLogin) {
        // // EventBus.getDefault().post(new Sos(""));
        // String nickname = AppContext.getInstance().getCurrentFamily().nickname;
        // String message = String.format("%s\nSOS Warning", nickname);
        //
        // Intent mintent = new Intent(context, QrcodeActivity.class);
        // mintent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        // mintent.putExtra("message", message);
        // // mintent.putExtra("bleadd", event.getBleAddress());
        // startActivity(mintent);
        // }
        //
        // if (StringUtils.isEmpty(content_type)
        // && AppContext.getInstance().isLogin) {
        // // define('PUSH_CONTENT_TYPE__DEVICE_CHANGE_TO_FLIGHT_MODE', 32);
        // // //设备进入飞行模式
        // // define('PUSH_CONTENT_TYPE__DEVICE_CHANGE_TO_NORMAL_MODE', 33);
        // // //设备进行正常模式
        // String family_id = AppContext.getInstance().getCurrentFamily().family_id;
        // if (content_type.equalsIgnoreCase("32")) {
        //
        // SqlDb dbx = SqlDb.get(context);
        // dbx.setDeviceModeBy_fid(family_id, 2);
        // AppContext.getInstance().currentFamily = dbx
        // .getFamilyBy_fid(family_id);
        // dbx.closeDb();
        // EventBus.getDefault().post(new DeviceModeChange(family_id, 2));
        // } else if (content_type.equalsIgnoreCase("33")) {
        // SqlDb dbx = SqlDb.get(context);
        // dbx.setDeviceModeBy_fid(family_id, 1);
        // AppContext.getInstance().currentFamily = dbx
        // .getFamilyBy_fid(family_id);
        // dbx.closeDb();
        // EventBus.getDefault().post(new DeviceModeChange(family_id, 1));
        // }
        // }

        // displayMessage(content);
        // notifies user

        if (title != null && title.length() >= 3) {
            CustomLog.d(TAG,"Will show notification with title:"+title+",content:"+content+",detail:"+detail);
            generateNotification(this, title, content, detail);
        } else {
            CustomLog.e(TAG,"title is too small, not showing anything");
        }

        // [END_EXCLUDE]
    }
    // [END receive_message]





    /**
     * Issues a notification to inform the user that server has sent a message.
     */
    private static void generateNotification(Context context, String title,
                                             String message, String detail) {

        Intent notificationIntent = new Intent(context, MainActivity.class); // 点击该通知后要跳转的Activity
        notificationIntent.putExtra("detail", detail);
        PendingIntent contentItent = PendingIntent.getActivity(context, 0,
                notificationIntent, 0);// notificationIntent


        NotificationCompat.Builder notificationBuilder =   new NotificationCompat.Builder(context, NOTIFICATION_CHANNEL_ID_PUSH)
                //.setContentTitle(title != null? title:getString(R.string.title_notif))
                .setContentText(message)
                .setSmallIcon(R.drawable.ic_launcher)
                .setWhen(System.currentTimeMillis())
                .setContentIntent(contentItent)
                .setAutoCancel(true)
                //.setStyle(new NotificationCompat.BigTextStyle().bigText(text))
                //.setVibrate(new long[] { 1000, 1000})
                //.setPriority(Notification.PRIORITY_HIGH)
                .setPriority(Notification.PRIORITY_DEFAULT)
                .setSound(Settings.System.DEFAULT_NOTIFICATION_URI);


        Notification notification=notificationBuilder.build();

        /*
        NotificationManager notificationManager = (NotificationManager) context
                .getSystemService(android.content.Context.NOTIFICATION_SERVICE);
        Notification notification = new Notification(R.drawable.ic_launcher,
                message, System.currentTimeMillis());

        // FLAG_AUTO_CANCEL 该通知能被状态栏的清除按钮给清除掉
        // FLAG_NO_CLEAR 该通知不能被状态栏的清除按钮给清除掉
        // FLAG_ONGOING_EVENT 通知放置在正在运行
        // FLAG_INSISTENT 是否一直进行，比如音乐一直播放，知道用户响应
        notification.flags |= Notification.FLAG_SHOW_LIGHTS;
        // DEFAULT_ALL 使用所有默认值，比如声音，震动，闪屏等等
        // DEFAULT_LIGHTS 使用默认闪光提示
        // DEFAULT_SOUNDS 使用默认提示声音
        // DEFAULT_VIBRATE 使用默认手机震动，需加上<uses-permission
        // android:name="android.permission.VIBRATE" />权限
        // notification.defaults = Notification.DEFAULT_LIGHTS;
        notification.defaults = Notification.DEFAULT_SOUND;
        */


        if (message.contains("time location successfully")) {
            CustomLog.d(TAG,"message contains 'time location successfully' so NOT showing anything");
            return;
        }



//		notification.setLatestEventInfo(context, title, message, contentItent);

        /*
        notification.flags |= Notification.FLAG_AUTO_CANCEL;
        notificationManager.notify(notify_id++, notification);

        */
        NotificationManagerCompat notificationManagerCompat;
        notificationManagerCompat = NotificationManagerCompat.from(context);
        notificationManagerCompat.notify(title, notify_id++,notification);


        CustomLog.d(TAG,"Started push notification:"+notification.toString());
        // 把Notification传递给NotificationManager
        // notificationManager.notify(0, notification);

        // int icon = R.drawable.ic_launcher;
        // long when = System.currentTimeMillis();
        // NotificationManager notificationManager = (NotificationManager)
        // context
        // .getSystemService(Context.NOTIFICATION_SERVICE);
        // Notification notification = new Notification(icon, message, when);
        //
        // Intent notificationIntent = new Intent(context, MainActivity.class);
        // // set intent so it does not start a new activity
        // notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
        // | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        // PendingIntent intent = PendingIntent.getActivity(context, 0,
        // notificationIntent, 0);
        // notification.setLatestEventInfo(context, title, message, intent);
        // // notification.flags |= Notification.FLAG_AUTO_CANCEL;
        // notification.defaults = Notification.DEFAULT_SOUND; // 调用系统自带声音
        // notification.defaults = Notification.DEFAULT_VIBRATE;// 设置默认震动
        // notificationManager.notify(0, notification);

//		Intent intent = new Intent(context, MainActivity.class);
//        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0 /* Request code */, intent,
//                PendingIntent.FLAG_ONE_SHOT);
//
//        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
//        android.support.v4.app.NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(context)
//                .setSmallIcon(R.drawable.icon_sos)
//                .setContentTitle(title)
//                .setContentText(message)
//                .setAutoCancel(true)
//                .setSound(defaultSoundUri)
//                .setContentIntent(pendingIntent);
//
//        NotificationManager notificationManager =
//                (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
//
//        notificationManager.notify(0 /* ID of notification */, notificationBuilder.build());
    }
}