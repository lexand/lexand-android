# Add project specific ProGuard rules here.
# By default, the flags in this file are appended to flags specified
# in C:\Users\Artem\AppData\Local\Android\sdk/tools/proguard/proguard-android.txt
# You can edit the include path and order by changing the proguardFiles
# directive in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# Add any project specific keep options here:

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}


# To let Crashlytics automatically upload the ProGuard or DexGuard mapping file, remove this line from the config file, if it's present:
-printmapping mapping.txt

# To preserve the info Crashlytics needs for readable crash reports, add the following lines to your Proguard or Dexguard config file:
-keepattributes *Annotation*
-keepattributes SourceFile,LineNumberTable
-keep public class * extends java.lang.Exception

#For faster builds with ProGuard, exclude Crashlytics. Add the following lines to your ProGuard config file
-keep class com.crashlytics.** { *; }
-dontwarn com.crashlytics.**

-keep public class com.pierfrancescosoffritti.youtubeplayer.** {
   public *;
}

-keepclassmembers class * implements android.os.Parcelable {
    static ** CREATOR;
}

-keep class com.google.android.gms.ads.** { *; }
-dontwarn okio.**

-keepattributes Exceptions, Signature, LineNumberTable

-keep class com.appsee.** { *; }
-dontwarn com.appsee.**
-keep class android.support.** { *; }
-keep interface android.support.** { *; }
-keep class androidx.** { *; }
-keep interface androidx.** { *; }
-keepattributes SourceFile,LineNumberTable


-keepclassmembers class * extends androidx.work.Worker {
    public <init>(android.content.Context,androidx.work.WorkerParameters);
}
